export function templateReplacer({
  html,
  settings,
  placeholder,
  width,
  height,
  font
}: {
  html: string
  settings: any
  placeholder?: any
  width?: number
  height?: number
  font?: string
}): string {
  html = html.replace(new RegExp("{settings.key}", "g"), settings.key)
  html = html.replace(new RegExp("KEY", "g"), settings.key)
  html = html.replace(new RegExp("{width}", "g"), String(width))
  html = html.replace(
    new RegExp("{placeholder.alt}", "g"),
    String(placeholder.alt)
  )
  html = html.replace(
    new RegExp("{settings.src}", "g"),
    settings.src
      ? settings.src
      : "https://via.placeholder.com/" +
        width +
        "x250/000000/FFFFFF?text={" +
        settings.key +
        "}"
  )
  html = html.replace(new RegExp("{FONT}", "g"), font)
  return html
}
export default templateReplacer
