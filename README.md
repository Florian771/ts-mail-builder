# Mail-Builder

## Install

```bash
npm install -D /Users/floriankirnbauer/Documents/chatify/mail-builder

yarn add https://bitbucket.org/Florian771/ts-mail-builder

npm i add git+https://bitbucket.org/Florian771/ts-mail-builder

npm i add git+ssh://bitbucket.org/Florian771/ts-mail-builder
```

## Dev

```bash
yarn run watch
yarn run liveServer
yarn run start
```

## Import

```javascript
import { start, MailBuilder, chatify } from "mail-builder"
```

## Configuration Design

```javascript
const chatifyOptions: MailBuilder.DesignSettings = {
  TemplateName: "1",
  primaryColor: "#dd2476",
  secundaryColor: "#ff512f",
  headlineColor: "#111", //NEU
  textColor: "#9B9B9B", //c
  textColorHighlight: "#000", //c
  backgroundColor: "#F2F2F2", //c
  backgroundColor2: "#fff", //c
  headlineFontName:
    '"Montserrat", "Open Sans", "Helvetica Neue", Helvetica, sans-serif',
  bodyFontName: '"Open Sans", "Helvetica Neue", Helvetica, sans-serif',
  bottomSlanted: true
}
const designSettings: MailBuilder.DesignSettings = chatifyOptions
```

## Configuration Data

```javascript
const user: chatify.UserSettings = {
  displayName: "Thomas Muster"
}
const client: chatify.Client = {
  companyName: "Muster GmbH",
  contactPersonFirstName: "Thomas",
  contactPersonLastName: "Musterss",
  email: "test@test.at",
  street: "Neutorstr. 4",
  postalCode: "5300",
  city: "Salzburg",
  country: "Österreich"
}
const data: any = {
  user: user,
  client: client
}
```

## Configuration Template

```javascript
const options: MailBuilder.BuildOptions = {
  name: "chatify",
  from: "noreply@chatify.info",
  to: "florian1677@gmail.com",
  data: data,
  designSettings: designSettings,
  templateType: {
    name: "minimal",
    alternativeHtmlInfo: {
      text: true,
      url: true
    },
    header: {
      logo: "",
      context: true,
      menuLinks: 3
    },
    button: {
      key: "BTN"
    },
    text: {
      key: "TXT"
    },
    sectionBottom: {
      active: true,
      type: "diagonal"
    },
    footer: {
      unsubscribeText: true,
      unsubscribeUrl: true,
      name: true,
      street: true,
      postalCode: true,
      city: true,
      country: true,
      phone: true,
      fax: true,
      email: true,
      website: true,
      uid: true
    }
  },
  contentType: "userCreated",
  buildType: "html", // or 'mail' or file
  testParse: true,
  translations: [
    { HTML_LINK_TEXT: "Chatify - bot up your business" },
    { HTML_LINK_URL: "" },
    { MAIL_CONTEXT: "" },
    { LOGO_URL: "https://admin.chatify.info/assets/img/logo-claim@2x.png" },
    { LOGO_TITLE: "Demo Hotel" },
    { LOGO_TARGET: "" }
  ]
}
```

## Start

```javascript
start(options).then((res: MailBuilder.Result) => {
  return res
})
```

## Development Mode

```bash
npm run watch
npm run liveServer
```

```bash
npm run startTouristScoutOffer
npm run startTouristScoutOfferDynamic
npm run startGdatErinnerung1
npm run startGdatNewsletterDynamic
```

http://127.0.0.1:3000/index_touristScoutOffer.html

http://127.0.0.1:3000/index_touristScoutOfferDynamic.html

http://127.0.0.1:3000/index_gdatErinnerung1.html

http://127.0.0.1:3000/index_gdatNewsletterDynamic.html

https://mailbuilder-564cf.firebaseapp.com/index_gdatErinnerung1.html

## Preview

[Template product offer example](https://mailbuilder-564cf.firebaseapp.com/index_gdat.html)

`https://mailbuilder-564cf.firebaseapp.com/index_gdat.html`

## Send Html Mail

https://mailbuilder-564cf.firebaseapp.com/erinnerung.html

```javascript
"node ./lib/sendEmailNode
```

E-Mail & Benutzername: noreply@hotelinformationstage.at
Server für SSL-Versand: sslout.de
SMTP-SSL-Port: 465
Pwd: !43LtMNKgzWW9nQs

Header Bilder:
https://www.gastrodat.com/wp-content/uploads/sites/213/2018/11/gdat-mail-header-1.jpg
https://www.gastrodat.com/wp-content/wp-content/uploads/sites/213/2018/11/gdat-mail-header-2.jpg
https://www.gastrodat.com/wp-content/wp-content/uploads/sites/213/2018/11/gdat-mail-header-3.jpg
https://www.gastrodat.com/wp-content/wp-content/uploads/sites/213/2018/11/gdat-mail-header-4.jpg

Profil Fotos:
https://www.gastrodat.com/wp-content/wp-content/uploads/sites/213/2018/11/gdat-mail-profil-foto-1.jpg
https://www.gastrodat.com/wp-content/wp-content/uploads/sites/213/2018/11/gdat-mail-profil-foto-2.jpg
https://www.gastrodat.com/wp-content/wp-content/uploads/sites/213/2018/11/gdat-mail-profil-foto-3.jpg
https://www.gastrodat.com/wp-content/wp-content/uploads/sites/213/2018/11/gdat-mail-profil-foto-4.jpg

## Gastrodat Digi Offer Sections

### Hero

Erstes Bild aus dem Slider Modul

### Headline

Begrüßung

### Text

Einleitungstext

### Carousel

Impressions Bildergalerie

### Subline

Zwischen Überschrift "Ihr Angebot"

### Button

Jetzt buchen

### Image

Zimmer Bild

### Accordeon

Teaser

## Standard Section Liste E-Mail

<Section_Liste_EMAIL>

<Section>
<ModulName>HERO</ModulName><ModulHash>HERO</ModulHash>
</Section>
<Section>
<ModulName>HEADLINE</ModulName><ModulHash>HEADLINE</ModulHash>
</Section>
<Section>
<ModulName>TEXT</ModulName><ModulHash> TEXT </ModulHash>
</Section>
<Section>
<ModulName>CAROUSEL</ModulName><ModulHash> CAROUSEL </ModulHash>
</Section>
<Section>
<ModulName>SUBLINE</ModulName><ModulHash> SUBLINE </ModulHash>
</Section>
<Section>
<ModulName>BUTTON</ModulName><ModulHash> BUTTON </ModulHash>
</Section>
<Section>
<ModulName>IMAGE</ModulName><ModulHash> IMAGE </ModulHash>
</Section>
<Section>
<ModulName>ACCORDEON</ModulName><ModulHash> ACCORDEON </ModulHash>
</Section>
</Section_Liste_EMAIL>
