const Koa = require("koa")
const json = require("koa-json")
const cors = require("@koa/cors")
const _ = require("koa-route")
const bodyParser = require("koa-bodyparser")
const app = new Koa()
const { start, getSingleComponentHtml } = require("../lib/index.js")
const fs = require("fs-promise")
const getFullPageHtmlTestOptions = require("./getFullPageHtmlTestOptions")
  .getFullPageHtmlTestOptions

// routes
const routes = {
  getSingleComponentHtml: async ctx => {
    var options = ctx.request.body
    ctx.body = await getSingleComponentHtml(options)
  },
  getFullPageHtml: async ctx => {
    var options = ctx.request.body
    ctx.body = await start(options)
  },
  getFullPageHtmlTest: async ctx => {
    var options = getFullPageHtmlTestOptions
    ctx.body = await start(options)
  }
}

// response
app.use(cors())
app.use(bodyParser())
app.use(json())
app.use(_.post("/getSingleComponentHtml", routes.getSingleComponentHtml))
app.use(_.post("/getFullPageHtml", routes.getFullPageHtml))
app.use(_.get("/getFullPageHtmlTest", routes.getFullPageHtml))
app.listen(4000)

console.log("listening on port http://localhost:4000")
console.log("listening on port http://localhost:4000/getFullPageHtmlTest")

console.log(
  "getFullPageHtmlTestOptions.templateType.header.getFullPageHtmlTestOptions: ",
  getFullPageHtmlTestOptions.templateType.header.logo
)
