import head from "./components/head"
import hero from "./components/hero"
import accordeon from "./components/accordeon"
import image from "./components/image"
import imageText from "./components/imageText"
import headline from "./components/headline"
import header from "./components/header"
import text from "./components/text"
import spacer from "./components/spacer"
import divider from "./components/divider"
import carousel from "./components/carousel"
import button from "./components/button"
import sectionBottom from "./components/sectionBottom"
import footer from "./components/footer"
import end from "./components/end"
import logoSection from "./components/logoSection"
import alternativeHtmlInfo from "./components/alternativeHtmlInfo"
import tableBill from "./components/tableBill"

export async function getHtml({
  options
}: {
  options: MailBuilder.BuildOptions
}): Promise<MailBuilder.Component> {
  const mailContent: MailBuilder.MailTemplateComponent = Object.create({
    html: "",
    text: "",
    translations: [],
    add: function({ component }: { component: MailBuilder.Component }) {
      this.html += component.html
      this.text += component.text
      this.translations += component.translations
    }
  })

  /* COMPONENTS */

  mailContent.add({ component: head({ options: options, settings: {} }) })

  mailContent.add({
    component: alternativeHtmlInfo({
      options: options,
      settings: options.templateType.alternativeHtmlInfo
    })
  })

  mailContent.add({
    component: image({
      options: options,
      settings: options.templateType.image
    })
  })

  mailContent.add({
    component: headline({
      options: options,
      settings: {
        headline: true,
        key: "HDL"
      }
    })
  })

  mailContent.add({ component: divider({ options: options, settings: {} }) })

  mailContent.add({
    component: text({
      options: options,
      settings: options.templateType.text
    })
  })

  mailContent.add({
    component: tableBill({
      options: options,
      settings: options.templateType.tableBill
    })
  })

  mailContent.add({
    component: spacer({
      options: options,
      settings: {}
    })
  })

  mailContent.add({
    component: imageText({
      options: options,
      settings: options.templateType.imageText
    })
  })

  mailContent.add({
    component: text({
      options: options,
      settings: options.templateType.text2
    })
  })

  mailContent.add({
    component: sectionBottom({
      options: options,
      settings: options.templateType.sectionBottom
    })
  })

  mailContent.add({
    component: footer({
      options: options,
      settings: options.templateType.footer
    })
  })

  mailContent.add({ component: end({ options: options, settings: {} }) })

  return {
    html: mailContent.html,
    text: mailContent.text,
    translations: mailContent.translations
  }
}
