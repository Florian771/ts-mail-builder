export interface Accordeon {
    title: string;
    content: string;
}
export interface AccordeonSettings {
    key: string;
    length?: number;
    items?: number;
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: AccordeonSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
