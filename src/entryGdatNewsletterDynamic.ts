import { start } from "./index"
import gdatData from "../dummy/json-default-template1"
import { gdatHotelDataToClient, convertTemplate1Colors } from "./converter"

const chatifyOptions: MailBuilder.DesignSettings = {
  TemplateName: "1",
  primaryColor: "#dd2476",
  secundaryColor: "#000000",
  sec_textColor: "#ffffff",
  headlineColor: "#111", //NEU
  textColor: "#9B9B9B", //c
  textColorHighlight: "#000", //c
  backgroundColor: "#F2F2F2", //c
  backgroundColor2: "#ffffff", //c
  headlineFontName: "'Arial Black', Arial, Helvetica, sans-serif",
  bodyFontName: "Arial, Helvetica, sans-serif",
  bottomSlanted: true
}

const options: MailBuilder.BuildOptions = {
  name: "gdatNewsletterDynamic",
  from: "noreply@touristscout.com",
  to: "florian1677@gmail.com",
  // to: "r.hofherr.gastrodat@gmail.com",
  data: {},
  zusatzTextBilder: [],
  socialMedia: [
    {
      name: "facebook",
      url: "https://wwww.facebook.com"
    }
  ],
  designSettings: chatifyOptions,
  sections: [
    {
      type: "image"
    },
    {
      type: "headlineWithSublineWithLink",
      inverseColors: "true"
    },
    {
      type: "image"
    },
    {
      type: "text"
    },
    {
      type: "headlineWithSubline",
      inverseColors: "true"
    },
    {
      type: "hero"
    },
    {
      type: "accordeon"
    },
    {
      type: "headline"
    },

    {
      type: "subline"
    },
    {
      type: "image"
    },
    {
      type: "button"
    },
    {
      type: "carousel"
    },
    {
      type: "text"
    },
    {
      type: "zusatz_text1"
    },
    {
      type: "zusatz_text2"
    },
    {
      type: "zusatz_text3"
    }
  ],
  templateType: {
    name: "dynamic",
    accordeonImages: 4,
    menuLinks: 3,
    alternativeHtmlInfo: {
      text: true,
      url: true
    },
    header: {
      logo: gdatData.API.ThemeData.StammDaten.HotelLogo_pic.PicUrl,
      context: false,
      menuLinks: 4
    },
    hero: {
      key: "HRO",
      headline: true,
      buttonText: true,
      buttonUrl: true,
      imgUrl: "https://touristscout.com/demo/assets/images/header/13.jpg" // 1, 13, 4
    },
    button: {
      key: "BTN"
    },
    text: {
      key: "TXT"
    },
    accordeon: {
      length: 2,
      key: "ACDN"
    },
    carousel: {
      key: "CSL",
      images: [
        "https://touristscout.com/demo/assets/images/header/12.jpg",
        "https://touristscout.com/demo/assets/images/header/2.jpg",
        "https://touristscout.com/demo/assets/images/header/11.jpg",
        "https://touristscout.com/demo/assets/images/header/10.jpg"
      ]
    },
    /* image: {
      src: "https://touristscout.com/demo/assets/images/header/4.jpg",
      key: "IMAGE_KEY"
    }, */
    sectionBottom: {
      active: false,
      type: "diagonal"
    },
    footer: {
      unsubscribeText: true,
      unsubscribeUrl: true,
      name: true,
      street: true,
      postalCode: true,
      city: true,
      country: true,
      phone: true,
      fax: true,
      email: true,
      website: true,
      uid: true
    }
  },
  contentType: "dynamic",
  buildType: "file",
  testParse: true,
  translations: [] // [{ HEADLINE_HDL_WSBL_WL2: "test" }]
}
start(options).then((res: MailBuilder.Result) => {
  return res
})
