export interface Settings {
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: Settings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
