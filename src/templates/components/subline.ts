import testParse from "../../testParse"
import { textBreak } from "../../config"
import { getTranslations } from "../../utils"

export interface SublineSettings {
  key: string
}

export interface Subline {
  subline: string
}
const placeholder: Subline = {
  subline: "{SUBLINE_KEY}"
}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: SublineSettings
}): MailBuilder.Component => {
  const translations: string[] = getTranslations({
    placeholder: placeholder,
    settings: settings
  })
  let html: string = ""
  let text: string = ""
  text = `${placeholder.subline.replace("KEY", settings.key)}${textBreak}`
  html = `<mj-section padding="10px 20px 10px 20px" background-color="{backgroundColor2}">
  <mj-column>
    <mj-text align="center" font-weight="300" padding="15px 20px 5px 20px" font-size="24px" line-height="30px" color="{textColor}">
      ${placeholder.subline.replace("KEY", settings.key)}
    </mj-text>
  </mj-column>
</mj-section>`

  testParse({ test: true, html: html, options: options, modulName: "subline" })
  return {
    html: html,
    text: text,
    translations: translations,
    parse: (args: any) => testParse(args)
  }
}
