export default {
  API: {
    HotelData: {
      Ort: "Grödig",
      HotelName: "Demo Hotel*****",
      PersName: "",
      Strasse: "Friedensstrasse 8",
      Gruss: "",
      PLZ: "5082",
      Telefon: "0043 6246 738 73 - 0",
      Fax: "",
      UID: "ATU44806304",
      Nation: "A",
      KtoNr: "12345679",
      Bank: "Volksbank",
      BLZ: "45010",
      Email: "office@gastrodat.com",
      BIC: "VBOEATWWSAL",
      IBAN: "AT123456789"
    },
    ReservationData: {
      LFNUMMER: "600",
      ZIMMER_NR: "8",
      PERS_NR: "71354",
      PERSON: "Sturm Thomas",
      VON: "03.09.2018",
      BIS: "05.09.2018",
      ANZ_K1: "0",
      ANZ_K2: "0",
      ANZ_E: "2",
      ANZ_K3: "0",
      ANZ_K4: "0",
      ANZ_E2: "0",
      ANGEBOTSURL:
        "https://apps.weratech-online.com/wtOnlineBooking/Home/CreateOnlineReservation/NTM4NA2?reservationid=5B2051005100&guestid=5CD052B05610597057C0&lang=DE"
    },
    GuestData: {
      LFNUMMER: "71354",
      ANREDE_H: "Herrn",
      FAMNAME: "Sturm",
      VORNAME: "Thomas",
      TITEL: "",
      GESCHLECHT: "M",
      STRASSE: "Klostermaierhofweg 28/6",
      PLZ: "5020",
      ORT: "Salzburg",
      ANREDE_FAM: "Sehr geehrter Herr Thomas Sturm",
      SPRACHE: "D Deutsch",
      NATION: "A",
      ZUSNAME: "",
      P_EMAIL: "sturm@gastrodat.com",
      NATION_TXT: ""
    },
    RoomData: {
      ZIMMER_NR: "8",
      ZIMMER_NAME: "008",
      GEEIGNET_VON: "2",
      GEEIGNET_BIS: "3",
      ZI_KAT: "4",
      ZIMMER_SAUBER: "J",
      GEEIGNET_STD: "2",
      STANDARDLEISTUNG: "10047,",
      TEXT123_exception:
        'Die Datei "C:\\gdem\\ZInfo\\ZIMMER_8.XML" konnte nicht gefunden werden.',
      ZIMMERBILD: "/Content/images/invalidimage.png",
      ZIMMERGRUNDRISS: "/Content/images/invalidimage.png"
    },
    ExtrasData: {
      Posio: [
        {
          FKT_NR: "1437",
          P_VON: "03.09.2018",
          P_BIS: "05.09.2018",
          P_TEXT: "Doppelzimmer Frühstück",
          P_ANZ: "2",
          P_SUM: "320",
          P_PREIS: "80",
          P_RABATT: "0",
          P_KG_NR: "10021",
          GROUPINDEX: "1",
          EXTRA_NR: "10021",
          DAUER_VON: "1",
          DAUER_BIS: "999",
          BESCHREIBUNG:
            '&#10046; Ausstattung &#10046;<br><br>\n<li> Frühstücksbuffet mit <strong>regionalen</strong> Speisen</li>\n<li> größtenteils mit <strong> Balkon oder Terasse</strong></li>\n<li> DU/WC im Separaten Raum</li>\n<li><strong>Gratis</strong> W-lan</li><br>\n&hearts; Freie Nutzung des <strong><font color="green">hauseigenen</font> </strong>Wellnessbereichs mit <strong>über 500m² Nutzungsfläche</strong><br><br>\n&check; Griechische&minus; &amp; Türkischesauna<br>\n&check; Hallenbad mit<strong> 25 Meter</strong><br>\n&check; Erlebnissbad mit 40&deg;Heißwasserbecken<br><br>\nGerne können Sie sich <a href="https://redaktionssystem.com/demohotel3/">hier</a> Informieren oder mehrere Bilder Anschauen<br><br>',
          EXTRABILD:
            "https://www.weratech-files.com/images/281793/gastrodat/Extras/EXTRA_10021.jpg?timestamp=636656984190000000",
          ZUSATZBILDER: {
            EXTRABILD:
              "https://www.weratech-files.com/images/281793/gastrodat/Extras/EXTRA_10021_2.JPG?timestamp=636639015230000000"
          },
          ZUSATZ: "21,10041,10042,10044,10011,10043,"
        },
        {
          FKT_NR: "1439",
          P_VON: "13.08.2018",
          P_BIS: "14.08.2018",
          P_TEXT: "ODER",
          P_ANZ: "0",
          P_SUM: "0",
          P_PREIS: "0",
          P_RABATT: "0",
          P_KG_NR: "0",
          GROUPINDEX: "-1",
          EXTRA_NR: "0",
          DAUER_VON: "0",
          DAUER_BIS: "0",
          BESCHREIBUNG: "",
          EXTRABILD: "/Content/images/invalidimage.png",
          ZUSATZ: ""
        },
        {
          FKT_NR: "1440",
          P_VON: "03.09.2018",
          P_BIS: "05.09.2018",
          P_TEXT: "Dreibettzimmer Frühstück",
          P_ANZ: "2",
          P_SUM: "1920",
          P_PREIS: "480",
          P_RABATT: "0",
          P_KG_NR: "10046",
          GROUPINDEX: "2",
          EXTRA_NR: "10046",
          DAUER_VON: "1",
          DAUER_BIS: "999",
          BESCHREIBUNG:
            '&#10046; Ausstattung &#10046;<br>\n<li> Frühstücksbuffet mit <strong>regionalen</strong> Speisen</li>\n<li> größtenteils mit <strong> Balkon oder Terasse</strong></li>\n<li> DU/WC im Separaten Raum</li>\n<li><strong>Gratis</strong> W-lan</li><br>\n&hearts; Freie Nutzung des <strong><font color="green">hauseigenen</font> </strong>Wellnessbereichs mit <strong>über 500m² Nutzungsfläche</strong><br><br>\n&check; Griechische&minus; &amp; Türkischesauna<br>\n&check; Hallenbad mit<strong> 25 Meter</strong><br>\n&check; Erlebnissbad mit 40&deg;Heißwasserbecken<br><br>\nGerne können Sie sich <a href="https://redaktionssystem.com/demohotel3/">hier</a> Informieren oder mehrere Bilder Anschauen<br><br>',
          EXTRABILD:
            "https://www.weratech-files.com/images/281793/gastrodat/Extras/EXTRA_10046.jpg?timestamp=636585386090000000",
          ZUSATZ: "21,10011,10041,10042,10044,10043,"
        },
        {
          FKT_NR: "1442",
          P_VON: "13.08.2018",
          P_BIS: "14.08.2018",
          P_TEXT: "ODER",
          P_ANZ: "0",
          P_SUM: "0",
          P_PREIS: "0",
          P_RABATT: "0",
          P_KG_NR: "0",
          GROUPINDEX: "-1",
          EXTRA_NR: "0",
          DAUER_VON: "0",
          DAUER_BIS: "0",
          BESCHREIBUNG: "",
          EXTRABILD: "/Content/images/invalidimage.png",
          ZUSATZ: ""
        },
        {
          FKT_NR: "1443",
          P_VON: "03.09.2018",
          P_BIS: "05.09.2018",
          P_TEXT: "Doppelzimmer/ Halbpension blubb",
          P_ANZ: "2",
          P_SUM: "398",
          P_PREIS: "99.5",
          P_RABATT: "0",
          P_KG_NR: "10012",
          GROUPINDEX: "3",
          EXTRA_NR: "10012",
          DAUER_VON: "7",
          DAUER_BIS: "999",
          BESCHREIBUNG:
            '&#10046; Ausstattung &#10046;<br><br>\n<li> Frühstücksbuffet mit <strong>regionalen</strong> Speisen</li>\n<li> <strong>4-Gang Abendmenü</strong> gezaubert von unserem <strong>3 Sterne Koch</strong> <font color="green">Alexander</font>\n<li> größtenteils mit <strong> Balkon oder Terasse</strong></li>\n<li> DU/WC im Separaten Raum</li>\n<li><strong>Gratis</strong> W-lan</li><br>\n&hearts; Freie Nutzung des <strong> hauseigenen</strong> <font color="green">Wellnessbereichs </font> mit <strong>über 500m² Nutzungsfläche</strong><br><br>\n&check; Griechische&minus; &amp; Türkischesauna<br>\n&check; Hallenbad mit <strong>25 Meter</strong><br>\n&check; Erlebnissbad mit 40&deg;Heißwasserbecken<br><br>\nGerne können Sie sich <a href="https://redaktionssystem.com/demohotel3/">hier</a> Informieren oder mehrere Bilder Anschauen<br><br>',
          EXTRABILD:
            "https://www.weratech-files.com/images/281793/gastrodat/Extras/EXTRA_10012.jpg?timestamp=636656977060000000",
          ZUSATZBILDER: {
            EXTRABILD:
              "https://www.weratech-files.com/images/281793/gastrodat/Extras/EXTRA_10012_2.JPG?timestamp=636639566900000000"
          },
          ZUSATZ: "21,10043,10041,10042,10044,"
        },
        {
          FKT_NR: "1445",
          P_VON: "13.08.2018",
          P_BIS: "14.08.2018",
          P_TEXT: "ODER",
          P_ANZ: "0",
          P_SUM: "0",
          P_PREIS: "0",
          P_RABATT: "0",
          P_KG_NR: "0",
          GROUPINDEX: "-1",
          EXTRA_NR: "0",
          DAUER_VON: "0",
          DAUER_BIS: "0",
          BESCHREIBUNG: "",
          EXTRABILD: "/Content/images/invalidimage.png",
          ZUSATZ: ""
        },
        {
          FKT_NR: "1446",
          P_VON: "03.09.2018",
          P_BIS: "05.09.2018",
          P_TEXT: "Dreibettzimmer Halbpension",
          P_ANZ: "2",
          P_SUM: "466",
          P_PREIS: "116.5",
          P_RABATT: "0",
          P_KG_NR: "10047",
          GROUPINDEX: "4",
          EXTRA_NR: "10047",
          DAUER_VON: "1",
          DAUER_BIS: "999",
          BESCHREIBUNG:
            '&#10046; Ausstattung &#10046;<br>\n<li> Frühstücksbuffet mit <strong>regionalen</strong> Speisen</li>\n<li> <strong>4-Gang Abendmenü</strong> gezaubert von unserem <strong>3 Sterne Koch</strong> <font color="green">Alexander</font>\n<li> größtenteils mit <strong> Balkon oder Terasse</strong></li>\n<li> DU/WC im Separaten Raum</li>\n<li><strong>Gratis</strong> W-lan</li><br>\n&hearts; Freie Nutzung des <strong> hauseigenen</strong> <font color="green">Wellnessbereichs </font> mit <strong>über 500m² Nutzungsfläche</strong><br><br>\n&check; Griechische&minus; &amp; Türkischesauna<br>\n&check; Hallenbad mit <strong>25 Meter</strong><br>\n&check; Erlebnissbad mit 40&deg;Heißwasserbecken<br><br>\nGerne können Sie sich <a href="https://redaktionssystem.com/demohotel3/">hier</a> Informieren oder mehrere Bilder Anschauen<br><br>',
          EXTRABILD:
            "https://www.weratech-files.com/images/281793/gastrodat/Extras/EXTRA_10047.jpg?timestamp=636431536140000000",
          ZUSATZ: "21,10041,10042,10044,10043,"
        }
      ]
    },
    ThemeData: {
      Themes: {
        gdatTheme: {
          theme_pics: {
            gdatPicture: [
              {
                PicId: "2b96fcc1ee889",
                PicName: "Ihr Weg",
                PicPath: "$i_offer\\theme_Sommer\\2b96fcc1ee889.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/281793/gastrodat/2b96fcc1ee889.jpg",
                PicHTML_Link: "",
                PicKategorie: "Ausblicke",
                LastPicUpload: "2018-07-25T11:09:59.1919655+02:00"
              },
              {
                PicId: "2b970089989f1",
                PicName: "Ruhe & Entspannung",
                PicPath: "$i_offer\\theme_Sommer\\2b970089989f1.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/281793/gastrodat/2b970089989f1.jpg",
                PicHTML_Link: "",
                PicKategorie: "Ausblicke",
                LastPicUpload: "2018-07-25T11:09:59.2232154+02:00"
              },
              {
                PicId: "2b97022f98541",
                PicName: "Genießen",
                PicPath: "$i_offer\\theme_Sommer\\2b97022f98541.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/281793/gastrodat/2b97022f98541.jpg",
                PicHTML_Link: "",
                PicKategorie: "Ausblicke",
                LastPicUpload: "2018-07-25T11:09:59.2568593+02:00"
              },
              {
                PicId: "2b9703ee882b7",
                PicName: "Familie Pur",
                PicPath: "$i_offer\\theme_Sommer\\2b9703ee882b7.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/281793/gastrodat/2b9703ee882b7.jpg",
                PicHTML_Link: "",
                PicKategorie: "Ausblicke",
                LastPicUpload: "2018-07-25T11:09:59.2724861+02:00"
              },
              {
                PicId: "2dfdfad8451fb",
                PicName: "Pool",
                PicPath: "$i_offer\\theme_Sommer\\2dfdfad8451fb.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/281793/gastrodat/2dfdfad8451fb.jpg",
                PicHTML_Link: "",
                PicKategorie: "",
                LastPicUpload: "2018-07-25T11:09:59.3037361+02:00"
              },
              {
                PicId: "2dfdfbe632e63",
                PicName: "Wiese",
                PicPath: "$i_offer\\theme_Sommer\\2dfdfbe632e63.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/281793/gastrodat/2dfdfbe632e63.jpg",
                PicHTML_Link: "",
                PicKategorie: "",
                LastPicUpload: "2018-07-25T11:09:59.3349704+02:00"
              }
            ]
          },
          theme_teasers: {
            gdatTeaser: [
              {
                Teaser_pic: {
                  PicId: "2b9707c1561c3",
                  PicName: "unserem Wellnessbereich",
                  PicPath: "$i_offer\\teaser_Entspannen in\\2b9707c1561c3.jpg",
                  PicUrl:
                    "https://www.weratech-files.com/images/281793/gastrodat/2b9707c1561c3.jpg",
                  PicHTML_Link: "",
                  PicKategorie: "",
                  LastPicUpload: "2018-07-25T11:09:59.3662203+02:00"
                },
                validFrom: "2018-06-06T13:11:12",
                validTo: "2018-11-04T13:11:12",
                SuchwortListe: "",
                TeaserId: "2b9707c1561c3",
                TeaserName: "Entspannen in...."
              },
              {
                Teaser_pic: {
                  PicId: "2b970d24214aa",
                  PicName: "mitfahren und Staunen...",
                  PicPath:
                    "$i_offer\\teaser_neuer ThemenTeaser\\2b970d24214aa.jpg",
                  PicUrl:
                    "https://www.weratech-files.com/images/281793/gastrodat/2b970d24214aa.jpg",
                  PicHTML_Link: "",
                  PicKategorie: "Ausflüge",
                  LastPicUpload: "2018-07-25T11:09:59.3974697+02:00"
                },
                validFrom: "2018-06-06T13:13:36.844305+02:00",
                validTo: "2018-11-04T13:13:36",
                SuchwortListe: "",
                TeaserId: "2b970d24214aa",
                TeaserName: "Lust auf eine Schifffahrt"
              },
              {
                Teaser_pic: {
                  PicId: "2b970f6aeef06",
                  PicName: "in den Schluchten",
                  PicPath:
                    "$i_offer\\teaser_neuer ThemenTeaser\\2b970f6aeef06.jpg",
                  PicUrl:
                    "https://www.weratech-files.com/images/281793/gastrodat/2b970f6aeef06.jpg",
                  PicHTML_Link: "",
                  PicKategorie: "Ausflüge",
                  LastPicUpload: "2018-07-25T11:09:59.4587797+02:00"
                },
                validFrom: "2018-06-06T13:14:37.9556614+02:00",
                validTo: "2018-11-04T13:14:37",
                SuchwortListe: "",
                TeaserId: "2b970f6aeef06",
                TeaserName: "Wandern..."
              },
              {
                Teaser_pic: {
                  PicId: "2c47f65804810",
                  PicName: "im Streichelzoo",
                  PicPath:
                    "$i_offer\\teaser_neuer ThemenTeaser\\2c47f65804810.jpg",
                  PicUrl:
                    "https://www.weratech-files.com/images/281793/gastrodat/2c47f65804810.jpg",
                  PicHTML_Link: "",
                  PicKategorie: "Ausflüge",
                  LastPicUpload: "2018-07-25T11:09:59.4744047+02:00"
                },
                validFrom: "2018-06-20T14:55:39",
                validTo: "2018-11-04T14:55:39",
                SuchwortListe: "",
                TeaserId: "2c47f65804810",
                TeaserName: "Abenteuer"
              }
            ]
          },
          validFrom: "2018-06-06T11:57:40.8815605+02:00",
          validTo: "2018-11-04T11:57:40",
          SuchwortListe: "",
          ThemeId: "2b96636b155f5",
          ThemeName: "Sommer"
        }
      },
      BilderGalerie_pics: {
        gdatPicture: [
          {
            PicId: "2b97309b9157f",
            PicName: "Eine Auswahl unserer Doppelzimmer",
            PicPath: "$i_offer\\impressions\\2b97309b9157f.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b97309b9157f.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer",
            LastPicUpload: "2018-07-25T11:09:59.6502432+02:00"
          },
          {
            PicId: "2b9733a375d3b",
            PicName: "Eine Auswahl unseer Dreibettzimmer",
            PicPath: "$i_offer\\impressions\\2b9733a375d3b.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b9733a375d3b.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer",
            LastPicUpload: "2018-07-25T11:09:59.6814944+02:00"
          },
          {
            PicId: "2b9735690fd0f",
            PicName: "Eine Auswahl unserer Suite´s",
            PicPath: "$i_offer\\impressions\\2b9735690fd0f.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b9735690fd0f.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer",
            LastPicUpload: "2018-07-25T11:09:59.7127431+02:00"
          },
          {
            PicId: "2b9737ea8b10d",
            PicName: "unser Türmchen",
            PicPath: "$i_offer\\impressions\\2b9737ea8b10d.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b9737ea8b10d.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer",
            LastPicUpload: "2018-07-25T11:09:59.7283681+02:00"
          },
          {
            PicId: "2b973b6bfabdd",
            PicName: "liebe zum Detail",
            PicPath: "$i_offer\\impressions\\2b973b6bfabdd.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b973b6bfabdd.jpg",
            PicHTML_Link: "",
            PicKategorie: "Genuss",
            LastPicUpload: "2018-07-25T11:09:59.7596187+02:00"
          },
          {
            PicId: "2b973de475bd0",
            PicName: "Einfach guad",
            PicPath: "$i_offer\\impressions\\2b973de475bd0.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b973de475bd0.jpg",
            PicHTML_Link: "",
            PicKategorie: "Genuss",
            LastPicUpload: "2018-07-25T11:09:59.7908693+02:00"
          },
          {
            PicId: "2b973f83bfeb7",
            PicName: "Etwas für´s Auge",
            PicPath: "$i_offer\\impressions\\2b973f83bfeb7.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b973f83bfeb7.jpg",
            PicHTML_Link: "",
            PicKategorie: "Genuss",
            LastPicUpload: "2018-07-25T11:09:59.8377437+02:00"
          },
          {
            PicId: "2baf330ed51d7",
            PicName: "Haus",
            PicPath: "$i_offer\\impressions\\2baf330ed51d7.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2baf330ed51d7.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer",
            LastPicUpload: "2018-07-25T11:09:59.8689973+02:00"
          },
          {
            PicId: "2baf357b3a0cc",
            PicName: "Suite",
            PicPath: "$i_offer\\impressions\\2baf357b3a0cc.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2baf357b3a0cc.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer",
            LastPicUpload: "2018-07-25T11:09:59.9002479+02:00"
          },
          {
            PicId: "2baf380bac3d1",
            PicName: "Einzelzimmer",
            PicPath: "$i_offer\\impressions\\2baf380bac3d1.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2baf380bac3d1.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer",
            LastPicUpload: "2018-07-25T11:09:59.9158716+02:00"
          }
        ]
      },
      impression_pics: {
        gdatPicture: [
          {
            PicId: "2b96fcc1ee889",
            PicName: "Ihr Weg",
            PicPath: "$i_offer\\theme_Sommer\\2b96fcc1ee889.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b96fcc1ee889.jpg",
            PicHTML_Link: "",
            PicKategorie: "Ausblicke",
            LastPicUpload: "2018-07-25T11:09:59.1919655+02:00"
          },
          {
            PicId: "2b970089989f1",
            PicName: "Ruhe & Entspannung",
            PicPath: "$i_offer\\theme_Sommer\\2b970089989f1.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b970089989f1.jpg",
            PicHTML_Link: "",
            PicKategorie: "Ausblicke",
            LastPicUpload: "2018-07-25T11:09:59.2232154+02:00"
          },
          {
            PicId: "2b97022f98541",
            PicName: "Genießen",
            PicPath: "$i_offer\\theme_Sommer\\2b97022f98541.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b97022f98541.jpg",
            PicHTML_Link: "",
            PicKategorie: "Ausblicke",
            LastPicUpload: "2018-07-25T11:09:59.2568593+02:00"
          },
          {
            PicId: "2b9703ee882b7",
            PicName: "Familie Pur",
            PicPath: "$i_offer\\theme_Sommer\\2b9703ee882b7.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b9703ee882b7.jpg",
            PicHTML_Link: "",
            PicKategorie: "Ausblicke",
            LastPicUpload: "2018-07-25T11:09:59.2724861+02:00"
          },
          {
            PicId: "2b970d24214aa",
            PicName: "mitfahren und Staunen...",
            PicPath: "$i_offer\\teaser_neuer ThemenTeaser\\2b970d24214aa.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b970d24214aa.jpg",
            PicHTML_Link: "",
            PicKategorie: "Ausflüge",
            LastPicUpload: "2018-07-25T11:09:59.3974697+02:00"
          },
          {
            PicId: "2b970f6aeef06",
            PicName: "in den Schluchten",
            PicPath: "$i_offer\\teaser_neuer ThemenTeaser\\2b970f6aeef06.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b970f6aeef06.jpg",
            PicHTML_Link: "",
            PicKategorie: "Ausflüge",
            LastPicUpload: "2018-07-25T11:09:59.4587797+02:00"
          },
          {
            PicId: "2c47f65804810",
            PicName: "im Streichelzoo",
            PicPath: "$i_offer\\teaser_neuer ThemenTeaser\\2c47f65804810.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2c47f65804810.jpg",
            PicHTML_Link: "",
            PicKategorie: "Ausflüge",
            LastPicUpload: "2018-07-25T11:09:59.4744047+02:00"
          },
          {
            PicId: "2c47fac44f1ee",
            PicName: "neues Bild: ",
            PicPath: "$i_offer\\teaser_neuer ThemenTeaser\\2c47fac44f1ee.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2c47fac44f1ee.jpg",
            PicHTML_Link: "",
            PicKategorie: "Entspannen",
            LastPicUpload: "2018-07-25T11:09:59.5056583+02:00"
          },
          {
            PicId: "2b97309b9157f",
            PicName: "Eine Auswahl unserer Doppelzimmer",
            PicPath: "$i_offer\\impressions\\2b97309b9157f.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b97309b9157f.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer",
            LastPicUpload: "2018-07-25T11:09:59.6502432+02:00"
          },
          {
            PicId: "2b9733a375d3b",
            PicName: "Eine Auswahl unseer Dreibettzimmer",
            PicPath: "$i_offer\\impressions\\2b9733a375d3b.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b9733a375d3b.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer",
            LastPicUpload: "2018-07-25T11:09:59.6814944+02:00"
          },
          {
            PicId: "2b9735690fd0f",
            PicName: "Eine Auswahl unserer Suite´s",
            PicPath: "$i_offer\\impressions\\2b9735690fd0f.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b9735690fd0f.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer",
            LastPicUpload: "2018-07-25T11:09:59.7127431+02:00"
          },
          {
            PicId: "2b9737ea8b10d",
            PicName: "unser Türmchen",
            PicPath: "$i_offer\\impressions\\2b9737ea8b10d.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b9737ea8b10d.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer",
            LastPicUpload: "2018-07-25T11:09:59.7283681+02:00"
          },
          {
            PicId: "2b973b6bfabdd",
            PicName: "liebe zum Detail",
            PicPath: "$i_offer\\impressions\\2b973b6bfabdd.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b973b6bfabdd.jpg",
            PicHTML_Link: "",
            PicKategorie: "Genuss",
            LastPicUpload: "2018-07-25T11:09:59.7596187+02:00"
          },
          {
            PicId: "2b973de475bd0",
            PicName: "Einfach guad",
            PicPath: "$i_offer\\impressions\\2b973de475bd0.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b973de475bd0.jpg",
            PicHTML_Link: "",
            PicKategorie: "Genuss",
            LastPicUpload: "2018-07-25T11:09:59.7908693+02:00"
          },
          {
            PicId: "2b973f83bfeb7",
            PicName: "Etwas für´s Auge",
            PicPath: "$i_offer\\impressions\\2b973f83bfeb7.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2b973f83bfeb7.jpg",
            PicHTML_Link: "",
            PicKategorie: "Genuss",
            LastPicUpload: "2018-07-25T11:09:59.8377437+02:00"
          },
          {
            PicId: "2baf330ed51d7",
            PicName: "Haus",
            PicPath: "$i_offer\\impressions\\2baf330ed51d7.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2baf330ed51d7.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer",
            LastPicUpload: "2018-07-25T11:09:59.8689973+02:00"
          },
          {
            PicId: "2baf357b3a0cc",
            PicName: "Suite",
            PicPath: "$i_offer\\impressions\\2baf357b3a0cc.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2baf357b3a0cc.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer",
            LastPicUpload: "2018-07-25T11:09:59.9002479+02:00"
          },
          {
            PicId: "2baf380bac3d1",
            PicName: "Einzelzimmer",
            PicPath: "$i_offer\\impressions\\2baf380bac3d1.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281793/gastrodat/2baf380bac3d1.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer",
            LastPicUpload: "2018-07-25T11:09:59.9158716+02:00"
          }
        ]
      },
      StammDaten: {
        HotelLogo_pic: {
          PicId: "2b96643bdc4bb",
          PicName: "HotelLogo",
          PicPath: "$i_offer\\LOGO\\i_offer_StammDaten_HotelLogo_pic_.png",
          PicUrl:
            "https://www.weratech-files.com/images/281793/gastrodat/i_offer_StammDaten_HotelLogo_pic_.png",
          PicHTML_Link: "https://redaktionssystem.com/demohotel3/",
          LastPicUpload: "2018-07-25T11:09:59.618992+02:00"
        },
        Section_Liste: {
          Section: [
            {
              ModulName: "MENU_BAND",
              ModulId: "",
              ModulHash: "MENU_BAND",
              SortOrder: "0"
            },
            {
              ModulName: "GREETING",
              ModulId: "",
              ModulHash: "GREETING",
              SortOrder: "1"
            },
            {
              ModulName: "WELCOME_TEXT",
              ModulId: "",
              ModulHash: "WELCOME_TEXT",
              SortOrder: "2"
            },
            {
              ModulName: "KACHELBILDER",
              ModulId: "",
              ModulHash: "KACHELBILDER",
              SortOrder: "3"
            },
            {
              ModulName: "ANGEBOT_DETAILS",
              ModulId: "",
              ModulHash: "ANGEBOT_DETAILS",
              SortOrder: "4"
            },
            {
              ModulName: "ZWISCHEN_BILD1",
              ModulId: "",
              ModulHash: "ZWISCHEN_BILD1",
              SortOrder: "5"
            },
            {
              ModulName: "ANREISE_BESCHREIBUNG",
              ModulId: "",
              ModulHash: "ANREISE_BESCHREIBUNG",
              SortOrder: "6"
            },
            {
              ModulName: "TEASER_BLOCK",
              ModulId: "",
              ModulHash: "TEASER_BLOCK",
              SortOrder: "7"
            },
            {
              ModulName: "NACHRICHTEN_BLOCK",
              ModulId: "",
              ModulHash: "NACHRICHTEN_BLOCK",
              SortOrder: "8"
            },
            {
              ModulName: "SLIDESHOW",
              ModulId: "",
              ModulHash: "SLIDESHOW",
              SortOrder: "9"
            },
            {
              ModulName: "FOOTER",
              ModulId: "",
              ModulHash: "FOOTER",
              SortOrder: "10"
            },
            {
              ModulName: "IMPRESSUM",
              ModulId: "",
              ModulHash: "IMPRESSUM",
              SortOrder: "11"
            }
          ]
        },
        EinzelText_Liste: {
          EinzelText: [
            {
              Tag: "gdat_chatten",
              Text: "haben Sie Fragen? Chatten Sie mit uns"
            },
            {
              Tag: "gdat_details",
              Text: "Details"
            },
            {
              Tag: "gdat_variante",
              Text: "Angebot"
            },
            {
              Tag: "gdat_angebot",
              Text: "Ihr persönliches Angebot"
            },
            {
              Tag: "gdat_jetzt_buchen",
              Text: "Jetzt buchen"
            },
            {
              Tag: "gdat_personen",
              Text: "Personen"
            },
            {
              Tag: "gdat_datum",
              Text: "Datum"
            },
            {
              Tag: "gdat_naechte",
              Text: "Nächte"
            },
            {
              Tag: "gdat_preis",
              Text: "Preis"
            },
            {
              Tag: "gdat_gesamt",
              Text: "GESAMT"
            },
            {
              Tag: "gdat_impressionen",
              Text: "IMPRESSIONEN"
            },
            {
              Tag: "gdat_machen_sie_sich_ein_bild",
              Text: "MACHEN SIE SICH EIN BILD"
            },
            {
              Tag: "gdat_kontakt",
              Text: "Haben Sie noch Fragen, schreiben Sie uns eine E-Mail."
            },
            {
              Tag: "gdat_wir_freuen_uns_von_ihnen_zu_hoeren",
              Text: "Wir freuen uns von Ihnen zu hören"
            },
            {
              Tag: "gdat_telefon",
              Text: "Telefon"
            },
            {
              Tag: "gdat_email",
              Text: "E-Mail"
            },
            {
              Tag: "gdat_adresse",
              Text: "Addresse"
            },
            {
              Tag: "gdat_karte_anzeigen",
              Text: "Karte anzeigen"
            },
            {
              Tag: "gdat_route_berechnen",
              Text: "Route berechnen"
            },
            {
              Tag: "gdat_name",
              Text: "Name"
            },
            {
              Tag: "gdat_nachricht",
              Text: "Nachricht"
            },
            {
              Tag: "gdat_input:name_hinweis",
              Text: "Bitte geben Sie Ihren Namen an"
            },
            {
              Tag: "gdat_input:email_hinweis",
              Text: "Bitte geben Sie Ihre E-Mail an"
            },
            {
              Tag: "gdat_textarea_nachricht_hinweis",
              Text: "Bitte geben Sie Ihren Namen an"
            },
            {
              Tag: "gdat_button_nachricht_senden",
              Text: "Nachricht senden"
            },
            {
              Tag: "gdat_page_loading_error",
              Text:
                "Hoppla! Die von Ihnen gesuchte Seite ist zur Zeit nicht verfügbar! Bitte laden Sie es neu, versuchen Sie es später oder melden Sie es. Wir werden es so schnell wie möglich beheben."
            },
            {
              Tag: "gdat_reload",
              Text: "Neu laden"
            },
            {
              Tag: "gdat_report",
              Text: "Melde dies"
            },
            {
              Tag: "gdat_newsletter",
              Text: "Newsletter"
            },
            {
              Tag: "gdat_input_newsletter_anmelden",
              Text: "Kostenlos anmelden"
            },
            {
              Tag: "gdat_zum_seitenanfang",
              Text: "Zum Seitenanfang"
            },
            {
              Tag: "gdat_please_choose",
              Text: "Bitte wählen"
            },
            {
              Tag: "gdat_highlights",
              Text: "Gesamt"
            },
            {
              Tag: "gdat_angebot_ansehen",
              Text: "Angebot ansehen"
            },
            {
              Tag: "gat_anreise_abreise",
              Text: "Anreise/Abreise"
            },
            {
              Tag: "gdat_preis_pro_person_und_nacht",
              Text: "Preis pro Tag/Pers,"
            },
            {
              Tag: "gdat_jetzt_anfragen",
              Text: "jetzt anfragen"
            },
            {
              Tag: "gdat_weitere_bilder",
              Text: "weitere Bilder"
            },
            {
              Tag: "gdat_fax",
              Text: "Fax"
            },
            {
              Tag: "gdat_uid",
              Text: "UID"
            },
            {
              Tag: "gdat_bank",
              Text: "Bank"
            },
            {
              Tag: "gdat_ktonr",
              Text: "KtoNr"
            },
            {
              Tag: "gdat_bic",
              Text: "BIC"
            },
            {
              Tag: "gdat_iban",
              Text: "IBAN"
            },
            {
              Tag: "gdat_webseite",
              Text: "Webseite"
            },
            {
              Tag: "gdat_vorname",
              Text: "Vorname"
            },
            {
              Tag: "gdat_nachname",
              Text: "Nachname"
            },
            {
              Tag: "gdat_ihr_vorname",
              Text: "Ihr Vorname"
            },
            {
              Tag: "gdat_ihr_nachname",
              Text: "Ihr Nachname"
            },
            {
              Tag: "gdat_ihre_email",
              Text: "Ihre E-Mail Adresse"
            },
            {
              Tag: "gdat_ihre_nachricht",
              Text: "Ihre Nachricht an uns..."
            },
            {
              Tag: "gdat_ergebnis_der_rechenaufgabe",
              Text: "Ergebnis der o.g. Rechenaufgabe"
            },
            {
              Tag: "gdat_anzahl",
              Text: "Anzahl"
            },
            {
              Tag: "gdat_impressum",
              Text: "Impressum"
            },
            {
              Tag: "gdat_datenschutz",
              Text: "das ist ein Test zur DSVGO"
            },
            {
              Tag: "gdat_teaser",
              Text: "Aktivitäten rund um Salzburg"
            },
            {
              Tag: "gdat_nr",
              Text: "Nr."
            },
            {
              Tag: "gdat_beschreibung",
              Text: "Beschreibung"
            },
            {
              Tag: "gdat_leistung",
              Text: "Leistung"
            },
            {
              Tag: "gdat_leistungen",
              Text: "Leistungen"
            }
          ]
        },
        MenuItem_Liste: {
          MenuItem: [
            {
              MenuId: "636638892197309744",
              MenuName: "unsere Inklusiv Leistungen",
              MenuURL:
                "https://touristscout.com/?gdatid=318-281793&reservationid=-1&lang=DE&pageid=i_offer1&template=2",
              Self: "true"
            },
            {
              MenuId: "636638898539851466",
              MenuName: "DemoHotel*****",
              MenuURL: "https://redaktionssystem.com/demohotel3/",
              Self: "false"
            },
            {
              MenuId: "636639788470523763",
              MenuName: "Telefon +43 (0) 6246 73 873-0",
              MenuURL: '<a href="tel:+436246738730">',
              Self: "true"
            },
            {
              MenuId: "636664703512554111",
              MenuName: "chatten",
              MenuURL: "https://www.facebook.de",
              Self: "false"
            },
            {
              MenuId: "636673323238015039",
              MenuName: "Angebot ",
              MenuURL: "#ANGEBOT_DETAILS",
              Self: "false"
            }
          ]
        },
        MenuImpressumItem_Liste: {
          MenuItem: {
            MenuId: "636643264998007870",
            MenuName: "GASTROdat.com",
            MenuURL: "https://www.gastrodat.com",
            Self: "false"
          }
        },
        HotelName: "DemoHotel*****",
        WellcomeText:
          '<?xml version="1.0" ?>\r<html xmlns="https://www.w3.org/1999/xhtml">\r<head>\r<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r<title></title>\r</head>\r<body>\r<p lang="de-DE" style="line-height:130%;">Hereinspaziert, hereinspaziert,</p>\r<p lang="de-DE" style="line-height:130%;">alles ist schon tapeziert.</p>\r<p lang="de-DE" style="line-height:130%;">Die Möbel sind zurechtgerückt,</p>\r<p lang="de-DE" style="line-height:130%;">der Essenstisch schon gut bestückt.</p>\r<p lang="de-DE" style="line-height:130%;">Gerne bieten wir Ihnen für Ihre ganz persönlichen Wohlfühltage unverbindlich folgendes Arrangement an:</p>\r</body>\r</html>',
        SubDomainUrl: ".https://www.touristscout.com",
        AngebotsText:
          '<?xml version="1.0" ?>\r<html xmlns="https://www.w3.org/1999/xhtml">\r<head>\r<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r<title></title>\r</head>\r<body>\r<p style="line-height:130%;">Hier finden Sie Ihr persönliches Angebot</p>\r</body>\r</html>',
        AnreiseInfoText:
          '<?xml version="1.0" ?>\r<html xmlns="https://www.w3.org/1999/xhtml">\r<head>\r<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r<title></title>\r</head>\r<body>\r<p lang="de-DE" style="line-height:130%;">Anreise: ab 15:00 Uhr</p>\r</body>\r</html>',
        AbreiseInfoText:
          '<?xml version="1.0" ?>\r<html xmlns="https://www.w3.org/1999/xhtml">\r<head>\r<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r<title></title>\r</head>\r<body>\r<p lang="de-DE" style="line-height:130%;">Abreise: bis 11:00 Uhr</p>\r<p lang="de-DE" style="line-height:130%;">Für einen Late Check out bitten wir Sie es uns rechzeitig mitzuteilen.</p>\r</body>\r</html>',
        ZusatzText1: "",
        ZusatzText1_Header: "Doppelzimmer",
        ZusatzText1_Collapsed: "false",
        ZusatzText_Bild1: {
          LastPicUpload: "0001-01-01T00:00:00"
        },
        ZusatzText2: "",
        ZusatzText2_Header: "",
        ZusatzText2_Collapsed: "false",
        ZusatzText_Bild2: {
          LastPicUpload: "0001-01-01T00:00:00"
        },
        ZusatzText3: "",
        ZusatzText3_Header: "",
        ZusatzText3_Collapsed: "false",
        ZusatzText_Bild3: {
          LastPicUpload: "0001-01-01T00:00:00"
        },
        ZusatzText4: "",
        ZusatzText4_Header: "",
        ZusatzText4_Collapsed: "false",
        ZusatzText_Bild4: {
          LastPicUpload: "0001-01-01T00:00:00"
        },
        ZusatzText5: "",
        ZusatzText5_Header: "",
        ZusatzText5_Collapsed: "false",
        ZusatzText_Bild5: {
          LastPicUpload: "0001-01-01T00:00:00"
        },
        ZwischenBild1: {
          PicId: "2b973de475bd0",
          PicName: "Einfach guad",
          PicPath: "$i_offer\\impressions\\2b973de475bd0.jpg",
          PicUrl:
            "https://www.weratech-files.com/images/281793/gastrodat/2b973de475bd0.jpg",
          PicHTML_Link: "",
          PicKategorie: "Genuss",
          LastPicUpload: "2018-07-23T11:20:30.1568304+02:00"
        },
        ZwischenBild2: {
          LastPicUpload: "0001-01-01T00:00:00"
        },
        ZwischenBild3: {
          LastPicUpload: "0001-01-01T00:00:00"
        },
        ZwischenBild4: {
          LastPicUpload: "0001-01-01T00:00:00"
        },
        ZwischenBild5: {
          LastPicUpload: "0001-01-01T00:00:00"
        },
        FaceBookPageId: "",
        SozMedia_FaceBook_Url: "",
        SozMedia_GooglePlus_Url: "",
        SozMedia_Pinterest_Url: "",
        SozMedia_Instagram_Url: "",
        SozMedia_YouTube_Url: "",
        SozMedia_Twitter_Url: "",
        ImageCompressionQuality: "50"
      },
      DesignSettings: {
        TextModulStyle_Liste: {
          TextModulStyle: [
            {
              StyleName: "H1",
              InlineStyle: "false",
              Bold: "true",
              FontName: "Arial",
              FontSize: "480",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "None",
              Strikeout: "false",
              TextBackColor: "#FFFFFF",
              Underline: "false",
              UseStyle: "false"
            },
            {
              StyleName: "H2",
              InlineStyle: "false",
              Bold: "true",
              FontName: "Verdana",
              FontSize: "239",
              ForeColor: "#008000",
              Italic: "false",
              ListFormat: "None",
              Strikeout: "false",
              TextBackColor: "transparent",
              Underline: "false",
              UseStyle: "false"
            },
            {
              StyleName: "H3",
              InlineStyle: "false",
              Bold: "true",
              FontName: "Verdana",
              FontSize: "239",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "None",
              Strikeout: "false",
              TextBackColor: "transparent",
              Underline: "false",
              UseStyle: "true"
            },
            {
              StyleName: "H4",
              InlineStyle: "false",
              Bold: "true",
              FontName: "Arial",
              FontSize: "220",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "None",
              Strikeout: "false",
              TextBackColor: "#FFFFFF",
              Underline: "false",
              UseStyle: "false"
            },
            {
              StyleName: "H5",
              InlineStyle: "false",
              Bold: "true",
              FontName: "Arial",
              FontSize: "180",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "None",
              Strikeout: "false",
              TextBackColor: "#FFFFFF",
              Underline: "false",
              UseStyle: "false"
            },
            {
              StyleName: "P",
              InlineStyle: "false",
              Bold: "false",
              FontName: "Arial",
              FontSize: "180",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "None",
              Strikeout: "false",
              TextBackColor: "#FFFFFF",
              Underline: "false",
              UseStyle: "false"
            },
            {
              StyleName: "UL",
              InlineStyle: "false",
              Bold: "false",
              FontName: "Arial",
              FontSize: "180",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "Bulleted",
              Strikeout: "false",
              TextBackColor: "#FFFFFF",
              Underline: "false",
              UseStyle: "false"
            }
          ]
        },
        TemplateName: "",
        primaryColor: "#1A7455",
        secundaryColor: "#0C6342",
        bckg_Color3: "#0C6342",
        bckg_Color4: "#1A7455",
        bckg_Color5: "#FFFFFF",
        prim_textColor: "#FFFFFF",
        sec_textColor: "#FFFFFF",
        textColor_Color3: "#FFFFFF",
        textColor_Color4: "",
        textColor_Color5: "",
        backgroundColor: "#0C6342",
        headlineFontName: "",
        bodyFontName: "",
        textColor: "#000000"
      }
    }
  }
}
