"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function gdatHotelDataToClient(client) {
    var outputClient = {
        city: client.Ort,
        companyName: client.HotelName,
        contactPersonFirstName: "",
        contactPersonLastName: client.PersName,
        street: client.Strasse,
        greeting: client.Gruss,
        postalCode: client.PLZ,
        phone: client.Telefon,
        fax: client.Fax,
        uid: client.UID,
        country: client.Nation,
        bankAccountNr: client.KtoNr,
        bankName: client.Bank,
        blz: client.BLZ,
        email: client.Email,
        bic: client.BIC,
        iban: client.IBAN
    };
    return outputClient;
}
exports.gdatHotelDataToClient = gdatHotelDataToClient;
function convertTemplate1Colors(options) {
    var updatedOptions = options;
    updatedOptions.secundaryColor = options.primaryColor;
    updatedOptions.primaryColor = options.bckg_Color4;
    return updatedOptions;
}
exports.convertTemplate1Colors = convertTemplate1Colors;
//# sourceMappingURL=converter.js.map