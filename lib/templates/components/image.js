"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
var config_1 = require("../../config");
var utils_1 = require("../../utils");
var templateReplacer_1 = require("../templateReplacer");
var placeholder = {
    alt: "{IMAGE_ALT_KEY}"
};
exports.mjmlImage = "<mj-section padding=\"0px\" background-color=\"{primaryColor}\">\n<mj-column>\n  <mj-image padding=\"0px\" width=\"{width}px\" rel=\"{settings.key}\" src=\"{settings.src}\" alt=\"{placeholder.alt}\" align=\"center\" border=\"none\"></mj-image>\n  </mj-column>\n</mj-section>";
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    // console.log("mailBuilder - image - settings.src", settings.src)
    var translations = utils_1.getTranslations({
        placeholder: placeholder,
        settings: settings
    });
    var html = "";
    var text = "";
    var width = settings.width ? settings.width : 600;
    // const height: number = settings.height ? settings.height : 320
    // if (settings.src) {
    text = "" + config_1.textBreak;
    html = templateReplacer_1.default({ html: exports.mjmlImage, settings: settings, placeholder: placeholder, width: width });
    // console.log("html: ", html)
    testParse_1.default({ test: true, html: html, options: options, modulName: "image" });
    // }
    return {
        html: html,
        text: text,
        translations: translations,
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=image.js.map