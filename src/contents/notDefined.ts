import CONSTANTS from "../constants"
import { createLink } from "../compose"

export default async function(
  options: MailBuilder.BuildOptions
): Promise<MailBuilder.MailContentsOutput> {
  const subject: string = "UNDEFINED CONTENT TYPE"
  const translations: string[] = []

  return {
    subject: subject,
    text: subject,
    html: subject,
    translations: translations,
    links: [],
    options: options
  }
}
