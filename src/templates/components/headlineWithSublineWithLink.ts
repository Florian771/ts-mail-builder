import testParse from "../../testParse"
import { textBreak } from "../../config"
import { getTranslations } from "../../utils"

export interface HeadlineWithSublineWithLinkSettings {
  key: string
}

export interface HeadlineWithSublineWithLink {
  headline: string
  subline: string
  link: string
  linkText: string
}
const placeholder: HeadlineWithSublineWithLink = {
  headline: "{HEADLINE_KEY}",
  subline: "{SUBLINE_KEY}",
  link: "{LINK_KEY}",
  linkText: "{LINK_TEXT_KEY}"
}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: HeadlineWithSublineWithLinkSettings
}): MailBuilder.Component => {
  const translations: string[] = getTranslations({
    placeholder: placeholder,
    settings: settings
  })
  let html: string = ""
  let text: string = ""
  let font: string = "Montserrat, Helvetica, Arial, sans-serif"
  if (
    options &&
    options.designSettings &&
    options.designSettings.headlineFontName
  ) {
    font = options.designSettings.headlineFontName
  }
  text = `${placeholder.headline.replace("KEY", settings.key)}${textBreak}`
  html = `<mj-section padding="20px 20px 0 20px" background-color="{backgroundColor2}">
  <mj-column>
    <mj-text font-family="${font}" align="center" font-weight="300" padding="30px 40px 0px 40px" font-size="32px" line-height="40px" color="{textColor}">
      ${placeholder.headline.replace("KEY", settings.key)}
    </mj-text>
    <mj-text align="center" font-weight="100" padding="0px 40px 0px 40px" font-size="24px" line-height="40px" color="{textColor}">
      ${placeholder.subline.replace("KEY", settings.key)}
    </mj-text>
    <mj-text align="center" font-weight="100" padding="0px 40px 20px 40px" font-size="16px" line-height="40px" color="{textColor}">
      <a href="${placeholder.link.replace(
        "KEY",
        settings.key
      )}" target="_blank">${placeholder.linkText.replace(
    "KEY",
    settings.key
  )}</a>
    </mj-text>
  </mj-column>
</mj-section>`

  testParse({ test: true, html: html, options: options, modulName: "headline" })
  return {
    html: html,
    text: text,
    translations: translations,
    parse: (args: any) => testParse(args)
  }
}
