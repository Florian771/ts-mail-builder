"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
var config_1 = require("../../config");
var utils_1 = require("../../utils");
var placeholder = {
    headline: "{HEADLINE_KEY}",
    subline: "{SUBLINE_KEY}",
    link: "{LINK_KEY}",
    linkText: "{LINK_TEXT_KEY}"
};
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    var translations = utils_1.getTranslations({
        placeholder: placeholder,
        settings: settings
    });
    var html = "";
    var text = "";
    var font = "Montserrat, Helvetica, Arial, sans-serif";
    if (options &&
        options.designSettings &&
        options.designSettings.headlineFontName) {
        font = options.designSettings.headlineFontName;
    }
    text = "" + placeholder.headline.replace("KEY", settings.key) + config_1.textBreak;
    html = "<mj-section padding=\"20px 20px 0 20px\" background-color=\"{backgroundColor2}\">\n  <mj-column>\n    <mj-text font-family=\"" + font + "\" align=\"center\" font-weight=\"300\" padding=\"30px 40px 0px 40px\" font-size=\"32px\" line-height=\"40px\" color=\"{textColor}\">\n      " + placeholder.headline.replace("KEY", settings.key) + "\n    </mj-text>\n    <mj-text align=\"center\" font-weight=\"100\" padding=\"0px 40px 0px 40px\" font-size=\"24px\" line-height=\"40px\" color=\"{textColor}\">\n      " + placeholder.subline.replace("KEY", settings.key) + "\n    </mj-text>\n    <mj-text align=\"center\" font-weight=\"100\" padding=\"0px 40px 20px 40px\" font-size=\"16px\" line-height=\"40px\" color=\"{textColor}\">\n      <a href=\"" + placeholder.link.replace("KEY", settings.key) + "\" target=\"_blank\">" + placeholder.linkText.replace("KEY", settings.key) + "</a>\n    </mj-text>\n  </mj-column>\n</mj-section>";
    testParse_1.default({ test: true, html: html, options: options, modulName: "headline" });
    return {
        html: html,
        text: text,
        translations: translations,
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=headlineWithSublineWithLink.js.map