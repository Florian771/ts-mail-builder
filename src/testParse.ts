import * as mjml2html from "mjml"
// const mjml2html = require("mjml").default
import replacerDesign from "./replacerDesign"
import { replaceTextAndHtml } from "./replacerContent"
import imageText from "./templates/components/imageText"
import CONSTANTS from "./constants"
import accordeon from "./templates/components/accordeon"
import headOnly from "./templates/components/headOnly"

const startMarkerHeader: string = "{HEADER_START}"
const endMarkerHeader: string = "{HEADER_END}"
const startMarker: string = "{CONTENT_START}"
const endMarker: string = "{CONTENT_END}"

function wrapHtmlToTestParse(html, options, modulName, test) {
  let content_start: string = ""
  let content_end: string = ""
  console.log("wrapHtmlToTestParse - modulName:", modulName)
  console.log(
    "wrapHtmlToTestParse - options.getSingleComponent",
    options.getSingleComponent
  )
  if (options.getSingleComponent) {
    console.log("testParse - getSingleComponent - add mj-raw")
    content_start = "<mj-raw>" + startMarker + "</mj-raw>"
    content_end = "<mj-raw>" + endMarker + "</mj-raw>"
  }
  if (modulName === "headOnly") {
    // PARSE HEAD ONLY WITH ELEMENTS, WHICH NEED ADDITIONAL HEAD STYLES
    console.log("testParse - headOnly")
    return `<mjml>
    ${html}
    <mj-body>
    ${
      accordeon({
        options: options,
        settings: {
          key: "ACD",
          items: 1
        }
      }).html
    }
    ${
      imageText({
        options: options,
        settings: {
          key: "TEST_IMAGE_TEXT",
          src:
            "https://via.placeholder.com/600x250/000000/FFFFFF?text={124x119}",
          width: 124,
          height: 119
        }
      }).html
    }
    </mj-body>
    </mjml>`
    // } else if (modulName.toLowerCase() === "carouseldesigner" && !test) {
  } else if (!test && options.getSingleComponent) {
    // PARSE HEAD ONLY WITH ELEMENTS, WHICH NEED ADDITIONAL HEAD STYLES
    console.log("parse with head")
    return `<mjml>
    ${
      headOnly({
        options: options,
        settings: options.sections[0].settings
      }).html
    }
    <mj-body>
    ${content_start}
    ${html}
    ${content_end}
    </mj-body>
    </mjml>`
  } else {
    console.log("testParse - normal")
    return `<mjml><mj-head>
  <mj-title></mj-title></mj-head><mj-body>
  ${content_start}
  ${html}
  ${content_end}
  </mj-body>
  </mjml>`
  }
}

export default function({
  html,
  options,
  modulName,
  test
}: {
  html: string
  options: MailBuilder.BuildOptions
  modulName: string
  test: boolean
}): MailBuilder.Template {
  let res: MailBuilder.Template
  // console.log("testParser - modulName: ", modulName)
  try {
    if (options.testParse) {
      /* if (modulName === "headline") {
        console.log("2.testParser - html:", html)
      } */
      let updatedHtml: string = wrapHtmlToTestParse(
        html,
        options,
        modulName,
        test
      )

      updatedHtml = replacerDesign({
        html: updatedHtml,
        designSettings: options.designSettings
      })

      updatedHtml = replaceTextAndHtml({
        mail: { subject: "", from: "", to: "", html: updatedHtml, text: "" },
        from: "{urlWeb}",
        to: CONSTANTS.web.urlWeb
      }).html

      if (modulName.toLowerCase() === "hero") {
        // if (modulName.toLowerCase() === "carouseldesigner") {
        // if (options.getSingleComponent) {
        console.log("testParser - updatedHtml:", updatedHtml)
      }

      const mjml2htmlOptions: any = {}
      res = mjml2html(updatedHtml, mjml2htmlOptions)

      if (
        modulName === "headOnly" ||
        // modulName.toLowerCase() === "carouseldesigner"
        (!test && options.getSingleComponent)
      ) {
        res.html = res.html.replace("<head>", "<head>" + startMarkerHeader)
        res.html = res.html.replace("</head>", endMarkerHeader + "</head>")
      }

      if (res.errors && res.errors.length > 0) {
        throw {
          code: 0,
          message: res.errors
        }
      }
    }
    return res
  } catch (err) {
    console.log("testParser - err", err.message)
    throw err
    return err
  }
}
