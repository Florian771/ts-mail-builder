export default `<API>
  <HotelData>
    <Ort>Grödig</Ort>
    <HotelName>Musterhotel Saalbacher Hof</HotelName>
    <PersName>Demo</PersName>
    <Strasse>Friedensstr. 8</Strasse>
    <Gruss></Gruss>
    <PLZ>5082</PLZ>
    <Telefon>0043 6246 73873</Telefon>
    <Fax>0043 6246 73873 42</Fax>
    <UID>ATU44806304</UID>
    <Nation>A</Nation>
    <KtoNr>12345679</KtoNr>
    <Bank>Volksbank</Bank>
    <BLZ>45010</BLZ>
    <Email>office@gastrodat.com</Email>
    <BIC>VBOEATWWSAL</BIC>
    <IBAN>AT123456789</IBAN>
  </HotelData>
  <ReservationData>
    <LFNUMMER>209</LFNUMMER>
    <ZIMMER_NR>6</ZIMMER_NR>
    <PERS_NR>4</PERS_NR>
    <PERSON>Huber Martin</PERSON>
    <VON>05.03.2018</VON>
    <BIS>10.03.2018</BIS>
    <ANGEBOTSURL><![CDATA[https://apps.weratech-online.com/wtOnlineBooking/Home/CreateOnlineReservation/NTMwMg2?reservationid=546051006030&lang=DE]]></ANGEBOTSURL>
  </ReservationData>
  <GuestData>
    <LFNUMMER>4</LFNUMMER>
    <ANREDE_H>Herrn</ANREDE_H>
    <FAMNAME>Huber</FAMNAME>
    <VORNAME>Martin</VORNAME>
    <TITEL></TITEL>
    <GESCHLECHT>M</GESCHLECHT>
    <STRASSE>Salzgasse 10</STRASSE>
    <PLZ>4020</PLZ>
    <ORT>Linz</ORT>
    <ANREDE_FAM>Sehr geehrter Herr Huber</ANREDE_FAM>
    <SPRACHE>D Deutsch</SPRACHE>
    <NATION>A</NATION>
    <ZUSNAME></ZUSNAME>
    <P_EMAIL>huber@aon.at</P_EMAIL>
    <NATION_TXT></NATION_TXT>
  </GuestData>
  <RoomData>
    <ZIMMER_NR>6</ZIMMER_NR>
    <ZIMMER_NAME>006</ZIMMER_NAME>
    <GEEIGNET_VON>2</GEEIGNET_VON>
    <GEEIGNET_BIS>4</GEEIGNET_BIS>
    <ZI_KAT>1</ZI_KAT>
    <ZIMMER_SAUBER>N</ZIMMER_SAUBER>
    <GEEIGNET_STD>2</GEEIGNET_STD>
    <STANDARDLEISTUNG>10019,</STANDARDLEISTUNG>
    <TEXT1></TEXT1>
    <TEXT2></TEXT2>
    <ZIMMERBILD>https://www.weratech-files.com/images/280942/gastrodat/Zimmer/Zimmer_Bild_6.jpg?timestamp=636439197780000000</ZIMMERBILD>
    <ZIMMERGRUNDRISS>https://www.weratech-files.com/images/280942/gastrodat/Zimmer/Grundriss/ZIMMER_GRUNDRISS_6.jpg?timestamp=636439197790000000</ZIMMERGRUNDRISS>
  </RoomData>
  <ExtrasData />
  <ThemeData>
    <Themes>
      <gdatTheme>
        <theme_pics>
          <gdatPicture>
            <PicId>1e7873dd5a439</PicId>
            <PicName>SKI EINFACH &lt;n&gt;ZUM&lt;/n&gt; GENIESSEN</PicName>
            <PicPath>$i_offer\\theme_Winter und Herbstsaisonen\\1e7873dd5a439.jpg</PicPath>
            <PicUrl>https://touristscout.com/demo/assets/images/header/1.jpg</PicUrl>
            <PicHTML_Link>https://www.gastrodat.com</PicHTML_Link>
            <PicKategorie>Skifahren</PicKategorie>
          </gdatPicture>
          <gdatPicture>
            <PicId>1e787497c6870</PicId>
            <PicName>EINFACH WOHLFÜHLEN</PicName>
            <PicPath>$i_offer\\theme_Winter und Herbstsaisonen\\1e787497c6870.jpg</PicPath>
            <PicUrl>https://touristscout.com/demo/assets/images/header/2.jpg</PicUrl>
            <PicHTML_Link />
            <PicKategorie>Wohnen</PicKategorie>
          </gdatPicture>
          <gdatPicture>
            <PicId>1ed2dc77b9e5d</PicId>
            <PicName>Direkt an der SKIPISTE</PicName>
            <PicPath>$i_offer\\theme_Wintersaisonen\\1ed2dc77b9e5d.jpg</PicPath>
            <PicUrl>https://touristscout.com/demo/assets/images/header/3.jpg</PicUrl>
            <PicHTML_Link />
            <PicKategorie>Skifahren</PicKategorie>
          </gdatPicture>
          <gdatPicture>
            <PicId>2056550e06e72</PicId>
            <PicName>Tradition</PicName>
            <PicPath>$i_offer\\theme_Winter und Herbstsaisonen\\1e7873dd5a439.jpg</PicPath>
            <PicUrl>https://touristscout.com/demo/assets/images/header/4.jpg</PicUrl>
            <PicHTML_Link />
            <PicKategorie>Tradition</PicKategorie>
          </gdatPicture>
          <gdatPicture>
            <PicId>1e7873dd5a439</PicId>
            <PicName>Gemütlichkeit</PicName>
            <PicPath>$i_offer\\theme_Winter und Herbstsaisonen\\1e7873dd5a439.jpg</PicPath>
            <PicUrl>https://touristscout.com/demo/assets/images/header/5.jpg</PicUrl>
            <PicHTML_Link />
            <PicKategorie />
          </gdatPicture>
          <gdatPicture>
            <PicId>206fb66441bb7</PicId>
            <PicName>Wellness nach dem Sporteln....</PicName>
            <PicPath>$i_offer\\theme_Winter und Herbstsaisonen\\206fb66441bb7.JPG</PicPath>
            <PicUrl>https://touristscout.com/demo/assets/images/header/6.jpg</PicUrl>
            <PicHTML_Link />
            <PicKategorie />
          </gdatPicture>
          <gdatPicture>
            <PicId>1e85a6bb31cfe</PicId>
            <PicName>Sommer im Zillertal </PicName>
            <PicPath>$i_offer\\theme_Winter und Herbstsaisonen\\1e85a6bb31cfe.jpg</PicPath>
            <PicUrl>https://touristscout.com/demo/assets/images/header/7.jpg</PicUrl>
            <PicHTML_Link />
            <PicKategorie />
          </gdatPicture>
        </theme_pics>
        <theme_teasers>
          <gdatTeaser>
            <Teaser_pic>
              <PicId>1e85a6bb31cfe</PicId>
              <PicName>Inmitten von Weinbergen in Lermoos in Tirol</PicName>
              <PicPath>$i_offer\\teaser_DURCHATMEN\\1e85a6bb31cfe.jpg</PicPath>
              <PicUrl>https://touristscout.com/demo/assets/images/header/8.jpg</PicUrl>
              <PicHTML_Link>https://www.gastrodat.com</PicHTML_Link>
              <PicKategorie />
            </Teaser_pic>
            <validFrom>2017-09-13T11:16:42.8311806+02:00</validFrom>
            <validTo>2017-12-13T11:16:42.8311806+01:00</validTo>
            <SuchwortListe />
            <TeaserId>1e85a6bb31cfe</TeaserId>
            <TeaserName>DURCHATMEN</TeaserName>
          </gdatTeaser>
          <gdatTeaser>
            <Teaser_pic>
              <PicId>2056610d032af</PicId>
              <PicName>Drei Generationen unter einem Dach. Da prallen hin und wieder Welten aufeinander. Nun liegt eine gewisse Sturheit zweifelsohne im Tiroler Naturell. Ganz zu schweigen vom kämpferischen Temperament ...</PicName>
              <PicPath>$i_offer\\teaser_neuer ThemenTeaser 2056610d032af\\2056610d032af.jpg</PicPath>
              <PicUrl>https://touristscout.com/demo/assets/images/header/9.jpg</PicUrl>
              <PicHTML_Link>https://www.perauer.at/de/diskussion-auf-tirolerisch.html</PicHTML_Link>
              <PicKategorie />
            </Teaser_pic>
            <validFrom>2017-10-20T10:23:08.0304303+02:00</validFrom>
            <validTo>2018-01-20T10:23:08.0304303+01:00</validTo>
            <SuchwortListe />
            <TeaserId>2056610d032af</TeaserId>
            <TeaserName>Diskussion auf Tirolerisch</TeaserName>
          </gdatTeaser>
          <gdatTeaser>
            <Teaser_pic>
              <PicId>2056677431490</PicId>
              <PicName>Die Jagd hat bei den Perauers lange Tradition. Von jeher beschert sie Vater und Sohn ein verbindendes Erlebnis im heimischen Wald ...</PicName>
              <PicPath>$i_offer\\teaser_neuer ThemenTeaser 2056677431490\\2056677431490.jpg</PicPath>
              <PicUrl>https://touristscout.com/demo/assets/images/header/10.jpg</PicUrl>
              <PicHTML_Link />
              <PicKategorie />
            </Teaser_pic>
            <validFrom>2017-11-20T10:25:59</validFrom>
            <validTo>2017-11-30T10:25:59</validTo>
            <SuchwortListe />
            <TeaserId>2056677431490</TeaserId>
            <TeaserName>Wildwochen</TeaserName>
          </gdatTeaser>
        </theme_teasers>
        <validFrom>2017-06-11T15:09:55.5028897+02:00</validFrom>
        <validTo>2017-12-20T15:09:55</validTo>
        <SuchwortListe />
        <ThemeId>5</ThemeId>
        <ThemeName>Winter und Herbstsaisonen</ThemeName>
      </gdatTheme>
    </Themes>
    <BilderGalerie_pics>
      <gdatPicture>
        <PicId>1f845a7170af8</PicId>
        <PicName>Hot Stone</PicName>
        <PicPath>$i_offer\\impressions\\1f845a7170af8.jpg</PicPath>
        <PicUrl>https://touristscout.com/demo/assets/images/header/11.jpg</PicUrl>
        <PicHTML_Link />
        <PicKategorie>Wellness</PicKategorie>
      </gdatPicture>
      <gdatPicture>
        <PicId>2056bdf42f2c5</PicId>
        <PicName>Gemütlichkeit</PicName>
        <PicPath>$i_offer\\impressions\\2056bdf42f2c5.jpg</PicPath>
        <PicUrl>https://touristscout.com/demo/assets/images/header/12.jpg</PicUrl>
        <PicHTML_Link />
        <PicKategorie>Wohnen</PicKategorie>
      </gdatPicture>
      <gdatPicture>
        <PicId>2056d314762f3</PicId>
        <PicName>Sandra</PicName>
        <PicPath>$i_offer\\impressions\\2056d314762f3.jpg</PicPath>
        <PicUrl>https://touristscout.com/demo/assets/images/header/13.jpg</PicUrl>
        <PicHTML_Link />
        <PicKategorie>Team</PicKategorie>
      </gdatPicture>
      <gdatPicture>
        <PicId>2056d41b58277</PicId>
        <PicName>Monika</PicName>
        <PicPath>$i_offer\\impressions\\2056d41b58277.jpg</PicPath>
        <PicUrl>https://touristscout.com/demo/assets/images/header/14.jpg</PicUrl>
        <PicHTML_Link />
        <PicKategorie>Team</PicKategorie>
      </gdatPicture>
      <gdatPicture>
        <PicId>2105346b0664f</PicId>
        <PicName>saalbacherhof</PicName>
        <PicPath>$i_offer\\impressions\\2105346b0664f.jpg</PicPath>
        <PicUrl>https://www.weratech-files.com/images/280942/gastrodat/2105346b0664f.jpg</PicUrl>
        <PicHTML_Link />
        <PicKategorie>Wohnen</PicKategorie>
      </gdatPicture>
    </BilderGalerie_pics>
    <impression_pics>
      <gdatPicture>
        <PicId>1e7873dd5a439</PicId>
        <PicName>SKI EINFACH &lt;n&gt;ZUM&lt;/n&gt; GENIESSEN</PicName>
        <PicPath>$i_offer\\theme_Winter und Herbstsaisonen\\1e7873dd5a439.jpg</PicPath>
        <PicUrl>https://www.weratech-files.com/images/280942/gastrodat/1e7873dd5a439.jpg</PicUrl>
        <PicHTML_Link>https://www.gastrodat.com</PicHTML_Link>
        <PicKategorie>Skifahren</PicKategorie>
      </gdatPicture>
      <gdatPicture>
        <PicId>1e787497c6870</PicId>
        <PicName>EINFACH WOHLFÜHLEN</PicName>
        <PicPath>$i_offer\\theme_Winter und Herbstsaisonen\\1e787497c6870.jpg</PicPath>
        <PicUrl>https://www.weratech-files.com/images/280942/gastrodat/1e787497c6870.jpg</PicUrl>
        <PicHTML_Link />
        <PicKategorie>Wohnen</PicKategorie>
      </gdatPicture>
      <gdatPicture>
        <PicId>1ed2dc77b9e5d</PicId>
        <PicName>Direkt an der SKIPISTE</PicName>
        <PicPath>$i_offer\\theme_Wintersaisonen\\1ed2dc77b9e5d.jpg</PicPath>
        <PicUrl>https://www.weratech-files.com/images/280942/gastrodat/1ed2dc77b9e5d.jpg</PicUrl>
        <PicHTML_Link />
        <PicKategorie>Skifahren</PicKategorie>
      </gdatPicture>
      <gdatPicture>
        <PicId>2056550e06e72</PicId>
        <PicName>Tradition</PicName>
        <PicPath>$i_offer\\theme_Winter und Herbstsaisonen\\1e7873dd5a439.jpg</PicPath>
        <PicUrl>https://www.weratech-files.com/images/280942/gastrodat/1e7873dd5a439.jpg</PicUrl>
        <PicHTML_Link />
        <PicKategorie>Tradition</PicKategorie>
      </gdatPicture>
      <gdatPicture>
        <PicId>20566e9de74a1</PicId>
        <PicName>Wildwochen 1</PicName>
        <PicPath>$i_offer\\theme_Wildwochen\\20566e9de74a1.jpg</PicPath>
        <PicUrl>https://www.weratech-files.com/images/280942/gastrodat/20566e9de74a1.jpg</PicUrl>
        <PicHTML_Link />
        <PicKategorie>Tradition</PicKategorie>
      </gdatPicture>
      <gdatPicture>
        <PicId>20566f719bac7</PicId>
        <PicName>Wildwochen 2</PicName>
        <PicPath>$i_offer\\theme_Wildwochen\\20566f719bac7.jpg</PicPath>
        <PicUrl>https://www.weratech-files.com/images/280942/gastrodat/20566f719bac7.jpg</PicUrl>
        <PicHTML_Link />
        <PicKategorie>Tradition</PicKategorie>
      </gdatPicture>
      <gdatPicture>
        <PicId>1f845a7170af8</PicId>
        <PicName>Hot Stone</PicName>
        <PicPath>$i_offer\\impressions\\1f845a7170af8.jpg</PicPath>
        <PicUrl>https://www.weratech-files.com/images/280942/gastrodat/1f845a7170af8.jpg</PicUrl>
        <PicHTML_Link />
        <PicKategorie>Wellness</PicKategorie>
      </gdatPicture>
      <gdatPicture>
        <PicId>2056bdf42f2c5</PicId>
        <PicName>Gemütlichkeit</PicName>
        <PicPath>$i_offer\\impressions\\2056bdf42f2c5.jpg</PicPath>
        <PicUrl>https://www.weratech-files.com/images/280942/gastrodat/2056bdf42f2c5.jpg</PicUrl>
        <PicHTML_Link />
        <PicKategorie>Wohnen</PicKategorie>
      </gdatPicture>
      <gdatPicture>
        <PicId>2056d314762f3</PicId>
        <PicName>Sandra</PicName>
        <PicPath>$i_offer\\impressions\\2056d314762f3.jpg</PicPath>
        <PicUrl>https://www.weratech-files.com/images/280942/gastrodat/2056d314762f3.jpg</PicUrl>
        <PicHTML_Link />
        <PicKategorie>Team</PicKategorie>
      </gdatPicture>
      <gdatPicture>
        <PicId>2056d41b58277</PicId>
        <PicName>Monika</PicName>
        <PicPath>$i_offer\\impressions\\2056d41b58277.jpg</PicPath>
        <PicUrl>https://www.weratech-files.com/images/280942/gastrodat/2056d41b58277.jpg</PicUrl>
        <PicHTML_Link />
        <PicKategorie>Team</PicKategorie>
      </gdatPicture>
      <gdatPicture>
        <PicId>2105346b0664f</PicId>
        <PicName>saalbacherhof</PicName>
        <PicPath>$i_offer\\impressions\\2105346b0664f.jpg</PicPath>
        <PicUrl>https://www.weratech-files.com/images/280942/gastrodat/2105346b0664f.jpg</PicUrl>
        <PicHTML_Link />
        <PicKategorie>Wohnen</PicKategorie>
      </gdatPicture>
    </impression_pics>
    <AngebotsName>lalala</AngebotsName>
    <StammDaten>
      <HotelLogo_pic>
        <PicId>1ecfce182b605</PicId>
        <PicName>HotelLogo</PicName>
        <PicPath>$i_offer\\LOGO\\StammDaten_HotelLogo_pic.jpg</PicPath>
        <PicUrl>https://touristscout.com/demo/assets/images/logo.png</PicUrl>
        <PicHTML_Link>https://touristscout.com/demo</PicHTML_Link>
      </HotelLogo_pic>
      <Section_Liste>
        <Section>
          <ModulName>SLIDESHOW</ModulName>
          <ModulId />
        </Section>
        <Section>
          <ModulName>WELCOME_TEXT</ModulName>
          <ModulId />
        </Section>
        <Section>
          <ModulName>ZUSATZ_TEXT3</ModulName>
          <ModulId />
        </Section>
        <Section>
          <ModulName>ZUSATZ_TEXT1</ModulName>
          <ModulId />
        </Section>
        <Section>
          <ModulName>ZUSATZ_TEXT4</ModulName>
          <ModulId />
        </Section>
        <Section>
          <ModulName>ZUSATZ_TEXT5</ModulName>
          <ModulId />
        </Section>
        <Section>
          <ModulName>TEASER_BLOCK</ModulName>
          <ModulId />
        </Section>
        <Section>
          <ModulName>ANGEBOTS_TEXT</ModulName>
          <ModulId />
        </Section>
        <Section>
          <ModulName>VARIANTEN_VORSCHAU</ModulName>
          <ModulId />
        </Section>
        <Section>
          <ModulName>ANGEBOT_DETAILS</ModulName>
          <ModulId />
        </Section>
        <Section>
          <ModulName>IMPRESSIONEN</ModulName>
          <ModulId />
        </Section>
        <Section>
          <ModulName>ANREISE_BESCHREIBUNG</ModulName>
          <ModulId />
        </Section>
        <Section>
          <ModulName>NACHRICHTEN_BLOCK</ModulName>
          <ModulId />
        </Section>
        <Section>
          <ModulName>IMPRESSUM</ModulName>
          <ModulId />
        </Section>
      </Section_Liste>
      <EinzelText_Liste>
        <EinzelText>
          <Tag />
          <Text>Text zu </Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_chatten</Tag>
          <Text>Text zu gdat_chatten</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_details</Tag>
          <Text>Details</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_variante</Tag>
          <Text>Variante</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_angebot</Tag>
          <Text>Angebot</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_jetzt_buchen</Tag>
          <Text>Jetzt buchen</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_personen</Tag>
          <Text>Personen</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_datum</Tag>
          <Text>Datum</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_naechte</Tag>
          <Text>Nächte</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_preis</Tag>
          <Text>Preis</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_gesamt</Tag>
          <Text>GESAMT</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_impressionen</Tag>
          <Text>IMPRESSIONEN</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_machen_sie_sich_ein_bild</Tag>
          <Text>MACHEN SIE SICH EIN BILD</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_kontakt</Tag>
          <Text>KONTAKT</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_wir_freuen_uns_von_ihnen_zu_hoeren</Tag>
          <Text>Wir freuen uns von Ihnen zu hören</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_telefon</Tag>
          <Text>Telefon</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_email</Tag>
          <Text>E-Mail</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_adresse</Tag>
          <Text>Addresse</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_karte_anzeigen</Tag>
          <Text>Karte anzeigen</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_route_berechnen</Tag>
          <Text>Route berechnen</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_name</Tag>
          <Text>Name</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_nachricht</Tag>
          <Text>Nachricht</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_input:name_hinweis</Tag>
          <Text>Bitte geben Sie Ihren Namen an</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_input:email_hinweis</Tag>
          <Text>Bitte geben Sie Ihre E-Mail an</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_textarea_nachricht_hinweis</Tag>
          <Text>Bitte geben Sie Ihren Namen an</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_button_nachricht_senden</Tag>
          <Text>Nachricht senden</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_page_loading_error</Tag>
          <Text>Hoppla! Die von Ihnen gesuchte Seite ist zur Zeit nicht verfügbar! Bitte laden Sie es neu, versuchen Sie es später oder melden Sie es. Wir werden es so schnell wie möglich beheben.</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_reload</Tag>
          <Text>Neu laden</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_report</Tag>
          <Text>Melde dies</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_newsletter</Tag>
          <Text>Newsletter</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_input_newsletter_anmelden</Tag>
          <Text>Kostenlos anmelden</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_zum_seitenanfang</Tag>
          <Text>Zum Seitenanfang</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_please_choose</Tag>
          <Text>Bitte wählen</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_highlights</Tag>
          <Text>Highlights</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_angebot_ansehen</Tag>
          <Text>Angebot ansehen</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gat_anreise_abreise</Tag>
          <Text>Anreise/Abreise</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_preis_pro_person_und_nacht</Tag>
          <Text>Preis pro Tag/Pers,</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_jetzt_anfragen</Tag>
          <Text>jetzt anfragen</Text>
        </EinzelText>
        <EinzelText>
          <Tag>gdat_weitere_bilder</Tag>
          <Text>weitere Bilder</Text>
        </EinzelText>
      </EinzelText_Liste>
      <HotelName>Ihr Muster-Hotel Angebot</HotelName>
      <WellcomeText>&lt;?xml version="1.0" ?&gt;
&lt;html xmlns="https://www.w3.org/1999/xhtml"&gt;
&lt;head&gt;
&lt;meta content="TX24_HTM 24.0.610.500" name="GENERATOR" /&gt;
&lt;title&gt;&lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;h1 style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;Ihr Urlaubsangebot: &lt;/h1&gt;
&lt;p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;Die feine Küche unseres urigen Gasthofs ist in aller Munde. Denn nicht nur Gäste, sondern auch viele Einheimische lassen sich die köstlichen uunseres Küchenteams genüsslich auf der Zunge zergehen. &lt;br /&gt;
&lt;br /&gt;
Wer einmal hier war, kommt immer wieder. Und wer gerade nicht hier ist, erzählt hoffentlich überall herum, wie gut es bei uns schmeckt :-) Der Name Perauer steht für Qualität. Das gilt fürs Lachsfilet im Gemüsebett genauso wie für den Gast im Kuschelbett ...&lt;/p&gt;
&lt;p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt; &lt;/p&gt;
&lt;p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;Mit lieben Grüßen aus Grödig und bis bald!&lt;/p&gt;
&lt;p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;Sabine, Alois, Gerline und das - ReceptionsTeam&lt;br /&gt;
Ihre Familie Maier und das Musterhotel Team&lt;/p&gt;
&lt;/body&gt;
&lt;/html&gt;</WellcomeText>
      <SubDomainUrl>.https://www.touristscout.com</SubDomainUrl>
      <AngebotsText>&lt;?xml version="1.0" ?&gt;
&lt;html xmlns="https://www.w3.org/1999/xhtml"&gt;
&lt;head&gt;
&lt;meta content="TX24_HTM 24.0.610.500" name="GENERATOR" /&gt;
&lt;title&gt;&lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;p lang="de-AT" class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;das ist der mitteltext des angebotes&lt;/p&gt;
&lt;/body&gt;
&lt;/html&gt;</AngebotsText>
      <AnreiseInfoText>&lt;?xml version="1.0" ?&gt;
&lt;html xmlns="https://www.w3.org/1999/xhtml"&gt;
&lt;head&gt;
&lt;meta content="TX24_HTM 24.0.610.500" name="GENERATOR" /&gt;
&lt;title&gt;&lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;EINCHECKEN&lt;/p&gt;
&lt;p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;MO-FR: BIS 11 UHR&lt;/p&gt;
&lt;p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;SA: BIS 17:30 UHR&lt;/p&gt;
&lt;/body&gt;
&lt;/html&gt;</AnreiseInfoText>
      <AbreiseInfoText>&lt;?xml version="1.0" ?&gt;
&lt;html xmlns="https://www.w3.org/1999/xhtml"&gt;
&lt;head&gt;
&lt;meta content="TX24_HTM 24.0.610.500" name="GENERATOR" /&gt;
&lt;title&gt;&lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;AUSCHECKEN&lt;/p&gt;
&lt;p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;MO-FR: BIS 10 UHR&lt;/p&gt;
&lt;p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;SA: BIS 12 UHR&lt;/p&gt;
&lt;/body&gt;
&lt;/html&gt;</AbreiseInfoText>
      <ZusatzText1>Text 1</ZusatzText1>
      <ZusatzText1_Header></ZusatzText1_Header>
      <ZusatzText1_Collapsed>false</ZusatzText1_Collapsed>
      <ZusatzText2 />
      <ZusatzText2_Header />
      <ZusatzText2_Collapsed>true</ZusatzText2_Collapsed>
      <ZusatzText3>&lt;?xml version="1.0" ?&gt;
&lt;html xmlns="https://www.w3.org/1999/xhtml"&gt;
&lt;head&gt;
&lt;meta content="TX24_HTM 24.0.610.500" name="GENERATOR" /&gt;
&lt;title&gt;&lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;p lang="de-AT" class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;das ist der zusatztext3&lt;/p&gt;
&lt;p lang="de-AT" class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt; &lt;/p&gt;
&lt;h1 lang="de-AT" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;header1&lt;/h1&gt;
&lt;p lang="de-AT" class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt; &lt;/p&gt;
&lt;h3 lang="de-AT" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;header3&lt;/h3&gt;
&lt;p lang="de-AT" class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt; &lt;/p&gt;
&lt;ul style="margin:0pt;padding:0pt;list-style-type:disc;"&gt;
&lt;li style="font-size:12pt;margin-left:18pt;"&gt;
&lt;p class="body-text" style="margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;aufz 1&lt;/p&gt;
&lt;/li&gt;
&lt;li style="list-style-type:disc;font-size:12pt;margin-left:18pt;"&gt;
&lt;p class="body-text" style="margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;aufz 2&lt;/p&gt;
&lt;/li&gt;
&lt;/ul&gt;
&lt;p lang="de-AT" class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt; &lt;/p&gt;
&lt;p lang="de-AT" class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;p-text klh kl lkj lkj lkj lkj lkj &lt;span style="font-weight:bold;text-decoration:underline ;"&gt;inline fett&lt;/span&gt; lalalaall&lt;/p&gt;
&lt;/body&gt;
&lt;/html&gt;</ZusatzText3>
      <ZusatzText3_Header>Header 3</ZusatzText3_Header>
      <ZusatzText3_Collapsed>true</ZusatzText3_Collapsed>
      <ZusatzText4>Text 4</ZusatzText4>
      <ZusatzText4_Header>Header 4</ZusatzText4_Header>
      <ZusatzText4_Collapsed>false</ZusatzText4_Collapsed>
      <ZusatzText5>Text 5</ZusatzText5>
      <ZusatzText5_Header>Header 5</ZusatzText5_Header>
      <ZusatzText5_Collapsed>true</ZusatzText5_Collapsed>
      <FaceBookPageId>GDAT_Facebook_Page_ID</FaceBookPageId>
      <SozMedia_FaceBook_Url>www.facebook.com/PerauerHotelRestaurant</SozMedia_FaceBook_Url>
      <SozMedia_GooglePlus_Url>www.googleplus.....</SozMedia_GooglePlus_Url>
      <SozMedia_Pinterest_Url>www.pinterest.....</SozMedia_Pinterest_Url>
      <SozMedia_Instagram_Url>www.instagramm.....</SozMedia_Instagram_Url>
      <SozMedia_YouTube_Url>www.youtube.com/user/perauer854/videos</SozMedia_YouTube_Url>
      <SozMedia_Twitter_Url>www.twitter....</SozMedia_Twitter_Url>
      <ImageCompressionQuality>50</ImageCompressionQuality>
    </StammDaten>
    <DesignSettings>
      <TextModulStyle_Liste>
        <TextModulStyle>
          <StyleName>H1</StyleName>
          <Bold>true</Bold>
          <FontName>Arial</FontName>
          <FontSize>480</FontSize>
          <ForeColor>#008000</ForeColor>
          <Italic>false</Italic>
          <ListFormat>None</ListFormat>
          <Strikeout>false</Strikeout>
          <TextBackColor>#FFFFFF</TextBackColor>
          <Underline>false</Underline>
        </TextModulStyle>
        <TextModulStyle>
          <StyleName>H2</StyleName>
          <Bold>true</Bold>
          <FontName>Arial</FontName>
          <FontSize>360</FontSize>
          <ForeColor>#000000</ForeColor>
          <Italic>false</Italic>
          <ListFormat>None</ListFormat>
          <Strikeout>false</Strikeout>
          <TextBackColor>#FFFFFF</TextBackColor>
          <Underline>false</Underline>
        </TextModulStyle>
        <TextModulStyle>
          <StyleName>H3</StyleName>
          <Bold>true</Bold>
          <FontName>Arial</FontName>
          <FontSize>266</FontSize>
          <ForeColor>#000000</ForeColor>
          <Italic>false</Italic>
          <ListFormat>None</ListFormat>
          <Strikeout>false</Strikeout>
          <TextBackColor>#FFFFFF</TextBackColor>
          <Underline>false</Underline>
        </TextModulStyle>
        <TextModulStyle>
          <StyleName>H4</StyleName>
          <Bold>true</Bold>
          <FontName>Arial</FontName>
          <FontSize>220</FontSize>
          <ForeColor>#000000</ForeColor>
          <Italic>false</Italic>
          <ListFormat>None</ListFormat>
          <Strikeout>false</Strikeout>
          <TextBackColor>#FFFFFF</TextBackColor>
          <Underline>false</Underline>
        </TextModulStyle>
        <TextModulStyle>
          <StyleName>H5</StyleName>
          <Bold>true</Bold>
          <FontName>Arial</FontName>
          <FontSize>180</FontSize>
          <ForeColor>#000000</ForeColor>
          <Italic>false</Italic>
          <ListFormat>None</ListFormat>
          <Strikeout>false</Strikeout>
          <TextBackColor>#FFFFFF</TextBackColor>
          <Underline>false</Underline>
        </TextModulStyle>
        <TextModulStyle>
          <StyleName>P</StyleName>
          <Bold>false</Bold>
          <FontName>Arial</FontName>
          <FontSize>180</FontSize>
          <ForeColor>#000000</ForeColor>
          <Italic>false</Italic>
          <ListFormat>None</ListFormat>
          <Strikeout>false</Strikeout>
          <TextBackColor>#FFFFFF</TextBackColor>
          <Underline>false</Underline>
        </TextModulStyle>
      </TextModulStyle_Liste>
      <TemplateName />
      <primaryColor>#96b3b9</primaryColor>
      <secundaryColor>#777</secundaryColor>
      <textColor>#999</textColor>
      <backgroundColor>#fff</backgroundColor>
      <backgroundColor2>#efeceb</backgroundColor2>
      <headlineFontName>Aharoni</headlineFontName>
      <bodyFontName>Arial Black</bodyFontName>
    </DesignSettings>
  </ThemeData>
</API>`
