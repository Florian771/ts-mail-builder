exports.default = {
  API: {
    HotelData: {
      Ort: "6533 Fis",
      HotelName: "****Demohotel",
      PersName: "Familie Demo",
      Strasse: "Demoweg 26",
      Gruss: "",
      PLZ: "5020",
      Telefon: "+ 43 662 / 343 34",
      Fax: "+ 43 662 / 343 3434",
      UID: "AT023434230",
      Nation: "A",
      KtoNr: "4545-3434-8788",
      Email: "hotel@demohotel.at",
      Bank: "Raiffeisen",
      BLZ: "34899",
      BIC: "893-3663-5-766",
      IBAN: "4355-35353-7878-7878"
    },
    ReservationData: {
      LFNUMMER: "169943",
      ZIMMER_NR: "5",
      PERS_NR: "125646",
      PERSON: "Termin gerade reserviert ...",
      VON: "16.12.2017",
      BIS: "23.12.2017",
      ANGEBOTSURL: "",
      ANGEBOTSURLTEST: "https://www.google.at"
    },
    GuestData: {
      LFNUMMER: "125646",
      ANREDE_H: "Frau",
      FAMNAME: "Viktoria",
      VORNAME: "Hofmann",
      TITEL: "",
      GESCHLECHT: "W",
      STRASSE: "Scheibe 532",
      PLZ: "6543",
      ORT: "Nauders",
      ANREDE_FAM: "Liebe Viktoria Hofmann",
      SPRACHE: "D Deutsch",
      NATION: "A",
      ZUSNAME: "",
      P_EMAIL: "v.hofmann@test.at",
      NATION_TXT: ""
    },
    RoomData: {
      ZIMMER_NR: "5",
      ZIMMER_NAME: "005",
      GEEIGNET_VON: "1",
      GEEIGNET_BIS: "1",
      ZI_KAT: "2",
      ZIMMER_SAUBER: "N",
      GEEIGNET_STD: "1",
      STANDARDLEISTUNG: "10005,7,",
      TEXT1: "Doppelzimmer",
      TEXT2:
        "Doppelbett, Holzboden, Leder Couch, Balkon und Bad mit Badewanne & Dusche",
      ZIMMERBILD: "https://touristscout.com/demo/assets/images/header/14.jpg",
      ZIMMERGRUNDRISS:
        "https://touristscout.com/demo/assets/images/zimmer-grundriss.jpg",
      ZUSATZBILDER: [
        {
          ZIMMERBILD:
            "https://touristscout.com/demo/assets/images/header/11.jpg"
        },
        {
          ZIMMERBILD:
            "https://touristscout.com/demo/assets/images/header/13.jpg"
        },
        {
          ZIMMERBILD: "https://touristscout.com/demo/assets/images/header/2.jpg"
        },
        {
          ZIMMERBILD: "https://touristscout.com/demo/assets/images/header/1.jpg"
        }
      ]
    },
    ExtrasData: {
      Posio: [
        {
          FKT_NR: "649868",
          P_VON: "16.12.2017",
          P_BIS: "23.12.2017",
          P_TEXT: "Pers. im Doppelzimmer",
          P_ANZ: "2",
          P_SUM: "2212",
          P_PREIS: "158",
          P_RABATT: "0",
          P_KG_NR: "10005",
          GROUPINDEX: "1",
          DAUER_VON: "1",
          DAUER_BIS: "999",
          BESCHREIBUNG:
            "Leistungsbeschreibungs Text Information Leistungsbeschreibungs Text Information",
          EXTRABILD: "https://touristscout.com/demo/assets/images/header/1.jpg",
          ZUSATZBILDER: {
            EXTRABILD:
              "https://touristscout.com/demo/assets/images/header/2.jpg"
          }
        },
        {
          FKT_NR: "649870",
          P_VON: "16.12.2017",
          P_BIS: "23.12.2017",
          P_TEXT: "Kind 0 bis 5 Jahre",
          P_ANZ: "1",
          P_SUM: "399",
          P_PREIS: "57",
          P_RABATT: "0",
          P_KG_NR: "10025",
          GROUPINDEX: "1",
          DAUER_VON: "1",
          DAUER_BIS: "999",
          BESCHREIBUNG:
            "Leistungsbeschreibungs Text Information Leistungsbeschreibungs Text Information Leistungsbeschreibungs Text Information Leistungsbeschreibungs Text Information Leistungsbeschreibungs Text Information",
          EXTRABILD: "https://touristscout.com/demo/assets/images/header/14.jpg"
        },
        {
          FKT_NR: "649871",
          P_VON: "15.12.2017",
          P_BIS: "16.12.2017",
          P_TEXT: "ODER",
          P_ANZ: "0",
          P_SUM: "0",
          P_PREIS: "0",
          P_RABATT: "0",
          P_KG_NR: "0",
          GROUPINDEX: "-1"
        },
        {
          FKT_NR: "649872",
          P_VON: "16.12.2017",
          P_BIS: "17.12.2017",
          P_TEXT: "Pers. in Sternensuite",
          P_ANZ: "2",
          P_SUM: "2856",
          P_PREIS: "204",
          P_RABATT: "0",
          P_KG_NR: "34",
          GROUPINDEX: "2",
          DAUER_VON: "1",
          DAUER_BIS: "999",
          BESCHREIBUNG:
            '<strong> Sternensuite </strong> (ca. 45m²) - im Sternenhaus <br>\nIdeal für Familien mit 1 - 2 Kindern <br>\nWohn/Schlafzimmer für die Eltern, abgetrenntes Kinderzimmer mit einem Stockbett (der untere Teil des Bettes kann in ein Gitterbett umfunktioniert werden) ausgestattet mir Wohnkork, Flat-Screen TV mit Radio, Kühlschrank, Telefon, Babyphon mit Mobilfunkempfänger, Safe, kostenloses W-LAN, großer Balkon, Badezimmer mit Doppelwaschtisch, Dusche und Badewanne, WC getrennt, Föhn, Kuschelige Bademäntel und Badeschlappen, Badetasche gefüllt mit Badetüchern <br>\n<i>Mehr Info\'s finden Sie </i> <a href="https://redaktionssystem.com/demohotel3/impressum/">hier</a> <br><br>',
          EXTRABILD:
            "https://touristscout.com/demo/assets/images/header/13.jpg",
          ZUSATZBILDER: {
            EXTRABILD: [
              "https://touristscout.com/demo/assets/images/header/14.jpg",
              "https://touristscout.com/demo/assets/images/header/8.jpg"
            ]
          }
        },
        {
          FKT_NR: "649869",
          P_VON: "16.12.2017",
          P_BIS: "23.12.2017",
          P_TEXT: "Kurtaxe",
          P_ANZ: "2",
          P_SUM: "36.4",
          P_PREIS: "2.6",
          P_RABATT: "0",
          P_KG_NR: "7",
          GROUPINDEX: "0",
          DAUER_VON: "1",
          DAUER_BIS: "999",
          BESCHREIBUNG:
            "Leistungsbeschreibungs Text Information Leistungsbeschreibungs Text Information Leistungsbeschreibungs Text Information",
          EXTRABILD: "https://touristscout.com/demo/assets/images/header/9.jpg"
        },
        {
          FKT_NR: "649875",
          P_VON: "16.12.2017",
          P_BIS: "17.12.2017",
          P_TEXT: "ZSUM",
          P_ANZ: "0",
          P_SUM: "0",
          P_PREIS: "0",
          P_RABATT: "0",
          P_KG_NR: "100000",
          GROUPINDEX: "2"
        },
        {
          FKT_NR: "649874",
          P_VON: "16.12.2017",
          P_BIS: "17.12.2017",
          P_TEXT: "Kind 0 bis 5 Jahre",
          P_ANZ: "1",
          P_SUM: "399",
          P_PREIS: "57",
          P_RABATT: "0",
          P_KG_NR: "100000",
          GROUPINDEX: "2"
        }
      ]
    },
    ThemeData: {
      Themes: {
        gdatTheme: {
          theme_pics: {
            gdatPicture: [
              {
                PicId: "2317502e5863f",
                PicName: " Wohlfühlen & Genießen",
                PicPath: "$i_offer\\theme_Winter\\2317502e5863f.jpg",
                PicUrl:
                  "https://touristscout.com/demo/assets/images/header/1.jpg",
                PicHTML_Link: "",
                PicKategorie: "Winter"
              },
              {
                PicId: "23175535f3026",
                PicName: "Relaxen",
                PicPath: "$i_offer\\theme_Winter\\23175535f3026.jpg",
                PicUrl:
                  "https://touristscout.com/demo/assets/images/header/2.jpg",
                PicHTML_Link: "",
                PicKategorie: "Winter"
              },
              {
                PicId: "2476ca7e7f211",
                PicName: "Kulinarische Highlights",
                PicPath: "$i_offer\\theme_Winter\\2476ca7e7f211.jpg",
                PicUrl:
                  "https://touristscout.com/demo/assets/images/header/10.jpg",
                PicHTML_Link: "",
                PicKategorie: "SPA"
              }
            ]
          },
          theme_teasers: {
            gdatTeaser: [
              {
                Teaser_pic: {
                  PicId: "23175cb9f0472",
                  PicName:
                    "Wir bieten Schwimmkurse für Babys und Kleinkinder an (gegen Gebühr)",
                  PicPath:
                    "$i_offer\\teaser_Schwimmkurse (gegen Gebühr)\\23175cb9f0472.jpg",
                  PicUrl:
                    "https://touristscout.com/demo/assets/images/header/12.jpg",
                  PicHTML_Link: "https://redaktionssystem.com/demohotel3/",
                  PicKategorie: ""
                },
                validFrom: "2017-12-15T12:06:35.0552178+01:00",
                validTo: "2018-04-08T12:06:35",
                SuchwortListe: "",
                TeaserId: "23175cb9f0472",
                TeaserName: "Deluxe"
              },
              {
                Teaser_pic: {
                  PicId: "2475eef6f7ca5",
                  PicName:
                    "Die Skischule Fiss-Ladis bietet professionellen Kinderskikurs ab 3 Jahren an! (gegen Gebühr)",
                  PicPath:
                    "$i_offer\\teaser_neuer ThemenTeaser\\2475eef6f7ca5.jpg",
                  PicUrl:
                    "https://touristscout.com/demo/assets/images/header/13.jpg",
                  PicHTML_Link: "https://redaktionssystem.com/demohotel3/",
                  PicKategorie: ""
                },
                validFrom: "2017-12-15T09:18:22",
                validTo: "2018-04-08T09:18:22",
                SuchwortListe: "",
                TeaserId: "2475eef6f7ca5",
                TeaserName: "Premium"
              },
              {
                Teaser_pic: {
                  PicId: "2475ff47ea86a",
                  PicName:
                    "Laurentius Verwöhnpension beinhaltet  - Frühstück, Mittagsbuffet, Nachmittagsjause und Abendessen ",
                  PicPath:
                    "$i_offer\\teaser_neuer ThemenTeaser\\2475ff47ea86a.jpg",
                  PicUrl:
                    "https://touristscout.com/demo/assets/images/header/14.jpg",
                  PicHTML_Link: "https://redaktionssystem.com/demohotel3/",
                  PicKategorie: ""
                },
                validFrom: "2017-12-15T09:25:40",
                validTo: "2018-04-08T09:25:40",
                SuchwortListe: "",
                TeaserId: "2475ff47ea86a",
                TeaserName: "Superior"
              }
            ]
          },
          zusatzleistungen: {
            leistungen: [
              {
                Teaser_pic: {
                  PicId: "23175cb9f0472",
                  PicName:
                    "Wir bieten Schwimmkurse für Babys und Kleinkinder an (gegen Gebühr)",
                  PicPath:
                    "$i_offer\\teaser_Schwimmkurse (gegen Gebühr)\\23175cb9f0472.jpg",
                  PicUrl:
                    "https://touristscout.com/demo/assets/images/header/12.jpg",
                  PicHTML_Link: "https://redaktionssystem.com/demohotel3/",
                  PicKategorie: ""
                },
                validFrom: "2017-12-15T12:06:35.0552178+01:00",
                validTo: "2018-04-08T12:06:35",
                SuchwortListe: "",
                TeaserId: "23175cb9f0472",
                TeaserName: "VIP Upgrade",
                Preis: "99.90",
                LeistungsNr: "1017"
              },
              {
                Teaser_pic: {
                  PicId: "2475eef6f7ca5",
                  PicName:
                    "Die Skischule Fiss-Ladis bietet professionellen Kinderskikurs ab 3 Jahren an! (gegen Gebühr)",
                  PicPath:
                    "$i_offer\\teaser_neuer ThemenTeaser\\2475eef6f7ca5.jpg",
                  PicUrl:
                    "https://touristscout.com/demo/assets/images/header/13.jpg",
                  PicHTML_Link: "https://redaktionssystem.com/demohotel3/",
                  PicKategorie: ""
                },
                validFrom: "2017-12-15T09:18:22",
                validTo: "2018-04-08T09:18:22",
                SuchwortListe: "",
                TeaserId: "2475eef6f7ca5",
                TeaserName: "VIP Family Upgrade",
                Preis: "199.90",
                LeistungsNr: "1017"
              }
            ]
          },
          validFrom: "2017-12-15T15:09:55",
          validTo: "2018-04-08T15:09:55",
          SuchwortListe: "",
          ThemeId: "5",
          ThemeName: "Winter"
        }
      },
      BilderGalerie_pics: {
        gdatPicture: [
          {
            PicId: "231776b31df79",
            PicName: "Suite im Sonnenhaus",
            PicPath: "$i_offer\\impressions\\231776b31df79.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/201014/gastrodat/231776b31df79.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer"
          },
          {
            PicId: "23177b8203fbe",
            PicName: "Sternensuite/Laurentius Suite",
            PicPath: "$i_offer\\impressions\\23177b8203fbe.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/201014/gastrodat/23177b8203fbe.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer"
          },
          {
            PicId: "23177d89163a0",
            PicName: "Familienwasserwelt",
            PicPath: "$i_offer\\impressions\\23177d89163a0.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/201014/gastrodat/23177d89163a0.jpg",
            PicHTML_Link: "",
            PicKategorie: "SPA"
          }
        ]
      },
      impression_pics: {
        gdatPicture: [
          {
            PicId: "2317502e5863f",
            PicName: "Eingangsbereich",
            PicPath: "$i_offer\\theme_Winter\\2317502e5863f.jpg",
            PicUrl: "https://touristscout.com/demo/assets/images/header/5.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel"
          },
          {
            PicId: "23175535f3026",
            PicName: "Top ausgestattete Zimmer",
            PicPath: "$i_offer\\theme_Winter\\23175535f3026.jpg",
            PicUrl: "https://touristscout.com/demo/assets/images/header/12.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer"
          },
          {
            PicId: "2317502e5863f",
            PicName: "Lounge",
            PicPath: "$i_offer\\theme_Winter\\2317502e5863f.jpg",
            PicUrl: "https://touristscout.com/demo/assets/images/header/2.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel"
          },
          {
            PicId: "2476ca7e7f211",
            PicName: "Schöne Terasse",
            PicPath: "$i_offer\\theme_Winter\\2476ca7e7f211.jpg",
            PicUrl: "https://touristscout.com/demo/assets/images/header/6.jpg",
            PicHTML_Link: "",
            PicKategorie: "Sommer"
          },
          {
            PicId: "2476ca7e7f211",
            PicName: "Zimmer 101",
            PicPath: "$i_offer\\theme_Winter\\2476ca7e7f211.jpg",
            PicUrl: "https://touristscout.com/demo/assets/images/header/13.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer"
          },
          {
            PicId: "2476ca7e7f211",
            PicName: "Zimmer 1",
            PicPath: "$i_offer\\theme_Winter\\2476ca7e7f211.jpg",
            PicUrl: "https://touristscout.com/demo/assets/images/header/12.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer"
          }
        ]
      },
      AngebotsName: "lalala",
      StammDaten: {
        HotelLogo_pic: {
          PicId: "1ecfce182b605",
          PicName: "HotelLogo",
          PicPath: "$i_offer\\LOGO\\StammDaten_HotelLogo_pic.png",
          PicUrl: "https://touristscout.com/demo/assets/images/logo.png",
          PicHTML_Link: ""
        },
        Section_Liste: {
          Section: [
            {
              ModulName: "LOGO_BLOCK",
              ModulId: "",
              ModulHash: "LOGO_BLOCK"
            },
            {
              ModulName: "SLIDESHOW",
              ModulId: "",
              ModulHash: "SLIDESHOW"
            },
            /*  {
              ModulName: "MENU_BAND",
              ModulId: "",
              ModulHash: "MENU_BAND"
            }, */
            {
              ModulName: "GREETING",
              ModulId: "",
              ModulHash: "GREETING"
            },
            {
              ModulName: "WELCOME_TEXT",
              ModulId: ""
            },
            {
              ModulName: "TEASER_BLOCK",
              ModulId: ""
            },
            {
              ModulName: "ANGEBOTS_TEXT",
              ModulId: ""
            },
            {
              ModulName: "VARIANTEN_VORSCHAU",
              ModulId: ""
            },
            /* {
              ModulName: "ZIMMER1",
              ModulId: "ZIMMER"
            }, */
            {
              ModulName: "ANGEBOT_DETAILS",
              ModulId: "",
              ModulHash: "ANGEBOT"
            },
            /* {
              ModulName: "ZUSATZLEISTUNGEN",
              ModulId: "",
              ModulHash: "ZUSATZLEISTUNGEN"
            }, */
            {
              ModulName: "IMPRESSIONEN",
              ModulId: "",
              ModulHash: "IMPRESSIONEN"
            },
            /* {
              ModulName: "CTA1",
              ModulId: ""
            }, */
            {
              ModulName: "ZUSATZ_TEXT1",
              ModulId: ""
            },
            {
              ModulName: "KACHELBILDER",
              ModulId: "",
              ModulHash: "KACHELBILDER"
            },
            {
              ModulName: "ZUSATZ_TEXT2",
              ModulId: ""
            },
            {
              ModulName: "ANREISE_BESCHREIBUNG",
              ModulId: ""
            },
            {
              ModulName: "NACHRICHTEN_BLOCK",
              ModulId: "",
              ModulHash: "KONTAKT"
            },
            {
              ModulName: "SOCIAL_MEDIA_BAND",
              ModulId: "",
              ModulHash: ""
            },
            {
              ModulName: "FOOTER",
              ModulId: ""
            },
            {
              ModulName: "IMPRESSUM",
              ModulId: "",
              ModulHash: "IMPRESSUM"
            }
          ]
        },
        MenuImpressumItem_Liste: {
          MenuItem: [
            {
              MenuId: "1",
              MenuName: "Impressum",
              MenuURL: "https://redaktionssystem.com/demohotel3/impressum/",
              Self: "false"
            },
            {
              MenuId: "2",
              MenuName: "Datenschutz",
              MenuURL: "https://redaktionssystem.com/demohotel3/impressum/",
              Self: "true"
            }
          ]
        },
        MenuItem_Liste: {
          MenuItem: [
            {
              MenuId: "1",
              MenuName: "Angebot",
              MenuURL: "#ANGEBOT",
              Self: "true"
            },
            {
              MenuId: "2",
              MenuName: "Impressionen",
              MenuURL: "#IMPRESSIONEN",
              Self: "true"
            },
            {
              MenuId: "2",
              MenuName: "Kontakt",
              MenuURL: "#KONTAKT",
              Self: "true"
            },
            {
              MenuId: "2",
              MenuName: "Webseite",
              MenuURL: "https://redaktionssystem.com/demohotel3/",
              Self: "false"
            }
          ]
        },
        EinzelText_Liste: {
          EinzelText: [
            {
              Tag: "gdat_chatten",
              Text:
                "Haben Sie Fragen? Tel. 0043 5476 6714 - wir sind gerne für Sie da."
            },
            {
              Tag: "*gdat_variante:",
              Text: "oder Sie entscheiden sich für:"
            },
            {
              Tag: "gdat_form_datenschutzhinweis",
              Text:
                "Mit meiner Anfrage stimme ich zu, dass das Demo Hotel, die von mir bekanntgegebenen Daten speichern und verwenden darf. Mir ist bekannt, dass ich diese Einwilligung aber jederzeit gemäß Artikel 21 DSGVO mittels Brief oder Mail an test@demohotel.at widerrufen kann."
            },
            {
              Tag: "gdat_datenschutzlinktext",
              Text: "Datenschutzerklärung"
            },
            {
              Tag: "gdat_datenschutzlink",
              Text: "https://redaktionssystem.com/demohotel3/impressum/"
            }
          ]
        },
        HotelName: "Familien.Urlaubs.Traum",
        WellcomeText:
          "<p>Das <b>**** Superior Baby &amp; Kinderhotel</b> Laurentius bietet die allerbesten Voraussetzungen für einen traumhaften Winter- und Sommerurlaub, mit einem riesigen Angebot an Wellness, Genuss, Spaß und Sport für die ganze Familie – und das - direkt an den Pisten von Tirols Skidimension Serfaus-Fiss-Ladis.</p>",
        SubDomainUrl: "https://redaktionssystem.com/demohotel3",
        AngebotsText:
          "<h2>Für Ihren Wunschtermin können wir Ihnen wie folgt anbieten:</h2>",
        AnreiseInfoText:
          "<p><b>EINCHECKEN</b><br>MO-SO:: ab 15 UHR</p><p > </p>",
        AbreiseInfoText: "<p><b>AUSCHECKEN</b><br>MO-SO: BIS 11 UHR</p>",
        ZusatzText1: "Lorem ipsum dolor sit amet, consetetur",
        ZusatzText1_Header: "Header ZusatzText 1",
        ZusatzText1_Collapsed: "false",
        ZusatzText_Bild1: {
          PicId: "1e85a7b85e334",
          PicName: "Hausgemachtes Slow Food, bei Wunsch auch vegetarisch",
          PicPath: "$i_offer\\teaser_ThemenTeaser disco\\1e85a7b85e334.jpg",
          PicUrl:
            "https://www.weratech-files.com/images/280942/gastrodat/1e85a7b85e334.jpg",
          LastPicUpload: "2018-04-04T09:19:28.6605536+02:00"
        },
        ZusatzText2:
          "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        ZusatzText2_Header: "Header ZusatzText 2",
        ZusatzText2_Collapsed: "true",
        ZusatzText_Bild2: {
          PicId: "1e85a7b85e334",
          PicName: "Hausgemachtes Slow Food, bei Wunsch auch vegetarisch",
          PicPath: "$i_offer\\teaser_ThemenTeaser disco\\1e85a7b85e334.jpg",
          PicUrl:
            "https://www.weratech-files.com/images/280942/gastrodat/1e85a7b85e334.jpg",
          LastPicUpload: "2018-04-04T09:19:28.6605536+02:00"
        },
        Cta1: {
          Name: "Click to action 1",
          Url: "https://www.gastrodat.com",
          Self: "false"
        },
        Cta2: {
          Name: "Click to action 2",
          Url: "https://www.gastrodat.com/produkte/",
          Self: "true"
        },
        FaceBookPageId: "807523689420185",
        SozMedia_FaceBook_Url: "https://www.facebook.com/gastrodat",
        SozMedia_GooglePlus_Url: "www.googleplus.....",
        SozMedia_Pinterest_Url: "www.pinterest.....",
        SozMedia_Instagram_Url: "www.instagramm.....",
        SozMedia_YouTube_Url: "www.youtube......",
        SozMedia_Twitter_Url: "www.twitter....",
        ImageCompressionQuality: "50"
      },
      DesignSettings: {
        LogoPosition: "left",
        TemplateName: "1",
        primaryColor: "#f8f8f8", // blau
        secundaryColor: "#000", // beige
        backgroundColor: "#fff", // weiß
        backgroundColor2: "#f8f8f8", // beige
        bckg_Color3: "#fff", // mittel beige 1 (collabsable bg)
        bckg_Color4: "#b0a8a3", // mittel beige 2 (pfeil bg)
        bckg_Color5: "#fff", //
        textColor: "#777", // dunkel grau
        prim_textColor: "#111", // weiss
        sec_textColor: "#fff", // weiss
        textColor_Color3: "#000", // textcolor mittel beige 2
        textColor_Color4: "#fff", // weiss

        headlineFontName: "'Yanone Kaffeesatz', sans-serif",
        bodyFontName: "'Open Sans', 'Helvetica Neue', Helvetica, sans-serif"
      }
    }
  }
}
