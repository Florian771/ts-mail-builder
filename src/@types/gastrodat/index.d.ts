export interface HotelData {
  Ort?: string
  HotelName?: string
  PersName?: string
  Strasse?: string
  Gruss?: string
  PLZ?: string
  Telefon?: string
  Fax?: string
  UID?: string
  Nation?: string
  KtoNr?: string
  Bank?: string
  BLZ?: string
  Email?: string
  BIC?: string
  IBAN?: string
}

export interface ReservationData {
  LFNUMMER: string
  ZIMMER_NR: string
  PERS_NR: string
  PERSON: string
  VON: string
  BIS: string
  ANZ_K1: string
  ANZ_K2: string
  ANZ_E: string
  ANZ_K3: string
  ANZ_K4: string
  ANZ_E2: string
  ANGEBOTSURL: string
}

export interface GuestData {
  LFNUMMER: string
  ANREDE_H: string
  FAMNAME: string
  VORNAME: string
  TITEL: string
  GESCHLECHT: string
  STRASSE: string
  PLZ: string
  ORT: string
  ANREDE_FAM: string
  SPRACHE: string
  NATION: string
  ZUSNAME: string
  P_EMAIL: string
  NATION_TXT: string
}

export interface RoomData {
  ZIMMER_NR: string
  ZIMMER_NAME: string
  GEEIGNET_VON: string
  GEEIGNET_BIS: string
  ZI_KAT: string
  ZIMMER_SAUBER: string
  GEEIGNET_STD: string
  STANDARDLEISTUNG: string
  TEXT123_exception: string
  ZIMMERBILD: string
  ZIMMERGRUNDRISS: string
}

export interface ZUSATZBILDER {
  EXTRABILD: string
}

export interface Posio {
  FKT_NR: string
  P_VON: string
  P_BIS: string
  P_TEXT: string
  P_ANZ: string
  P_SUM: string
  P_PREIS: string
  P_RABATT: string
  P_KG_NR: string
  GROUPINDEX: string
  EXTRA_NR: string
  DAUER_VON: string
  DAUER_BIS: string
  BESCHREIBUNG: string
  EXTRABILD: string
  ZUSATZBILDER: ZUSATZBILDER
  ZUSATZ: string
}

export interface ExtrasData {
  Posio: Posio[]
}

export interface GdatPicture {
  PicId: string
  PicName: string
  PicPath: string
  PicUrl: string
  PicHTML_Link: string
  PicKategorie: string
  LastPicUpload: Date
}

export interface ThemePics {
  gdatPicture: GdatPicture[]
}

export interface TeaserPic {
  PicId: string
  PicName: string
  PicPath: string
  PicUrl: string
  PicHTML_Link: string
  PicKategorie: string
  LastPicUpload: Date
}

export interface GdatTeaser {
  Teaser_pic: TeaserPic
  validFrom: Date
  validTo: Date
  SuchwortListe: string
  TeaserId: string
  TeaserName: string
}

export interface ThemeTeasers {
  gdatTeaser: GdatTeaser[]
}

export interface GdatTheme {
  theme_pics: ThemePics
  theme_teasers: ThemeTeasers
  validFrom: Date
  validTo: Date
  SuchwortListe: string
  ThemeId: string
  ThemeName: string
}

export interface Themes {
  gdatTheme: GdatTheme
}

export interface GdatPicture2 {
  PicId: string
  PicName: string
  PicPath: string
  PicUrl: string
  PicHTML_Link: string
  PicKategorie: string
  LastPicUpload: Date
}

export interface BilderGaleriePics {
  gdatPicture: GdatPicture2[]
}

export interface GdatPicture3 {
  PicId: string
  PicName: string
  PicPath: string
  PicUrl: string
  PicHTML_Link: string
  PicKategorie: string
  LastPicUpload: Date
}

export interface ImpressionPics {
  gdatPicture: GdatPicture3[]
}

export interface HotelLogoPic {
  PicId: string
  PicName: string
  PicPath: string
  PicUrl: string
  PicHTML_Link: string
  LastPicUpload: Date
}

export interface Section {
  ModulName: string
  ModulId: string
  ModulHash: string
  SortOrder: string
}

export interface SectionListe {
  Section: Section[]
}

export interface EinzelText {
  Tag: string
  Text: string
}

export interface EinzelTextListe {
  EinzelText: EinzelText[]
}

export interface MenuItem {
  MenuId: string
  MenuName: string
  MenuURL: string
  Self: string
}

export interface MenuItemListe {
  MenuItem: MenuItem[]
}

export interface MenuItem2 {
  MenuId: string
  MenuName: string
  MenuURL: string
  Self: string
}

export interface MenuImpressumItemListe {
  MenuItem: MenuItem2
}

export interface ZusatzTextBild1 {
  LastPicUpload: Date
}

export interface ZusatzTextBild2 {
  LastPicUpload: Date
}

export interface ZusatzTextBild3 {
  LastPicUpload: Date
}

export interface ZusatzTextBild4 {
  LastPicUpload: Date
}

export interface ZusatzTextBild5 {
  LastPicUpload: Date
}

export interface ZwischenBild1 {
  PicId: string
  PicName: string
  PicPath: string
  PicUrl: string
  PicHTML_Link: string
  PicKategorie: string
  LastPicUpload: Date
}

export interface ZwischenBild2 {
  LastPicUpload: Date
}

export interface ZwischenBild3 {
  LastPicUpload: Date
}

export interface ZwischenBild4 {
  LastPicUpload: Date
}

export interface ZwischenBild5 {
  LastPicUpload: Date
}

export interface StammDaten {
  HotelLogo_pic: HotelLogoPic
  Section_Liste: SectionListe
  EinzelText_Liste: EinzelTextListe
  MenuItem_Liste: MenuItemListe
  MenuImpressumItem_Liste: MenuImpressumItemListe
  HotelName: string
  WellcomeText: string
  SubDomainUrl: string
  AngebotsText: string
  AnreiseInfoText: string
  AbreiseInfoText: string
  ZusatzText1: string
  ZusatzText1_Header: string
  ZusatzText1_Collapsed: string
  ZusatzText_Bild1: ZusatzTextBild1
  ZusatzText2: string
  ZusatzText2_Header: string
  ZusatzText2_Collapsed: string
  ZusatzText_Bild2: ZusatzTextBild2
  ZusatzText3: string
  ZusatzText3_Header: string
  ZusatzText3_Collapsed: string
  ZusatzText_Bild3: ZusatzTextBild3
  ZusatzText4: string
  ZusatzText4_Header: string
  ZusatzText4_Collapsed: string
  ZusatzText_Bild4: ZusatzTextBild4
  ZusatzText5: string
  ZusatzText5_Header: string
  ZusatzText5_Collapsed: string
  ZusatzText_Bild5: ZusatzTextBild5
  ZwischenBild1: ZwischenBild1
  ZwischenBild2: ZwischenBild2
  ZwischenBild3: ZwischenBild3
  ZwischenBild4: ZwischenBild4
  ZwischenBild5: ZwischenBild5
  FaceBookPageId: string
  SozMedia_FaceBook_Url: string
  SozMedia_GooglePlus_Url: string
  SozMedia_Pinterest_Url: string
  SozMedia_Instagram_Url: string
  SozMedia_YouTube_Url: string
  SozMedia_Twitter_Url: string
  ImageCompressionQuality: string
}

export interface TextModulStyle {
  StyleName: string
  InlineStyle: string
  Bold: string
  FontName: string
  FontSize: string
  ForeColor: string
  Italic: string
  ListFormat: string
  Strikeout: string
  TextBackColor: string
  Underline: string
  UseStyle: string
}

export interface TextModulStyleListe {
  TextModulStyle: TextModulStyle[]
}

export interface DesignSettings {
  TextModulStyle_Liste: TextModulStyleListe
  TemplateName: string
  primaryColor: string
  secundaryColor: string
  bckg_Color3: string
  bckg_Color4: string
  bckg_Color5: string
  prim_textColor: string
  sec_textColor: string
  textColor_Color3: string
  textColor_Color4: string
  textColor_Color5: string
  backgroundColor: string
  headlineFontName: string
  bodyFontName: string
  textColor: string
}

export interface ThemeData {
  Themes: Themes
  BilderGalerie_pics: BilderGaleriePics
  impression_pics: ImpressionPics
  StammDaten: StammDaten
  DesignSettings: DesignSettings
}

export interface API {
  HotelData: HotelData
  ReservationData: ReservationData
  GuestData: GuestData
  RoomData: RoomData
  ExtrasData: ExtrasData
  ThemeData: ThemeData
}

export as namespace gastrodat
