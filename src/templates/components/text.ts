import testParse from "../../testParse"
import { textBreak } from "../../config"
import { getTranslations } from "../../utils"

export interface TextSettings {
  key: string
}

export interface Text {
  text: string
}

const placeholder: Text = {
  text: "{CONTENT_TEXT_KEY}"
}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: TextSettings
}): MailBuilder.Component => {
  const translations: string[] = getTranslations({
    placeholder: placeholder,
    settings: settings
  })
  let html: string = ""
  let text: string = ""
  text = `${placeholder.text.replace("KEY", settings.key)}${textBreak}`
  html = `
  <mj-section padding="0 20px 20px 20px" background-color="{backgroundColor2}">
  <mj-column width="100%">
    <mj-text align="center" padding="15px 40px 15px 40px" font-weight="300">
    ${placeholder.text.replace("KEY", settings.key)}
    </mj-text>
  </mj-column>
</mj-section>`

  testParse({ test: true, html: html, options: options, modulName: "text" })
  return {
    html: html,
    text: text,
    translations: translations,
    parse: (args: any) => testParse(args)
  }
}
