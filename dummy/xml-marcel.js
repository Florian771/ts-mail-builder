export default `<API><HotelData><Ort>6533 Fiss</Ort><HotelName>****Familienhotel St. Laurentius</HotelName><PersName>Familie Neururer</PersName><Strasse>Leiteweg 26</Strasse><Gruss></Gruss><PLZ></PLZ><Telefon></Telefon><Fax></Fax><UID></UID><Nation>A</Nation><KtoNr></KtoNr><Email>info@laurentius.at</Email><Bank></Bank><BLZ></BLZ><BIC></BIC><IBAN></IBAN></HotelData><ReservationData><LFNUMMER>169943</LFNUMMER><ZIMMER_NR>5</ZIMMER_NR><PERS_NR>125646</PERS_NR><PERSON>Termin gerade reserviert ...</PERSON><VON>16.12.2017</VON><BIS>23.12.2017</BIS><ANGEBOTSURL><![CDATA[]]></ANGEBOTSURL></ReservationData><GuestData><LFNUMMER>125646</LFNUMMER><ANREDE_H>Frau</ANREDE_H><FAMNAME>Viktoria</FAMNAME><VORNAME>Federspiel</VORNAME><TITEL></TITEL><GESCHLECHT>W</GESCHLECHT><STRASSE>Scheibe 532</STRASSE><PLZ>6543</PLZ><ORT>Nauders</ORT><ANREDE_FAM>liebe Viktoria</ANREDE_FAM><SPRACHE>D Deutsch</SPRACHE><NATION>A</NATION><ZUSNAME></ZUSNAME><P_EMAIL>reservierung@laurentius.at</P_EMAIL><NATION_TXT></NATION_TXT></GuestData><RoomData><ZIMMER_NR>5</ZIMMER_NR><ZIMMER_NAME>005</ZIMMER_NAME><GEEIGNET_VON>1</GEEIGNET_VON><GEEIGNET_BIS>1</GEEIGNET_BIS><ZI_KAT>2</ZI_KAT><ZIMMER_SAUBER>N</ZIMMER_SAUBER><GEEIGNET_STD>1</GEEIGNET_STD><STANDARDLEISTUNG>10005,7,</STANDARDLEISTUNG><TEXT1>Doppelzimmer</TEXT1><TEXT2>schön; Nachbar; Badewanne - WC in Bad; Holzboden, Leder Couch; Balkon sehr schmal</TEXT2><ZIMMERBILD>/Content/images/invalidimage.png</ZIMMERBILD><ZIMMERGRUNDRISS>/Content/images/invalidimage.png</ZIMMERGRUNDRISS></RoomData><ExtrasData><Posio><FKT_NR>649868</FKT_NR><P_VON>16.12.2017</P_VON><P_BIS>23.12.2017</P_BIS><P_TEXT>Pers. im Doppelzimmer</P_TEXT><P_ANZ>2</P_ANZ><P_SUM>2212</P_SUM><P_PREIS>158</P_PREIS><P_RABATT>0</P_RABATT><P_KG_NR>10005</P_KG_NR><GROUPINDEX>0</GROUPINDEX><DAUER_VON>1</DAUER_VON><DAUER_BIS>999</DAUER_BIS><BESCHREIBUNG></BESCHREIBUNG><EXTRABILD>https://www.weratech-files.com/images/201014/gastrodat/Extras/EXTRA_10005.jpg?timestamp=636489420900000000</EXTRABILD></Posio><Posio><FKT_NR>649869</FKT_NR><P_VON>16.12.2017</P_VON><P_BIS>23.12.2017</P_BIS><P_TEXT>Kurtaxe</P_TEXT><P_ANZ>2</P_ANZ><P_SUM>36.4</P_SUM><P_PREIS>2.6</P_PREIS><P_RABATT>0</P_RABATT><P_KG_NR>7</P_KG_NR><GROUPINDEX>0</GROUPINDEX><DAUER_VON>1</DAUER_VON><DAUER_BIS>999</DAUER_BIS><BESCHREIBUNG></BESCHREIBUNG><EXTRABILD>https://www.weratech-files.com/images/201014/gastrodat/Extras/EXTRA_7.jpg?timestamp=636489379110000000</EXTRABILD></Posio><Posio><FKT_NR>649870</FKT_NR><P_VON>16.12.2017</P_VON><P_BIS>23.12.2017</P_BIS><P_TEXT>Kind 0 bis 5 Jahre</P_TEXT><P_ANZ>1</P_ANZ><P_SUM>399</P_SUM><P_PREIS>57</P_PREIS><P_RABATT>0</P_RABATT><P_KG_NR>10025</P_KG_NR><GROUPINDEX>1</GROUPINDEX><DAUER_VON>1</DAUER_VON><DAUER_BIS>999</DAUER_BIS><BESCHREIBUNG></BESCHREIBUNG><EXTRABILD>https://www.weratech-files.com/images/201014/gastrodat/Extras/EXTRA_10025.jpg?timestamp=636489379110000000</EXTRABILD></Posio><Posio><FKT_NR>649871</FKT_NR><P_VON>15.12.2017</P_VON><P_BIS>16.12.2017</P_BIS><P_TEXT>ODER</P_TEXT><P_ANZ>0</P_ANZ><P_SUM>0</P_SUM><P_PREIS>0</P_PREIS><P_RABATT>0</P_RABATT><P_KG_NR>0</P_KG_NR><GROUPINDEX>-1</GROUPINDEX></Posio><Posio><FKT_NR>649872</FKT_NR><P_VON>16.12.2017</P_VON><P_BIS>23.12.2017</P_BIS><P_TEXT>Pers. in Sternensuite</P_TEXT><P_ANZ>2</P_ANZ><P_SUM>2856</P_SUM><P_PREIS>204</P_PREIS><P_RABATT>0</P_RABATT><P_KG_NR>34</P_KG_NR><GROUPINDEX>2</GROUPINDEX><DAUER_VON>1</DAUER_VON><DAUER_BIS>999</DAUER_BIS><BESCHREIBUNG>&lt;strong&gt; Sternensuite &lt;/strong&gt; (ca. 45m²) - im Sternenhaus &lt;br&gt;
Ideal für Familien mit 1 - 2 Kindern &lt;br&gt;
Wohn/Schlafzimmer für die Eltern, abgetrenntes Kinderzimmer mit einem Stockbett (der untere Teil des Bettes kann in ein Gitterbett umfunktioniert werden) ausgestattet mir Wohnkork, Flat-Screen TV mit Radio, Kühlschrank, Telefon, Babyphon mit Mobilfunkempfänger, Safe, kostenloses W-LAN, großer Balkon, Badezimmer mit Doppelwaschtisch, Dusche und Badewanne, WC getrennt, Föhn, Kuschelige Bademäntel und Badeschlappen, Badetasche gefüllt mit Badetüchern &lt;br&gt;
&lt;i&gt;Mehr Info's finden Sie &lt;/i&gt; &lt;a href="https://www.familienhotel-laurentius.com/sternensuite.aspx"&gt;hier&lt;/a&gt; &lt;br&gt;&lt;br&gt;</BESCHREIBUNG><EXTRABILD>https://www.weratech-files.com/images/201014/gastrodat/Extras/EXTRA_34.jpg?timestamp=636543832620000000</EXTRABILD></Posio><Posio><FKT_NR>649873</FKT_NR><P_VON>16.12.2017</P_VON><P_BIS>23.12.2017</P_BIS><P_TEXT>Kurtaxe</P_TEXT><P_ANZ>2</P_ANZ><P_SUM>36.4</P_SUM><P_PREIS>2.6</P_PREIS><P_RABATT>0</P_RABATT><P_KG_NR>100000</P_KG_NR><GROUPINDEX>2</GROUPINDEX></Posio><Posio><FKT_NR>649874</FKT_NR><P_VON>16.12.2017</P_VON><P_BIS>23.12.2017</P_BIS><P_TEXT>Kind 0 bis 5 Jahre</P_TEXT><P_ANZ>1</P_ANZ><P_SUM>399</P_SUM><P_PREIS>57</P_PREIS><P_RABATT>0</P_RABATT><P_KG_NR>100000</P_KG_NR><GROUPINDEX>2</GROUPINDEX></Posio><Posio><FKT_NR>649875</FKT_NR><P_VON>15.12.2017</P_VON><P_BIS>16.12.2017</P_BIS><P_TEXT>ZSUM</P_TEXT><P_ANZ>0</P_ANZ><P_SUM>0</P_SUM><P_PREIS>0</P_PREIS><P_RABATT>0</P_RABATT><P_KG_NR>100000</P_KG_NR><GROUPINDEX>2</GROUPINDEX></Posio></ExtrasData><ThemeData><Themes><gdatTheme><theme_pics><gdatPicture><PicId>2317502e5863f</PicId><PicName> Winterspaß für Groß und Klein</PicName><PicPath>$i_offer\theme_Winter\2317502e5863f.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/2317502e5863f.jpg</PicUrl><PicHTML_Link /><PicKategorie>Winter</PicKategorie></gdatPicture><gdatPicture><PicId>23175535f3026</PicId><PicName>Traumhafte Lage - direkt an den 212 km Pisten von Serfaus-Fiss-Ladis</PicName><PicPath>$i_offer\theme_Winter\23175535f3026.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/23175535f3026.jpg</PicUrl><PicHTML_Link /><PicKategorie>Winter</PicKategorie></gdatPicture><gdatPicture><PicId>2476ca7e7f211</PicId><PicName>Wasservernügen in unserer Familien.Wasser.Welt</PicName><PicPath>$i_offer\theme_Winter\2476ca7e7f211.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/2476ca7e7f211.jpg</PicUrl><PicHTML_Link /><PicKategorie>SPA</PicKategorie></gdatPicture><gdatPicture><PicId>2476cd0d677e2</PicId><PicName>bei uns sind ALLE glücklich!</PicName><PicPath>$i_offer\theme_Winter\2476cd0d677e2.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/2476cd0d677e2.jpg</PicUrl><PicHTML_Link /><PicKategorie>SPA</PicKategorie></gdatPicture></theme_pics><theme_teasers><gdatTeaser><Teaser_pic><PicId>23175cb9f0472</PicId><PicName>Wir bieten Schwimmkurse für Babys und Kleinkinder an (gegen Gebühr)</PicName><PicPath>$i_offer\teaser_Schwimmkurse (gegen Gebühr)\23175cb9f0472.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/23175cb9f0472.jpg</PicUrl><PicHTML_Link>https://www.familienhotel-laurentius.com/Kinderschwimmkurs.aspx</PicHTML_Link><PicKategorie /></Teaser_pic><validFrom>2017-12-15T12:06:35.0552178+01:00</validFrom><validTo>2018-04-08T12:06:35</validTo><SuchwortListe /><TeaserId>23175cb9f0472</TeaserId><TeaserName>Schwimmkurse (gegen Gebühr)</TeaserName></gdatTeaser><gdatTeaser><Teaser_pic><PicId>2475eef6f7ca5</PicId><PicName>Die Skischule Fiss-Ladis bietet professionellen Kinderskikurs ab 3 Jahren an! (gegen Gebühr)</PicName><PicPath>$i_offer\teaser_neuer ThemenTeaser\2475eef6f7ca5.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/2475eef6f7ca5.jpg</PicUrl><PicHTML_Link>https://www.familienhotel-laurentius.com/bertas-kinderland.aspx</PicHTML_Link><PicKategorie /></Teaser_pic><validFrom>2017-12-15T09:18:22</validFrom><validTo>2018-04-08T09:18:22</validTo><SuchwortListe /><TeaserId>2475eef6f7ca5</TeaserId><TeaserName>Skifahren mit der Berta (gegen Gebühr)</TeaserName></gdatTeaser><gdatTeaser><Teaser_pic><PicId>2475ff47ea86a</PicId><PicName>Laurentius Verwöhnpension beinhaltet  - Frühstück, Mittagsbuffet, Nachmittagsjause und Abendessen </PicName><PicPath>$i_offer\teaser_neuer ThemenTeaser\2475ff47ea86a.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/2475ff47ea86a.jpg</PicUrl><PicHTML_Link>https://www.familienhotel-laurentius.com/hotel-kueche-genuss.aspx</PicHTML_Link><PicKategorie /></Teaser_pic><validFrom>2017-12-15T09:25:40</validFrom><validTo>2018-04-08T09:25:40</validTo><SuchwortListe /><TeaserId>2475ff47ea86a</TeaserId><TeaserName>Laurentius Verwöhnpension</TeaserName></gdatTeaser></theme_teasers><validFrom>2017-12-15T15:09:55</validFrom><validTo>2018-04-08T15:09:55</validTo><SuchwortListe /><ThemeId>5</ThemeId><ThemeName>Winter</ThemeName></gdatTheme></Themes><BilderGalerie_pics><gdatPicture><PicId>231776b31df79</PicId><PicName>Suite im Sonnenhaus</PicName><PicPath>$i_offer\impressions\231776b31df79.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/231776b31df79.jpg</PicUrl><PicHTML_Link /><PicKategorie>Zimmer</PicKategorie></gdatPicture><gdatPicture><PicId>23177b8203fbe</PicId><PicName>Sternensuite/Laurentius Suite</PicName><PicPath>$i_offer\impressions\23177b8203fbe.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/23177b8203fbe.jpg</PicUrl><PicHTML_Link /><PicKategorie>Zimmer</PicKategorie></gdatPicture><gdatPicture><PicId>23177d89163a0</PicId><PicName>Familienwasserwelt</PicName><PicPath>$i_offer\impressions\23177d89163a0.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/23177d89163a0.jpg</PicUrl><PicHTML_Link /><PicKategorie>SPA</PicKategorie></gdatPicture></BilderGalerie_pics><impression_pics><gdatPicture><PicId>2317502e5863f</PicId><PicName> Winterspaß für Groß und Klein</PicName><PicPath>$i_offer\theme_Winter\2317502e5863f.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/2317502e5863f.jpg</PicUrl><PicHTML_Link /><PicKategorie>Winter</PicKategorie></gdatPicture><gdatPicture><PicId>23175535f3026</PicId><PicName>Traumhafte Lage - direkt an den 212 km Pisten von Serfaus-Fiss-Ladis</PicName><PicPath>$i_offer\theme_Winter\23175535f3026.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/23175535f3026.jpg</PicUrl><PicHTML_Link /><PicKategorie>Winter</PicKategorie></gdatPicture><gdatPicture><PicId>2476ca7e7f211</PicId><PicName>Wasservernügen in unserer Familien.Wasser.Welt</PicName><PicPath>$i_offer\theme_Winter\2476ca7e7f211.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/2476ca7e7f211.jpg</PicUrl><PicHTML_Link /><PicKategorie>SPA</PicKategorie></gdatPicture><gdatPicture><PicId>2476cd0d677e2</PicId><PicName>bei uns sind ALLE glücklich!</PicName><PicPath>$i_offer\theme_Winter\2476cd0d677e2.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/2476cd0d677e2.jpg</PicUrl><PicHTML_Link /><PicKategorie>SPA</PicKategorie></gdatPicture><gdatPicture><PicId>23176bd98bac0</PicId><PicName>Bei uns sind ALLE glücklich!</PicName><PicPath>$i_offer\theme_Sommer\23176bd98bac0.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/23176bd98bac0.jpg</PicUrl><PicHTML_Link /><PicKategorie>Sommer</PicKategorie></gdatPicture><gdatPicture><PicId>23176f4be06dc</PicId><PicName>1500m² Spielplatz direkt vor der Haustüre</PicName><PicPath>$i_offer\theme_Sommer\23176f4be06dc.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/23176f4be06dc.jpg</PicUrl><PicHTML_Link /><PicKategorie>Sommer</PicKategorie></gdatPicture><gdatPicture><PicId>24762624981c2</PicId><PicName>Super.Sommer.Card - kostenlose Nutzung von 11 Seilbahnen</PicName><PicPath>$i_offer\theme_Sommer\24762624981c2.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/24762624981c2.jpg</PicUrl><PicHTML_Link /><PicKategorie>Sommer</PicKategorie></gdatPicture><gdatPicture><PicId>231776b31df79</PicId><PicName>Suite im Sonnenhaus</PicName><PicPath>$i_offer\impressions\231776b31df79.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/231776b31df79.jpg</PicUrl><PicHTML_Link /><PicKategorie>Zimmer</PicKategorie></gdatPicture><gdatPicture><PicId>23177b8203fbe</PicId><PicName>Sternensuite/Laurentius Suite</PicName><PicPath>$i_offer\impressions\23177b8203fbe.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/23177b8203fbe.jpg</PicUrl><PicHTML_Link /><PicKategorie>Zimmer</PicKategorie></gdatPicture><gdatPicture><PicId>23177d89163a0</PicId><PicName>Familienwasserwelt</PicName><PicPath>$i_offer\impressions\23177d89163a0.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/23177d89163a0.jpg</PicUrl><PicHTML_Link /><PicKategorie>SPA</PicKategorie></gdatPicture></impression_pics><AngebotsName>lalala</AngebotsName><StammDaten><HotelLogo_pic><PicId>1ecfce182b605</PicId><PicName>HotelLogo</PicName><PicPath>$i_offer\LOGO\StammDaten_HotelLogo_pic.png</PicPath><PicUrl>https://www.weratech-files.com/images/201014/gastrodat/StammDaten_HotelLogo_pic.png</PicUrl><PicHTML_Link /></HotelLogo_pic><EinzelText_Liste><EinzelText><Tag>gdat_chatten</Tag><Text>Haben Sie Fragen? Tel. 0043 5476 6714 - wir sind gerne für Sie da.</Text></EinzelText><EinzelText><Tag>*gdat_variante:</Tag><Text>oder Sie entscheiden sich für:</Text></EinzelText></EinzelText_Liste><HotelName>Familien.Urlaubs.Traum</HotelName><WellcomeText>&lt;?xml version="1.0" ?&gt;
&lt;html xmlns="https://www.w3.org/1999/xhtml"&gt;
&lt;head&gt;
&lt;meta content="TX24_HTM 24.0.610.500" name="GENERATOR" /&gt;
&lt;title&gt;&lt;/title&gt;
&lt;style type="text/css"&gt;
/* &lt;![CDATA[ */
BODY {
	widows: 2;
	orphans: 2;
	font-family: 'Arial';
	font-size: 12pt;
	font-weight: normal;
	font-style: normal;
}
H1 {
	widows: 2;
	orphans: 2;
	margin: 7.95pt 0pt 15.85pt 0pt;
	text-indent: 0pt;
	font-family: 'Ebrima';
	font-size: 10pt;
	font-weight: normal;
	font-style: normal;
}
H2 {
	widows: 2;
	orphans: 2;
	margin: 5.95pt 0pt 11.9pt 0pt;
	text-indent: 0pt;
	font-family: 'Ebrima';
	font-size: 10pt;
	font-weight: normal;
	font-style: normal;
}
H3 {
	widows: 2;
	orphans: 2;
	margin: 4.65pt 0pt 9.3pt 0pt;
	text-indent: 0pt;
	font-family: 'Arial';
	font-size: 14pt;
	font-weight: bold;
	font-style: normal;
}
H4 {
	widows: 2;
	orphans: 2;
	margin: 3.65pt 0pt 7.3pt 0pt;
	text-indent: 0pt;
	font-family: 'Arial';
	font-size: 11pt;
	font-weight: bold;
	font-style: normal;
}
H5 {
	widows: 2;
	orphans: 2;
	margin: 3pt 0pt 6pt 0pt;
	text-indent: 0pt;
	font-family: 'Arial';
	font-size: 9pt;
	font-weight: bold;
	font-style: normal;
}
H6 {
	widows: 2;
	orphans: 2;
	margin: 2.3pt 0pt 4.65pt 0pt;
	text-indent: 0pt;
	font-family: 'Arial';
	font-size: 7pt;
	font-weight: bold;
	font-style: normal;
}
BLOCKQUOTE {
	widows: 2;
	orphans: 2;
	margin: 6pt 30pt 6pt 30pt;
	text-indent: 0pt;
	font-family: 'Ebrima';
	font-size: 11pt;
	font-weight: normal;
	font-style: normal;
}
p {
	widows: 2;
	orphans: 2;
	margin: 0pt 0pt 0pt 0pt;
	text-indent: 0pt;
	font-family: 'Arial';
	font-size: 12pt;
	font-weight: normal;
	font-style: normal;
}
.body-text {
	widows: 2;
	orphans: 2;
	margin: 0pt 0pt 0pt 0pt;
	text-indent: 0pt;
	font-family: 'Arial';
	font-size: 12pt;
	font-weight: normal;
	font-style: normal;
}
.Normal {
	widows: 2;
	orphans: 2;
	margin: 0pt 0pt 0pt 0pt;
	text-indent: 0pt;
	font-family: 'Times New Roman';
	font-size: 12pt;
	font-weight: normal;
	font-style: normal;
}
A {
}
PRE {
	widows: 2;
	orphans: 2;
	margin: 0pt 0pt 0pt 0pt;
	text-indent: 0pt;
	font-family: 'Courier New';
	font-size: 12pt;
	font-weight: normal;
	font-style: normal;
}
B {
	font-family: 'Arial';
	font-weight: bold;
}
CAPTION {
	widows: 2;
	orphans: 2;
	margin: 0pt 0pt 0pt 0pt;
	text-indent: 0pt;
	font-family: 'Arial';
	font-size: 12pt;
	font-weight: normal;
	font-style: normal;
}
CODE {
	font-family: 'Courier New';
}
DEL {
}
EM {
	font-family: 'Arial';
	font-style: italic;
}
I {
	font-family: 'Arial';
	font-style: italic;
}
INS {
}
KBD {
	font-family: 'Courier New';
}
MARK {
	widows: 2;
	orphans: 2;
	margin: 0pt 0pt 0pt 0pt;
	text-indent: 0pt;
	font-family: 'Arial';
	font-size: 12pt;
	font-weight: normal;
	font-style: normal;
	background-color: #FFFF00;
}
S {
}
SAMP {
	font-family: 'Courier New';
}
STRIKE {
}
STRONG {
	font-family: 'Arial';
	font-weight: bold;
}
TH {
	widows: 2;
	orphans: 2;
	margin: 0pt 0pt 0pt 0pt;
	text-indent: 0pt;
	font-family: 'Arial';
	font-size: 12pt;
	font-weight: bold;
	font-style: normal;
}
TT {
	font-family: 'Courier New';
}
U {
}
/* ]]&gt; */
&lt;/style&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;h1 style="line-height:100%;"&gt; &lt;/h1&gt;
&lt;h1 style="line-height:100%;"&gt;Das &lt;span style="font-weight:bold;"&gt;**** Superior Baby &amp;amp; Kinderhotel Laurentius&lt;/span&gt; bietet die allerbesten Voraussetzungen für einen &lt;span style="font-weight:bold;"&gt;traumhaften Winter- und Sommerurlaub&lt;/span&gt;, mit einem riesigen Angebot an Wellness, Genuss, Spaß und Sport für die ganze Familie – und das - direkt an den Pisten von Tirols Skidimension Serfaus-Fiss-Ladis.&lt;br /&gt;
&lt;/h1&gt;
&lt;/body&gt;
&lt;/html&gt;</WellcomeText><SubDomainUrl>.https://www.touristscout.com</SubDomainUrl><AngebotsText>&lt;?xml version="1.0" ?&gt;
&lt;html xmlns="https://www.w3.org/1999/xhtml"&gt;
&lt;head&gt;
&lt;meta content="TX24_HTM 24.0.610.500" name="GENERATOR" /&gt;
&lt;title&gt;&lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;h2 style="text-indent:0pt;margin-left:0pt;margin-right:0pt;line-height:100%;"&gt;&lt;span style="font-size:11pt;font-weight:normal;"&gt;Für Ihren Wunschtermin können wir Ihnen wie folgt anbieten:&lt;/span&gt;&lt;/h2&gt;
&lt;/body&gt;
&lt;/html&gt;</AngebotsText><AnreiseInfoText>&lt;?xml version="1.0" ?&gt;
&lt;html xmlns="https://www.w3.org/1999/xhtml"&gt;
&lt;head&gt;
&lt;meta content="TX24_HTM 24.0.610.500" name="GENERATOR" /&gt;
&lt;title&gt;&lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;&lt;span style="font-size:10.2pt;color:#111111;background-color:#FFFFFF;"&gt;EINCHECKEN&lt;/span&gt;&lt;/p&gt;
&lt;p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:2pt;margin-right:0pt;line-height:100%;background-color:#FFFFFF;"&gt;&lt;span style="font-size:10.2pt;color:#999999;"&gt;MO-SO:: ab 15 UHR&lt;/span&gt;&lt;/p&gt;
&lt;p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:2pt;margin-right:0pt;line-height:100%;background-color:#FFFFFF;"&gt; &lt;/p&gt;
&lt;/body&gt;
&lt;/html&gt;</AnreiseInfoText><AbreiseInfoText>&lt;?xml version="1.0" ?&gt;
&lt;html xmlns="https://www.w3.org/1999/xhtml"&gt;
&lt;head&gt;
&lt;meta content="TX24_HTM 24.0.610.500" name="GENERATOR" /&gt;
&lt;title&gt;&lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;&lt;span style="font-size:10.2pt;color:#111111;background-color:#FFFFFF;"&gt;AUSCHECKEN&lt;/span&gt;&lt;/p&gt;
&lt;p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:2pt;margin-right:0pt;line-height:100%;background-color:#FFFFFF;"&gt;&lt;span style="font-size:10.2pt;color:#999999;"&gt;MO-SO: BIS 11 UHR&lt;/span&gt;&lt;/p&gt;
&lt;/body&gt;
&lt;/html&gt;</AbreiseInfoText><FaceBookPageId>GDAT_Facebook_Page_ID</FaceBookPageId><SozMedia_FaceBook_Url>https://www.facebook.com/kinderhotel.laurentius/</SozMedia_FaceBook_Url><SozMedia_GooglePlus_Url>www.googleplus.....</SozMedia_GooglePlus_Url><SozMedia_Pinterest_Url>www.pinterest.....</SozMedia_Pinterest_Url><SozMedia_Instagram_Url>www.instagramm.....</SozMedia_Instagram_Url><SozMedia_YouTube_Url>www.youtube......</SozMedia_YouTube_Url><SozMedia_Twitter_Url>www.twitter....</SozMedia_Twitter_Url><ImageCompressionQuality>50</ImageCompressionQuality></StammDaten><DesignSettings><TemplateName /><primaryColor>#FF80C0</primaryColor><secundaryColor>#0080FF</secundaryColor><textColor>#00FFFF</textColor><backgroundColor>#F7FEC5</backgroundColor><headlineFontName>Ebrima</headlineFontName><bodyFontName>Ebrima</bodyFontName></DesignSettings></ThemeData></API>`