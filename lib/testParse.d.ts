export default function ({ html, options, modulName, test }: {
    html: string;
    options: MailBuilder.BuildOptions;
    modulName: string;
    test: boolean;
}): MailBuilder.Template;
