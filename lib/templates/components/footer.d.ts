export interface FooterSettings {
    unsubscribeText: boolean;
    unsubscribeUrl: boolean;
    name: boolean;
    street: boolean;
    postalCode: boolean;
    city: boolean;
    country: boolean;
    phone: boolean;
    fax: boolean;
    email: boolean;
    website: boolean;
    uid: boolean;
}
export interface Footer {
    unsubscribeText: string;
    unsubscribeUrl: string;
    name: string;
    street: string;
    postalCode: string;
    city: string;
    country: string;
    phone: string;
    fax: string;
    email: string;
    website: string;
    uid: string;
    phoneText?: string;
    emailText?: string;
    websiteText?: string;
    uidText?: string;
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: FooterSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
