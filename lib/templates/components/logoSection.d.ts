export interface LogoSettings {
    key?: string;
    logo?: boolean;
    src?: string;
    width?: number;
    height?: number;
}
export interface Logo {
    title: string;
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: LogoSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
