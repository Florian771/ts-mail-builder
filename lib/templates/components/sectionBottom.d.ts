export interface SectionBottomSettings {
    active: boolean;
    type?: "diagonal";
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: SectionBottomSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
