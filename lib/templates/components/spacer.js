"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
var config_1 = require("../../config");
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    var html = "";
    var text = "";
    text = "" + config_1.textBreak;
    html = "\n  <mj-section padding=\"20px 20px 0px 20px\" background-color=\"{backgroundColor2}\"></mj-section>";
    testParse_1.default({ test: true, html: html, options: options, modulName: "spacer" });
    return {
        html: html,
        text: text,
        translations: [],
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=spacer.js.map