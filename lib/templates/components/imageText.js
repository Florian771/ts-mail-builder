"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
var config_1 = require("../../config");
var utils_1 = require("../../utils");
var placeholder = {
    alt: "{IMAGE_ALT_KEY}",
    tel: "{CONTENT_TEL}",
    email: "{CONTENT_EMAIL}"
};
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    // console.log("mailBuilder - imageText - settings.src", settings.src)
    var translations = utils_1.getTranslations({
        placeholder: placeholder,
        settings: settings
    });
    var html = "";
    var text = "";
    var width = settings.width ? settings.width : 600;
    // const height: number = settings.height ? settings.height : 320
    if (settings.src) {
        text = "" + config_1.textBreak;
        html = "<mj-section padding=\"0px\" background-color=\"{backgroundColor2}\">\n    <mj-column width=\"15%\">\n    </mj-column>\n    <mj-column width=\"25%\">\n        <mj-image align=\"center\" padding=\"0px\" width=\"" + width + "px\" src=\"" + settings.src + "\" alt=\"" + placeholder.alt.replace("KEY", settings.key) + "\" align=\"center\" border=\"none\"></mj-image>\n        </mj-column>\n        <mj-column width=\"60%\" padding=\"30px 0px 0px 0px\">\n\n        <mj-text padding=\"0\" font-weight=\"300\" font-size=\"24px\" line-height=\"32px\">\n          <div class=\"mobileTextCenter\">\n            <a href=\"tel:" + placeholder.tel.replace("KEY", settings.key) + "\" style=\"color:{textColor};text-decoration:none;\">" + placeholder.tel.replace("KEY", settings.key) + "</a>\n          </div>\n        </mj-text>\n        <mj-text padding=\"0\">\n          <div class=\"mobileTextCenter\">\n          <a href=\"mailto:" + placeholder.email.replace("KEY", settings.key) + "\" style=\"color:{textColor};text-decoration:none;\">" + placeholder.email.replace("KEY", settings.key) + "</a>\n          </div>\n        </mj-text>\n      </mj-column>\n    </mj-section>";
        // console.log("html: ", html)
        testParse_1.default({
            test: true,
            html: html,
            options: options,
            modulName: "imageText"
        });
    }
    return {
        html: html,
        text: text,
        translations: translations,
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=imageText.js.map