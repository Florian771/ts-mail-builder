import testParse from "../../testParse"
// import { getTranslations } from "../../utils"

export interface CarouselDesignerSettings {
  key: string
  items: number
}
export interface CarouselDesigner {
  src: string
}

const placeholder: CarouselDesigner = {
  src: "{CAROUSEL_SRC_KEY_XXX}"
}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: CarouselDesignerSettings
}): MailBuilder.Component => {
  let html: string = ""
  let text: string = ""
  text = ``
  html = `
  <mj-section padding="20px 20px 20px 20px" background-color="{backgroundColor2}">
  <mj-column>
    <mj-carousel css-class="${
      settings.key
    }" border-radius="0" tb-border-radius="0" right-icon="{urlWeb}/arrow-right.png" left-icon="{urlWeb}/arrow-left.png">`
  if (settings.items && settings.items > 0) {
    for (let index: number = 0; index < settings.items; index++) {
      html += `<mj-carousel-image border-radius="0" tb-border-radius="0" src="${placeholder.src
        .replace("KEY", settings.key)
        .replace("XXX", String(index))}" />`
    }
  }

  html += `</mj-carousel>
  </mj-column>
</mj-section>`

  testParse({
    test: true,
    html: html,
    options: options,
    modulName: "carouselDesigner"
  })
  return {
    html: html,
    text: text,
    translations: [],
    parse: (args: any) => testParse(args)
  }
}
