import testParse from "../../testParse"
import { textBreak } from "../../config"
import { getTranslations } from "../../utils"

export interface FooterSettings {
  unsubscribeText: boolean
  unsubscribeUrl: boolean
  name: boolean
  street: boolean
  postalCode: boolean
  city: boolean
  country: boolean
  phone: boolean
  fax: boolean
  email: boolean
  website: boolean
  uid: boolean
}

export interface Footer {
  unsubscribeText: string
  unsubscribeUrl: string
  name: string
  street: string
  postalCode: string
  city: string
  country: string
  phone: string
  fax: string
  email: string
  website: string
  uid: string
  phoneText?: string
  emailText?: string
  websiteText?: string
  uidText?: string
}

const placeholder: Footer = {
  unsubscribeText: "{UNSUBSCRIBE_TEXT}",
  unsubscribeUrl: "{UNSUBSCRIBE_URL}",
  name: "{COMPANY_NAME}",
  street: "{COMPANY_STREET}",
  postalCode: "{COMPANY_POSTAL_CODE}",
  city: "{COMPANY_CITY}",
  country: "{COMPANY_COUNTRY}",
  phone: "{COMPANY_PHONE}",
  fax: "{COMPANY_FAX}",
  email: "{COMPANY_EMAIL}",
  website: "{COMPANY_WEBSITE}",
  uid: "{COMPANY_UID}"
}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: FooterSettings
}): MailBuilder.Component => {
  const translations: string[] = getTranslations({
    placeholder: placeholder,
    settings: settings
  })
  let html: string = ""
  let text: string = ""
  text = `${textBreak}`
  html = `<mj-section padding="10px 0 20px 0">
  <mj-column>
    <mj-text align="center" color="{textColor}" font-size="11px">`

  if (settings.name) {
    text += `${placeholder.name}${textBreak}`
    html += `
    <br><b>${placeholder.name}</b>`
  }

  text += `${textBreak}`
  html += `<br>`

  if (settings.street) {
    text += `{TEXT_STREET}: ${placeholder.street}${textBreak}`
    html += `${placeholder.street}`
  }

  if (settings.postalCode) {
    text += `{TEXT_POSTAL_CODE}: ${placeholder.postalCode}${textBreak}`
    if (settings.street) {
      html += ` / `
    }
    html += `${placeholder.postalCode}`
  }

  if (settings.city) {
    text += `{TEXT_CITY}: ${placeholder.city}${textBreak}`
    if (settings.street || settings.postalCode) {
      html += ` / `
    }
    html += `${placeholder.city}`
  }

  if (settings.country) {
    text += `{TEXT_CITY}: ${placeholder.country}${textBreak}`
    if (settings.street || settings.postalCode || settings.city) {
      html += ` / `
    }
    html += `${placeholder.country}`
  }

  text += `${textBreak}`
  html += `<br>`

  if (settings.phone) {
    text += `{TEXT_PHONE}: ${placeholder.phone}${textBreak}`
    html += `{TEXT_PHONE}: ${placeholder.phone}`
  }

  if (settings.email) {
    text += `${placeholder.email}`
    if (settings.phone) {
      html += ` / `
    }
    html += `{TEXT_EMAIL}: <a style="color: {textColor};" href="mailto:${
      placeholder.email
    }">${placeholder.email}</a>`
  }

  text += `${textBreak}`
  html += `<br>`

  if (settings.website) {
    text += `{TEXT_WEBSITE}: ${placeholder.website}${textBreak}`
    html += `<a style="color:{textColor};" href="${placeholder.website}">${
      placeholder.website
    }</a>`
  }

  if (settings.uid) {
    text += `{TEXT_UID}: ${placeholder.uid}${textBreak}`
    html += `<br>{TEXT_UID}: ${placeholder.uid}`
  }

  text += `${textBreak}`
  html += `<br>`

  text += `${textBreak}`
  html += `<br>`

  if (settings.unsubscribeUrl) {
    text += `${placeholder.unsubscribeText}: ${textBreak}${
      placeholder.unsubscribeUrl
    }${textBreak}`
    html += `<a href="${
      placeholder.unsubscribeUrl
    }" style="color: {textColor};">${placeholder.unsubscribeText}</a><br>`
  } else if (settings.unsubscribeText) {
    text += `${placeholder.unsubscribeText}${textBreak}`
    html += `<div style="color: {textColor};">${
      placeholder.unsubscribeText
    }</div><br>`
  }

  html += `</mj-text>
  </mj-column>
</mj-section>`

  testParse({ test: true, html: html, options: options, modulName: "footer" })
  return {
    html: html,
    text: text,
    translations: translations,
    parse: (args: any) => testParse(args)
  }
}
