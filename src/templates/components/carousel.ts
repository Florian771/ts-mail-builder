import testParse from "../../testParse"
// import { getTranslations } from "../../utils"

export interface CarouselSettings {
  key: string
  images: string[]
}
export interface Carousel {
  images: string[]
}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: CarouselSettings
}): MailBuilder.Component => {
  let html: string = ""
  let text: string = ""
  text = ``
  html = `
  <mj-section padding="20px 20px 0 20px" background-color="{backgroundColor2}">
  <mj-column>
    <mj-carousel css-class="${
      settings.key
    }" border-radius="0" tb-border-radius="0" right-icon="{urlWeb}/arrow-right.png" left-icon="{urlWeb}/arrow-left.png">`
  if (settings.images && settings.images.length > 0) {
    settings.images.map((img: string) => {
      html += `<mj-carousel-image border-radius="0" tb-border-radius="0" src="${img}" />`
    })
  }

  html += `</mj-carousel>
  </mj-column>
</mj-section>`

  testParse({ test: true, html: html, options: options, modulName: "carousel" })
  return {
    html: html,
    text: text,
    translations: [],
    parse: (args: any) => testParse(args)
  }
}
