import * as templateBuilder from "./templateBuilder"
import replacerContent from "./replacerContent"

export function createLink({
  text,
  url
}: MailBuilder.MailLink): MailBuilder.MailLink {
  const button: MailBuilder.MailLink = {
    type: "button",
    url: url,
    text: text
  }
  button.textLink = text + ": " + url
  return button
}

export async function composeEmail(
  mailContentsOutput: MailBuilder.MailContentsOutput
): Promise<MailBuilder.ComposeMailResult> {
  // GET TEMPLATE html, text AND TEMPLATE BUILD errors
  const template: MailBuilder.TemaplateBuilderResult = await templateBuilder.htmlOutput(
    {
      mailContentsOutput: mailContentsOutput
    }
  )
  // console.log("template", template)

  let mail: MailBuilder.Mail = {
    to: mailContentsOutput.options.to,
    from: mailContentsOutput.options.from,
    subject: mailContentsOutput.subject,
    html: template.html,
    text: template.text
    /* html: mailContentsOutput.html,
    text: mailContentsOutput.text */
  }

  mail = replacerContent({
    mail: mail,
    mailContentsOutput: mailContentsOutput
  })

  return { mail: mail, translations: template.translations }
}
