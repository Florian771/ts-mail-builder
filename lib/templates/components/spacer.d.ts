export interface SpacerSettings {
    key?: string;
}
export interface Spacer {
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: SpacerSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
