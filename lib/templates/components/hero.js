"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
var config_1 = require("../../config");
var utils_1 = require("../../utils");
var placeholder = {
    headline: "{HERO_HEADLINE_KEY}",
    buttonText: "{HERO_BUTTON_TEXT_KEY}",
    buttonUrl: "{HERO_BUTTON_URL_KEY}"
};
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    var translations = utils_1.getTranslations({
        placeholder: placeholder,
        settings: settings
    });
    var html = "";
    var text = "";
    text = "" + placeholder.headline.replace("KEY", settings.key) + config_1.textBreak + "\n  " + placeholder.buttonText.replace("KEY", settings.key) + ": " + placeholder.buttonUrl.replace("KEY", settings.key) + config_1.textBreak + "\n  ";
    html = "\n  <mj-hero\n  mode=\"fixed-height\"\n  height=\"320px\"\n  background-width=\"600px\"\n  background-height=\"320px\"\n  background-url=\"" + settings.imgUrl + "\"\n  background-color=\"{primaryColor}\"\n  padding=\"0px\">\n  <mj-text\n    padding=\"180px 20px 20px 20px\"\n    color=\"#ffffff\"\n    font-family=\"Helvetica\"\n    align=\"center\"\n    font-size=\"45px\"\n    line-height=\"45px\"\n    font-weight=\"900\">\n    " + placeholder.headline.replace("KEY", settings.key) + "\n  </mj-text>\n\n  <mj-button padding-bottom=\"20px\" padding-top=\"0px\" align=\"center\" background-color=\"{secundaryColor}\" color=\"{sec_textColor}\" border-radius=\"0px\" href=\"" + placeholder.buttonUrl.replace("KEY", settings.key) + "\" inner-padding=\"15px 30px\">\n  " + placeholder.buttonText.replace("KEY", settings.key) + "\n  </mj-button>\n\n</mj-hero>";
    testParse_1.default({ test: true, html: html, options: options, modulName: "hero" });
    return {
        html: html,
        text: text,
        translations: translations,
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=hero.js.map