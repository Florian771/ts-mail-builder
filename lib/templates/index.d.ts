export declare function getHtmlFromTemplateType({ templateType, options }: {
    templateType: MailBuilder.TemplateType;
    options: MailBuilder.BuildOptions;
}): Promise<MailBuilder.Component>;
