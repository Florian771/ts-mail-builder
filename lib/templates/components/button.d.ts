export interface ButtonSettings {
    key: string;
}
export interface Button {
    buttonText: string;
    buttonUrl: string;
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: ButtonSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
