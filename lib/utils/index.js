"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getHtmlFileName(_a) {
    var options = _a.options, type = _a.type;
    var fileName = "index";
    if (options.name !== "chatify") {
        fileName += "_" + options.name;
    }
    if (type === "pebuild") {
        fileName += "_" + type;
    }
    return fileName;
}
exports.getHtmlFileName = getHtmlFileName;
function replaceKey(_a) {
    var text = _a.text, settings = _a.settings;
    if (settings.key) {
        return text.replace("KEY", settings.key);
    }
    return text;
}
function replaceIndex(_a) {
    var translations = _a.translations, text = _a.text, settings = _a.settings;
    if (text) {
        var updatedText = replaceKey({ text: text, settings: settings });
        if (settings.length && settings.length > 0) {
            for (var index = 0; index < settings.length; index++) {
                translations.push(updatedText.replace("XXX", String(index)));
            }
        }
        else {
            translations.push(updatedText);
        }
    }
    return translations;
}
function getTranslations(_a) {
    var placeholder = _a.placeholder, settings = _a.settings;
    var translations = [];
    Object.keys(placeholder).forEach(function (key) {
        if (typeof placeholder[key] === "object") {
            Object.keys(placeholder[key]).forEach(function (key2) {
                if (typeof placeholder[key][key2] === "object") {
                    Object.keys(placeholder[key][key2]).forEach(function (key3) {
                        if (placeholder[key][key2][key3]) {
                            translations = replaceIndex({
                                translations: translations,
                                text: placeholder[key][key2][key3],
                                settings: settings
                            });
                        }
                    });
                }
                else {
                    if (placeholder[key][key2]) {
                        translations = replaceIndex({
                            translations: translations,
                            text: placeholder[key][key2],
                            settings: settings
                        });
                    }
                }
            });
        }
        else {
            if (placeholder[key]) {
                translations = replaceIndex({
                    translations: translations,
                    text: placeholder[key],
                    settings: settings
                });
            }
        }
    });
    return translations;
}
exports.getTranslations = getTranslations;
//# sourceMappingURL=index.js.map