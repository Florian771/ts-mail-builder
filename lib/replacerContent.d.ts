export declare function replaceTextAndHtml({ mail, from, to }: {
    mail: MailBuilder.Mail;
    from: string;
    to: string;
}): {
    text: string;
    html: string;
    subject: string;
    from: string;
    to: string;
    options?: import("../../mail-builder-types").BuildOptions;
};
export default function replacer({ mail, mailContentsOutput }: {
    mail: MailBuilder.Mail;
    mailContentsOutput: MailBuilder.MailContentsOutput;
}): MailBuilder.Mail;
