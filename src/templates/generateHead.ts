export function generateHead({ font }: { font: string }): string {
  return `<mj-head>
<mj-style>
    @media (max-width:480px) {
      .mobileHidden {
        display: none;
      }
    }
    @media (max-width:480px) {
      .mobileTextCenter {
        text-align: center;
      }
    }
  </mj-style>
        <mj-title></mj-title>
        <mj-font name="Montserrat" href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500"></mj-font>
        <mj-attributes>
          <mj-all font-family="${font}"></mj-all>
          <mj-text font-weight="400" font-size="16px" color="{textColor}" line-height="24px"></mj-text>
          <mj-section padding="0px"></mj-section>
        </mj-attributes>
      </mj-head>`
}
export default generateHead
