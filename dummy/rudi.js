export default {
  API: {
    HotelData: {
      Ort: "Grödig",
      HotelName: "Musterhotel Saalbacher Hof",
      PersName: "Demo",
      Strasse: "Friedensstr. 8",
      Gruss: "",
      PLZ: "5082",
      Telefon: "0043 6246 73873",
      Fax: "0043 6246 73873 42",
      UID: "ATU44806304",
      Nation: "A",
      KtoNr: "12345679",
      Bank: "Volksbank",
      BLZ: "45010",
      Email: "office@gastrodat.com",
      BIC: "VBOEATWWSAL",
      IBAN: "AT123456789"
    },
    ReservationData: {
      LFNUMMER: "209",
      ZIMMER_NR: "6",
      PERS_NR: "4",
      PERSON: "Huber Martin",
      VON: "05.03.2018",
      BIS: "10.03.2018",
      ANGEBOTSURL:
        "https://apps.weratech-online.com/wtOnlineBooking/Home/CreateOnlineReservation/NTMwMg2?reservationid=546051006030&guestid=57C0&lang=DE"
    },
    GuestData: {
      LFNUMMER: "4",
      ANREDE_H: "Herrn",
      FAMNAME: "Huber",
      VORNAME: "Martin",
      TITEL: "",
      GESCHLECHT: "M",
      STRASSE: "Salzgasse 10",
      PLZ: "4020",
      ORT: "Linz",
      ANREDE_FAM: "Sehr geehrter Herr Huber",
      SPRACHE: "D Deutsch",
      NATION: "A",
      ZUSNAME: "",
      P_EMAIL: "huber@aon.at",
      NATION_TXT: ""
    },
    RoomData: {
      ZIMMER_NR: "6",
      ZIMMER_NAME: "006",
      GEEIGNET_VON: "2",
      GEEIGNET_BIS: "4",
      ZI_KAT: "1",
      ZIMMER_SAUBER: "N",
      GEEIGNET_STD: "2",
      STANDARDLEISTUNG: "10019,",
      TEXT1: "",
      TEXT2: "",
      ZIMMERBILD:
        "https://www.weratech-files.com/images/280942/gastrodat/Zimmer/Zimmer_Bild_6.jpg?timestamp=636439197780000000",
      ZIMMERGRUNDRISS:
        "https://www.weratech-files.com/images/280942/gastrodat/Zimmer/Grundriss/ZIMMER_GRUNDRISS_6.jpg?timestamp=636439197790000000"
    },
    ExtrasData: "",
    ThemeData: {
      Themes: {
        gdatTheme: {
          theme_pics: {
            gdatPicture: [
              {
                PicId: "1e7873dd5a439",
                PicName: "SKI EINFACH <n>ZUM</n> GENIESSEN",
                PicPath:
                  "$i_offer\\theme_Winter und Herbstsaisonen\\1e7873dd5a439.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/280942/gastrodat/1e7873dd5a439.jpg",
                PicHTML_Link: "https://www.gastrodat.com",
                PicKategorie: "Skifahren"
              },
              {
                PicId: "1e787497c6870",
                PicName: "EINFACH WOHLFÜHLEN",
                PicPath:
                  "$i_offer\\theme_Winter und Herbstsaisonen\\1e787497c6870.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/280942/gastrodat/1e787497c6870.jpg",
                PicHTML_Link: "",
                PicKategorie: "Wohnen"
              },
              {
                PicId: "1ed2dc77b9e5d",
                PicName: "Direkt an der SKIPISTE",
                PicPath: "$i_offer\\theme_Wintersaisonen\\1ed2dc77b9e5d.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/280942/gastrodat/1ed2dc77b9e5d.jpg",
                PicHTML_Link: "",
                PicKategorie: "Skifahren"
              },
              {
                PicId: "2056550e06e72",
                PicName: "Tradition",
                PicPath:
                  "$i_offer\\theme_Winter und Herbstsaisonen\\2056550e06e72.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/280942/gastrodat/2056550e06e72.jpg",
                PicHTML_Link: "",
                PicKategorie: "Tradition"
              },
              {
                PicId: "2056bfa69faa7",
                PicName: "Gemütlichkeit",
                PicPath:
                  "$i_offer\\theme_Winter und Herbstsaisonen\\2056bfa69faa7.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/280942/gastrodat/2056bfa69faa7.jpg",
                PicHTML_Link: "",
                PicKategorie: ""
              },
              {
                PicId: "206fb66441bb7",
                PicName: "Wellness nach dem Sporteln....",
                PicPath:
                  "$i_offer\\theme_Winter und Herbstsaisonen\\206fb66441bb7.JPG",
                PicUrl:
                  "https://www.weratech-files.com/images/280942/gastrodat/206fb66441bb7.JPG",
                PicHTML_Link: "",
                PicKategorie: ""
              },
              {
                PicId: "206fd9d37342f",
                PicName: "Sommer im Zillertal ",
                PicPath:
                  "$i_offer\\theme_Winter und Herbstsaisonen\\206fd9d37342f.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/280942/gastrodat/206fd9d37342f.jpg",
                PicHTML_Link: "",
                PicKategorie: ""
              }
            ]
          },
          theme_teasers: {
            gdatTeaser: [
              {
                Teaser_pic: {
                  PicId: "1e85a6bb31cfe",
                  PicName: "Inmitten von Weinbergen in Lermoos in Tirol",
                  PicPath: "$i_offer\\teaser_DURCHATMEN\\1e85a6bb31cfe.jpg",
                  PicUrl:
                    "https://www.weratech-files.com/images/280942/gastrodat/1e85a6bb31cfe.jpg",
                  PicHTML_Link: "https://www.gastrodat.com",
                  PicKategorie: ""
                },
                validFrom: "2017-09-13T11:16:42.8311806+02:00",
                validTo: "2017-12-13T11:16:42.8311806+01:00",
                SuchwortListe: "",
                TeaserId: "1e85a6bb31cfe",
                TeaserName: "DURCHATMEN"
              },
              {
                Teaser_pic: {
                  PicId: "2056610d032af",
                  PicName:
                    "Drei Generationen unter einem Dach. Da prallen hin und wieder Welten aufeinander. Nun liegt eine gewisse Sturheit zweifelsohne im Tiroler Naturell. Ganz zu schweigen vom kämpferischen Temperament ...",
                  PicPath:
                    "$i_offer\\teaser_neuer ThemenTeaser 2056610d032af\\2056610d032af.jpg",
                  PicUrl:
                    "https://www.weratech-files.com/images/280942/gastrodat/2056610d032af.jpg",
                  PicHTML_Link:
                    "https://www.perauer.at/de/diskussion-auf-tirolerisch.html",
                  PicKategorie: ""
                },
                validFrom: "2017-10-20T10:23:08.0304303+02:00",
                validTo: "2018-01-20T10:23:08.0304303+01:00",
                SuchwortListe: "",
                TeaserId: "2056610d032af",
                TeaserName: "Diskussion auf Tirolerisch"
              },
              {
                Teaser_pic: {
                  PicId: "2056677431490",
                  PicName:
                    "Die Jagd hat bei den Perauers lange Tradition. Von jeher beschert sie Vater und Sohn ein verbindendes Erlebnis im heimischen Wald ...",
                  PicPath:
                    "$i_offer\\teaser_neuer ThemenTeaser 2056677431490\\2056677431490.jpg",
                  PicUrl:
                    "https://www.weratech-files.com/images/280942/gastrodat/2056677431490.jpg",
                  PicHTML_Link: "",
                  PicKategorie: ""
                },
                validFrom: "2017-11-20T10:25:59",
                validTo: "2017-11-30T10:25:59",
                SuchwortListe: "",
                TeaserId: "2056677431490",
                TeaserName: "Wildwochen"
              }
            ]
          },
          validFrom: "2017-06-11T15:09:55.5028897+02:00",
          validTo: "2017-12-20T15:09:55",
          SuchwortListe: "",
          ThemeId: "5",
          ThemeName: "Winter und Herbstsaisonen"
        }
      },
      BilderGalerie_pics: {
        gdatPicture: [
          {
            PicId: "1f845a7170af8",
            PicName: "Hot Stone",
            PicPath: "$i_offer\\impressions\\1f845a7170af8.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/280942/gastrodat/1f845a7170af8.jpg",
            PicHTML_Link: "",
            PicKategorie: "Wellness"
          },
          {
            PicId: "2056bdf42f2c5",
            PicName: "Gemütlichkeit",
            PicPath: "$i_offer\\impressions\\2056bdf42f2c5.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/280942/gastrodat/2056bdf42f2c5.jpg",
            PicHTML_Link: "",
            PicKategorie: "Wohnen"
          },
          {
            PicId: "2056d314762f3",
            PicName: "Sandra",
            PicPath: "$i_offer\\impressions\\2056d314762f3.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/280942/gastrodat/2056d314762f3.jpg",
            PicHTML_Link: "",
            PicKategorie: "Team"
          },
          {
            PicId: "2056d41b58277",
            PicName: "Monika",
            PicPath: "$i_offer\\impressions\\2056d41b58277.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/280942/gastrodat/2056d41b58277.jpg",
            PicHTML_Link: "",
            PicKategorie: "Team"
          },
          {
            PicId: "2105346b0664f",
            PicName: "saalbacherhof",
            PicPath: "$i_offer\\impressions\\2105346b0664f.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/280942/gastrodat/2105346b0664f.jpg",
            PicHTML_Link: "",
            PicKategorie: "Wohnen"
          }
        ]
      },
      impression_pics: {
        gdatPicture: [
          {
            PicId: "1e7873dd5a439",
            PicName: "SKI EINFACH <n>ZUM</n> GENIESSEN",
            PicPath:
              "$i_offer\\theme_Winter und Herbstsaisonen\\1e7873dd5a439.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/280942/gastrodat/1e7873dd5a439.jpg",
            PicHTML_Link: "https://www.gastrodat.com",
            PicKategorie: "Skifahren"
          },
          {
            PicId: "1e787497c6870",
            PicName: "EINFACH WOHLFÜHLEN",
            PicPath:
              "$i_offer\\theme_Winter und Herbstsaisonen\\1e787497c6870.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/280942/gastrodat/1e787497c6870.jpg",
            PicHTML_Link: "",
            PicKategorie: "Wohnen"
          },
          {
            PicId: "1ed2dc77b9e5d",
            PicName: "Direkt an der SKIPISTE",
            PicPath: "$i_offer\\theme_Wintersaisonen\\1ed2dc77b9e5d.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/280942/gastrodat/1ed2dc77b9e5d.jpg",
            PicHTML_Link: "",
            PicKategorie: "Skifahren"
          },
          {
            PicId: "2056550e06e72",
            PicName: "Tradition",
            PicPath:
              "$i_offer\\theme_Winter und Herbstsaisonen\\2056550e06e72.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/280942/gastrodat/2056550e06e72.jpg",
            PicHTML_Link: "",
            PicKategorie: "Tradition"
          },
          {
            PicId: "20566e9de74a1",
            PicName: "Wildwochen 1",
            PicPath: "$i_offer\\theme_Wildwochen\\20566e9de74a1.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/280942/gastrodat/20566e9de74a1.jpg",
            PicHTML_Link: "",
            PicKategorie: "Tradition"
          },
          {
            PicId: "20566f719bac7",
            PicName: "Wildwochen 2",
            PicPath: "$i_offer\\theme_Wildwochen\\20566f719bac7.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/280942/gastrodat/20566f719bac7.jpg",
            PicHTML_Link: "",
            PicKategorie: "Tradition"
          },
          {
            PicId: "1f845a7170af8",
            PicName: "Hot Stone",
            PicPath: "$i_offer\\impressions\\1f845a7170af8.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/280942/gastrodat/1f845a7170af8.jpg",
            PicHTML_Link: "",
            PicKategorie: "Wellness"
          },
          {
            PicId: "2056bdf42f2c5",
            PicName: "Gemütlichkeit",
            PicPath: "$i_offer\\impressions\\2056bdf42f2c5.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/280942/gastrodat/2056bdf42f2c5.jpg",
            PicHTML_Link: "",
            PicKategorie: "Wohnen"
          },
          {
            PicId: "2056d314762f3",
            PicName: "Sandra",
            PicPath: "$i_offer\\impressions\\2056d314762f3.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/280942/gastrodat/2056d314762f3.jpg",
            PicHTML_Link: "",
            PicKategorie: "Team"
          },
          {
            PicId: "2056d41b58277",
            PicName: "Monika",
            PicPath: "$i_offer\\impressions\\2056d41b58277.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/280942/gastrodat/2056d41b58277.jpg",
            PicHTML_Link: "",
            PicKategorie: "Team"
          },
          {
            PicId: "2105346b0664f",
            PicName: "saalbacherhof",
            PicPath: "$i_offer\\impressions\\2105346b0664f.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/280942/gastrodat/2105346b0664f.jpg",
            PicHTML_Link: "",
            PicKategorie: "Wohnen"
          }
        ]
      },
      AngebotsName: "lalala",
      StammDaten: {
        HotelLogo_pic: {
          PicId: "1ecfce182b605",
          PicName: "HotelLogo",
          PicPath: "$i_offer\\LOGO\\StammDaten_HotelLogo_pic.jpg",
          PicUrl:
            "https://www.weratech-files.com/images/280942/gastrodat/StammDaten_HotelLogo_pic.jpg",
          PicHTML_Link: ""
        },
        Section_Liste: {
          Section: [
            { ModulName: "ZUSATZ_TEXT2", ModulId: "" },
            { ModulName: "ZUSATZ_TEXT1", ModulId: "" },
            { ModulName: "ZUSATZ_TEXT3", ModulId: "" },
            { ModulName: "WELCOME_TEXT", ModulId: "" },
            { ModulName: "TEASER_BLOCK", ModulId: "" },
            { ModulName: "ANGEBOTS_TEXT", ModulId: "" },
            { ModulName: "VARIANTEN_VORSCHAU", ModulId: "" },
            { ModulName: "ANGEBOT_DETAILS", ModulId: "" },
            { ModulName: "IMPRESSIONEN", ModulId: "" },
            { ModulName: "SLIDESHOW", ModulId: "" },
            { ModulName: "ANREISE_BESCHREIBUNG", ModulId: "" },
            { ModulName: "NACHRICHTEN_BLOCK", ModulId: "" },
            { ModulName: "IMPRESSUM", ModulId: "" }
          ]
        },
        EinzelText_Liste: {
          EinzelText: [
            { Tag: "", Text: "Text zu " },
            { Tag: "gdat_chatten", Text: "Text zu gdat_chatten" },
            { Tag: "gdat_details", Text: "Details" },
            { Tag: "gdat_variante", Text: "Variante" },
            { Tag: "gdat_angebot", Text: "Angebot" },
            { Tag: "gdat_jetzt_buchen", Text: "Jetzt buchen" },
            { Tag: "gdat_personen", Text: "Personen" },
            { Tag: "gdat_datum", Text: "Datum" },
            { Tag: "gdat_naechte", Text: "Nächte" },
            { Tag: "gdat_preis", Text: "Preis" },
            { Tag: "gdat_gesamt", Text: "GESAMT" },
            { Tag: "gdat_impressionen", Text: "IMPRESSIONEN" },
            {
              Tag: "gdat_machen_sie_sich_ein_bild",
              Text: "MACHEN SIE SICH EIN BILD"
            },
            { Tag: "gdat_kontakt", Text: "KONTAKT" },
            {
              Tag: "gdat_wir_freuen_uns_von_ihnen_zu_hoeren",
              Text: "Wir freuen uns von Ihnen zu hören"
            },
            { Tag: "gdat_telefon", Text: "Telefon" },
            { Tag: "gdat_email", Text: "E-Mail" },
            { Tag: "gdat_adresse", Text: "Addresse" },
            { Tag: "gdat_karte_anzeigen", Text: "Karte anzeigen" },
            { Tag: "gdat_route_berechnen", Text: "Route berechnen" },
            { Tag: "gdat_name", Text: "Name" },
            { Tag: "gdat_nachricht", Text: "Nachricht" },
            {
              Tag: "gdat_input:name_hinweis",
              Text: "Bitte geben Sie Ihren Namen an"
            },
            {
              Tag: "gdat_input:email_hinweis",
              Text: "Bitte geben Sie Ihre E-Mail an"
            },
            {
              Tag: "gdat_textarea_nachricht_hinweis",
              Text: "Bitte geben Sie Ihren Namen an"
            },
            { Tag: "gdat_button_nachricht_senden", Text: "Nachricht senden" },
            {
              Tag: "gdat_page_loading_error",
              Text:
                "Hoppla! Die von Ihnen gesuchte Seite ist zur Zeit nicht verfügbar! Bitte laden Sie es neu, versuchen Sie es später oder melden Sie es. Wir werden es so schnell wie möglich beheben."
            },
            { Tag: "gdat_reload", Text: "Neu laden" },
            { Tag: "gdat_report", Text: "Melde dies" },
            { Tag: "gdat_newsletter", Text: "Newsletter" },
            {
              Tag: "gdat_input_newsletter_anmelden",
              Text: "Kostenlos anmelden"
            },
            { Tag: "gdat_zum_seitenanfang", Text: "Zum Seitenanfang" },
            { Tag: "gdat_please_choose", Text: "Bitte wählen" },
            { Tag: "gdat_highlights", Text: "Highlights" },
            { Tag: "gdat_angebot_ansehen", Text: "Angebot ansehen" },
            { Tag: "gat_anreise_abreise", Text: "Anreise/Abreise" },
            {
              Tag: "gdat_preis_pro_person_und_nacht",
              Text: "Preis pro Tag/Pers,"
            },
            { Tag: "gdat_jetzt_anfragen", Text: "jetzt anfragen" },
            { Tag: "gdat_weitere_bilder", Text: "weitere Bilder" }
          ]
        },
        HotelName: "Ihr Muster-Hotel Angebot",
        WellcomeText:
          '<?xml version="1.0" ?>\r<html xmlns="https://www.w3.org/1999/xhtml">\r<head>\r<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r<title></title>\r</head>\r<body>\r<h1 style="">Ihr Urlaubsangebot: </h1>\r<p style="">Die feine Küche unseres urigen Gasthofs ist in aller Munde. Denn nicht nur Gäste, sondern auch viele Einheimische lassen sich die köstlichen uunseres Küchenteams genüsslich auf der Zunge zergehen. <br />\r<br />\rWer einmal hier war, kommt immer wieder. Und wer gerade nicht hier ist, erzählt hoffentlich überall herum, wie gut es bei uns schmeckt :-) Der Name Perauer steht für Qualität. Das gilt fürs Lachsfilet im Gemüsebett genauso wie für den Gast im Kuschelbett ...</p>\r<h2 style=""> lalala</h2>\r<p style=""> </p>\r<p style="">Mit lieben Grüßen aus Grödig und bis bald!</p>\r<p style="">Sabine, Alois, Gerline und das - ReceptionsTeam<br />\rIhre Familie Maier und das Musterhotel Team</p>\r</body>\r</html>',
        SubDomainUrl: ".https://www.touristscout.com",
        AngebotsText:
          '<?xml version="1.0" ?>\r<html xmlns="https://www.w3.org/1999/xhtml">\r<head>\r<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r<title></title>\r</head>\r<body>\r<p lang="de-AT" style="">das ist der mitteltext des angebotes</p>\r</body>\r</html>',
        AnreiseInfoText:
          '<?xml version="1.0" ?>\r<html xmlns="https://www.w3.org/1999/xhtml">\r<head>\r<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r<title></title>\r</head>\r<body>\r<p style="">EINCHECKEN</p>\r<p style="">MO-FR: BIS 11 UHR</p>\r<p style="">SA: BIS 17:30 UHR</p>\r</body>\r</html>',
        AbreiseInfoText:
          '<?xml version="1.0" ?>\r<html xmlns="https://www.w3.org/1999/xhtml">\r<head>\r<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r<title></title>\r</head>\r<body>\r<p style="">AUSCHECKEN</p>\r<p style="">MO-FR: BIS 10 UHR</p>\r<p style="">SA: BIS 12 UHR</p>\r</body>\r</html>',
        ZusatzText1:
          '<?xml version="1.0" ?>\r<html xmlns="https://www.w3.org/1999/xhtml">\r<head>\r<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r<title></title>\r</head>\r<body>\r<p style="">Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1</p>\r<p style="">Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1</p>\r<p style="">Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1</p>\r<p style="">Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1</p>\r<p style="">Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1</p>\r<p style="">Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1Zusatztext1</p>\r<p style="">Zusatztext1Zusatztext1</p>\r</body>\r</html>',
        ZusatzText1_Header: "Zusatztext1",
        ZusatzText1_Collapsed: "false",
        ZusatzText2:
          '<?xml version="1.0" ?>\r<html xmlns="https://www.w3.org/1999/xhtml">\r<head>\r<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r<title></title>\r</head>\r<body>\r<p style="">zustext2</p>\r</body>\r</html>',
        ZusatzText2_Header: "",
        ZusatzText2_Collapsed: "false",
        ZusatzText3:
          '<?xml version="1.0" ?>\r<html xmlns="https://www.w3.org/1999/xhtml">\r<head>\r<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r<title></title>\r</head>\r<body>\r<p lang="de-AT" style="">das ist der zusatztext3</p>\r<p lang="de-AT" style=""> </p>\r<h1 lang="de-AT" style="">header1</h1>\r<p lang="de-AT" style=""> </p>\r<h3 lang="de-AT" style="">header3</h3>\r<p lang="de-AT" style=""> </p>\r<ul style="margin:0pt;padding:0pt;list-style-type:disc;">\r<li style="font-size:9pt;color:#000000;">\r<ul style=""><span style="color:#000000;">aufz 1</span></ul>\r</li>\r<li style="list-style-type:disc;font-size:9pt;color:#000000;">\r<ul style=""><span style="color:#000000;"> </span></ul>\r</li>\r</ul>\r<p style=""> </p>\r<p style=""> </p>\r<p style=""> </p>\r<p style=""> </p>\r<p style=""> </p>\r<p style=""> </p>\r<p style=""> </p>\r<p style=""> </p>\r<p style=""> </p>\r<p style=""> </p>\r<p style=""> </p>\r<p style=""> </p>\r<p style=""> </p>\r<p style=""> </p>\r<ul style="margin:0pt;padding:0pt;list-style-type:disc;">\r<li style="font-size:9pt;color:#000000;">\r<ul style=""><span style="color:#000000;"> </span></ul>\r</li>\r</ul>\r<p style="">aufz 2</p>\r<p lang="de-AT" style=""> </p>\r<p lang="de-AT" style="">p-text klh kl lkj lkj lkj lkj lkj inline fett lalalaall</p>\r</body>\r</html>',
        ZusatzText3_Header: "",
        ZusatzText3_Collapsed: "true",
        ZusatzText4: "",
        ZusatzText4_Header: "",
        ZusatzText4_Collapsed: "false",
        ZusatzText5: "",
        ZusatzText5_Header: "",
        ZusatzText5_Collapsed: "false",
        FaceBookPageId: "GDAT_Facebook_Page_ID",
        SozMedia_FaceBook_Url: "www.facebook.com/PerauerHotelRestaurant",
        SozMedia_GooglePlus_Url: "www.googleplus.....",
        SozMedia_Pinterest_Url: "www.pinterest.....",
        SozMedia_Instagram_Url: "www.instagramm.....",
        SozMedia_YouTube_Url: "www.youtube.com/user/perauer854/videos",
        SozMedia_Twitter_Url: "www.twitter....",
        ImageCompressionQuality: "50"
      },
      DesignSettings: {
        TextModulStyle_Liste: {
          TextModulStyle: [
            {
              StyleName: "H1",
              InlineStyle: "false",
              Bold: "true",
              FontName: "Verdana",
              FontSize: "480",
              ForeColor: "#008000",
              Italic: "true",
              ListFormat: "None",
              Strikeout: "false",
              TextBackColor: "#FFFFFF",
              Underline: "false"
            },
            {
              StyleName: "H2",
              InlineStyle: "false",
              Bold: "true",
              FontName: "Arial",
              FontSize: "360",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "None",
              Strikeout: "false",
              TextBackColor: "#FFFFFF",
              Underline: "false"
            },
            {
              StyleName: "H3",
              InlineStyle: "false",
              Bold: "true",
              FontName: "Arial",
              FontSize: "266",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "None",
              Strikeout: "false",
              TextBackColor: "#FFFFFF",
              Underline: "false"
            },
            {
              StyleName: "H4",
              InlineStyle: "false",
              Bold: "true",
              FontName: "Arial",
              FontSize: "220",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "None",
              Strikeout: "false",
              TextBackColor: "#FFFFFF",
              Underline: "false"
            },
            {
              StyleName: "H5",
              InlineStyle: "false",
              Bold: "true",
              FontName: "Arial",
              FontSize: "180",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "None",
              Strikeout: "false",
              TextBackColor: "#FFFFFF",
              Underline: "false"
            },
            {
              StyleName: "P",
              InlineStyle: "false",
              Bold: "false",
              FontName: "Arial",
              FontSize: "180",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "None",
              Strikeout: "false",
              TextBackColor: "#FFFFFF",
              Underline: "false"
            },
            {
              StyleName: "UL",
              InlineStyle: "false",
              Bold: "false",
              FontName: "Arial",
              FontSize: "180",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "Bulleted",
              Strikeout: "false",
              TextBackColor: "#FFFFFF",
              Underline: "false"
            }
          ]
        },
        TemplateName: "",
        primaryColor: "#FF80C0",
        secundaryColor: "#0080FF",
        textColor: "#00FFFF",
        backgroundColor: "#F7FEC5",
        headlineFontName: "Aharoni",
        bodyFontName: "Arial Black"
      }
    }
  }
}
