export default function applyDesignSettings({
  html,
  designSettings
}: {
  html: string
  designSettings: MailBuilder.DesignSettings
}): string {
  try {
    if (!html) {
      throw { code: 0, message: "replacerDesign: no html" }
    } else {
      // console.log("primaryColor: " + designSettings.primaryColor)

      let updatedHtml: string = html
      /* DESIGN */
      updatedHtml = updatedHtml.replace(
        new RegExp("{primaryColor}", "g"),
        designSettings.primaryColor
      )

      updatedHtml = updatedHtml.replace(
        new RegExp("{secundaryColor}", "g"),
        designSettings.secundaryColor
      )

      updatedHtml = updatedHtml.replace(
        new RegExp("{sec_textColor}", "g"),
        designSettings.sec_textColor
      )

      updatedHtml = updatedHtml.replace(
        new RegExp("{headlineColor}", "g"),
        designSettings.headlineColor
      )

      updatedHtml = updatedHtml.replace(
        new RegExp("{textColor}", "g"),
        designSettings.textColor
      )

      updatedHtml = updatedHtml.replace(
        new RegExp("{backgroundColor}", "g"),
        designSettings.backgroundColor
      )

      updatedHtml = updatedHtml.replace(
        new RegExp("{backgroundColor2}", "g"),
        designSettings.backgroundColor2
      )

      updatedHtml = updatedHtml.replace(
        new RegExp("{headlineFontName}", "g"),
        designSettings.headlineFontName
      )

      updatedHtml = updatedHtml.replace(
        new RegExp("{bodyFontName}", "g"),
        designSettings.bodyFontName
      )

      return updatedHtml
    }
  } catch (err) {
    console.log("replacerDesign - error: ", err.message)
    throw err
  }
}
