export interface HeadlineWithSublineWithLinkSettings {
    key: string;
}
export interface HeadlineWithSublineWithLink {
    headline: string;
    subline: string;
    link: string;
    linkText: string;
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: HeadlineWithSublineWithLinkSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
