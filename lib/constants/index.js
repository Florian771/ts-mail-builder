"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var generalSettings = {
    default: {
        language: "de"
    },
    web: {
        urlWeb: "https://mailbuilder-564cf.firebaseapp.com"
    }
};
exports.default = generalSettings;
//# sourceMappingURL=index.js.map