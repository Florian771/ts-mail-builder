"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    var html = "";
    var text = "";
    text = "";
    html = "\n  <mj-section padding=\"20px 20px 0 20px\" background-color=\"{backgroundColor2}\">\n  <mj-column>\n    <mj-carousel css-class=\"" + settings.key + "\" border-radius=\"0\" tb-border-radius=\"0\" right-icon=\"{urlWeb}/arrow-right.png\" left-icon=\"{urlWeb}/arrow-left.png\">";
    if (settings.images && settings.images.length > 0) {
        settings.images.map(function (img) {
            html += "<mj-carousel-image border-radius=\"0\" tb-border-radius=\"0\" src=\"" + img + "\" />";
        });
    }
    html += "</mj-carousel>\n  </mj-column>\n</mj-section>";
    testParse_1.default({ test: true, html: html, options: options, modulName: "carousel" });
    return {
        html: html,
        text: text,
        translations: [],
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=carousel.js.map