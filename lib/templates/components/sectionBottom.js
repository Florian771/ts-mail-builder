"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    var html = "";
    var text = "";
    if (settings.active) {
        if (settings.type === "diagonal") {
            html = "<mj-section vertical-align=\"middle\" background-size=\"cover\" background-repeat=\"no-repeat\">\n  <mj-column width=\"100%\">\n    <mj-image src=\"https://admin.chatify.info/assets/mail/shadow.png\" alt=\"\" align=\"center\" border=\"none\" padding=\"0px\"></mj-image>\n  </mj-column>\n</mj-section>";
        }
    }
    testParse_1.default({
        test: true,
        html: html,
        options: options,
        modulName: "sectionBottom"
    });
    return {
        html: html,
        text: text,
        translations: [],
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=sectionBottom.js.map