export function getHtmlFileName({
  options,
  type
}: {
  options: MailBuilder.BuildOptions
  type: "pebuild" | "final"
}) {
  let fileName: string = "index"
  if (options.name !== "chatify") {
    fileName += "_" + options.name
  }
  if (type === "pebuild") {
    fileName += "_" + type
  }
  return fileName
}

function replaceKey({ text, settings }: { text: string; settings: any }) {
  if (settings.key) {
    return text.replace("KEY", settings.key)
  }
  return text
}

function replaceIndex({
  translations,
  text,
  settings
}: {
  translations: any[]
  text: string
  settings: any
}) {
  if (text) {
    const updatedText = replaceKey({ text: text, settings: settings })
    if (settings.length && settings.length > 0) {
      for (let index: number = 0; index < settings.length; index++) {
        translations.push(updatedText.replace("XXX", String(index)))
      }
    } else {
      translations.push(updatedText)
    }
  }
  return translations
}

export function getTranslations({
  placeholder,
  settings
}: {
  placeholder: any
  settings: any
}): string[] {
  let translations: string[] = []
  Object.keys(placeholder).forEach(key => {
    if (typeof placeholder[key] === "object") {
      Object.keys(placeholder[key]).forEach(key2 => {
        if (typeof placeholder[key][key2] === "object") {
          Object.keys(placeholder[key][key2]).forEach(key3 => {
            if (placeholder[key][key2][key3]) {
              translations = replaceIndex({
                translations: translations,
                text: placeholder[key][key2][key3],
                settings: settings
              })
            }
          })
        } else {
          if (placeholder[key][key2]) {
            translations = replaceIndex({
              translations: translations,
              text: placeholder[key][key2],
              settings: settings
            })
          }
        }
      })
    } else {
      if (placeholder[key]) {
        translations = replaceIndex({
          translations: translations,
          text: placeholder[key],
          settings: settings
        })
      }
    }
  })
  return translations
}
