"use strict";
/*
<mj-font name="HeadlineFont" href="https://fast.fonts.net/dv2/14/18597b82-9a06-46a3-b68b-c250c1105515.woff2?d44f19a684109620e484147ea690e818ee8d0f94c8efc5f31894b34456dd6eba45b7199fe6a76b3f16243440c93c58e473c2b02ad5244cba2c4f7ba3b91d2e05b3bce074c230ba&projectId=d07801b5-a55d-4cc3-92ca-167ffa4bbcb1"></mj-font>
<mj-font name="ContentFont" href="https://fast.fonts.net/dv2/14/14c73713-e4df-4dba-933b-057feeac8dd1.woff2?d44f19a684109620e484147ea690e818ee8d0f94c8efc5f31894b34456dd6eba45b7199fe6a76b3f16243440c93c58e473c2b02ad5244cba2c4f7ba3b91d2e05b3bce074c230ba&projectId=d07801b5-a55d-4cc3-92ca-167ffa4bbcb1"></mj-font>
*/
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = require("../../config");
var generateHead_1 = require("../generateHead");
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    var html = "";
    var head = "";
    var text = "";
    var font = "Montserrat, Helvetica, Arial, sans-serif";
    if (options &&
        options.designSettings &&
        options.designSettings.bodyFontName) {
        font = options.designSettings.bodyFontName;
    }
    text = "" + config_1.textBreak;
    head = generateHead_1.default({ font: font });
    html = "\n  <mjml>" + head + "<mj-body background-color=\"{backgroundColor}\">";
    return { html: html, text: text, translations: [] };
});
//# sourceMappingURL=head.js.map