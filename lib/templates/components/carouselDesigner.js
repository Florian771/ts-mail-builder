"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
var placeholder = {
    src: "{CAROUSEL_SRC_KEY_XXX}"
};
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    var html = "";
    var text = "";
    text = "";
    html = "\n  <mj-section padding=\"20px 20px 20px 20px\" background-color=\"{backgroundColor2}\">\n  <mj-column>\n    <mj-carousel css-class=\"" + settings.key + "\" border-radius=\"0\" tb-border-radius=\"0\" right-icon=\"{urlWeb}/arrow-right.png\" left-icon=\"{urlWeb}/arrow-left.png\">";
    if (settings.items && settings.items > 0) {
        for (var index = 0; index < settings.items; index++) {
            html += "<mj-carousel-image border-radius=\"0\" tb-border-radius=\"0\" src=\"" + placeholder.src
                .replace("KEY", settings.key)
                .replace("XXX", String(index)) + "\" />";
        }
    }
    html += "</mj-carousel>\n  </mj-column>\n</mj-section>";
    testParse_1.default({
        test: true,
        html: html,
        options: options,
        modulName: "carouselDesigner"
    });
    return {
        html: html,
        text: text,
        translations: [],
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=carouselDesigner.js.map