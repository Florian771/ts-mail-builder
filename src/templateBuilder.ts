import * as mjml2html from "mjml"
import { getHtmlFromTemplateType } from "./templates"
import replacerDesign from "./replacerDesign"
import { getHtmlFileName } from "./utils"
// const fs = require("fs-extra")

export async function htmlOutput({
  mailContentsOutput
}: {
  mailContentsOutput: MailBuilder.MailContentsOutput
}): Promise<MailBuilder.TemaplateBuilderResult> {
  let html: string
  let text: string
  let translations: string[]
  try {
    const res: {
      html: string
      text: string
      translations: string[]
    } = await getHtmlFromTemplateType({
      templateType: mailContentsOutput.options.templateType,
      options: mailContentsOutput.options
    })
    text = res.text
    html = res.html
    translations = res.translations
  } catch (err) {
    console.log("templateBuilder - getHtml - error: ", err.message)
    return err
  }

  try {
    html = replacerDesign({
      html: html,
      designSettings: mailContentsOutput.options.designSettings
    })

    // console.log("html", html)
    const fileName: string = getHtmlFileName({
      options: mailContentsOutput.options,
      type: "pebuild"
    })
    /* await fs.writeFile("./public/" + fileName + ".html", html, err => {
      if (err) {
        throw err
      }
    }) */
    // process.exit()

    const options: any = {}
    const res: MailBuilder.Template = mjml2html(html, options)

    if (res.errors && res.errors.length > 0) {
      console.log("templateBuilder - errors: ", res.errors)
      throw { code: 0, message: JSON.stringify(res.errors) }
    }

    /* 
    await fs.writeFile("./public/indexTemplate.html", res.html, err => {
      if (err) {
        throw err
      }
    }) 
    */
    // console.log("html generated and saved", html)

    return {
      html: res.html,
      text: text,
      translations: translations,
      errors: res.errors
    }
  } catch (err) {
    console.log("templateBuilder - error: ", err.message)
    return err
  }
}
