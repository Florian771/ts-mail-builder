"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = require("./index");
var json_default_template1_1 = require("../dummy/json-default-template1");
var converter_1 = require("./converter");
var user = {
    displayName: "Thomas Muster"
};
/*
const client: chatify.Client = {
  companyName: "Muster GmbH",
  contactPersonFirstName: "Thomas",
  contactPersonLastName: "Musterss",
  email: "test@test.at",
  street: "Neutorstr. 4",
  postalCode: "5300",
  city: "Salzburg",
  country: "Österreich"
}
*/
var HotelData = json_default_template1_1.default.API.HotelData;
var designSettings = json_default_template1_1.default.API.ThemeData.DesignSettings;
var data = {
    user: user,
    client: converter_1.gdatHotelDataToClient(HotelData)
};
/* if (
  designSettings.TemplateName === "1" ||
  designSettings.TemplateName === "1b"
) {
  designSettings = convertTemplate1Colors(designSettings)
} */
var options = {
    name: "TouristScoutOfferDynamic",
    from: "noreply@touristscout.com",
    to: "florian1677@gmail.com",
    // to: "kf@chatify.info",
    // to: "r.hofherr.gastrodat@gmail.com",
    data: data,
    zusatzTextBilder: [
        {
            name: "Bild 11",
            url: "https://touristscout.com/demo/assets/images/header/13.jpg"
        },
        {
            name: "",
            url: ""
        },
        {
            name: "Bild 3",
            url: "https://touristscout.com/demo/assets/images/header/6.jpg"
        }
    ],
    socialMedia: [
        {
            name: "facebook",
            url: "https://wwww.facebook.com"
        },
        {
            name: "youtube",
            url: "https://wwww.youtube.com"
        },
        {
            name: "instagram",
            url: "https://wwww.instagram.com"
        },
        {
            name: "twitter",
            url: "https://wwww.twitter.com"
        },
        {
            name: "pinterest",
            url: "https://wwww.pinterest.com"
        },
        {
            name: "google",
            url: "https://wwww.google.com"
        }
    ],
    designSettings: designSettings,
    sections: [
        {
            type: "alternativeHtmlInfo"
        },
        {
            type: "header"
        },
        {
            type: "accordeon"
        },
        {
            type: "headline"
        },
        {
            type: "hero"
        },
        {
            type: "subline"
        },
        {
            type: "image"
        },
        {
            type: "button"
        },
        {
            type: "carousel"
        },
        {
            type: "text"
        },
        {
            type: "zusatz_text1"
        },
        {
            type: "zusatz_text2"
        },
        {
            type: "zusatz_text3"
        }
    ],
    templateType: {
        name: "product",
        accordeonImages: 4,
        menuLinks: 3,
        alternativeHtmlInfo: {
            text: true,
            url: true
        },
        header: {
            logo: json_default_template1_1.default.API.ThemeData.StammDaten.HotelLogo_pic.PicUrl,
            context: false,
            menuLinks: 4
        },
        hero: {
            key: "HRO",
            headline: true,
            buttonText: true,
            buttonUrl: true,
            imgUrl: "https://touristscout.com/demo/assets/images/header/13.jpg" // 1, 13, 4
        },
        button: {
            key: "BTN"
        },
        text: {
            key: "TXT"
        },
        accordeon: {
            length: 2,
            key: "ACDN"
        },
        carousel: {
            key: "CSL",
            images: [
                "https://touristscout.com/demo/assets/images/header/12.jpg",
                "https://touristscout.com/demo/assets/images/header/2.jpg",
                "https://touristscout.com/demo/assets/images/header/11.jpg",
                "https://touristscout.com/demo/assets/images/header/10.jpg"
            ]
        },
        image: {
            src: "https://touristscout.com/demo/assets/images/header/4.jpg",
            key: "IMAGE_KEY"
        },
        sectionBottom: {
            active: false,
            type: "diagonal"
        },
        footer: {
            unsubscribeText: true,
            unsubscribeUrl: true,
            name: true,
            street: true,
            postalCode: true,
            city: true,
            country: true,
            phone: true,
            fax: true,
            email: true,
            website: true,
            uid: true
        }
    },
    contentType: "hotelRoomOffer",
    buildType: "file",
    testParse: true,
    translations: [
        { HTML_LINK_TEXT: "Probleme? Browseransicht öffnen" },
        { HTML_LINK_URL: "http://touristscout.com/?template=1b" },
        { MAIL_CONTEXT: "" },
        { LOGO_URL: json_default_template1_1.default.API.ThemeData.StammDaten.HotelLogo_pic.PicUrl },
        { LOGO_TITLE: "Demo Hotel" },
        { LOGO_TARGET: "" },
        { WEBSITE_TEXT_0: "Webseite" },
        { WEBSITE_TEXT_1: "Angebot" },
        { WEBSITE_TEXT_2: "Kontakt" },
        { WEBSITE_URL_0: "" },
        { WEBSITE_URL_1: "" },
        { WEBSITE_URL_2: "" },
        { HERO_HEADLINE_HRO: "Ihr persönliches Angebot" },
        { HERO_BUTTON_TEXT_HRO: "Angebot öffnen" },
        { HERO_BUTTON_URL_HRO: "http://touristscout.com/?template=1b" },
        { HEADLINE_HDL: "Liebe Viktoria Hofmann," },
        {
            SUBLINE_SL: "Für Ihren Wunschtermin können wir Ihnen wie folgt anbieten"
        },
        {
            CONTENT_TEXT_TXT: "Das **** Superior Baby & Kinderhotel Laurentius bietet die allerbesten Voraussetzungen für einen traumhaften Winter- und Sommerurlaub, mit einem riesigen Angebot an Wellness, Genuss, Spaß und Sport für die ganze Familie – und das - direkt an den Pisten von Tirols Skidimension Serfaus-Fiss-Ladis."
        },
        { BUTTON_TEXT_BTN: "Angebot öffnen" },
        { BUTTON_URL_BTN: "http://touristscout.com/?template=1b" },
        { IMAGE_ALT_IMAGE_KEY: "" },
        { ACCORDEON_TITLE_ACDN_0: "Sehenswürdigkeiten" },
        { ACCORDEON_TITLE_ACDN_1: "Ausflugsziele" },
        {
            ACCORDEON_CONTENT_ACDN_0: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."
        },
        {
            ACCORDEON_CONTENT_ACDN_1: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."
        },
        { UNSUBSCRIBE_TEXT: "Benachrichtigungen deaktivieren" },
        { UNSUBSCRIBE_URL: "mailto:info@demo.at" },
        { COMPANY_NAME: "Demo Hotel" },
        { COMPANY_STREET: "Bayerhofstr. 44" },
        { COMPANY_POSTAL_CODE: "6589" },
        { COMPANY_CITY: "Salzburg" },
        { COMPANY_COUNTRY: "Österreich" },
        { COMPANY_PHONE: "+43 (0)653 / 435 785 22" },
        { COMPANY_FAX: "+43 (0)653 / 435 785 22 - 1" },
        { COMPANY_EMAIL: "info@demo.at" },
        { COMPANY_WEBSITE: "https://www.demo.at" },
        { COMPANY_UID: "ATU 320403432" },
        { TEXT_EMAIL: "E-Mail" },
        { TEXT_PHONE: "Tel" },
        { TEXT_FAX: "Fax" },
        { TEXT_UID: "UID" },
        { TEXT_WEBSITE: "Webseite" }
    ]
};
// console.log("alternativeHtmlText", options.general.alternativeHtmlText)
// console.log("primaryColor:", options.designSettings.primaryColor)
index_1.start(options).then(function (res) {
    // console.log("res.translations: ", res.translations)
    return res;
});
//# sourceMappingURL=entryTouristScoutOfferDynamic.js.map