"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
var config_1 = require("../../config");
var utils_1 = require("../../utils");
var templateReplacer_1 = require("../templateReplacer");
var placeholder = {
    headline: "{HEADLINE_KEY}"
};
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    var translations = utils_1.getTranslations({
        placeholder: placeholder,
        settings: settings
    });
    var html = "";
    var text = "";
    var font = "Montserrat, Helvetica, Arial, sans-serif";
    if (options &&
        options.designSettings &&
        options.designSettings.headlineFontName) {
        font = options.designSettings.headlineFontName;
    }
    var mjmlHeadline = "<mj-section padding=\"20px 20px 0 20px\" background-color=\"{backgroundColor2}\">\n<mj-column>\n  <mj-text font-family=\"" + font + "\" align=\"center\" font-weight=\"300\" padding=\"30px 40px 10px 40px\" font-size=\"32px\" line-height=\"40px\" color=\"{textColor}\">\n    " + placeholder.headline + "\n  </mj-text>\n</mj-column>\n</mj-section>";
    text = "" + placeholder.headline.replace("KEY", settings.key) + config_1.textBreak;
    html = mjmlHeadline;
    html = templateReplacer_1.default({ html: mjmlHeadline, settings: settings, placeholder: placeholder, font: font });
    testParse_1.default({
        test: true,
        html: html,
        options: options,
        modulName: "headline"
    });
    return {
        html: html,
        text: text,
        translations: translations,
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=headline.js.map