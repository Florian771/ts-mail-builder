export function gdatHotelDataToClient(
  client: gastrodat.HotelData
): chatify.Client {
  const outputClient: chatify.Client = {
    city: client.Ort,
    companyName: client.HotelName,
    contactPersonFirstName: "",
    contactPersonLastName: client.PersName,
    street: client.Strasse,
    greeting: client.Gruss,
    postalCode: client.PLZ,
    phone: client.Telefon,
    fax: client.Fax,
    uid: client.UID,
    country: client.Nation,
    bankAccountNr: client.KtoNr,
    bankName: client.Bank,
    blz: client.BLZ,
    email: client.Email,
    bic: client.BIC,
    iban: client.IBAN
  }
  return outputClient
}

export function convertTemplate1Colors(
  options: MailBuilder.DesignSettings
): MailBuilder.DesignSettings {
  let updatedOptions: MailBuilder.DesignSettings = options
  updatedOptions.secundaryColor = options.primaryColor
  updatedOptions.primaryColor = options.bckg_Color4

  return updatedOptions
}
