export default {
  API: {
    HotelData: {
      Ort: "6533 Fiss",
      HotelName: "****Familienhotel St. Laurentius",
      PersName: "Familie Neururer",
      Strasse: "Leiteweg 26",
      Gruss: "",
      PLZ: "",
      Telefon: "",
      Fax: "",
      UID: "",
      Nation: "A",
      KtoNr: "",
      Email: "info@laurentius.at",
      Bank: "",
      BLZ: "",
      BIC: "",
      IBAN: ""
    },
    ReservationData: {
      LFNUMMER: "169943",
      ZIMMER_NR: "5",
      PERS_NR: "125646",
      PERSON: "Termin gerade reserviert ...",
      VON: "16.12.2017",
      BIS: "23.12.2017",
      ANGEBOTSURL: "",
      ANGEBOTSURLTEST: "https://www.google.at"
    },
    GuestData: {
      LFNUMMER: "125646",
      ANREDE_H: "Frau",
      FAMNAME: "Viktoria",
      VORNAME: "Federspiel",
      TITEL: "",
      GESCHLECHT: "W",
      STRASSE: "Scheibe 532",
      PLZ: "6543",
      ORT: "Nauders",
      ANREDE_FAM: "liebe Viktoria",
      SPRACHE: "D Deutsch",
      NATION: "A",
      ZUSNAME: "",
      P_EMAIL: "reservierung@laurentius.at",
      NATION_TXT: ""
    },
    RoomData: {
      ZIMMER_NR: "5",
      ZIMMER_NAME: "005",
      GEEIGNET_VON: "1",
      GEEIGNET_BIS: "1",
      ZI_KAT: "2",
      ZIMMER_SAUBER: "N",
      GEEIGNET_STD: "1",
      STANDARDLEISTUNG: "10005,7,",
      TEXT1: "Doppelzimmer",
      TEXT2:
        "schön; Nachbar; Badewanne - WC in Bad; Holzboden, Leder Couch; Balkon sehr schmal",
      ZIMMERBILD: "/Content/images/invalidimage.png",
      ZIMMERGRUNDRISS: "/Content/images/invalidimage.png"
    },
    ExtrasData: {
      Posio: [
        {
          FKT_NR: "649868",
          P_VON: "16.12.2017",
          P_BIS: "23.12.2017",
          P_TEXT: "Pers. im Doppelzimmer",
          P_ANZ: "2",
          P_SUM: "2212",
          P_PREIS: "158",
          P_RABATT: "0",
          P_KG_NR: "10005",
          GROUPINDEX: "0",
          DAUER_VON: "1",
          DAUER_BIS: "999",
          BESCHREIBUNG: "",
          EXTRABILD:
            "https://www.weratech-files.com/images/201014/gastrodat/Extras/EXTRA_10005.jpg?timestamp=636489420900000000"
        },
        {
          FKT_NR: "649869",
          P_VON: "16.12.2017",
          P_BIS: "23.12.2017",
          P_TEXT: "Kurtaxe",
          P_ANZ: "2",
          P_SUM: "36.4",
          P_PREIS: "2.6",
          P_RABATT: "0",
          P_KG_NR: "7",
          GROUPINDEX: "0",
          DAUER_VON: "1",
          DAUER_BIS: "999",
          BESCHREIBUNG: "",
          EXTRABILD:
            "https://www.weratech-files.com/images/201014/gastrodat/Extras/EXTRA_7.jpg?timestamp=636489379110000000"
        },
        {
          FKT_NR: "649870",
          P_VON: "16.12.2017",
          P_BIS: "23.12.2017",
          P_TEXT: "Kind 0 bis 5 Jahre",
          P_ANZ: "1",
          P_SUM: "399",
          P_PREIS: "57",
          P_RABATT: "0",
          P_KG_NR: "10025",
          GROUPINDEX: "1",
          DAUER_VON: "1",
          DAUER_BIS: "999",
          BESCHREIBUNG: "",
          EXTRABILD:
            "https://www.weratech-files.com/images/201014/gastrodat/Extras/EXTRA_10025.jpg?timestamp=636489379110000000"
        },
        {
          FKT_NR: "649871",
          P_VON: "15.12.2017",
          P_BIS: "16.12.2017",
          P_TEXT: "ODER",
          P_ANZ: "0",
          P_SUM: "0",
          P_PREIS: "0",
          P_RABATT: "0",
          P_KG_NR: "0",
          GROUPINDEX: "-1"
        },
        {
          FKT_NR: "649872",
          P_VON: "16.12.2017",
          P_BIS: "23.12.2017",
          P_TEXT: "Pers. in Sternensuite",
          P_ANZ: "2",
          P_SUM: "2856",
          P_PREIS: "204",
          P_RABATT: "0",
          P_KG_NR: "34",
          GROUPINDEX: "2",
          DAUER_VON: "1",
          DAUER_BIS: "999",
          BESCHREIBUNG:
            '<strong> Sternensuite </strong> (ca. 45m²) - im Sternenhaus <br>\nIdeal für Familien mit 1 - 2 Kindern <br>\nWohn/Schlafzimmer für die Eltern, abgetrenntes Kinderzimmer mit einem Stockbett (der untere Teil des Bettes kann in ein Gitterbett umfunktioniert werden) ausgestattet mir Wohnkork, Flat-Screen TV mit Radio, Kühlschrank, Telefon, Babyphon mit Mobilfunkempfänger, Safe, kostenloses W-LAN, großer Balkon, Badezimmer mit Doppelwaschtisch, Dusche und Badewanne, WC getrennt, Föhn, Kuschelige Bademäntel und Badeschlappen, Badetasche gefüllt mit Badetüchern <br>\n<i>Mehr Info\'s finden Sie </i> <a href="https://www.familienhotel-laurentius.com/sternensuite.aspx">hier</a> <br><br>',
          EXTRABILD:
            "https://www.weratech-files.com/images/201014/gastrodat/Extras/EXTRA_34.jpg?timestamp=636543832620000000"
        },
        {
          FKT_NR: "649873",
          P_VON: "16.12.2017",
          P_BIS: "23.12.2017",
          P_TEXT: "Kurtaxe",
          P_ANZ: "2",
          P_SUM: "36.4",
          P_PREIS: "2.6",
          P_RABATT: "0",
          P_KG_NR: "100000",
          GROUPINDEX: "2"
        },
        {
          FKT_NR: "649874",
          P_VON: "16.12.2017",
          P_BIS: "23.12.2017",
          P_TEXT: "Kind 0 bis 5 Jahre",
          P_ANZ: "1",
          P_SUM: "399",
          P_PREIS: "57",
          P_RABATT: "0",
          P_KG_NR: "100000",
          GROUPINDEX: "2"
        },
        {
          FKT_NR: "649875",
          P_VON: "15.12.2017",
          P_BIS: "16.12.2017",
          P_TEXT: "ZSUM",
          P_ANZ: "0",
          P_SUM: "0",
          P_PREIS: "0",
          P_RABATT: "0",
          P_KG_NR: "100000",
          GROUPINDEX: "2"
        }
      ]
    },
    ThemeData: {
      Themes: {
        gdatTheme: {
          theme_pics: {
            gdatPicture: [
              {
                PicId: "2317502e5863f",
                PicName: " Winterspaß für Groß und Klein",
                PicPath: "$i_offer\\theme_Winter\\2317502e5863f.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/201014/gastrodat/2317502e5863f.jpg",
                PicHTML_Link: "",
                PicKategorie: "Winter"
              },
              {
                PicId: "23175535f3026",
                PicName:
                  "Traumhafte Lage - direkt an den 212 km Pisten von Serfaus-Fiss-Ladis",
                PicPath: "$i_offer\\theme_Winter\\23175535f3026.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/201014/gastrodat/23175535f3026.jpg",
                PicHTML_Link: "",
                PicKategorie: "Winter"
              },
              {
                PicId: "2476ca7e7f211",
                PicName: "Wasservernügen in unserer Familien.Wasser.Welt",
                PicPath: "$i_offer\\theme_Winter\\2476ca7e7f211.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/201014/gastrodat/2476ca7e7f211.jpg",
                PicHTML_Link: "",
                PicKategorie: "SPA"
              },
              {
                PicId: "2476cd0d677e2",
                PicName: "bei uns sind ALLE glücklich!",
                PicPath: "$i_offer\\theme_Winter\\2476cd0d677e2.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/201014/gastrodat/2476cd0d677e2.jpg",
                PicHTML_Link: "",
                PicKategorie: "SPA"
              }
            ]
          },
          theme_teasers: {
            gdatTeaser: [
              {
                Teaser_pic: {
                  PicId: "23175cb9f0472",
                  PicName:
                    "Wir bieten Schwimmkurse für Babys und Kleinkinder an (gegen Gebühr)",
                  PicPath:
                    "$i_offer\\teaser_Schwimmkurse (gegen Gebühr)\\23175cb9f0472.jpg",
                  PicUrl:
                    "https://www.weratech-files.com/images/201014/gastrodat/23175cb9f0472.jpg",
                  PicHTML_Link:
                    "https://www.familienhotel-laurentius.com/Kinderschwimmkurs.aspx",
                  PicKategorie: ""
                },
                validFrom: "2017-12-15T12:06:35.0552178+01:00",
                validTo: "2018-04-08T12:06:35",
                SuchwortListe: "",
                TeaserId: "23175cb9f0472",
                TeaserName: "Schwimmkurse (gegen Gebühr)"
              },
              {
                Teaser_pic: {
                  PicId: "2475eef6f7ca5",
                  PicName:
                    "Die Skischule Fiss-Ladis bietet professionellen Kinderskikurs ab 3 Jahren an! (gegen Gebühr)",
                  PicPath:
                    "$i_offer\\teaser_neuer ThemenTeaser\\2475eef6f7ca5.jpg",
                  PicUrl:
                    "https://www.weratech-files.com/images/201014/gastrodat/2475eef6f7ca5.jpg",
                  PicHTML_Link:
                    "https://www.familienhotel-laurentius.com/bertas-kinderland.aspx",
                  PicKategorie: ""
                },
                validFrom: "2017-12-15T09:18:22",
                validTo: "2018-04-08T09:18:22",
                SuchwortListe: "",
                TeaserId: "2475eef6f7ca5",
                TeaserName: "Skifahren mit der Berta (gegen Gebühr)"
              },
              {
                Teaser_pic: {
                  PicId: "2475ff47ea86a",
                  PicName:
                    "Laurentius Verwöhnpension beinhaltet  - Frühstück, Mittagsbuffet, Nachmittagsjause und Abendessen ",
                  PicPath:
                    "$i_offer\\teaser_neuer ThemenTeaser\\2475ff47ea86a.jpg",
                  PicUrl:
                    "https://www.weratech-files.com/images/201014/gastrodat/2475ff47ea86a.jpg",
                  PicHTML_Link:
                    "https://www.familienhotel-laurentius.com/hotel-kueche-genuss.aspx",
                  PicKategorie: ""
                },
                validFrom: "2017-12-15T09:25:40",
                validTo: "2018-04-08T09:25:40",
                SuchwortListe: "",
                TeaserId: "2475ff47ea86a",
                TeaserName: "Laurentius Verwöhnpension"
              }
            ]
          },
          validFrom: "2017-12-15T15:09:55",
          validTo: "2018-04-08T15:09:55",
          SuchwortListe: "",
          ThemeId: "5",
          ThemeName: "Winter"
        }
      },
      BilderGalerie_pics: {
        gdatPicture: [
          {
            PicId: "231776b31df79",
            PicName: "Suite im Sonnenhaus",
            PicPath: "$i_offer\\impressions\\231776b31df79.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/201014/gastrodat/231776b31df79.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer"
          },
          {
            PicId: "23177b8203fbe",
            PicName: "Sternensuite/Laurentius Suite",
            PicPath: "$i_offer\\impressions\\23177b8203fbe.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/201014/gastrodat/23177b8203fbe.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer"
          },
          {
            PicId: "23177d89163a0",
            PicName: "Familienwasserwelt",
            PicPath: "$i_offer\\impressions\\23177d89163a0.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/201014/gastrodat/23177d89163a0.jpg",
            PicHTML_Link: "",
            PicKategorie: "SPA"
          }
        ]
      },
      impression_pics: {
        gdatPicture: [
          {
            PicId: "2317502e5863f",
            PicName: " Winterspaß für Groß und Klein",
            PicPath: "$i_offer\\theme_Winter\\2317502e5863f.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/201014/gastrodat/2317502e5863f.jpg",
            PicHTML_Link: "",
            PicKategorie: "Winter"
          },
          {
            PicId: "23175535f3026",
            PicName:
              "Traumhafte Lage - direkt an den 212 km Pisten von Serfaus-Fiss-Ladis",
            PicPath: "$i_offer\\theme_Winter\\23175535f3026.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/201014/gastrodat/23175535f3026.jpg",
            PicHTML_Link: "",
            PicKategorie: "Winter"
          },
          {
            PicId: "2476ca7e7f211",
            PicName: "Wasservernügen in unserer Familien.Wasser.Welt",
            PicPath: "$i_offer\\theme_Winter\\2476ca7e7f211.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/201014/gastrodat/2476ca7e7f211.jpg",
            PicHTML_Link: "",
            PicKategorie: "SPA"
          },
          {
            PicId: "2476cd0d677e2",
            PicName: "bei uns sind ALLE glücklich!",
            PicPath: "$i_offer\\theme_Winter\\2476cd0d677e2.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/201014/gastrodat/2476cd0d677e2.jpg",
            PicHTML_Link: "",
            PicKategorie: "SPA"
          },
          {
            PicId: "23176bd98bac0",
            PicName: "Bei uns sind ALLE glücklich!",
            PicPath: "$i_offer\\theme_Sommer\\23176bd98bac0.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/201014/gastrodat/23176bd98bac0.jpg",
            PicHTML_Link: "",
            PicKategorie: "Sommer"
          },
          {
            PicId: "23176f4be06dc",
            PicName: "1500m² Spielplatz direkt vor der Haustüre",
            PicPath: "$i_offer\\theme_Sommer\\23176f4be06dc.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/201014/gastrodat/23176f4be06dc.jpg",
            PicHTML_Link: "",
            PicKategorie: "Sommer"
          },
          {
            PicId: "24762624981c2",
            PicName: "Super.Sommer.Card - kostenlose Nutzung von 11 Seilbahnen",
            PicPath: "$i_offer\\theme_Sommer\\24762624981c2.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/201014/gastrodat/24762624981c2.jpg",
            PicHTML_Link: "",
            PicKategorie: "Sommer"
          },
          {
            PicId: "231776b31df79",
            PicName: "Suite im Sonnenhaus",
            PicPath: "$i_offer\\impressions\\231776b31df79.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/201014/gastrodat/231776b31df79.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer"
          },
          {
            PicId: "23177b8203fbe",
            PicName: "Sternensuite/Laurentius Suite",
            PicPath: "$i_offer\\impressions\\23177b8203fbe.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/201014/gastrodat/23177b8203fbe.jpg",
            PicHTML_Link: "",
            PicKategorie: "Zimmer"
          },
          {
            PicId: "23177d89163a0",
            PicName: "Familienwasserwelt",
            PicPath: "$i_offer\\impressions\\23177d89163a0.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/201014/gastrodat/23177d89163a0.jpg",
            PicHTML_Link: "",
            PicKategorie: "SPA"
          }
        ]
      },
      AngebotsName: "lalala",
      StammDaten: {
        HotelLogo_pic: {
          PicId: "1ecfce182b605",
          PicName: "HotelLogo",
          PicPath: "$i_offer\\LOGO\\StammDaten_HotelLogo_pic.png",
          PicUrl:
            "https://www.weratech-files.com/images/201014/gastrodat/StammDaten_HotelLogo_pic.png",
          PicHTML_Link: ""
        },
        Section_Liste: {
          Section: [
            {
              ModulName: "SOCIAL_MEDIA_BAND",
              ModulId: "",
              ModulHash: "SOCIAL_MEDIA_BAND"
            },
            { ModulName: "MENU_BAND", ModulId: "", ModulHash: "MENU_BAND" },
            { ModulName: "LOGO_BLOCK", ModulId: "", ModulHash: "LOGO_BLOCK" },
            {
              ModulName: "ZUSATZ_TEXT1",
              ModulId: "",
              ModulHash: "ZUSATZ_TEXT1"
            },
            { ModulName: "SLIDESHOW", ModulId: "" },
            { ModulName: "WELCOME_TEXT", ModulId: "" },
            { ModulName: "TEASER_BLOCK", ModulId: "" },
            { ModulName: "ZWISCHEN_BILD1", ModulId: "" },
            { ModulName: "ANGEBOTS_TEXT", ModulId: "" },
            { ModulName: "VARIANTEN_VORSCHAU", ModulId: "" },
            { ModulName: "ANGEBOT_DETAILS", ModulId: "" },
            {
              ModulName: "IMPRESSIONEN",
              ModulId: "",
              ModulHash: "IMPRESSIONEN"
            },
            { ModulName: "ANREISE_BESCHREIBUNG", ModulId: "" },
            { ModulName: "NACHRICHTEN_BLOCK", ModulId: "" },
            { ModulName: "IMPRESSUM", ModulId: "", ModulHash: "IMPRESSUM" }
          ]
        },
        MenuItem_Liste: {
          MenuItem: [
            {
              MenuId: "1",
              MenuName: "Menu1",
              MenuURL: "#IMPRESSUM"
            },
            {
              MenuId: "2",
              MenuName: "Menu2",
              MenuURL: "?menu2=test"
            }
          ]
        },
        EinzelText_Liste: {
          EinzelText: [
            {
              Tag: "gdat_chatten",
              Text:
                "Haben Sie Fragen? Tel. 0043 5476 6714 - wir sind gerne für Sie da."
            },
            { Tag: "*gdat_variante:", Text: "oder Sie entscheiden sich für:" }
          ]
        },
        HotelName: "Familien.Urlaubs.Traum",
        WellcomeText:
          "<?xml version=\"1.0\" ?>\r<html xmlns=\"https://www.w3.org/1999/xhtml\">\r<head>\r<meta content=\"TX24_HTM 24.0.610.500\" name=\"GENERATOR\" />\r<title></title>\r<style type=\"text/css\">\r/* <![CDATA[ */\rBODY {\r\twidows: 2;\r\torphans: 2;\r\tfont-family: 'Arial';\r\tfont-size: 12pt;\r\tfont-weight: normal;\r\tfont-style: normal;\r}\rH1 {\r\twidows: 2;\r\torphans: 2;\r\tmargin: 7.95pt 0pt 15.85pt 0pt;\r\ttext-indent: 0pt;\r\tfont-family: 'Ebrima';\r\tfont-size: 10pt;\r\tfont-weight: normal;\r\tfont-style: normal;\r}\rH2 {\r\twidows: 2;\r\torphans: 2;\r\tmargin: 5.95pt 0pt 11.9pt 0pt;\r\ttext-indent: 0pt;\r\tfont-family: 'Ebrima';\r\tfont-size: 10pt;\r\tfont-weight: normal;\r\tfont-style: normal;\r}\rH3 {\r\twidows: 2;\r\torphans: 2;\r\tmargin: 4.65pt 0pt 9.3pt 0pt;\r\ttext-indent: 0pt;\r\tfont-family: 'Arial';\r\tfont-size: 14pt;\r\tfont-weight: bold;\r\tfont-style: normal;\r}\rH4 {\r\twidows: 2;\r\torphans: 2;\r\tmargin: 3.65pt 0pt 7.3pt 0pt;\r\ttext-indent: 0pt;\r\tfont-family: 'Arial';\r\tfont-size: 11pt;\r\tfont-weight: bold;\r\tfont-style: normal;\r}\rH5 {\r\twidows: 2;\r\torphans: 2;\r\tmargin: 3pt 0pt 6pt 0pt;\r\ttext-indent: 0pt;\r\tfont-family: 'Arial';\r\tfont-size: 9pt;\r\tfont-weight: bold;\r\tfont-style: normal;\r}\rH6 {\r\twidows: 2;\r\torphans: 2;\r\tmargin: 2.3pt 0pt 4.65pt 0pt;\r\ttext-indent: 0pt;\r\tfont-family: 'Arial';\r\tfont-size: 7pt;\r\tfont-weight: bold;\r\tfont-style: normal;\r}\rBLOCKQUOTE {\r\twidows: 2;\r\torphans: 2;\r\tmargin: 6pt 30pt 6pt 30pt;\r\ttext-indent: 0pt;\r\tfont-family: 'Ebrima';\r\tfont-size: 11pt;\r\tfont-weight: normal;\r\tfont-style: normal;\r}\rp {\r\twidows: 2;\r\torphans: 2;\r\tmargin: 0pt 0pt 0pt 0pt;\r\ttext-indent: 0pt;\r\tfont-family: 'Arial';\r\tfont-size: 12pt;\r\tfont-weight: normal;\r\tfont-style: normal;\r}\r.body-text {\r\twidows: 2;\r\torphans: 2;\r\tmargin: 0pt 0pt 0pt 0pt;\r\ttext-indent: 0pt;\r\tfont-family: 'Arial';\r\tfont-size: 12pt;\r\tfont-weight: normal;\r\tfont-style: normal;\r}\r.Normal {\r\twidows: 2;\r\torphans: 2;\r\tmargin: 0pt 0pt 0pt 0pt;\r\ttext-indent: 0pt;\r\tfont-family: 'Times New Roman';\r\tfont-size: 12pt;\r\tfont-weight: normal;\r\tfont-style: normal;\r}\rA {\r}\rPRE {\r\twidows: 2;\r\torphans: 2;\r\tmargin: 0pt 0pt 0pt 0pt;\r\ttext-indent: 0pt;\r\tfont-family: 'Courier New';\r\tfont-size: 12pt;\r\tfont-weight: normal;\r\tfont-style: normal;\r}\rB {\r\tfont-family: 'Arial';\r\tfont-weight: bold;\r}\rCAPTION {\r\twidows: 2;\r\torphans: 2;\r\tmargin: 0pt 0pt 0pt 0pt;\r\ttext-indent: 0pt;\r\tfont-family: 'Arial';\r\tfont-size: 12pt;\r\tfont-weight: normal;\r\tfont-style: normal;\r}\rCODE {\r\tfont-family: 'Courier New';\r}\rDEL {\r}\rEM {\r\tfont-family: 'Arial';\r\tfont-style: italic;\r}\rI {\r\tfont-family: 'Arial';\r\tfont-style: italic;\r}\rINS {\r}\rKBD {\r\tfont-family: 'Courier New';\r}\rMARK {\r\twidows: 2;\r\torphans: 2;\r\tmargin: 0pt 0pt 0pt 0pt;\r\ttext-indent: 0pt;\r\tfont-family: 'Arial';\r\tfont-size: 12pt;\r\tfont-weight: normal;\r\tfont-style: normal;\r\tbackground-color: #FFFF00;\r}\rS {\r}\rSAMP {\r\tfont-family: 'Courier New';\r}\rSTRIKE {\r}\rSTRONG {\r\tfont-family: 'Arial';\r\tfont-weight: bold;\r}\rTH {\r\twidows: 2;\r\torphans: 2;\r\tmargin: 0pt 0pt 0pt 0pt;\r\ttext-indent: 0pt;\r\tfont-family: 'Arial';\r\tfont-size: 12pt;\r\tfont-weight: bold;\r\tfont-style: normal;\r}\rTT {\r\tfont-family: 'Courier New';\r}\rU {\r}\r/* ]]> */\r</style>\r</head>\r<body>\r<h1 style=\"line-height:100%;\"> </h1>\r<h1 style=\"line-height:100%;\">Das <span style=\"font-weight:bold;\">**** Superior Baby &amp; Kinderhotel Laurentius</span> bietet die allerbesten Voraussetzungen für einen <span style=\"font-weight:bold;\">traumhaften Winter- und Sommerurlaub</span>, mit einem riesigen Angebot an Wellness, Genuss, Spaß und Sport für die ganze Familie – und das - direkt an den Pisten von Tirols Skidimension Serfaus-Fiss-Ladis.<br />\r</h1>\r</body>\r</html>",
        SubDomainUrl: ".https://www.touristscout.com",
        AngebotsText:
          '<?xml version="1.0" ?>\r\n<html xmlns="https://www.w3.org/1999/xhtml">\r\n<head>\r\n<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r\n<title></title>\r\n</head>\r\n<body>\r\n<h2 style="text-indent:0pt;margin-left:0pt;margin-right:0pt;line-height:100%;"><span style="font-size:11pt;font-weight:normal;">Für Ihren Wunschtermin können wir Ihnen wie folgt anbieten:</span></h2>\r\n</body>\r\n</html>',
        AnreiseInfoText:
          '<?xml version="1.0" ?>\r\n<html xmlns="https://www.w3.org/1999/xhtml">\r\n<head>\r\n<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r\n<title></title>\r\n</head>\r\n<body>\r\n<p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"><span style="font-size:10.2pt;color:#111111;background-color:#FFFFFF;">EINCHECKEN</span></p>\r\n<p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:2pt;margin-right:0pt;line-height:100%;"><span style="font-size:10.2pt;color:#999999;">MO-SO:: ab 15 UHR</span></p>\r\n<p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:2pt;margin-right:0pt;line-height:100%;background-color:#FFFFFF;"> </p>\r\n</body>\r\n</html>',
        AbreiseInfoText:
          '<?xml version="1.0" ?>\r\n<html xmlns="https://www.w3.org/1999/xhtml">\r\n<head>\r\n<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r\n<title></title>\r\n</head>\r\n<body>\r\n<p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"><span style="font-size:10.2pt;color:#111111;background-color:#FFFFFF;">AUSCHECKEN</span></p>\r\n<p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:2pt;margin-right:0pt;line-height:100%;"><span style="font-size:10.2pt;color:#999999;">MO-SO: BIS 11 UHR</span></p>\r\n</body>\r\n</html>',
        FaceBookPageId: "GDAT_Facebook_Page_ID",
        SozMedia_FaceBook_Url:
          "https://www.facebook.com/kinderhotel.laurentius/",
        SozMedia_GooglePlus_Url: "www.googleplus.....",
        SozMedia_Pinterest_Url: "www.pinterest.....",
        SozMedia_Instagram_Url: "www.instagramm.....",
        SozMedia_YouTube_Url: "www.youtube......",
        SozMedia_Twitter_Url: "www.twitter....",
        ImageCompressionQuality: "50"
      },
      DesignSettings: {
        TemplateName: "",
        primaryColor: "#98109a", //lila = button, headline color bei nicht collapsablen textmodulen, collabsable headline text, menu text, collapsable pfeil hintergrund farbe
        secundaryColor: "#e09ce0", //rosa = greeting bg color
        backgroundColor: "#FFF", //weiß = seitenhintegrund & angebotsvarianten hintergrund innerhaln collapsabler inhalte
        backgroundColor2: "#000", //weiß
        bckg_Color3: "#FF8040", //orange = collapsable inhalt hintergrund
        bckg_Color4: "#000", // sccharz = angebots details tabs over bg gcolor
        bckg_Color5: "#FF8040",
        textColor: "#1a3b8e", //blau = seiten Textfarbed & angebotsvarianten text innerhaln collapsabler inhalte
        prim_textColor: "#333", //grau = button text farbe, , collabsable headline hintergrund farbe, menu hintergrund farbe,  collapsable pfeil farbe
        sec_textColor: "#FF0000", //rot = greeting text color
        textColor_Color3: "#6cff00", //grün = collapsable inhalt text farbe
        textColor_Color4: "#fff", // weiss = angebots details tabs over text

        headlineFontName: "Ebrima",
        bodyFontName: "Ebrima"
      }
    }
  }
}
