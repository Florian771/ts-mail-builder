import testParse from "../../testParse"
import { textBreak } from "../../config"
import { getTranslations } from "../../utils"

export interface HeroSettings {
  key: string
  headline: boolean
  buttonText: boolean
  buttonUrl: boolean
  imgUrl: string
}

export interface Hero {
  headline: string
  buttonText: string
  buttonUrl: string
}

const placeholder: Hero = {
  headline: "{HERO_HEADLINE_KEY}",
  buttonText: "{HERO_BUTTON_TEXT_KEY}",
  buttonUrl: "{HERO_BUTTON_URL_KEY}"
}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: HeroSettings
}): MailBuilder.Component => {
  const translations: string[] = getTranslations({
    placeholder: placeholder,
    settings: settings
  })
  let html: string = ""
  let text: string = ""
  text = `${placeholder.headline.replace("KEY", settings.key)}${textBreak}
  ${placeholder.buttonText.replace(
    "KEY",
    settings.key
  )}: ${placeholder.buttonUrl.replace("KEY", settings.key)}${textBreak}
  `
  html = `
  <mj-hero
  mode="fixed-height"
  height="320px"
  background-width="600px"
  background-height="320px"
  background-url="${settings.imgUrl}"
  background-color="{primaryColor}"
  padding="0px">
  <mj-text
    padding="180px 20px 20px 20px"
    color="#ffffff"
    font-family="Helvetica"
    align="center"
    font-size="45px"
    line-height="45px"
    font-weight="900">
    ${placeholder.headline.replace("KEY", settings.key)}
  </mj-text>

  <mj-button padding-bottom="20px" padding-top="0px" align="center" background-color="{secundaryColor}" color="{sec_textColor}" border-radius="0px" href="${placeholder.buttonUrl.replace(
    "KEY",
    settings.key
  )}" inner-padding="15px 30px">
  ${placeholder.buttonText.replace("KEY", settings.key)}
  </mj-button>

</mj-hero>`

  testParse({ test: true, html: html, options: options, modulName: "hero" })
  return {
    html: html,
    text: text,
    translations: translations,
    parse: (args: any) => testParse(args)
  }
}
