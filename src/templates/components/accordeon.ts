import testParse from "../../testParse"
import { textBreak } from "../../config"
import { getTranslations } from "../../utils"

export interface Accordeon {
  title: string
  content: string
}
export interface AccordeonSettings {
  key: string
  length?: number
  items?: number
}

const placeholder: Accordeon = {
  title: "{ACCORDEON_TITLE_KEY_XXX}",
  content: "{ACCORDEON_CONTENT_KEY_XXX}"
}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: AccordeonSettings
}): MailBuilder.Component => {
  const translations: string[] = getTranslations({
    placeholder: placeholder,
    settings: settings
  })
  let html: string = ""
  let text: string = ""
  let accordeonHtml: string = ""
  // let accordeonText: string = textBreak
  if (settings["length"]) {
    settings.items = Number(settings["length"])
  } else {
    settings.items = Number(settings.items)
  }

  if (settings.items && settings.items > 0) {
    for (let index = 0; index < settings.items; index++) {
      let color: string = "backgroundColor"
      if (index % 2 > 0) {
        color = "backgroundColor2"
      }
      accordeonHtml += `<mj-accordion-element background-color="{${color}}">
      <mj-accordion-title font-size="15px" color="{textColor}">${placeholder.title
        .replace("KEY", settings.key)
        .replace("XXX", String(index))}</mj-accordion-title>
        <mj-accordion-text>
          <span style="line-height:20px">
          ${placeholder.content
            .replace("KEY", settings.key)
            .replace("XXX", String(index))}
          </span>
        </mj-accordion-text>
      </mj-accordion-element>
      `
      /* accordeonText +=
        placeholder.title
          .replace("KEY", settings.key)
          .replace("XXX", String(index)) + textBreak
      accordeonText +=
        placeholder.content
          .replace("KEY", settings.key)
          .replace("XXX", String(index)) + textBreak */
    }
  }

  text = `${textBreak}`
  html = `
  <mj-section padding="0px 0px 0px 0px" background-color="{backgroundColor2}">
    <mj-column width="600px" padding="0px 0px 0px 0px">
      <mj-accordion padding="0px 0px 0px 0px" border="0px" container-background-color="{primaryColor}"  icon-wrapped-url="{urlWeb}/arrow-bottom.png" icon-unwrapped-url="{urlWeb}/arrow-top.png">
        ${accordeonHtml}
      </mj-accordion>
    </mj-column>
  </mj-section>`

  testParse({
    test: true,
    html: html,
    options: options,
    modulName: "accordeon"
  })
  return {
    html: html,
    text: text,
    translations: translations,
    parse: (args: any) => testParse(args)
  }
}
