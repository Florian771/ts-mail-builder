"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
var config_1 = require("../../config");
var utils_1 = require("../../utils");
var placeholder = {
    unsubscribeText: "{UNSUBSCRIBE_TEXT}",
    unsubscribeUrl: "{UNSUBSCRIBE_URL}",
    name: "{COMPANY_NAME}",
    street: "{COMPANY_STREET}",
    postalCode: "{COMPANY_POSTAL_CODE}",
    city: "{COMPANY_CITY}",
    country: "{COMPANY_COUNTRY}",
    phone: "{COMPANY_PHONE}",
    fax: "{COMPANY_FAX}",
    email: "{COMPANY_EMAIL}",
    website: "{COMPANY_WEBSITE}",
    uid: "{COMPANY_UID}"
};
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    var translations = utils_1.getTranslations({
        placeholder: placeholder,
        settings: settings
    });
    var html = "";
    var text = "";
    text = "" + config_1.textBreak;
    html = "<mj-section padding=\"10px 0 20px 0\">\n  <mj-column>\n    <mj-text align=\"center\" color=\"{textColor}\" font-size=\"11px\">";
    if (settings.name) {
        text += "" + placeholder.name + config_1.textBreak;
        html += "\n    <br><b>" + placeholder.name + "</b>";
    }
    text += "" + config_1.textBreak;
    html += "<br>";
    if (settings.street) {
        text += "{TEXT_STREET}: " + placeholder.street + config_1.textBreak;
        html += "" + placeholder.street;
    }
    if (settings.postalCode) {
        text += "{TEXT_POSTAL_CODE}: " + placeholder.postalCode + config_1.textBreak;
        if (settings.street) {
            html += " / ";
        }
        html += "" + placeholder.postalCode;
    }
    if (settings.city) {
        text += "{TEXT_CITY}: " + placeholder.city + config_1.textBreak;
        if (settings.street || settings.postalCode) {
            html += " / ";
        }
        html += "" + placeholder.city;
    }
    if (settings.country) {
        text += "{TEXT_CITY}: " + placeholder.country + config_1.textBreak;
        if (settings.street || settings.postalCode || settings.city) {
            html += " / ";
        }
        html += "" + placeholder.country;
    }
    text += "" + config_1.textBreak;
    html += "<br>";
    if (settings.phone) {
        text += "{TEXT_PHONE}: " + placeholder.phone + config_1.textBreak;
        html += "{TEXT_PHONE}: " + placeholder.phone;
    }
    if (settings.email) {
        text += "" + placeholder.email;
        if (settings.phone) {
            html += " / ";
        }
        html += "{TEXT_EMAIL}: <a style=\"color: {textColor};\" href=\"mailto:" + placeholder.email + "\">" + placeholder.email + "</a>";
    }
    text += "" + config_1.textBreak;
    html += "<br>";
    if (settings.website) {
        text += "{TEXT_WEBSITE}: " + placeholder.website + config_1.textBreak;
        html += "<a style=\"color:{textColor};\" href=\"" + placeholder.website + "\">" + placeholder.website + "</a>";
    }
    if (settings.uid) {
        text += "{TEXT_UID}: " + placeholder.uid + config_1.textBreak;
        html += "<br>{TEXT_UID}: " + placeholder.uid;
    }
    text += "" + config_1.textBreak;
    html += "<br>";
    text += "" + config_1.textBreak;
    html += "<br>";
    if (settings.unsubscribeUrl) {
        text += placeholder.unsubscribeText + ": " + config_1.textBreak + placeholder.unsubscribeUrl + config_1.textBreak;
        html += "<a href=\"" + placeholder.unsubscribeUrl + "\" style=\"color: {textColor};\">" + placeholder.unsubscribeText + "</a><br>";
    }
    else if (settings.unsubscribeText) {
        text += "" + placeholder.unsubscribeText + config_1.textBreak;
        html += "<div style=\"color: {textColor};\">" + placeholder.unsubscribeText + "</div><br>";
    }
    html += "</mj-text>\n  </mj-column>\n</mj-section>";
    testParse_1.default({ test: true, html: html, options: options, modulName: "footer" });
    return {
        html: html,
        text: text,
        translations: translations,
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=footer.js.map