export interface DividerSettings {
    key?: string;
}
export interface Divider {
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: DividerSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
