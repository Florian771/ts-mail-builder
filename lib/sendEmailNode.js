"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
var sendEmail_1 = require("./sendEmail");
function send(mail) {
    return __awaiter(this, void 0, void 0, function () {
        var res, err_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    try {
                        sendEmail_1.initEmail();
                        console.log("sendEmail authorized");
                    }
                    catch (err) {
                        throw err;
                    }
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, sendEmail_1.sendEmail(mail)];
                case 2:
                    res = _a.sent();
                    console.log("mail sent", res.statusCode, res.statusMessage);
                    return [3 /*break*/, 4];
                case 3:
                    err_1 = _a.sent();
                    console.log("sendgrid err:", err_1.message);
                    throw err_1;
                case 4: return [2 /*return*/];
            }
        });
    });
}
exports.send = send;
var html = "\n<!doctype html>\n<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\">\n  <head>\n    <title>\n      \n    </title>\n    <!--[if !mso]><!-- -->\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n    <!--<![endif]-->\n    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n    <style type=\"text/css\">\n      #outlook a { padding:0; }\n      .ReadMsgBody { width:100%; }\n      .ExternalClass { width:100%; }\n      .ExternalClass * { line-height:100%; }\n      body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }\n      table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }\n      img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }\n      p { display:block;margin:13px 0; }\n    </style>\n    <!--[if !mso]><!-->\n    <style type=\"text/css\">\n      @media only screen and (max-width:480px) {\n        @-ms-viewport { width:320px; }\n        @viewport { width:320px; }\n      }\n    </style>\n    <!--<![endif]-->\n    <!--[if mso]>\n    <xml>\n    <o:OfficeDocumentSettings>\n      <o:AllowPNG/>\n      <o:PixelsPerInch>96</o:PixelsPerInch>\n    </o:OfficeDocumentSettings>\n    </xml>\n    <![endif]-->\n    <!--[if lte mso 11]>\n    <style type=\"text/css\">\n      .outlook-group-fix { width:100% !important; }\n    </style>\n    <![endif]-->\n    \n  <!--[if !mso]><!-->\n    <link href=\"https://fonts.googleapis.com/css?family=Montserrat:300,400,500\" rel=\"stylesheet\" type=\"text/css\">\n    <style type=\"text/css\">\n      @import url(https://fonts.googleapis.com/css?family=Montserrat:300,400,500);\n    </style>\n  <!--<![endif]-->\n\n\n    \n<style type=\"text/css\">\n  @media only screen and (min-width:480px) {\n    .mj-column-per-100 { width:100% !important; max-width: 100%; }\n.mj-column-per-35 { width:35% !important; max-width: 35%; }\n.mj-column-per-65 { width:65% !important; max-width: 65%; }\n.mj-column-per-80 { width:80% !important; max-width: 80%; }\n.mj-column-px-600 { width:600px !important; max-width: 600px; }\n  }\n</style>\n\n\n    <style type=\"text/css\">\n    \n\n.mj-carousel {\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  user-select: none;\n}\n\n.mj-carousel-cbe40664845b-icons-cell {\n  display: table-cell !important;\n  width: 44px !important;\n}\n\n.mj-carousel-radio,\n.mj-carousel-next,\n.mj-carousel-previous {\n  display: none !important;\n}\n\n.mj-carousel-thumbnail,\n.mj-carousel-next,\n.mj-carousel-previous {\n  touch-action: manipulation;\n}\n\n.mj-carousel-cbe40664845b-radio:checked + .mj-carousel-content .mj-carousel-image,.mj-carousel-cbe40664845b-radio:checked + * + .mj-carousel-content .mj-carousel-image,.mj-carousel-cbe40664845b-radio:checked + * + * + .mj-carousel-content .mj-carousel-image,.mj-carousel-cbe40664845b-radio:checked + * + * + * + .mj-carousel-content .mj-carousel-image {\n  display: none !important;\n}\n\n.mj-carousel-cbe40664845b-radio-1:checked + * + * + * + .mj-carousel-content .mj-carousel-image-1,.mj-carousel-cbe40664845b-radio-2:checked + * + * + .mj-carousel-content .mj-carousel-image-2,.mj-carousel-cbe40664845b-radio-3:checked + * + .mj-carousel-content .mj-carousel-image-3,.mj-carousel-cbe40664845b-radio-4:checked + .mj-carousel-content .mj-carousel-image-4 {\n  display: block !important;\n}\n\n.mj-carousel-previous-icons,\n.mj-carousel-next-icons,\n.mj-carousel-cbe40664845b-radio-1:checked + * + * + * + .mj-carousel-content .mj-carousel-next-2,.mj-carousel-cbe40664845b-radio-2:checked + * + * + .mj-carousel-content .mj-carousel-next-3,.mj-carousel-cbe40664845b-radio-3:checked + * + .mj-carousel-content .mj-carousel-next-4,.mj-carousel-cbe40664845b-radio-4:checked + .mj-carousel-content .mj-carousel-next-1,\n.mj-carousel-cbe40664845b-radio-1:checked + * + * + * + .mj-carousel-content .mj-carousel-previous-4,.mj-carousel-cbe40664845b-radio-2:checked + * + * + .mj-carousel-content .mj-carousel-previous-1,.mj-carousel-cbe40664845b-radio-3:checked + * + .mj-carousel-content .mj-carousel-previous-2,.mj-carousel-cbe40664845b-radio-4:checked + .mj-carousel-content .mj-carousel-previous-3 {\n  display: block !important;\n}\n\n.mj-carousel-cbe40664845b-radio-1:checked + * + * + * + .mj-carousel-content .mj-carousel-cbe40664845b-thumbnail-1,.mj-carousel-cbe40664845b-radio-2:checked + * + * + .mj-carousel-content .mj-carousel-cbe40664845b-thumbnail-2,.mj-carousel-cbe40664845b-radio-3:checked + * + .mj-carousel-content .mj-carousel-cbe40664845b-thumbnail-3,.mj-carousel-cbe40664845b-radio-4:checked + .mj-carousel-content .mj-carousel-cbe40664845b-thumbnail-4 {\n  border-color: #cccccc !important;\n}\n\n.mj-carousel-image img + div,\n.mj-carousel-thumbnail img + div {\n  display: none !important;\n}\n\n.mj-carousel-cbe40664845b-thumbnail:hover + * + * + * + .mj-carousel-main .mj-carousel-image,.mj-carousel-cbe40664845b-thumbnail:hover + * + * + .mj-carousel-main .mj-carousel-image,.mj-carousel-cbe40664845b-thumbnail:hover + * + .mj-carousel-main .mj-carousel-image,.mj-carousel-cbe40664845b-thumbnail:hover + .mj-carousel-main .mj-carousel-image {\n  display: none !important;\n}\n\n.mj-carousel-thumbnail:hover {\n  border-color: #fead0d !important;\n}\n\n.mj-carousel-cbe40664845b-thumbnail-1:hover + * + * + * + .mj-carousel-main .mj-carousel-image-1,.mj-carousel-cbe40664845b-thumbnail-2:hover + * + * + .mj-carousel-main .mj-carousel-image-2,.mj-carousel-cbe40664845b-thumbnail-3:hover + * + .mj-carousel-main .mj-carousel-image-3,.mj-carousel-cbe40664845b-thumbnail-4:hover + .mj-carousel-main .mj-carousel-image-4 {\n  display: block !important;\n}\n\n\n  .mj-carousel noinput { display:block !important; }\n  .mj-carousel noinput .mj-carousel-image-1 { display: block !important;  }\n  .mj-carousel noinput .mj-carousel-arrows,\n  .mj-carousel noinput .mj-carousel-thumbnails { display: none !important; }\n\n  [owa] .mj-carousel-thumbnail { display: none !important; }\n\n  @media screen yahoo {\n      .mj-carousel-cbe40664845b-icons-cell,\n      .mj-carousel-previous-icons,\n      .mj-carousel-next-icons {\n          display: none !important;\n      }\n\n      .mj-carousel-cbe40664845b-radio-1:checked + *+ *+ *+ .mj-carousel-content .mj-carousel-cbe40664845b-thumbnail-1 {\n          border-color: transparent;\n      }\n  }\n\n    \n\n@media only screen and (max-width:480px) {\n  table.full-width-mobile { width: 100% !important; }\n  td.full-width-mobile { width: auto !important; }\n}\n\n\n  noinput.mj-accordion-checkbox { display:block!important; }\n\n  @media yahoo, only screen and (min-width:0) {\n    .mj-accordion-element { display:block; }\n    input.mj-accordion-checkbox, .mj-accordion-less { display:none!important; }\n    input.mj-accordion-checkbox + * .mj-accordion-title { cursor:pointer; touch-action:manipulation; -webkit-user-select:none; -moz-user-select:none; user-select:none; }\n    input.mj-accordion-checkbox + * .mj-accordion-content { overflow:hidden; display:none; }\n    input.mj-accordion-checkbox + * .mj-accordion-more { display:block!important; }\n    input.mj-accordion-checkbox:checked + * .mj-accordion-content { display:block; }\n    input.mj-accordion-checkbox:checked + * .mj-accordion-more { display:none!important; }\n    input.mj-accordion-checkbox:checked + * .mj-accordion-less { display:block!important; }\n  }\n\n  @goodbye { @gmail }\n\n    </style>\n    \n    \n  </head>\n  <body style=\"background-color:#e5e5e5;text-align: center;\">\n    <image style=\"margin:auto;width: 700px, height: 1032px;\" src=\"https://mailbuilder-564cf.firebaseapp.com/assets/images/gdat-mail-header.png\"/>\n  </body>\n  </html>";
send({
    to: "fk@chatify.info",
    from: "office@teamformultimedia.at",
    subject: "GASTROdat Erinnerung von Flo",
    html: html
});
//# sourceMappingURL=sendEmailNode.js.map