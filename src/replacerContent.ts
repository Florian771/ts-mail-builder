import CONSTANTS from "./constants"

export function replaceTextAndHtml({
  mail,
  from,
  to
}: {
  mail: MailBuilder.Mail
  from: string
  to: string
}) {
  /* if (from === "{urlWeb}") {
    console.log("from:", from)
    console.log("to:", to)
    if (mail.html.includes(from)) {
      console.log("html before replace includes from")
    }
    if (mail.text.includes(from)) {
      console.log("text before replace includes from")
    }
  } */

  const updatedText: string = mail.text.replace(new RegExp(from, "g"), to)
  const updatedHtml: string = mail.html.replace(new RegExp(from, "g"), to)

  new RegExp("hello")

  const updatedMail = {
    ...mail,
    ...{
      text: updatedText,
      html: updatedHtml
    }
  }

  /* if (from === "{urlWeb}") {
    if (updatedHtml.includes(from)) {
      console.log("html after replace includes from")
    }
    if (updatedText.includes(from)) {
      console.log("text after replace includes from")
    }
  } */

  return updatedMail
}

export default function replacer({
  mail,
  mailContentsOutput
}: {
  mail: MailBuilder.Mail
  mailContentsOutput: MailBuilder.MailContentsOutput
}): MailBuilder.Mail {
  try {
    if (!mail.text) {
      throw { code: 0, message: "replacer: no text" }
    } else if (!mail.html) {
      throw { code: 0, message: "replacer: no html" }
    } else {
      let updatedMail: MailBuilder.Mail = mail

      if (
        mailContentsOutput.options.translations &&
        mailContentsOutput.options.translations.length > 0
      ) {
        mailContentsOutput.options.translations.map((translation: any) => {
          Object.keys(translation).forEach(key => {
            updatedMail = replaceTextAndHtml({
              mail: updatedMail,
              from: "{" + key + "}",
              to: translation[key]
            })
          })
        })
      }

      /* 
      updatedMail = replaceTextAndHtml({
        mail: updatedMail,
        from: "{LOGO_URL}",
        to: mailContentsOutput.options.general.logo
      })

      updatedMail = replaceTextAndHtml({
        mail: updatedMail,
        from: "{HEADLINE}",
        to: mail.subject
      })

      updatedMail = replaceTextAndHtml({
        mail: updatedMail,
        from: "{CONTENT}",
        to: mailContentsOutput.html
      })

      if (mailContentsOutput.links && mailContentsOutput.links.length > 0) {
        mail.html = mail.html.replace(
          "{BUTTON_URL}",
          mailContentsOutput.links[0].url
        )
        mail.html = mail.html.replace(
          "{BUTTON_TEXT}",
          mailContentsOutput.links[0].text
        )
      }

      updatedMail = replaceTextAndHtml({
        mail: updatedMail,
        from: "{HTML_LINK_TEXT}",
        to: mailContentsOutput.options.general.alternativeHtmlText
      })

      updatedMail = replaceTextAndHtml({
        mail: updatedMail,
        from: "{HTML_LINK_URL}",
        to: mailContentsOutput.options.general.alternativeHtmlUrl
      }) 
      */

      // console.log("CONSTANTS.web.urlWeb:", CONSTANTS.web.urlWeb)

      updatedMail = replaceTextAndHtml({
        mail: updatedMail,
        from: "{urlWeb}",
        to: CONSTANTS.web.urlWeb
      })

      return updatedMail
    }
  } catch (err) {
    console.log("compose - replacerContent - error: ", err.message)
    throw err
  }
}
