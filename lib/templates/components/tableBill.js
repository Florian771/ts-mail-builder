"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
var config_1 = require("../../config");
var utils_1 = require("../../utils");
var placeholder = {
    headline: "{TB_HEADLINE_KEY}",
    header1: "{TB_B_KEY}",
    header2: "{TB_F_KEY}",
    header3: "{TB_A_KEY}",
    header4: "{TB_P_KEY}",
    header5: "{TB_O_KEY}",
    content1: "{CONTENT1}",
    content2: "{CONTENT2}",
    content3: "{CONTENT3}",
    content4: "{CONTENT4}",
    content5: "{CONTENT5}"
};
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    var translations = utils_1.getTranslations({
        placeholder: placeholder,
        settings: settings
    });
    var html = "";
    var text = "";
    text = "\n  " + placeholder.headline.replace("KEY", settings.key) + config_1.textBreak + "\n  " + placeholder.header1.replace("KEY", settings.key) + ": " + placeholder.content1.replace("KEY", settings.key) + " " + config_1.textBreak + "\n  " + placeholder.header2.replace("KEY", settings.key) + ": " + placeholder.content2.replace("KEY", settings.key) + " " + config_1.textBreak + "\n  " + placeholder.header3.replace("KEY", settings.key) + ": " + placeholder.content3.replace("KEY", settings.key) + " " + config_1.textBreak + "\n  " + placeholder.header4.replace("KEY", settings.key) + ": " + placeholder.content4.replace("KEY", settings.key) + " " + config_1.textBreak + "\n  " + placeholder.header5.replace("KEY", settings.key) + ": " + placeholder.content5.replace("KEY", settings.key) + " " + config_1.textBreak + "\n  ";
    html = "\n  <mj-section padding-left=\"30px\" padding-right=\"30px\" background-color=\"#ffffff\">\n      <mj-column background-color=\"#eaeaea\">\n        <mj-text>\n          <b>\n            " + placeholder.headline.replace("KEY", settings.key) + "\n          </b>\n        </mj-text>\n        <mj-table>\n          <tr style=\"border-bottom:1px solid #eaeaea;text-align:left;padding:15px 0;\">\n            <th style=\"padding: 0 15px 0 0;\">" + placeholder.header1.replace("KEY", settings.key) + "</th>\n            <th style=\"padding: 0 15px;\">" + placeholder.header2.replace("KEY", settings.key) + "</th>\n            <th style=\"padding: 0 0 0 15px;\">" + placeholder.header3.replace("KEY", settings.key) + "</th>\n            <th style=\"padding: 0 0 0 15px;\">" + placeholder.header4.replace("KEY", settings.key) + "</th>\n            <th style=\"padding: 0 0 0 15px;\">" + placeholder.header5.replace("KEY", settings.key) + "</th>\n          </tr>\n          <tr>\n            <td style=\"padding: 0 15px 0 0;\">" + placeholder.content1.replace("KEY", settings.key) + "</td>\n            <td style=\"padding: 0 15px;\">" + placeholder.content2.replace("KEY", settings.key) + "</td>\n            <td style=\"padding: 0 0 0 15px;\">" + placeholder.content3.replace("KEY", settings.key) + "</td>\n            <td style=\"padding: 0 0 0 15px;\">" + placeholder.content4.replace("KEY", settings.key) + "</td>\n            <td style=\"padding: 0 0 0 15px;\">" + placeholder.content5.replace("KEY", settings.key) + "</td>\n          </tr>\n        </mj-table>\n      </mj-column>\n    </mj-section>";
    testParse_1.default({
        test: true,
        html: html,
        options: options,
        modulName: "tableBill"
    });
    return {
        html: html,
        text: text,
        translations: translations,
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=tableBill.js.map