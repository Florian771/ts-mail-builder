import { start } from "./index"
import * as chatify from "chatify-types"

const chatifyOptions: MailBuilder.DesignSettings = {
  TemplateName: "1",
  primaryColor: "#dd2476",
  secundaryColor: "#ff512f",
  headlineColor: "#111", //NEU
  textColor: "#9B9B9B", //c
  textColorHighlight: "#000", //c
  backgroundColor: "#F2F2F2", //c
  backgroundColor2: "#fff", //c
  headlineFontName:
    '"Montserrat", "Open Sans", "Helvetica Neue", Helvetica, sans-serif',
  bodyFontName: '"Open Sans", "Helvetica Neue", Helvetica, sans-serif',
  bottomSlanted: true
}

const user: chatify.UserSettings = {
  displayName: "Thomas Muster"
}
const client: chatify.Client = {
  companyName: "Muster GmbH",
  contactPersonFirstName: "Thomas",
  contactPersonLastName: "Musterss",
  email: "test@test.at",
  street: "Neutorstr. 4",
  postalCode: "5300",
  city: "Salzburg",
  country: "Österreich"
}
const data: any = {
  user: user,
  client: client
}
const designSettings: MailBuilder.DesignSettings = chatifyOptions
const options: MailBuilder.BuildOptions = {
  name: "gdatErinnerung1",
  from: "noreply@chatify.info",
  // to: "r.hofherr.gastrodat@gmail.com",
  to: "florian1677@gmail.com",
  subject: "Test Erinnerung (von Flo)",
  data: data,
  designSettings: designSettings,
  templateType: {
    // button: "",
    name: "zahlungsErinnerung",
    alternativeHtmlInfo: {
      text: true,
      url: true
    },
    image: {
      src:
        "https://mailbuilder-564cf.firebaseapp.com/assets/images/gdat-mail-header-1.jpg",
      key: "IMAGE_KEY",
      width: 600
      // height: 231
    },
    header: {
      logo: "",
      context: true,
      menuLinks: 3
    },
    text: {
      key: "TXT"
    },
    imageText: {
      src:
        "https://mailbuilder-564cf.firebaseapp.com/assets/images/gdat-mail-profil-foto-1.jpg",
      key: "IMAGE_KEY",
      width: 124,
      height: 119
    },
    text2: {
      key: "TXT2"
    },
    tableBill: {
      key: "1",
      headline: "Zusammenfassung",
      bill: "{NR}",
      amount: "{PREIS}",
      from: "{DATUM}",
      paid: "{PREIS2}",
      open: "{PREIS3}"
    },
    sectionBottom: {
      active: true
      // type: "diagonal"
    },
    footer: {
      unsubscribeText: false,
      unsubscribeUrl: false,
      name: true,
      street: true,
      postalCode: true,
      city: true,
      country: true,
      phone: true,
      fax: true,
      email: true,
      website: true,
      uid: true
    }
  },
  contentType: "userCreated",
  buildType: "file", // "mail", // "file",
  testParse: true,
  translations: [
    { HTML_LINK_TEXT: "GASTROdat - Hotel.Software & Marketing" },
    { HTML_LINK_URL: "" },
    { MAIL_CONTEXT: "" },
    { LOGO_URL: "https://admin.chatify.info/assets/img/logo-claim@2x.png" },
    { LOGO_TITLE: "Demo Hotel" },
    { LOGO_TARGET: "" }
  ]
}
start(options).then((res: MailBuilder.Result) => {
  /* tableBill: {
    key: "1",
    headline: "Zusammenfassung",
    bill: "Rechnung",
    amount: "Betrag",
    from: "vom",
    paid: "bisher bezahlt",
    open: "offen"
  } */
  return res
})
