export interface TextSettings {
    key: string;
}
export interface Text {
    text: string;
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: TextSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
