export const textBreak: string = "\r\n"

export const defaultOptions: MailBuilder.DesignSettings = {
  TemplateName: "1",
  primaryColor: "#000",
  secundaryColor: "#777",
  headlineColor: "#111", //NEU
  textColor: "#999",
  backgroundColor: "#fff",
  backgroundColor2: "#f4f4f4", //NEU
  headlineFontName:
    '"Montserrat", "Open Sans", "Helvetica Neue", Helvetica, sans-serif',
  bodyFontName: '"Open Sans", "Helvetica Neue", Helvetica, sans-serif'
}

export const defaultClientData: any = {
  // gastrodat.HotelData = {
  Ort: "Grödig",
  HotelName: "Demo Hotel*****",
  PersName: "",
  Strasse: "Friedensstrasse 8",
  Gruss: "",
  PLZ: "5082",
  Telefon: "0043 6246 738 73 - 0",
  Fax: "",
  UID: "ATU44806304",
  Nation: "A",
  KtoNr: "12345679",
  Bank: "Volksbank",
  BLZ: "45010",
  Email: "office@gastrodat.com",
  BIC: "VBOEATWWSAL",
  IBAN: "AT123456789"
}
