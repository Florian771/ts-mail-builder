import testParse from "../../testParse"
import { textBreak } from "../../config"
import { getTranslations } from "../../utils"

export interface HeaderDesignerSettings {
  logo: boolean
  context: boolean
  menuLinks: number
  logoWidth?: string
  logoHeight?: string
}

export interface HeaderDesigner {
  context?: string
  logoSrc?: string
  logoUrl?: string
  logoTitle?: string
  logoTarget?: string
  menuLink?: MailBuilder.TextLink
}

const placeholder: HeaderDesigner = {
  context: "{MAIL_CONTEXT}",
  logoSrc: "{LOGO_SRC}",
  logoUrl: "{LOGO_URL}",
  logoTitle: "{LOGO_TITLE}",
  logoTarget: "{LOGO_TARGET}",
  menuLink: {
    text: "{WEBSITE_TEXT_XXX}",
    title: "",
    url: "{WEBSITE_URL_XXX}"
  }
}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: HeaderDesignerSettings
}): MailBuilder.Component => {
  const translations: string[] = getTranslations({
    placeholder: placeholder,
    settings: settings
  })
  let html: string = ""
  let text: string = textBreak
  let textMenuLinks: string = ""
  let htmlMenuLinks: string = ""
  let textLogo: string = ""
  let htmlLogo: string = ""
  let textContext: string = ""
  let htmlContext: string = ""
  const padding = "&nbsp;&nbsp;&nbsp;"

  if (settings.logo) {
    /* let logoWidth = 100
    const ratio = settings.logoWidth / settings.logoHeight
    let logoHeight = logoWidth / ratio
    if (logoHeight > 100) {
      logoHeight = 100
      logoWidth = logoHeight * ratio
    } */
    if (settings.logoWidth) {
      settings.logoWidth = `width="${settings.logoWidth}px"`
    }
    if (settings.logoHeight) {
      settings.logoHeight = `height="${settings.logoHeight}px"`
    }
    textLogo = placeholder.logoTitle + textBreak
    htmlLogo = `<mj-image href="{COMPANY_WEBSITE}" ${settings.logoWidth} ${
      settings.logoHeight
    } src="${
      placeholder.logoSrc
    }" alt="{LOGO_TITLE}" align="center" border="none" padding="0px"></mj-image>`
  } else if (settings.context) {
    textContext = placeholder.context + textBreak
    htmlContext = ` <mj-text align="left" font-size="16px" font-weight="500">${
      placeholder.context
    }</mj-text>`
  }

  if (settings.menuLinks && settings.menuLinks > 0) {
    for (let index = 0; index < settings.menuLinks; index++) {
      textMenuLinks += `${placeholder.menuLink.text.replace(
        "XXX",
        String(index)
      )}: ${placeholder.menuLink.url.replace("XXX", String(index))}${textBreak}`
      if (htmlMenuLinks) {
        htmlMenuLinks += padding
      }
      htmlMenuLinks += `<a target="_blank" href="${placeholder.menuLink.url.replace(
        "XXX",
        String(index)
      )}" style="color: {textColor}; text-decoration: none;">
        ${placeholder.menuLink.text.replace("XXX", String(index))}</a>`
    }
  }

  text = textLogo
  text += textContext
  text += textMenuLinks

  html = `<mj-section padding="20px 20px 20px 20px" background-color="{backgroundColor2}">
  <mj-column width="35%">
    ${htmlLogo}${htmlContext}
  </mj-column>
  <mj-column width="65%">
    <mj-text align="center" font-size="11px" padding="0px">`
  // html = `ratio: ${ratio} - logoWidth: ${logoWidth} - logoHeight: ${logoHeight} - `
  html += `${htmlMenuLinks}
    </mj-text>
  </mj-column>
</mj-section>`

  // console.log("header - text: " + text)
  // console.log("header - html: " + html)

  testParse({
    test: true,
    html: html,
    options: options,
    modulName: "headerDesigner"
  })
  return {
    html: html,
    text: text,
    translations: translations,
    parse: (args: any) => testParse(args)
  }
}
