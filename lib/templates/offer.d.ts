export declare function getHtml({ options }: {
    options: MailBuilder.BuildOptions;
}): Promise<MailBuilder.Component>;
