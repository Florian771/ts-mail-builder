export interface ZusatzTextBilder {
    name?: string;
    url: string;
}
export interface TextWithImageSettings {
    key: string;
    image?: ZusatzTextBilder;
}
export interface TextWithImage {
    text: string;
    imageName: string;
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: TextWithImageSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
