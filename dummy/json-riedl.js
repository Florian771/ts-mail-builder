export default {
    "API": {
        "HotelData": {
            "Ort": "Stumm",
            "HotelName": "Hotel Riedl, Cafe, Konditorei",
            "PersName": "",
            "Strasse": "Dorfstr. 9",
            "Gruss": "",
            "PLZ": "6275",
            "Telefon": "",
            "Fax": "",
            "UID": "",
            "Nation": "A",
            "KtoNr": "",
            "Email": "info@hotel-riedl.com",
            "Bank": "",
            "BLZ": "",
            "BIC": "SPSCAT22",
            "IBAN": "AT562051000600600209"
        },
        "ReservationData": {
            "LFNUMMER": "26155",
            "ZIMMER_NR": "14",
            "PERS_NR": "16384",
            "PERSON": "Wagner Marcel",
            "VON": "08.01.2019",
            "BIS": "12.01.2019",
            "ANZ_K1": "0",
            "ANZ_K2": "0",
            "ANZ_E": "1",
            "ANZ_K3": "0",
            "ANZ_K4": "0",
            "ANZ_E2": "0",
            "ANGEBOTSURL": ""
        },
        "GuestData": {
            "LFNUMMER": "16384",
            "ANREDE_H": "Familie",
            "FAMNAME": "Wagner",
            "VORNAME": "Marcel",
            "TITEL": "",
            "GESCHLECHT": "",
            "STRASSE": "",
            "PLZ": "",
            "ORT": "",
            "ANREDE_FAM": "Liebe Familie Wagner",
            "SPRACHE": "D Deutsch",
            "NATION": "",
            "ZUSNAME": "",
            "P_EMAIL": "wagner@gastrodat.com",
            "NATION_TXT": ""
        },
        "RoomData": {
            "ZIMMER_NR": "14",
            "ZIMMER_NAME": "",
            "GEEIGNET_VON": "1",
            "GEEIGNET_BIS": "1",
            "ZI_KAT": "0",
            "ZIMMER_SAUBER": "N",
            "GEEIGNET_STD": "1",
            "STANDARDLEISTUNG": "",
            "TEXT123_exception": "Die Datei \"C:\\gdat\\ZInfo\\ZIMMER_14.XML\" konnte nicht gefunden werden.",
            "ZIMMERBILD": "/Content/images/invalidimage.png",
            "ZIMMERGRUNDRISS": "/Content/images/invalidimage.png"
        },
        "ExtrasData": {
            "Posio": [{
                "FKT_NR": "41497",
                "P_VON": "08.01.2019",
                "P_BIS": "12.01.2019",
                "P_TEXT": "Turm Doppelzimmer",
                "P_ANZ": "1",
                "P_SUM": "478",
                "P_PREIS": "119.5",
                "P_RABATT": "0",
                "P_KG_NR": "10016",
                "GROUPINDEX": "1",
                "EXTRA_NR": "10016",
                "DAUER_VON": "1",
                "DAUER_BIS": "999",
                "BESCHREIBUNG": "<li>Turmzimmer sind südseitig mit herrlichem Panoramablick auf die Zillertaler Berge</li>\n<li>ein großes Wohn-Schlafzimmer mit Holzfußboden</li>\n<li>Erker mit Sitzgelegenheit</li>\n<li>Badezimmer mit Duschbadewanne, separates WC</li>\n<li>Sat-Flat-TV, Haarföhn, Safe, Telefon</li>\n<li>kostenloses WLAN</li>\n<li>großzügiger Balkon</li>\n<li>sowie eine Wellnesstasche mit Bademantel, Badeslipper und Saunatuch</li><br>>",
                "EXTRABILD": "https://www.weratech-files.com/images/200902/gastrodat/Extras/EXTRA_10016.jpg?timestamp=636620579270000000"
            }, {
                "FKT_NR": "41499",
                "P_VON": "05.06.2018",
                "P_BIS": "06.06.2018",
                "P_TEXT": "ODER",
                "P_ANZ": "0",
                "P_SUM": "0",
                "P_PREIS": "0",
                "P_RABATT": "0",
                "P_KG_NR": "0",
                "GROUPINDEX": "-1"
            }, {
                "FKT_NR": "41500",
                "P_VON": "08.01.2019",
                "P_BIS": "12.01.2019",
                "P_TEXT": "Turm Doppelzimmer",
                "P_ANZ": "1",
                "P_SUM": "478",
                "P_PREIS": "119.5",
                "P_RABATT": "0",
                "P_KG_NR": "10016",
                "GROUPINDEX": "2",
                "EXTRA_NR": "10016",
                "DAUER_VON": "1",
                "DAUER_BIS": "999",
                "BESCHREIBUNG": "<li>Turmzimmer sind südseitig mit herrlichem Panoramablick auf die Zillertaler Berge</li>\n<li>ein großes Wohn-Schlafzimmer mit Holzfußboden</li>\n<li>Erker mit Sitzgelegenheit</li>\n<li>Badezimmer mit Duschbadewanne, separates WC</li>\n<li>Sat-Flat-TV, Haarföhn, Safe, Telefon</li>\n<li>kostenloses WLAN</li>\n<li>großzügiger Balkon</li>\n<li>sowie eine Wellnesstasche mit Bademantel, Badeslipper und Saunatuch</li><br>>",
                "EXTRABILD": "https://www.weratech-files.com/images/200902/gastrodat/Extras/EXTRA_10016.jpg?timestamp=636620579270000000"
            }]
        },
        "ThemeData": {
            "Themes": {
                "gdatTheme": {
                    "theme_pics": {
                        "gdatPicture": [{
                            "PicId": "2aa677ff551b4",
                            "PicName": "Im Herzen des Zillertals nur 500 Meter vom weltbekannten Skigebiet Hochzillertal/Hochfügen entfernt",
                            "PicPath": "$i_offer\\theme_Wintersaison\\2aa677ff551b4.jpg",
                            "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2aa677ff551b4.jpg",
                            "PicHTML_Link": "",
                            "PicKategorie": "Im Herzen des Zillertals",
                            "LastPicUpload": "2018-06-05T16:00:12.5584342+02:00"
                        }, {
                            "PicId": "2aa74ebdff68f",
                            "PicName": "Wintererlebniss der besonderen KLASSE",
                            "PicPath": "$i_offer\\theme_Wintersaison\\2aa74ebdff68f.jpg",
                            "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2aa74ebdff68f.jpg",
                            "PicHTML_Link": "",
                            "PicKategorie": "Winter",
                            "LastPicUpload": "2018-06-05T16:00:12.5740342+02:00"
                        }, {
                            "PicId": "2aa75332c2358",
                            "PicName": "Klein, fein, herzlich & genussvoll",
                            "PicPath": "$i_offer\\theme_Wintersaison\\2aa75332c2358.jpg",
                            "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2aa75332c2358.jpg",
                            "PicHTML_Link": "",
                            "PicKategorie": "Hotel Riedl",
                            "LastPicUpload": "2018-06-05T16:00:12.5896342+02:00"
                        }, {
                            "PicId": "2ab844c7e8501",
                            "PicName": "Wohnen wie Zuhause",
                            "PicPath": "$i_offer\\theme_Wintersaison\\2ab844c7e8501.jpg",
                            "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2ab844c7e8501.jpg",
                            "PicHTML_Link": "",
                            "PicKategorie": "Zimmer",
                            "LastPicUpload": "2018-06-05T16:00:12.6052343+02:00"
                        }]
                    },
                    "theme_teasers": {
                        "gdatTeaser": [{
                            "Teaser_pic": {
                                "PicId": "2aa7563198327",
                                "PicName": "...500 m nah an der Talstation des bekannten Skigebiets Hochzillertal",
                                "PicPath": "$i_offer\\teaser_Skifahren im Zillertal\\2aa7563198327.jpg",
                                "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2aa7563198327.jpg",
                                "PicHTML_Link": "",
                                "PicKategorie": "",
                                "LastPicUpload": "2018-06-05T16:00:12.6052343+02:00"
                            },
                            "validFrom": "2018-12-15T11:38:30",
                            "validTo": "2019-04-01T11:38:30",
                            "SuchwortListe": "",
                            "TeaserId": "2aa7563198327",
                            "TeaserName": "Skifahren im Zillertal"
                        }, {
                            "Teaser_pic": {
                                "PicId": "2b01dd037d118",
                                "PicName": "...genießen Sie die bezaubernde Bergkulisse auch im Winter",
                                "PicPath": "$i_offer\\teaser_neuer ThemenTeaser\\2b01dd037d118.jpg",
                                "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2b01dd037d118.jpg",
                                "PicHTML_Link": "",
                                "PicKategorie": "",
                                "LastPicUpload": "2018-06-05T16:00:12.6208343+02:00"
                            },
                            "validFrom": "2018-12-15T16:26:44",
                            "validTo": "2019-04-23T16:26:44",
                            "SuchwortListe": "",
                            "TeaserId": "2b01dd037d118",
                            "TeaserName": "Entspannung Pur"
                        }, {
                            "Teaser_pic": {
                                "PicId": "2b01e956955ce",
                                "PicName": "... wir legen stets besonderes Augenmerk auf regionale Zutaten",
                                "PicPath": "$i_offer\\teaser_neuer ThemenTeaser\\2b01e956955ce.jpg",
                                "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2b01e956955ce.jpg",
                                "PicHTML_Link": "",
                                "PicKategorie": "",
                                "LastPicUpload": "2018-06-05T16:00:12.6364343+02:00"
                            },
                            "validFrom": "2018-12-15T16:32:15",
                            "validTo": "2019-04-25T16:32:15",
                            "SuchwortListe": "",
                            "TeaserId": "2b01e956955ce",
                            "TeaserName": "Schmankerl aus dem Zillertal"
                        }]
                    },
                    "validFrom": "2018-12-15T09:58:10",
                    "validTo": "2019-04-23T09:58:10",
                    "SuchwortListe": "",
                    "ThemeId": "2aa675f08b696",
                    "ThemeName": "Wintersaison 2018"
                }
            },
            "BilderGalerie_pics": {
                "gdatPicture": [{
                    "PicId": "1f84429aa129c",
                    "PicName": "Genuss pur: Erleben Sie den unverfälschten Geschmack der Alpen!",
                    "PicPath": "$i_offer\\impressions\\impression_1.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/impression_1.jpg",
                    "PicHTML_Link": "https://www.hotel-riedl.com",
                    "PicKategorie": "Kulinarische Köstlichkeiten",
                    "LastPicUpload": "2018-06-05T16:00:13.0333353+02:00"
                }, {
                    "PicId": "1f8443cca4a89",
                    "PicName": "Natur pur",
                    "PicPath": "$i_offer\\impressions\\impression_2.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/impression_2.jpg",
                    "PicHTML_Link": "https://www.hotel-riedl.com",
                    "PicKategorie": "Natur pur",
                    "LastPicUpload": "2018-06-05T16:00:13.0489353+02:00"
                }, {
                    "PicId": "27100ddc7531a",
                    "PicName": "Winterspass für GROSS und klein",
                    "PicPath": "$i_offer\\impressions\\impression_3.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/impression_3.jpg",
                    "PicHTML_Link": "https://www.hotel-riedl.com",
                    "PicKategorie": "Winter",
                    "LastPicUpload": "2018-06-05T16:00:13.0489353+02:00"
                }, {
                    "PicId": "2aa79bffc26f0",
                    "PicName": "Genießen ",
                    "PicPath": "$i_offer\\impressions\\2aa79bffc26f0.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2aa79bffc26f0.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Kulinarische Köstlichkeiten",
                    "LastPicUpload": "2018-06-05T16:00:13.0645354+02:00"
                }, {
                    "PicId": "2ace214117201",
                    "PicName": "3/4 Genießerpension",
                    "PicPath": "$i_offer\\impressions\\2ace214117201.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2ace214117201.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Kulinarische Köstlichkeiten",
                    "LastPicUpload": "2018-06-05T16:00:13.0801354+02:00"
                }, {
                    "PicId": "2b01ce52cb0ca",
                    "PicName": "Wellness für die Seele",
                    "PicPath": "$i_offer\\impressions\\2b01ce52cb0ca.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2b01ce52cb0ca.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Wellness für die Seele",
                    "LastPicUpload": "2018-06-05T16:00:13.0957354+02:00"
                }, {
                    "PicId": "2b0a64a8a1f4a",
                    "PicName": "Spüren Sie die erfrischende Wirkung des Wassers",
                    "PicPath": "$i_offer\\impressions\\2b0a64a8a1f4a.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2b0a64a8a1f4a.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Wellness für die Seele",
                    "LastPicUpload": "2018-06-05T16:00:13.1113354+02:00"
                }]
            },
            "impression_pics": {
                "gdatPicture": [{
                    "PicId": "270fffa2d23f9",
                    "PicName": "Natur pur Erleben",
                    "PicPath": "$i_offer\\theme_Sommersaison\\270fffa2d23f9.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/270fffa2d23f9.jpg",
                    "PicHTML_Link": "https://www.hotel-riedl.com",
                    "PicKategorie": "Natur pur erleben",
                    "LastPicUpload": "2018-06-05T16:00:12.464834+02:00"
                }, {
                    "PicId": "27100573d7ea0",
                    "PicName": "Wohnen wie Zuhause",
                    "PicPath": "$i_offer\\theme_Sommersaison\\27100573d7ea0.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/27100573d7ea0.jpg",
                    "PicHTML_Link": "https://www.hotel-riedl.com",
                    "PicKategorie": "Zimmer",
                    "LastPicUpload": "2018-06-05T16:00:12.480434+02:00"
                }, {
                    "PicId": "2ab86a2b2483c",
                    "PicName": "Urlaub mit Herz & Genuss",
                    "PicPath": "$i_offer\\theme_Sommersaison\\2ab86a2b2483c.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2ab86a2b2483c.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Hotel Riedl",
                    "LastPicUpload": "2018-06-05T16:00:12.4960341+02:00"
                }, {
                    "PicId": "2ace397d2c14b",
                    "PicName": "Erleben Sie den unverfälschten Geschmack der Alpen",
                    "PicPath": "$i_offer\\theme_Sommersaison\\2ace397d2c14b.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2ace397d2c14b.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Essen",
                    "LastPicUpload": "2018-06-05T16:00:12.5116341+02:00"
                }, {
                    "PicId": "1e87f40a6f428",
                    "PicName": "...neue Kraft gewinnen zwischen traumhafter Bergkulisse",
                    "PicPath": "$i_offer\\teaser_Entspannung Pur\\1e87f40a6f428.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/1e87f40a6f428.jpg",
                    "PicHTML_Link": "https://www.hotel-riedl.com",
                    "PicKategorie": "Wellness",
                    "LastPicUpload": "2018-06-05T16:00:12.5428341+02:00"
                }, {
                    "PicId": "2aa677ff551b4",
                    "PicName": "Im Herzen des Zillertals nur 500 Meter vom weltbekannten Skigebiet Hochzillertal/Hochfügen entfernt",
                    "PicPath": "$i_offer\\theme_Wintersaison\\2aa677ff551b4.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2aa677ff551b4.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Im Herzen des Zillertals",
                    "LastPicUpload": "2018-06-05T16:00:12.5584342+02:00"
                }, {
                    "PicId": "2aa74ebdff68f",
                    "PicName": "Wintererlebniss der besonderen KLASSE",
                    "PicPath": "$i_offer\\theme_Wintersaison\\2aa74ebdff68f.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2aa74ebdff68f.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Winter",
                    "LastPicUpload": "2018-06-05T16:00:12.5740342+02:00"
                }, {
                    "PicId": "2aa75332c2358",
                    "PicName": "Klein, fein, herzlich & genussvoll",
                    "PicPath": "$i_offer\\theme_Wintersaison\\2aa75332c2358.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2aa75332c2358.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Hotel Riedl",
                    "LastPicUpload": "2018-06-05T16:00:12.5896342+02:00"
                }, {
                    "PicId": "2ab844c7e8501",
                    "PicName": "Wohnen wie Zuhause",
                    "PicPath": "$i_offer\\theme_Wintersaison\\2ab844c7e8501.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2ab844c7e8501.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Zimmer",
                    "LastPicUpload": "2018-06-05T16:00:12.6052343+02:00"
                }, {
                    "PicId": "270fffa2d23f9",
                    "PicName": "Natur pur Erleben",
                    "PicPath": "$i_offer\\theme_Sommersaison\\270fffa2d23f9.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/270fffa2d23f9.jpg",
                    "PicHTML_Link": "https://www.hotel-riedl.com",
                    "PicKategorie": "Natur pur erleben",
                    "LastPicUpload": "2018-06-05T16:00:12.6520343+02:00"
                }, {
                    "PicId": "27100573d7ea0",
                    "PicName": "Wohnen wie Zuhause",
                    "PicPath": "$i_offer\\theme_Sommersaison\\27100573d7ea0.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/27100573d7ea0.jpg",
                    "PicHTML_Link": "https://www.hotel-riedl.com",
                    "PicKategorie": "Zimmer",
                    "LastPicUpload": "2018-06-05T16:00:12.6676344+02:00"
                }, {
                    "PicId": "2ab86a2b2483c",
                    "PicName": "Urlaub mit Herz & Genuss",
                    "PicPath": "$i_offer\\theme_Sommersaison\\2ab86a2b2483c.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2ab86a2b2483c.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Hotel Riedl",
                    "LastPicUpload": "2018-06-05T16:00:12.6676344+02:00"
                }, {
                    "PicId": "2ace397d2c14b",
                    "PicName": "Erleben Sie den unverfälschten Geschmack der Alpen",
                    "PicPath": "$i_offer\\theme_Sommersaison\\2ace397d2c14b.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2ace397d2c14b.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Essen",
                    "LastPicUpload": "2018-06-05T16:00:12.6832344+02:00"
                }, {
                    "PicId": "1e87f40a6f428",
                    "PicName": "...neue Kraft gewinnen zwischen traumhafter Bergkulisse",
                    "PicPath": "$i_offer\\teaser_Entspannung Pur\\1e87f40a6f428.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/1e87f40a6f428.jpg",
                    "PicHTML_Link": "https://www.hotel-riedl.com",
                    "PicKategorie": "Wellness",
                    "LastPicUpload": "2018-06-05T16:00:12.7144344+02:00"
                }, {
                    "PicId": "2aa677ff551b4",
                    "PicName": "Im Herzen des Zillertals nur 500 Meter vom weltbekannten Skigebiet Hochzillertal/Hochfügen entfernt",
                    "PicPath": "$i_offer\\theme_Wintersaison\\2aa677ff551b4.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2aa677ff551b4.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Im Herzen des Zillertals",
                    "LastPicUpload": "2018-06-05T16:00:12.7300345+02:00"
                }, {
                    "PicId": "2aa74ebdff68f",
                    "PicName": "Wintererlebniss der besonderen KLASSE",
                    "PicPath": "$i_offer\\theme_Wintersaison\\2aa74ebdff68f.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2aa74ebdff68f.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Winter",
                    "LastPicUpload": "2018-06-05T16:00:12.7581347+02:00"
                }, {
                    "PicId": "2aa75332c2358",
                    "PicName": "Klein, fein, herzlich & genussvoll",
                    "PicPath": "$i_offer\\theme_Wintersaison\\2aa75332c2358.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2aa75332c2358.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Hotel Riedl",
                    "LastPicUpload": "2018-06-05T16:00:12.7631348+02:00"
                }, {
                    "PicId": "2ab844c7e8501",
                    "PicName": "Wohnen wie Zuhause",
                    "PicPath": "$i_offer\\theme_Wintersaison\\2ab844c7e8501.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2ab844c7e8501.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Zimmer",
                    "LastPicUpload": "2018-06-05T16:00:12.7787348+02:00"
                }, {
                    "PicId": "270fffa2d23f9",
                    "PicName": "Natur pur Erleben",
                    "PicPath": "$i_offer\\theme_Sommersaison\\270fffa2d23f9.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/270fffa2d23f9.jpg",
                    "PicHTML_Link": "https://www.hotel-riedl.com",
                    "PicKategorie": "Natur pur erleben",
                    "LastPicUpload": "2018-06-05T16:00:12.8255349+02:00"
                }, {
                    "PicId": "27100573d7ea0",
                    "PicName": "Wohnen wie Zuhause",
                    "PicPath": "$i_offer\\theme_Sommersaison\\27100573d7ea0.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/27100573d7ea0.jpg",
                    "PicHTML_Link": "https://www.hotel-riedl.com",
                    "PicKategorie": "Zimmer",
                    "LastPicUpload": "2018-06-05T16:00:12.8411349+02:00"
                }, {
                    "PicId": "2ab86a2b2483c",
                    "PicName": "Urlaub mit Herz & Genuss",
                    "PicPath": "$i_offer\\theme_Sommersaison\\2ab86a2b2483c.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2ab86a2b2483c.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Hotel Riedl",
                    "LastPicUpload": "2018-06-05T16:00:12.8567349+02:00"
                }, {
                    "PicId": "2ace397d2c14b",
                    "PicName": "Erleben Sie den unverfälschten Geschmack der Alpen",
                    "PicPath": "$i_offer\\theme_Sommersaison\\2ace397d2c14b.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2ace397d2c14b.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Essen",
                    "LastPicUpload": "2018-06-05T16:00:12.872335+02:00"
                }, {
                    "PicId": "1e87f40a6f428",
                    "PicName": "...neue Kraft gewinnen zwischen traumhafter Bergkulisse",
                    "PicPath": "$i_offer\\teaser_Entspannung Pur\\1e87f40a6f428.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/1e87f40a6f428.jpg",
                    "PicHTML_Link": "https://www.hotel-riedl.com",
                    "PicKategorie": "Wellness",
                    "LastPicUpload": "2018-06-05T16:00:12.903535+02:00"
                }, {
                    "PicId": "2aa677ff551b4",
                    "PicName": "Im Herzen des Zillertals nur 500 Meter vom weltbekannten Skigebiet Hochzillertal/Hochfügen entfernt",
                    "PicPath": "$i_offer\\theme_Wintersaison\\2aa677ff551b4.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2aa677ff551b4.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Im Herzen des Zillertals",
                    "LastPicUpload": "2018-06-05T16:00:12.919135+02:00"
                }, {
                    "PicId": "2aa74ebdff68f",
                    "PicName": "Wintererlebniss der besonderen KLASSE",
                    "PicPath": "$i_offer\\theme_Wintersaison\\2aa74ebdff68f.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2aa74ebdff68f.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Winter",
                    "LastPicUpload": "2018-06-05T16:00:12.9347351+02:00"
                }, {
                    "PicId": "2aa75332c2358",
                    "PicName": "Klein, fein, herzlich & genussvoll",
                    "PicPath": "$i_offer\\theme_Wintersaison\\2aa75332c2358.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2aa75332c2358.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Hotel Riedl",
                    "LastPicUpload": "2018-06-05T16:00:12.9503351+02:00"
                }, {
                    "PicId": "2ab844c7e8501",
                    "PicName": "Wohnen wie Zuhause",
                    "PicPath": "$i_offer\\theme_Wintersaison\\2ab844c7e8501.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2ab844c7e8501.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Zimmer",
                    "LastPicUpload": "2018-06-05T16:00:12.9659351+02:00"
                }, {
                    "PicId": "1f84429aa129c",
                    "PicName": "Genuss pur: Erleben Sie den unverfälschten Geschmack der Alpen!",
                    "PicPath": "$i_offer\\impressions\\impression_1.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/impression_1.jpg",
                    "PicHTML_Link": "https://www.hotel-riedl.com",
                    "PicKategorie": "Kulinarische Köstlichkeiten",
                    "LastPicUpload": "2018-06-05T16:00:13.0333353+02:00"
                }, {
                    "PicId": "1f8443cca4a89",
                    "PicName": "Natur pur",
                    "PicPath": "$i_offer\\impressions\\impression_2.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/impression_2.jpg",
                    "PicHTML_Link": "https://www.hotel-riedl.com",
                    "PicKategorie": "Natur pur",
                    "LastPicUpload": "2018-06-05T16:00:13.0489353+02:00"
                }, {
                    "PicId": "27100ddc7531a",
                    "PicName": "Winterspass für GROSS und klein",
                    "PicPath": "$i_offer\\impressions\\impression_3.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/impression_3.jpg",
                    "PicHTML_Link": "https://www.hotel-riedl.com",
                    "PicKategorie": "Winter",
                    "LastPicUpload": "2018-06-05T16:00:13.0489353+02:00"
                }, {
                    "PicId": "2aa79bffc26f0",
                    "PicName": "Genießen ",
                    "PicPath": "$i_offer\\impressions\\2aa79bffc26f0.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2aa79bffc26f0.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Kulinarische Köstlichkeiten",
                    "LastPicUpload": "2018-06-05T16:00:13.0645354+02:00"
                }, {
                    "PicId": "2ace214117201",
                    "PicName": "3/4 Genießerpension",
                    "PicPath": "$i_offer\\impressions\\2ace214117201.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2ace214117201.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Kulinarische Köstlichkeiten",
                    "LastPicUpload": "2018-06-05T16:00:13.0801354+02:00"
                }, {
                    "PicId": "2b01ce52cb0ca",
                    "PicName": "Wellness für die Seele",
                    "PicPath": "$i_offer\\impressions\\2b01ce52cb0ca.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2b01ce52cb0ca.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Wellness für die Seele",
                    "LastPicUpload": "2018-06-05T16:00:13.0957354+02:00"
                }, {
                    "PicId": "2b0a64a8a1f4a",
                    "PicName": "Spüren Sie die erfrischende Wirkung des Wassers",
                    "PicPath": "$i_offer\\impressions\\2b0a64a8a1f4a.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2b0a64a8a1f4a.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Wellness für die Seele",
                    "LastPicUpload": "2018-06-05T16:00:13.1113354+02:00"
                }]
            },
            "AngebotsName": "lalala",
            "StammDaten": {
                "HotelLogo_pic": {
                    "PicId": "1ecfce182b605",
                    "PicName": "HotelLogo",
                    "PicPath": "$i_offer\\LOGO\\StammDaten_HotelLogo_pic.png",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/StammDaten_HotelLogo_pic.png",
                    "PicHTML_Link": "https://www.hotel-riedl.com",
                    "LastPicUpload": "2018-06-05T16:00:13.0127352+02:00"
                },
                "Section_Liste": {
                    "Section": [{
                        "ModulName": "LOGO_BLOCK",
                        "ModulId": "",
                        "ModulHash": "LOGO_BLOCK",
                        "SortOrder": "0"
                    }, {
                        "ModulName": "SLIDESHOW",
                        "ModulId": "",
                        "ModulHash": "SLIDESHOW",
                        "SortOrder": "1"
                    }, {
                        "ModulName": "GREETING",
                        "ModulId": "",
                        "ModulHash": "GREETING",
                        "SortOrder": "2"
                    }, {
                        "ModulName": "WELCOME_TEXT",
                        "ModulId": "",
                        "ModulHash": "WELCOME_TEXT",
                        "SortOrder": "3"
                    }, {
                        "ModulName": "TEASER_BLOCK",
                        "ModulId": "",
                        "ModulHash": "TEASER_BLOCK",
                        "SortOrder": "4"
                    }, {
                        "ModulName": "ANGEBOTS_TEXT",
                        "ModulId": "",
                        "ModulHash": "ANGEBOTS_TEXT",
                        "SortOrder": "5"
                    }, {
                        "ModulName": "VARIANTEN_VORSCHAU",
                        "ModulId": "",
                        "ModulHash": "VARIANTEN_VORSCHAU",
                        "SortOrder": "6"
                    }, {
                        "ModulName": "ANGEBOT_DETAILS",
                        "ModulId": "",
                        "ModulHash": "ANGEBOT_DETAILS",
                        "SortOrder": "7"
                    }, {
                        "ModulName": "ZWISCHEN_BILD5",
                        "ModulId": "",
                        "ModulHash": "ZWISCHEN_BILD5",
                        "SortOrder": "8"
                    }, {
                        "ModulName": "ZUSATZ_TEXT4",
                        "ModulId": "",
                        "ModulHash": "ZUSATZ_TEXT4",
                        "SortOrder": "9"
                    }, {
                        "ModulName": "ZWISCHEN_BILD2",
                        "ModulId": "",
                        "ModulHash": "ZWISCHEN_BILD2",
                        "SortOrder": "10"
                    }, {
                        "ModulName": "ZUSATZ_TEXT3",
                        "ModulId": "",
                        "ModulHash": "ZUSATZ_TEXT3",
                        "SortOrder": "11"
                    }, {
                        "ModulName": "ANREISE_BESCHREIBUNG",
                        "ModulId": "",
                        "ModulHash": "ANREISE_BESCHREIBUNG",
                        "SortOrder": "12"
                    }, {
                        "ModulName": "NACHRICHTEN_BLOCK",
                        "ModulId": "",
                        "ModulHash": "NACHRICHTEN_BLOCK",
                        "SortOrder": "13"
                    }, {
                        "ModulName": "FOOTER",
                        "ModulId": "",
                        "ModulHash": "FOOTER",
                        "SortOrder": "14"
                    }, {
                        "ModulName": "IMPRESSUM",
                        "ModulId": "",
                        "ModulHash": "IMPRESSUM",
                        "SortOrder": "15"
                    }]
                },
                "EinzelText_Liste": {
                    "EinzelText": [{
                        "Tag": "gdat_chatten",
                        "Text": "haben Sie Fragen? Chatten Sie mit uns"
                    }, {
                        "Tag": "gdat_details",
                        "Text": "Details"
                    }, {
                        "Tag": "gdat_variante",
                        "Text": "Variante"
                    }, {
                        "Tag": "gdat_angebot",
                        "Text": "Ihr persönliches Angebot"
                    }, {
                        "Tag": "gdat_jetzt_buchen",
                        "Text": "Jetzt buchen"
                    }, {
                        "Tag": "gdat_personen",
                        "Text": "Personen"
                    }, {
                        "Tag": "gdat_datum",
                        "Text": "Datum"
                    }, {
                        "Tag": "gdat_naechte",
                        "Text": "Nächte"
                    }, {
                        "Tag": "gdat_preis",
                        "Text": "Preis"
                    }, {
                        "Tag": "gdat_gesamt",
                        "Text": "GESAMT"
                    }, {
                        "Tag": "gdat_impressionen",
                        "Text": "IMPRESSIONEN"
                    }, {
                        "Tag": "gdat_machen_sie_sich_ein_bild",
                        "Text": "MACHEN SIE SICH EIN BILD"
                    }, {
                        "Tag": "gdat_kontakt",
                        "Text": "Sie haben noch Fragen?"
                    }, {
                        "Tag": "gdat_wir_freuen_uns_von_ihnen_zu_hoeren",
                        "Text": "Wir freuen uns von Ihnen zu hören"
                    }, {
                        "Tag": "gdat_telefon",
                        "Text": "Telefon"
                    }, {
                        "Tag": "gdat_email",
                        "Text": "E-Mail"
                    }, {
                        "Tag": "gdat_adresse",
                        "Text": "Addresse"
                    }, {
                        "Tag": "gdat_karte_anzeigen",
                        "Text": "Karte anzeigen"
                    }, {
                        "Tag": "gdat_route_berechnen",
                        "Text": "Route berechnen"
                    }, {
                        "Tag": "gdat_name",
                        "Text": "Name"
                    }, {
                        "Tag": "gdat_nachricht",
                        "Text": "Nachricht"
                    }, {
                        "Tag": "gdat_input:name_hinweis",
                        "Text": "Bitte geben Sie Ihren Namen an"
                    }, {
                        "Tag": "gdat_input:email_hinweis",
                        "Text": "Bitte geben Sie Ihre E-Mail an"
                    }, {
                        "Tag": "gdat_textarea_nachricht_hinweis",
                        "Text": "Bitte geben Sie Ihren Namen an"
                    }, {
                        "Tag": "gdat_button_nachricht_senden",
                        "Text": "Nachricht senden"
                    }, {
                        "Tag": "gdat_page_loading_error",
                        "Text": "Hoppla! Die von Ihnen gesuchte Seite ist zur Zeit nicht verfügbar! Bitte laden Sie es neu, versuchen Sie es später oder melden Sie es. Wir werden es so schnell wie möglich beheben."
                    }, {
                        "Tag": "gdat_reload",
                        "Text": "Neu laden"
                    }, {
                        "Tag": "gdat_report",
                        "Text": "Melde dies"
                    }, {
                        "Tag": "gdat_newsletter",
                        "Text": "Newsletter"
                    }, {
                        "Tag": "gdat_input_newsletter_anmelden",
                        "Text": "Kostenlos anmelden"
                    }, {
                        "Tag": "gdat_zum_seitenanfang",
                        "Text": "Zum Seitenanfang"
                    }, {
                        "Tag": "gdat_please_choose",
                        "Text": "Bitte wählen"
                    }, {
                        "Tag": "gdat_highlights",
                        "Text": "Highlights"
                    }, {
                        "Tag": "gdat_angebot_ansehen",
                        "Text": "Angebot ansehen"
                    }, {
                        "Tag": "gat_anreise_abreise",
                        "Text": "Anreise/Abreise"
                    }, {
                        "Tag": "gdat_preis_pro_person_und_nacht",
                        "Text": "Preis pro Tag/Pers,"
                    }, {
                        "Tag": "gdat_jetzt_anfragen",
                        "Text": "Jetzt reservieren"
                    }, {
                        "Tag": "gdat_weitere_bilder",
                        "Text": "weitere Bilder"
                    }, {
                        "Tag": "gdat_fax",
                        "Text": "Fax"
                    }, {
                        "Tag": "gdat_uid",
                        "Text": "UID"
                    }, {
                        "Tag": "gdat_bank",
                        "Text": "Bank"
                    }, {
                        "Tag": "gdat_ktonr",
                        "Text": "KtoNr"
                    }, {
                        "Tag": "gdat_bic",
                        "Text": "BIC"
                    }, {
                        "Tag": "gdat_iban",
                        "Text": "IBAN"
                    }, {
                        "Tag": "gdat_webseite",
                        "Text": "Webseite"
                    }, {
                        "Tag": "gdat_vorname",
                        "Text": "Vorname"
                    }, {
                        "Tag": "gdat_nachname",
                        "Text": "Nachname"
                    }, {
                        "Tag": "gdat_ihr_vorname",
                        "Text": "Ihr Vorname"
                    }, {
                        "Tag": "gdat_ihr_nachname",
                        "Text": "Ihr Nachname"
                    }, {
                        "Tag": "gdat_ihre_email",
                        "Text": "Ihre E-Mail Adresse"
                    }, {
                        "Tag": "gdat_ihre_nachricht",
                        "Text": "Ihre Nachricht an uns..."
                    }, {
                        "Tag": "gdat_ergebnis_der_rechenaufgabe",
                        "Text": "Ergebnis der o.g. Rechenaufgabe"
                    }, {
                        "Tag": "gdat_anzahl",
                        "Text": "Anzahl"
                    }, {
                        "Tag": "gdat_impressum",
                        "Text": "Impressum"
                    }, {
                        "Tag": "gdat_datenschutz",
                        "Text": "Datenschutz"
                    }, {
                        "Tag": "gdat_teaser",
                        "Text": "Ihre Vorteile als unser Gast"
                    }, {
                        "Tag": "gdat_nr",
                        "Text": "Nr."
                    }, {
                        "Tag": "gdat_beschreibung",
                        "Text": "Beschreibung"
                    }, {
                        "Tag": "gdat_leistung",
                        "Text": "Leistung"
                    }, {
                        "Tag": "gdat_leistungen",
                        "Text": "Leistungen"
                    }]
                },
                "MenuItem_Liste": {
                    "MenuItem": [{
                        "MenuId": "636583454721537482",
                        "MenuName": "Slideshow...",
                        "MenuURL": "#SLIDESHOW",
                        "Self": "true"
                    }, {
                        "MenuId": "636583454945747957",
                        "MenuName": "GASTROdat.com",
                        "MenuURL": "https://www.gastrodat.com",
                        "Self": "false"
                    }, {
                        "MenuId": "636583644494061389",
                        "MenuName": "touristscout",
                        "MenuURL": "individuelle Homepage",
                        "Self": "false"
                    }]
                },
                "MenuImpressumItem_Liste": {
                    "MenuItem": [{
                        "MenuId": "636585129479988014",
                        "MenuName": "Datenschutzen",
                        "MenuURL": "https://www.gastrodat.com",
                        "Self": "false"
                    }, {
                        "MenuId": "636585129821190422",
                        "MenuName": "Impressum",
                        "MenuURL": "https://www.gastrodat.com",
                        "Self": "false"
                    }]
                },
                "HotelName": "Hotel Cafe Riedl",
                "WellcomeText": "<?xml version=\"1.0\" ?>\r<html xmlns=\"https://www.w3.org/1999/xhtml\">\r<head>\r<meta content=\"TX24_HTM 24.0.610.500\" name=\"GENERATOR\" />\r<title></title>\r<base href=\"file:///C:/GDat/\" />\r</head>\r<body style=\"background-color:#FFFFFF\">\r<h2 lang=\"de-DE\" style=\"line-height:130%;\">Willkommen im 4 Sterne Hotel Riedl im Zillertal, Tirol!</h2>\r<h3 lang=\"de-DE\" style=\"line-height:130%;\">Ihr Hotel im Zillertal - ein Geheimtipp für Tirol-Urlaub mit Herz &amp; Genuss...</h3>\r<p lang=\"de-DE\" style=\"line-height:130%;\"> </p>\r<p lang=\"de-DE\" style=\"line-height:130%;\">Das Hotel Riedl ist ein Familienbetrieb in einzigartiger Lage im Herzen der Zillertaler Alpen. Direkt vor der Hotel-Türe geht es los zum Wandern, Biken oder Skifahren. Perfekt im Winter: Die Talstation ins Skigebiet Kaltenbach liegt 500 m nah. Für Ihren Langlauf-Urlaub starten Sie auf Loipen direkt vor dem Hotel - und können sich nach Ihrem aktiven Tag in den Bergen auf Wellness und feine Kulinarik freuen. Damit ist unser Hotel Riedl die perfekte Adresse für Ihren Winterurlaub und Sommerurlaub im Zillertal.</p>\r<p lang=\"de-DE\" style=\"line-height:130%;\"> </p>\r<p lang=\"de-DE\" style=\"line-height:130%;\">Mit allen Sinnen genießen: Lassen Sie sich von uns durch Ihren Urlaub im Zillertal begleiten - mit reichhaltigem Frühstücksbuffet, herzhafter Küche mit regionalen Produkten aus dem Zillertal - und süßen Verführungen aus der hauseigenen Konditorei. Und Sie erleben mit jedem Bissen, wie köstlich Tirol schmeckt ...</p>\r</body>\r</html>",
                "SubDomainUrl": ".https://www.touristscout.com",
                "AngebotsText": "<?xml version=\"1.0\" ?>\r<html xmlns=\"https://www.w3.org/1999/xhtml\">\r<head>\r<meta content=\"TX24_HTM 24.0.610.500\" name=\"GENERATOR\" />\r<title></title>\r</head>\r<body>\r<p lang=\"de-DE\" style=\"line-height:130%;\">Sehen Sie hier Ihr persönliches Urlaubsangebot:</p>\r</body>\r</html>",
                "AnreiseInfoText": "<?xml version=\"1.0\" ?>\r<html xmlns=\"https://www.w3.org/1999/xhtml\">\r<head>\r<meta content=\"TX24_HTM 24.0.610.500\" name=\"GENERATOR\" />\r<title></title>\r</head>\r<body>\r<p lang=\"de-DE\" style=\"line-height:130%;\">Anreise ab 14:00 Uhr</p>\r</body>\r</html>",
                "AbreiseInfoText": "<?xml version=\"1.0\" ?>\r<html xmlns=\"https://www.w3.org/1999/xhtml\">\r<head>\r<meta content=\"TX24_HTM 24.0.610.500\" name=\"GENERATOR\" />\r<title></title>\r</head>\r<body>\r<p style=\"line-height:130%;\">Abreise bis 10:00 Uhr</p>\r</body>\r</html>",
                "ZusatzText1": "",
                "ZusatzText1_Header": "",
                "ZusatzText1_Collapsed": "false",
                "ZusatzText_Bild1": {
                    "LastPicUpload": "0001-01-01T00:00:00"
                },
                "ZusatzText2": "",
                "ZusatzText2_Header": "",
                "ZusatzText2_Collapsed": "false",
                "ZusatzText_Bild2": {
                    "LastPicUpload": "0001-01-01T00:00:00"
                },
                "ZusatzText3": "<?xml version=\"1.0\" ?>\r<html xmlns=\"https://www.w3.org/1999/xhtml\">\r<head>\r<meta content=\"TX24_HTM 24.0.610.500\" name=\"GENERATOR\" />\r<title></title>\r</head>\r<body>\r<p style=\"line-height:130%;\">* Finnische Sauna (90°C)</p>\r<p style=\"line-height:130%;\">* Kräuterstadl (60 °C)</p>\r<p style=\"line-height:130%;\">* Bio-Zirbensauna (60 °C)</p>\r<p style=\"line-height:130%;\">* Aroma-Dampfbad</p>\r<p style=\"line-height:130%;\">* Infrarotkabine</p>\r<p style=\"line-height:130%;\">* Erlebnis-Duschen </p>\r<p style=\"line-height:130%;\">* Fußsprudelbecken</p>\r<p style=\"line-height:130%;\">* Eisgrotte</p>\r<p style=\"line-height:130%;\">* Zirben-Ruheraum mit Wasserbetten</p>\r<p style=\"line-height:130%;\">* Heustadl mit Zillertaler Heubetten</p>\r<p style=\"line-height:130%;\">* Ganztägig Obst, Säfte und Wasser im Alpine SPA Bereich</p>\r<p style=\"line-height:130%;\">* Beheizter Aussenpool (34 °C) mit Liegewiese und herrlichem Blick auf die Zillertaler Berge</p>\r</body>\r</html>",
                "ZusatzText3_Header": "Alpin SPA - Wellness auf 800 m²",
                "ZusatzText3_Collapsed": "false",
                "ZusatzText_Bild3": {
                    "LastPicUpload": "0001-01-01T00:00:00"
                },
                "ZusatzText4": "<?xml version=\"1.0\" ?>\r<html xmlns=\"https://www.w3.org/1999/xhtml\">\r<head>\r<meta content=\"TX24_HTM 24.0.610.500\" name=\"GENERATOR\" />\r<title></title>\r</head>\r<body>\r<p style=\"line-height:130%;\">* Reichhaltiges Frühstücksbuffet mit Bio-Vollwertecke, frisch gebackenem Brot, vitaminreichen frischen Säften, Tees, Kakao, Wurst-Käsebuffet, Obst, Müsli, Joghurt sowie einer großen Auswahl an gesunden Köstlichkeiten</p>\r<p style=\"line-height:130%;\">* Frühstückskarte mit frisch zubereiteten Eier-Spezialitäten und ausgewählten Kaffee-Zubereitungen</p>\r<p style=\"line-height:130%;\">* Köstlichkeiten am Nachmittag aus aus der familieneigenen Traditionskonditorei, serviert im Cafe oder gern auch auf der Sonnenterrasse mit Panorama-Blick</p>\r<p style=\"line-height:130%;\">* Abends abwechslungsreiches 4-gängiges Wahl-Menü</p>\r<p style=\"line-height:130%;\">* 1 x wöchentlich Gala-Abend</p>\r<p style=\"line-height:130%;\">* Umfangreiches knackiges Salatbuffet</p>\r</body>\r</html>",
                "ZusatzText4_Header": "Unsere 3/4 Genießer-Pension",
                "ZusatzText4_Collapsed": "false",
                "ZusatzText_Bild4": {
                    "LastPicUpload": "0001-01-01T00:00:00"
                },
                "ZusatzText5": "",
                "ZusatzText5_Header": "",
                "ZusatzText5_Collapsed": "false",
                "ZusatzText_Bild5": {
                    "LastPicUpload": "0001-01-01T00:00:00"
                },
                "ZwischenBild1": {
                    "PicId": "1f84429aa129c",
                    "PicName": "Genuss pur: Erleben Sie den unverfälschten Geschmack der Alpen!",
                    "PicPath": "$i_offer\\impressions\\impression_1.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/impression_1.jpg",
                    "PicHTML_Link": "https://www.hotel-riedl.com",
                    "PicKategorie": "Kulinarische Köstlichkeiten",
                    "LastPicUpload": "2018-06-05T14:26:50.5852918+02:00"
                },
                "ZwischenBild2": {
                    "PicId": "2b0a64a8a1f4a",
                    "PicName": "Spüren Sie die erfrischende Wirkung des Wassers",
                    "PicPath": "$i_offer\\impressions\\2b0a64a8a1f4a.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2b0a64a8a1f4a.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Wellness für die Seele",
                    "LastPicUpload": "2018-06-05T14:26:50.663292+02:00"
                },
                "ZwischenBild3": {
                    "LastPicUpload": "0001-01-01T00:00:00"
                },
                "ZwischenBild4": {
                    "LastPicUpload": "0001-01-01T00:00:00"
                },
                "ZwischenBild5": {
                    "PicId": "2aa79bffc26f0",
                    "PicName": "Genießen ",
                    "PicPath": "$i_offer\\impressions\\2aa79bffc26f0.jpg",
                    "PicUrl": "https://www.weratech-files.com/images/200902/gastrodat/2aa79bffc26f0.jpg",
                    "PicHTML_Link": "",
                    "PicKategorie": "Kulinarische Köstlichkeiten",
                    "LastPicUpload": "2018-06-05T14:26:50.6164919+02:00"
                },
                "FaceBookPageId": "GDAT_Facebook_Page_ID",
                "SozMedia_FaceBook_Url": "www.facebook....",
                "SozMedia_GooglePlus_Url": "www.googleplus",
                "SozMedia_Pinterest_Url": "www.pinterest",
                "SozMedia_Instagram_Url": "www.instagramm",
                "SozMedia_YouTube_Url": "www.youtube",
                "SozMedia_Twitter_Url": "www.twitter",
                "ImageCompressionQuality": "50"
            },
            "DesignSettings": {
                "TextModulStyle_Liste": {
                    "TextModulStyle": [{
                        "StyleName": "H1",
                        "InlineStyle": "false",
                        "Bold": "true",
                        "FontName": "Arial",
                        "FontSize": "480",
                        "ForeColor": "#000000",
                        "Italic": "false",
                        "ListFormat": "None",
                        "Strikeout": "false",
                        "TextBackColor": "#FFFFFF",
                        "Underline": "false",
                        "UseStyle": "false"
                    }, {
                        "StyleName": "H2",
                        "InlineStyle": "false",
                        "Bold": "true",
                        "FontName": "Arial",
                        "FontSize": "360",
                        "ForeColor": "#000000",
                        "Italic": "false",
                        "ListFormat": "None",
                        "Strikeout": "false",
                        "TextBackColor": "#FFFFFF",
                        "Underline": "false",
                        "UseStyle": "false"
                    }, {
                        "StyleName": "H3",
                        "InlineStyle": "false",
                        "Bold": "true",
                        "FontName": "Arial",
                        "FontSize": "266",
                        "ForeColor": "#000000",
                        "Italic": "false",
                        "ListFormat": "None",
                        "Strikeout": "false",
                        "TextBackColor": "#FFFFFF",
                        "Underline": "false",
                        "UseStyle": "false"
                    }, {
                        "StyleName": "H4",
                        "InlineStyle": "false",
                        "Bold": "true",
                        "FontName": "Arial",
                        "FontSize": "220",
                        "ForeColor": "#000000",
                        "Italic": "false",
                        "ListFormat": "None",
                        "Strikeout": "false",
                        "TextBackColor": "#FFFFFF",
                        "Underline": "false",
                        "UseStyle": "false"
                    }, {
                        "StyleName": "H5",
                        "InlineStyle": "false",
                        "Bold": "true",
                        "FontName": "Arial",
                        "FontSize": "180",
                        "ForeColor": "#000000",
                        "Italic": "false",
                        "ListFormat": "None",
                        "Strikeout": "false",
                        "TextBackColor": "#FFFFFF",
                        "Underline": "false",
                        "UseStyle": "false"
                    }, {
                        "StyleName": "P",
                        "InlineStyle": "false",
                        "Bold": "false",
                        "FontName": "Arial",
                        "FontSize": "180",
                        "ForeColor": "#000000",
                        "Italic": "false",
                        "ListFormat": "None",
                        "Strikeout": "false",
                        "TextBackColor": "#FFFFFF",
                        "Underline": "false",
                        "UseStyle": "false"
                    }, {
                        "StyleName": "UL",
                        "InlineStyle": "false",
                        "Bold": "false",
                        "FontName": "Arial",
                        "FontSize": "180",
                        "ForeColor": "#000000",
                        "Italic": "false",
                        "ListFormat": "Bulleted",
                        "Strikeout": "false",
                        "TextBackColor": "#FFFFFF",
                        "Underline": "false",
                        "UseStyle": "false"
                    }]
                },
                "TemplateName": "",
                "primaryColor": "#CDA827",
                "secundaryColor": "#333333",
                "bckg_Color3": "#4B4B4B",
                "bckg_Color4": "#AD3B3D",
                "bckg_Color5": "#FFFFFF",
                "prim_textColor": "#FFFFFF",
                "sec_textColor": "#FFFFFF",
                "textColor_Color3": "#FFFFFF",
                "textColor_Color4": "#FFFFFF",
                "textColor_Color5": "#0080FF",
                "backgroundColor": "#FFFFFF",
                "headlineFontName": "Arial",
                "bodyFontName": "Arial",
                "textColor": "#000000"
            }
        }
    }
}