export interface Default {
    language?: string;
    contactMail?: string;
    supportMail?: string;
}
interface FirebaseCloudMessaging {
    serverkey: string;
}
interface Api {
    url: string;
}
interface Chatbot {
    id: string;
    name: string;
}
interface Chat {
    company: string;
    homepage: string;
    logo: string;
    defaultFallbackAnswer: string;
    sendButton: boolean;
    colors: Colors;
    welcomeMessage: string;
    inputPlaceholder: string;
    undefinedAnswer: string;
}
interface Colors {
    primary: string;
}
interface Ui {
    rightPanelChat: boolean;
}
export interface Constants {
    env?: "development" | "production";
    googleMapApiKey?: string;
    location?: string;
    default?: Default;
    fcm?: FirebaseCloudMessaging;
    api?: Api;
    chatbot?: Chatbot;
    chat?: Chat;
    ui?: Ui;
    dynamicLink?: string;
    packageName?: {
        ios: string;
        android: string;
    };
    native?: ConstantsNative;
    web?: ConstantsWeb;
    electron?: ConstantsElectron;
    highlightColors?: string[];
    primaryColor?: string;
    secondaryColor?: string;
}
export interface ConstantsNative {
    backgroundColor?: string;
    backgroundColorPage?: string;
    h1: any;
    contentPadding: any;
    blackBorder: any;
    buttonFlat: any;
    listItem: any;
}
export interface ConstantsWeb {
    urlAdmin?: string;
    urlWeb?: string;
    backgroundColor?: string;
    backgroundColorPage?: string;
}
export interface ConstantsElectron {
}
declare const generalSettings: Constants;
export default generalSettings;
