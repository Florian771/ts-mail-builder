"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
var config_1 = require("../../config");
var utils_1 = require("../../utils");
var placeholder = {
    context: "{MAIL_CONTEXT}",
    logoSrc: "{LOGO_SRC}",
    logoUrl: "{LOGO_URL}",
    logoTitle: "{LOGO_TITLE}",
    logoTarget: "{LOGO_TARGET}",
    menuLink: {
        text: "{WEBSITE_TEXT_XXX}",
        title: "",
        url: "{WEBSITE_URL_XXX}"
    }
};
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    var translations = utils_1.getTranslations({
        placeholder: placeholder,
        settings: settings
    });
    var html = "";
    var text = config_1.textBreak;
    var textMenuLinks = "";
    var htmlMenuLinks = "";
    var textLogo = "";
    var htmlLogo = "";
    var textContext = "";
    var htmlContext = "";
    var padding = "&nbsp;&nbsp;&nbsp;";
    if (settings.logo) {
        /* let logoWidth = 100
        const ratio = settings.logoWidth / settings.logoHeight
        let logoHeight = logoWidth / ratio
        if (logoHeight > 100) {
          logoHeight = 100
          logoWidth = logoHeight * ratio
        } */
        if (settings.logoWidth) {
            settings.logoWidth = "width=\"" + settings.logoWidth + "px\"";
        }
        if (settings.logoHeight) {
            settings.logoHeight = "height=\"" + settings.logoHeight + "px\"";
        }
        textLogo = placeholder.logoTitle + config_1.textBreak;
        htmlLogo = "<mj-image href=\"{COMPANY_WEBSITE}\" " + settings.logoWidth + " " + settings.logoHeight + " src=\"" + placeholder.logoSrc + "\" alt=\"{LOGO_TITLE}\" align=\"center\" border=\"none\" padding=\"0px\"></mj-image>";
    }
    else if (settings.context) {
        textContext = placeholder.context + config_1.textBreak;
        htmlContext = " <mj-text align=\"left\" font-size=\"16px\" font-weight=\"500\">" + placeholder.context + "</mj-text>";
    }
    if (settings.menuLinks && settings.menuLinks > 0) {
        for (var index = 0; index < settings.menuLinks; index++) {
            textMenuLinks += placeholder.menuLink.text.replace("XXX", String(index)) + ": " + placeholder.menuLink.url.replace("XXX", String(index)) + config_1.textBreak;
            if (htmlMenuLinks) {
                htmlMenuLinks += padding;
            }
            htmlMenuLinks += "<a target=\"_blank\" href=\"" + placeholder.menuLink.url.replace("XXX", String(index)) + "\" style=\"color: {textColor}; text-decoration: none;\">\n        " + placeholder.menuLink.text.replace("XXX", String(index)) + "</a>";
        }
    }
    text = textLogo;
    text += textContext;
    text += textMenuLinks;
    html = "<mj-section padding=\"20px 20px 20px 20px\" background-color=\"{backgroundColor2}\">\n  <mj-column width=\"35%\">\n    " + htmlLogo + htmlContext + "\n  </mj-column>\n  <mj-column width=\"65%\">\n    <mj-text align=\"center\" font-size=\"11px\" padding=\"0px\">";
    // html = `ratio: ${ratio} - logoWidth: ${logoWidth} - logoHeight: ${logoHeight} - `
    html += htmlMenuLinks + "\n    </mj-text>\n  </mj-column>\n</mj-section>";
    // console.log("header - text: " + text)
    // console.log("header - html: " + html)
    testParse_1.default({
        test: true,
        html: html,
        options: options,
        modulName: "headerDesigner"
    });
    return {
        html: html,
        text: text,
        translations: translations,
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=headerDesigner.js.map