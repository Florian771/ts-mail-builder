export interface SendEmailButtonOptions {
    buttonUrl: string;
    buttonText: string;
}
export interface SendEmailOptions {
    to: string;
    from: string;
    subject: string;
    text?: string;
    html?: string;
}
export declare function initEmail(): void;
export declare function sendEmail(mail: SendEmailOptions): Promise<any>;
