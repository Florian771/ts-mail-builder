exports.getFullPageHtmlTestOptions = {
  name: "gdat",
  from: "noreply@touristscout.com",
  to: "florian1677@gmail.com",
  data: {},
  socialMedia: [
    { name: "facebook", url: "https://www.facebook.com/gastrodat" },
    { name: "google", url: "www.googleplus....." },
    { name: "pinterest", url: "www.pinterest....." },
    { name: "instagram", url: "www.instagramm....." },
    { name: "youtube", url: "www.youtube......" },
    { name: "twitter", url: "www.twitter...." }
  ],
  sections: [
    { type: "alternativeHtmlInfo" },
    { type: "header" },
    { ModulId: "", ModulHash: "HERO", type: "HERO" },
    { ModulId: "", ModulHash: "HEADLINE", type: "HEADLINE" },
    { ModulId: "", ModulHash: "TEXT", type: "TEXT" },
    { ModulId: "", ModulHash: "CAROUSEL", type: "CAROUSEL" },
    { ModulId: "", ModulHash: "SUBLINE", type: "SUBLINE" },
    { ModulId: "", ModulHash: "BUTTON", type: "BUTTON" },
    { ModulId: "", ModulHash: "IMAGE", type: "IMAGE" },
    { ModulId: "", ModulHash: "ZUSATZ_TEXT3", type: "ZUSATZ_TEXT3" },
    { ModulId: "", ModulHash: "ACCORDEON", type: "ACCORDEON" },
    { ModulId: "", ModulHash: "ZUSATZ_TEXT4", type: "ZUSATZ_TEXT4" }
  ],
  zusatzTextBilder: [
    {
      name: "Hausgemachtes Slow Food, bei Wunsch auch vegetarisch",
      url:
        "https://www.weratech-files.com/images/280942/gastrodat/1e85a7b85e334.jpg"
    },
    { name: "Hausgemachtes Slow Food, bei Wunsch auch vegetarisch", url: "" },
    { name: "Hausgemachtes Slow Food, bei Wunsch auch vegetarisch", url: "" },
    {
      name: "Hausgemachtes Slow Food, bei Wunsch auch vegetarisch",
      url:
        "https://www.weratech-files.com/images/280942/gastrodat/1e85a7b85e334.jpg"
    },
    {
      name: "Hausgemachtes Slow Food, bei Wunsch auch vegetarisch",
      url:
        "https://www.weratech-files.com/images/280942/gastrodat/1e85a7b85e334.jpg"
    }
  ],
  templateType: {
    name: "product",
    alternativeHtmlInfo: { text: true, url: true },
    header: {
      logo: "https://touristscout.com/demo/assets/images/logo.png",
      context: false,
      menuLinks: 4
    },
    hero: {
      key: "HRO",
      headline: true,
      buttonText: true,
      buttonUrl: true,
      imgUrl: "https://touristscout.com/demo/assets/images/header/1.jpg"
    },
    button: { key: "BTN" },
    text: { key: "TXT" },
    image: {
      src: "https://touristscout.com/demo/assets/images/header/1.jpg",
      key: "IMAGE_KEY"
    },
    accordeon: { length: 3, key: "ACDN" },
    carousel: {
      key: "CSL",
      images: [
        "https://touristscout.com/demo/assets/images/header/5.jpg",
        "https://touristscout.com/demo/assets/images/header/12.jpg",
        "https://touristscout.com/demo/assets/images/header/2.jpg"
      ]
    },
    sectionBottom: { active: false, type: "diagonal" },
    footer: {
      unsubscribeText: true,
      unsubscribeUrl: true,
      name: true,
      street: true,
      postalCode: true,
      city: true,
      country: true,
      phone: true,
      fax: true,
      email: true,
      website: true,
      uid: true
    }
  },
  contentType: "hotelRoomOffer",
  buildType: "html",
  testParse: true,
  designSettings: {
    TemplateName: "2",
    primaryColor: "#96b3b9",
    secundaryColor: "#655758",
    headlineColor: "#111",
    textColor: "#999",
    backgroundColor: "#fff",
    backgroundColor2: "#efeceb",
    backgroundColor2Text: "#655758",
    zusatzTextHeaderBackgroundColor: "#998b7e",
    zusatzTextHeaderColor: "#fff",
    zusatzTextHeaderArrowBackgroundColor: "#b0a8a3",
    zusatzTextHeaderArrowColor: "#fff",
    zusatzTextBackgroundColor: "#efeceb",
    zusatzTextInhaltBackgroundColor: "#fff",
    zusatzTextColor: "#655758",
    headlineFontName: "'Yanone Kaffeesatz', sans-serif",
    bodyFontName: "'Open Sans', 'Helvetica Neue', Helvetica, sans-serif",
    headerBackgroundColor: "#fff",
    menuBackgroundColor: "#e6e2e1",
    menuTextColor: "#655758",
    menuFontName:
      '"LucidaGrandeRegular", "Helvetica Neue", Helvetica, sans-serif',
    greetingBackgroundColor: "#655758",
    greetingTextColor: "#fff",
    textModulCss:
      ".btn.btn-primary{background-image:none;border-radius:0;border-width:0;border-color:transparent;background-color:#96b3b9;color:undefined;}",
    LogoPosition: "left",
    bckg_Color3: "#998b7e",
    bckg_Color4: "#b0a8a3",
    bckg_Color5: "#fff",
    prim_textColor: "#fff",
    sec_textColor: "#fff",
    textColor_Color3: "#fff",
    textColor_Color4: "#fff"
  }
}
