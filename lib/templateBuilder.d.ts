export declare function htmlOutput({ mailContentsOutput }: {
    mailContentsOutput: MailBuilder.MailContentsOutput;
}): Promise<MailBuilder.TemaplateBuilderResult>;
