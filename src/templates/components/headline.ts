import testParse from "../../testParse"
import { textBreak } from "../../config"
import { getTranslations } from "../../utils"
import templateReplacer from "../templateReplacer"

export interface HeadlineSettings {
  headline: boolean
  key: string
}

export interface Headline {
  headline: string
}
const placeholder: Headline = {
  headline: "{HEADLINE_KEY}"
}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: HeadlineSettings
}): MailBuilder.Component => {
  const translations: string[] = getTranslations({
    placeholder: placeholder,
    settings: settings
  })
  let html: string = ""
  let text: string = ""
  let font: string = "Montserrat, Helvetica, Arial, sans-serif"
  if (
    options &&
    options.designSettings &&
    options.designSettings.headlineFontName
  ) {
    font = options.designSettings.headlineFontName
  }

  const mjmlHeadline = `<mj-section padding="20px 20px 0 20px" background-color="{backgroundColor2}">
<mj-column>
  <mj-text font-family="${font}" align="center" font-weight="300" padding="30px 40px 10px 40px" font-size="32px" line-height="40px" color="{textColor}">
    ${placeholder.headline}
  </mj-text>
</mj-column>
</mj-section>`

  text = `${placeholder.headline.replace("KEY", settings.key)}${textBreak}`
  html = mjmlHeadline
  html = templateReplacer({ html: mjmlHeadline, settings, placeholder, font })

  testParse({
    test: true,
    html: html,
    options: options,
    modulName: "headline"
  })
  return {
    html: html,
    text: text,
    translations: translations,
    parse: (args: any) => testParse(args)
  }
}
