/*
<mj-font name="HeadlineFont" href="https://fast.fonts.net/dv2/14/18597b82-9a06-46a3-b68b-c250c1105515.woff2?d44f19a684109620e484147ea690e818ee8d0f94c8efc5f31894b34456dd6eba45b7199fe6a76b3f16243440c93c58e473c2b02ad5244cba2c4f7ba3b91d2e05b3bce074c230ba&projectId=d07801b5-a55d-4cc3-92ca-167ffa4bbcb1"></mj-font>
<mj-font name="ContentFont" href="https://fast.fonts.net/dv2/14/14c73713-e4df-4dba-933b-057feeac8dd1.woff2?d44f19a684109620e484147ea690e818ee8d0f94c8efc5f31894b34456dd6eba45b7199fe6a76b3f16243440c93c58e473c2b02ad5244cba2c4f7ba3b91d2e05b3bce074c230ba&projectId=d07801b5-a55d-4cc3-92ca-167ffa4bbcb1"></mj-font>
*/

import { textBreak } from "../../config"
import generateHead from "../generateHead"

export interface Settings {}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: Settings
}): MailBuilder.Component => {
  let html: string = ""
  let head: string = ""
  let text: string = ""
  let font: string = "Montserrat, Helvetica, Arial, sans-serif"
  if (
    options &&
    options.designSettings &&
    options.designSettings.bodyFontName
  ) {
    font = options.designSettings.bodyFontName
  }
  text = `${textBreak}`
  head = generateHead({ font })
  html = `
  <mjml>${head}<mj-body background-color="{backgroundColor}">`

  return { html: html, text: text, translations: [] }
}
