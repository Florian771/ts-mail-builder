import { sendEmail, initEmail } from "./sendEmail"
import { composeEmail } from "./compose"
import { createMailContents } from "./createMailContents"
import { defaultOptions } from "./config"
import { getHtmlFileName } from "./utils"
import CONSTANTS from "./constants"
import { getHtml } from "./templates/dynamic"

const fs = require("fs-extra")

// export { mjmlHeadline } from "./templates/components/headline"
export { mjmlImage } from "./templates/components/image"
/* 
export { default as testParse } from "./testParse"
export { default as head } from "./templates/components/head"
export { default as hero } from "./templates/components/hero"
export { default as accordeon } from "./templates/components/accordeon"
export {
  default as headlineWithSubline
} from "./templates/components/headlineWithSubline"
export {
  default as headlineWithSublineWithLink
} from "./templates/components/headlineWithSublineWithLink"
export { default as subline } from "./templates/components/subline"
export { default as header } from "./templates/components/header"
export { default as text } from "./templates/components/text"
export { default as textWithImage } from "./templates/components/textWithImage"
export { default as spacer } from "./templates/components/spacer"
export { default as divider } from "./templates/components/divider"
export { default as carousel } from "./templates/components/carousel"
export { default as button } from "./templates/components/button"
export { default as sectionBottom } from "./templates/components/sectionBottom"
export { default as footer } from "./templates/components/footer"
export { default as end } from "./templates/components/end"
export { default as social } from "./templates/components/social"
export {
  default as alternativeHtmlInfo
} from "./templates/components/alternativeHtmlInfo"
 */

export const getSingleComponentHtml = async (
  options: MailBuilder.BuildOptions
): Promise<MailBuilder.Template> => {
  try {
    options.getSingleComponent = true
    const component: MailBuilder.Component = await getHtml({ options })
    /* console.log(
      "getSingleComponentHtml - getHtml - component.html:",
      component.html
    ) */
    /* console.log(
      "getSingleComponentHtml - getHtml - component.parse:",
      typeof component.parse
    ) */
    console.log(
      "getSingleComponentHtml - component type:",
      options.sections[0].type
    )
    console.log("options.sections[0].settings:", options.sections[0].settings)
    return component.parse({
      html: component.html,
      options,
      modulName: options.sections[0].type,
      test: false
    })
  } catch (err) {
    console.log("index.js: error: ", err.message)
    return err
  }
}

export const start = async (
  options: MailBuilder.BuildOptions
): Promise<MailBuilder.Result> => {
  try {
    console.log("mailBuilder start - options: ", options)
    const buildOptions: MailBuilder.BuildOptions = options
    const designSettings: MailBuilder.DesignSettings = {
      ...defaultOptions,
      ...options.designSettings
    }
    buildOptions.designSettings = designSettings
    // console.log("primaryColor:", buildOptions.designSettings.primaryColor)

    const mailContentsOutput: MailBuilder.MailContentsOutput = await createMailContents(
      options
    )
    console.log("mailContentsOutput", mailContentsOutput)

    const composedEmail: MailBuilder.ComposeMailResult = await composeEmail({
      subject: mailContentsOutput.subject,
      links: mailContentsOutput.links,
      text: mailContentsOutput.text,
      html: mailContentsOutput.html,
      options: mailContentsOutput.options,
      translations: mailContentsOutput.translations
    })

    const fileName: string = getHtmlFileName({
      options: mailContentsOutput.options,
      type: "final"
    })

    console.log("http://127.0.0.1:3000/" + fileName + ".html")

    if (mailContentsOutput.options.buildType === "html") {
      return {
        mail: composedEmail.mail,
        translations: composedEmail.translations,
        url: "",
        type: mailContentsOutput.options.buildType
      }
    } else if (mailContentsOutput.options.buildType === "file") {
      try {
        await fs.writeFile(
          "./public/" + fileName + ".html",
          composedEmail.mail.html,
          err => {
            if (err) {
              throw err
            }
          }
        )
        console.log(fileName + ".html saved")
        return {
          mail: composedEmail.mail,
          translations: composedEmail.translations,
          url: CONSTANTS.web.urlWeb + "/" + fileName + ".html",
          type: mailContentsOutput.options.buildType
        }
      } catch (err) {
        throw err
      }
    } else {
      try {
        initEmail()
        console.log("sendEmail authorized")
      } catch (err) {
        throw err
      }
      try {
        if (options.subject) {
          composedEmail.mail.subject = options.subject
        }
        const res: any = await sendEmail(composedEmail.mail)
        console.log(
          "mail " +
            composedEmail.mail.subject +
            " sent to " +
            composedEmail.mail.to
        )
        return {
          mail: composedEmail.mail,
          translations: composedEmail.translations,
          url: CONSTANTS.web.urlWeb + "/" + fileName + ".html",
          type: mailContentsOutput.options.buildType,
          sendResponse: {
            statusCode: res.statusCode,
            statusMessage: res.statusMessage,
            body: res.body
          }
        }
      } catch (err) {
        console.log("composedEmail subject: ", composedEmail.mail.subject)
        console.log("composedEmail from: ", composedEmail.mail.from)
        console.log("composedEmail to: ", composedEmail.mail.to)
        console.log("sendgrid err:", err.message)
        throw err
      }
    }
  } catch (err) {
    console.log("index.js: error: ", err.message)
    return err
  }
}
