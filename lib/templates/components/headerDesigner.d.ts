export interface HeaderDesignerSettings {
    logo: boolean;
    context: boolean;
    menuLinks: number;
    logoWidth?: string;
    logoHeight?: string;
}
export interface HeaderDesigner {
    context?: string;
    logoSrc?: string;
    logoUrl?: string;
    logoTitle?: string;
    logoTarget?: string;
    menuLink?: MailBuilder.TextLink;
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: HeaderDesignerSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
