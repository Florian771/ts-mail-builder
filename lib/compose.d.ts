export declare function createLink({ text, url }: MailBuilder.MailLink): MailBuilder.MailLink;
export declare function composeEmail(mailContentsOutput: MailBuilder.MailContentsOutput): Promise<MailBuilder.ComposeMailResult>;
