export interface SublineSettings {
    key: string;
}
export interface Subline {
    subline: string;
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: SublineSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
