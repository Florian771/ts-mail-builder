import CONSTANTS from "../constants"
import { createLink } from "../compose"

export default async function(
  options: MailBuilder.BuildOptions
): Promise<MailBuilder.MailContentsOutput> {
  const subject: string = "User created: " + options.data.user.displayName

  let words: string = ""
  let text: string = ""
  let html: string = ""
  let translations: string[] = []
  const textBreak: string = "\r\n"

  const companyName: string = options.data.client.companyName

  if (options.data.user.displayName) {
    words = "Neuer User für Account: "
    text += textBreak + words + companyName
    html += "<br>" + words + companyName
  }

  if (options.data.user.displayName) {
    words = "Benutzer: "
    text += textBreak + words + options.data.user.displayName
    html += "<br" + words + options.data.user.displayName
  }

  if (options.data.user.email) {
    words = "E-Mail: "
    text += textBreak + words + options.data.user.email
    html +=
      "<br>" +
      words +
      ' <a style="color:#000000;text-decoration:none;" href="mailto:' +
      options.data.user.email +
      '">' +
      options.data.user.email +
      "</a>"
  }

  if (options.data.user.phoneNumber) {
    words = "Phone number: "
    text += textBreak + words + options.data.user.phoneNumber
    html += "<br" + words + options.data.user.phoneNumber
  }

  if (options.data.user.photoURL) {
    words = "Photo: "
    text += textBreak + words + options.data.user.photoURL
    html +=
      "<br" +
      words +
      '<img src="' +
      options.data.user.photoURL +
      '" border="0"/>'
  }

  /* 
  if (options.data.user.provideruser) {
    words = "Auth-Provider: "
    text += textBreak + words + options.data.user.provideruser
    html += "<br" + words + options.data.user.provideruser
  } 
  */

  const links: MailBuilder.MailLink = createLink({
    type: "button",
    text: "Show user",
    url: CONSTANTS.web.urlAdmin + "/admins/" + options.data.user.uid
  })

  return {
    subject: subject,
    text: text,
    html: html,
    translations: translations,
    links: [links],
    options: options
  }
}
