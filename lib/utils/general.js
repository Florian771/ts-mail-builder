"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var moment = require("moment");
exports.log = console.log;
function timestamp() {
    return parseInt(moment().format("x"));
}
exports.timestamp = timestamp;
function removeUndefinedFromObject(dataObject) {
    for (var key in dataObject) {
        if (dataObject[key] === undefined) {
            dataObject[key] = "";
        }
    }
    console.log("after removeUndefinedFromObject", dataObject);
    return dataObject;
}
exports.removeUndefinedFromObject = removeUndefinedFromObject;
//# sourceMappingURL=general.js.map