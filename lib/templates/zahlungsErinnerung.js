"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var head_1 = require("./components/head");
var image_1 = require("./components/image");
var imageText_1 = require("./components/imageText");
var headline_1 = require("./components/headline");
var text_1 = require("./components/text");
var spacer_1 = require("./components/spacer");
var divider_1 = require("./components/divider");
var sectionBottom_1 = require("./components/sectionBottom");
var footer_1 = require("./components/footer");
var end_1 = require("./components/end");
var alternativeHtmlInfo_1 = require("./components/alternativeHtmlInfo");
var tableBill_1 = require("./components/tableBill");
function getHtml(_a) {
    var options = _a.options;
    return __awaiter(this, void 0, void 0, function () {
        var mailContent;
        return __generator(this, function (_b) {
            mailContent = Object.create({
                html: "",
                text: "",
                translations: [],
                add: function (_a) {
                    var component = _a.component;
                    this.html += component.html;
                    this.text += component.text;
                    this.translations += component.translations;
                }
            });
            /* COMPONENTS */
            mailContent.add({ component: head_1.default({ options: options, settings: {} }) });
            mailContent.add({
                component: alternativeHtmlInfo_1.default({
                    options: options,
                    settings: options.templateType.alternativeHtmlInfo
                })
            });
            mailContent.add({
                component: image_1.default({
                    options: options,
                    settings: options.templateType.image
                })
            });
            mailContent.add({
                component: headline_1.default({
                    options: options,
                    settings: {
                        headline: true,
                        key: "HDL"
                    }
                })
            });
            mailContent.add({ component: divider_1.default({ options: options, settings: {} }) });
            mailContent.add({
                component: text_1.default({
                    options: options,
                    settings: options.templateType.text
                })
            });
            mailContent.add({
                component: tableBill_1.default({
                    options: options,
                    settings: options.templateType.tableBill
                })
            });
            mailContent.add({
                component: spacer_1.default({
                    options: options,
                    settings: {}
                })
            });
            mailContent.add({
                component: imageText_1.default({
                    options: options,
                    settings: options.templateType.imageText
                })
            });
            mailContent.add({
                component: text_1.default({
                    options: options,
                    settings: options.templateType.text2
                })
            });
            mailContent.add({
                component: sectionBottom_1.default({
                    options: options,
                    settings: options.templateType.sectionBottom
                })
            });
            mailContent.add({
                component: footer_1.default({
                    options: options,
                    settings: options.templateType.footer
                })
            });
            mailContent.add({ component: end_1.default({ options: options, settings: {} }) });
            return [2 /*return*/, {
                    html: mailContent.html,
                    text: mailContent.text,
                    translations: mailContent.translations
                }];
        });
    });
}
exports.getHtml = getHtml;
//# sourceMappingURL=zahlungsErinnerung.js.map