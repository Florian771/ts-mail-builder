export interface SocialMediaSettings {
    key?: string;
    facebook: boolean;
    youtube: boolean;
    instagram: boolean;
    twitter: boolean;
    pinterest: boolean;
    google: boolean;
}
export interface SocialMedia {
    key?: string;
    name: string;
    url: string;
}
export declare const predefinedPlatforms: string[];
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: SocialMediaSettings | SocialMedia[];
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
