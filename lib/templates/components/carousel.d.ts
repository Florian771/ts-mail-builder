export interface CarouselSettings {
    key: string;
    images: string[];
}
export interface Carousel {
    images: string[];
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: CarouselSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
