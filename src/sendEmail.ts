import * as mailService from "@sendgrid/mail"
require("dotenv").config()

export interface SendEmailButtonOptions {
  buttonUrl: string
  buttonText: string
}
export interface SendEmailOptions {
  to: string
  from: string
  subject: string
  text?: string
  html?: string
}

let SENDGRID: any
let SENDGRID_API_KEY: string
let SENDGRID_SENDER: string

export function initEmail() {
  try {
    console.log("process.env.SENDGRID_API_KEY", process.env.SENDGRID_API_KEY)
    if (process.env.SENDGRID_API_KEY) {
      SENDGRID_API_KEY = process.env.SENDGRID_API_KEY
      SENDGRID_SENDER = process.env.SENDGRID_SENDER
    } else {
      /* 
        SENDGRID = functions.config().sendgrid
        SENDGRID_API_KEY = SENDGRID.api_key
        SENDGRID_SENDER = SENDGRID.sender
 */
    }

    if (!SENDGRID_API_KEY) {
      console.log("SENDGRID_API_KEY ERROR")
      mailService.setApiKey(
        "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"
      )
    } else {
      mailService.setApiKey(process.env.SENDGRID_API_KEY)
    }
  } catch (err) {
    throw err
  }
}

export async function sendEmail(mail: SendEmailOptions): Promise<any> {
  try {
    return await mailService.send(mail)
  } catch (err) {
    throw err
  }
}
