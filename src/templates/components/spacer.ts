import testParse from "../../testParse"
import { textBreak } from "../../config"

export interface SpacerSettings {
  key?: string
}
export interface Spacer {}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: SpacerSettings
}): MailBuilder.Component => {
  let html: string = ""
  let text: string = ""
  text = `${textBreak}`
  html = `
  <mj-section padding="20px 20px 0px 20px" background-color="{backgroundColor2}"></mj-section>`

  testParse({ test: true, html: html, options: options, modulName: "spacer" })
  return {
    html: html,
    text: text,
    translations: [],
    parse: (args: any) => testParse(args)
  }
}
