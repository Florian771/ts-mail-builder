import * as mailService from "@sendgrid/mail"
require("dotenv").config()
import { sendEmail, SendEmailOptions, initEmail } from "./sendEmail"

export async function send(mail: SendEmailOptions): Promise<any> {
  try {
    initEmail()
    console.log("sendEmail authorized")
  } catch (err) {
    throw err
  }
  try {
    const res: any = await sendEmail(mail)
    console.log("mail sent", res.statusCode, res.statusMessage)
  } catch (err) {
    console.log("sendgrid err:", err.message)
    throw err
  }
}

const html = `
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <title>
      
    </title>
    <!--[if !mso]><!-- -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
      #outlook a { padding:0; }
      .ReadMsgBody { width:100%; }
      .ExternalClass { width:100%; }
      .ExternalClass * { line-height:100%; }
      body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
      table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
      img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
      p { display:block;margin:13px 0; }
    </style>
    <!--[if !mso]><!-->
    <style type="text/css">
      @media only screen and (max-width:480px) {
        @-ms-viewport { width:320px; }
        @viewport { width:320px; }
      }
    </style>
    <!--<![endif]-->
    <!--[if mso]>
    <xml>
    <o:OfficeDocumentSettings>
      <o:AllowPNG/>
      <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if lte mso 11]>
    <style type="text/css">
      .outlook-group-fix { width:100% !important; }
    </style>
    <![endif]-->
    
  <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500" rel="stylesheet" type="text/css">
    <style type="text/css">
      @import url(https://fonts.googleapis.com/css?family=Montserrat:300,400,500);
    </style>
  <!--<![endif]-->


    
<style type="text/css">
  @media only screen and (min-width:480px) {
    .mj-column-per-100 { width:100% !important; max-width: 100%; }
.mj-column-per-35 { width:35% !important; max-width: 35%; }
.mj-column-per-65 { width:65% !important; max-width: 65%; }
.mj-column-per-80 { width:80% !important; max-width: 80%; }
.mj-column-px-600 { width:600px !important; max-width: 600px; }
  }
</style>


    <style type="text/css">
    

.mj-carousel {
  -webkit-user-select: none;
  -moz-user-select: none;
  user-select: none;
}

.mj-carousel-cbe40664845b-icons-cell {
  display: table-cell !important;
  width: 44px !important;
}

.mj-carousel-radio,
.mj-carousel-next,
.mj-carousel-previous {
  display: none !important;
}

.mj-carousel-thumbnail,
.mj-carousel-next,
.mj-carousel-previous {
  touch-action: manipulation;
}

.mj-carousel-cbe40664845b-radio:checked + .mj-carousel-content .mj-carousel-image,.mj-carousel-cbe40664845b-radio:checked + * + .mj-carousel-content .mj-carousel-image,.mj-carousel-cbe40664845b-radio:checked + * + * + .mj-carousel-content .mj-carousel-image,.mj-carousel-cbe40664845b-radio:checked + * + * + * + .mj-carousel-content .mj-carousel-image {
  display: none !important;
}

.mj-carousel-cbe40664845b-radio-1:checked + * + * + * + .mj-carousel-content .mj-carousel-image-1,.mj-carousel-cbe40664845b-radio-2:checked + * + * + .mj-carousel-content .mj-carousel-image-2,.mj-carousel-cbe40664845b-radio-3:checked + * + .mj-carousel-content .mj-carousel-image-3,.mj-carousel-cbe40664845b-radio-4:checked + .mj-carousel-content .mj-carousel-image-4 {
  display: block !important;
}

.mj-carousel-previous-icons,
.mj-carousel-next-icons,
.mj-carousel-cbe40664845b-radio-1:checked + * + * + * + .mj-carousel-content .mj-carousel-next-2,.mj-carousel-cbe40664845b-radio-2:checked + * + * + .mj-carousel-content .mj-carousel-next-3,.mj-carousel-cbe40664845b-radio-3:checked + * + .mj-carousel-content .mj-carousel-next-4,.mj-carousel-cbe40664845b-radio-4:checked + .mj-carousel-content .mj-carousel-next-1,
.mj-carousel-cbe40664845b-radio-1:checked + * + * + * + .mj-carousel-content .mj-carousel-previous-4,.mj-carousel-cbe40664845b-radio-2:checked + * + * + .mj-carousel-content .mj-carousel-previous-1,.mj-carousel-cbe40664845b-radio-3:checked + * + .mj-carousel-content .mj-carousel-previous-2,.mj-carousel-cbe40664845b-radio-4:checked + .mj-carousel-content .mj-carousel-previous-3 {
  display: block !important;
}

.mj-carousel-cbe40664845b-radio-1:checked + * + * + * + .mj-carousel-content .mj-carousel-cbe40664845b-thumbnail-1,.mj-carousel-cbe40664845b-radio-2:checked + * + * + .mj-carousel-content .mj-carousel-cbe40664845b-thumbnail-2,.mj-carousel-cbe40664845b-radio-3:checked + * + .mj-carousel-content .mj-carousel-cbe40664845b-thumbnail-3,.mj-carousel-cbe40664845b-radio-4:checked + .mj-carousel-content .mj-carousel-cbe40664845b-thumbnail-4 {
  border-color: #cccccc !important;
}

.mj-carousel-image img + div,
.mj-carousel-thumbnail img + div {
  display: none !important;
}

.mj-carousel-cbe40664845b-thumbnail:hover + * + * + * + .mj-carousel-main .mj-carousel-image,.mj-carousel-cbe40664845b-thumbnail:hover + * + * + .mj-carousel-main .mj-carousel-image,.mj-carousel-cbe40664845b-thumbnail:hover + * + .mj-carousel-main .mj-carousel-image,.mj-carousel-cbe40664845b-thumbnail:hover + .mj-carousel-main .mj-carousel-image {
  display: none !important;
}

.mj-carousel-thumbnail:hover {
  border-color: #fead0d !important;
}

.mj-carousel-cbe40664845b-thumbnail-1:hover + * + * + * + .mj-carousel-main .mj-carousel-image-1,.mj-carousel-cbe40664845b-thumbnail-2:hover + * + * + .mj-carousel-main .mj-carousel-image-2,.mj-carousel-cbe40664845b-thumbnail-3:hover + * + .mj-carousel-main .mj-carousel-image-3,.mj-carousel-cbe40664845b-thumbnail-4:hover + .mj-carousel-main .mj-carousel-image-4 {
  display: block !important;
}


  .mj-carousel noinput { display:block !important; }
  .mj-carousel noinput .mj-carousel-image-1 { display: block !important;  }
  .mj-carousel noinput .mj-carousel-arrows,
  .mj-carousel noinput .mj-carousel-thumbnails { display: none !important; }

  [owa] .mj-carousel-thumbnail { display: none !important; }

  @media screen yahoo {
      .mj-carousel-cbe40664845b-icons-cell,
      .mj-carousel-previous-icons,
      .mj-carousel-next-icons {
          display: none !important;
      }

      .mj-carousel-cbe40664845b-radio-1:checked + *+ *+ *+ .mj-carousel-content .mj-carousel-cbe40664845b-thumbnail-1 {
          border-color: transparent;
      }
  }

    

@media only screen and (max-width:480px) {
  table.full-width-mobile { width: 100% !important; }
  td.full-width-mobile { width: auto !important; }
}


  noinput.mj-accordion-checkbox { display:block!important; }

  @media yahoo, only screen and (min-width:0) {
    .mj-accordion-element { display:block; }
    input.mj-accordion-checkbox, .mj-accordion-less { display:none!important; }
    input.mj-accordion-checkbox + * .mj-accordion-title { cursor:pointer; touch-action:manipulation; -webkit-user-select:none; -moz-user-select:none; user-select:none; }
    input.mj-accordion-checkbox + * .mj-accordion-content { overflow:hidden; display:none; }
    input.mj-accordion-checkbox + * .mj-accordion-more { display:block!important; }
    input.mj-accordion-checkbox:checked + * .mj-accordion-content { display:block; }
    input.mj-accordion-checkbox:checked + * .mj-accordion-more { display:none!important; }
    input.mj-accordion-checkbox:checked + * .mj-accordion-less { display:block!important; }
  }

  @goodbye { @gmail }

    </style>
    
    
  </head>
  <body style="background-color:#e5e5e5;text-align: center;">
    <image style="margin:auto;width: 700px, height: 1032px;" src="https://mailbuilder-564cf.firebaseapp.com/assets/images/gdat-mail-header.png"/>
  </body>
  </html>`

send({
  to: "fk@chatify.info", // "r.hofherr.gastrodat@gmail.com", // "florian1677@gmail.com",
  from: "office@teamformultimedia.at",
  subject: "GASTROdat Erinnerung von Flo",
  html: html
})
