"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var mjml2html = require("mjml");
var templates_1 = require("./templates");
var replacerDesign_1 = require("./replacerDesign");
var utils_1 = require("./utils");
// const fs = require("fs-extra")
function htmlOutput(_a) {
    var mailContentsOutput = _a.mailContentsOutput;
    return __awaiter(this, void 0, void 0, function () {
        var html, text, translations, res, err_1, fileName, options, res;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, templates_1.getHtmlFromTemplateType({
                            templateType: mailContentsOutput.options.templateType,
                            options: mailContentsOutput.options
                        })];
                case 1:
                    res = _b.sent();
                    text = res.text;
                    html = res.html;
                    translations = res.translations;
                    return [3 /*break*/, 3];
                case 2:
                    err_1 = _b.sent();
                    console.log("templateBuilder - getHtml - error: ", err_1.message);
                    return [2 /*return*/, err_1];
                case 3:
                    try {
                        html = replacerDesign_1.default({
                            html: html,
                            designSettings: mailContentsOutput.options.designSettings
                        });
                        fileName = utils_1.getHtmlFileName({
                            options: mailContentsOutput.options,
                            type: "pebuild"
                        });
                        options = {};
                        res = mjml2html(html, options);
                        if (res.errors && res.errors.length > 0) {
                            console.log("templateBuilder - errors: ", res.errors);
                            throw { code: 0, message: JSON.stringify(res.errors) };
                        }
                        /*
                        await fs.writeFile("./public/indexTemplate.html", res.html, err => {
                          if (err) {
                            throw err
                          }
                        })
                        */
                        // console.log("html generated and saved", html)
                        return [2 /*return*/, {
                                html: res.html,
                                text: text,
                                translations: translations,
                                errors: res.errors
                            }];
                    }
                    catch (err) {
                        console.log("templateBuilder - error: ", err.message);
                        return [2 /*return*/, err];
                    }
                    return [2 /*return*/];
            }
        });
    });
}
exports.htmlOutput = htmlOutput;
//# sourceMappingURL=templateBuilder.js.map