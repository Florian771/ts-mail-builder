export interface HeroSettings {
    key: string;
    headline: boolean;
    buttonText: boolean;
    buttonUrl: boolean;
    imgUrl: string;
}
export interface Hero {
    headline: string;
    buttonText: string;
    buttonUrl: string;
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: HeroSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
