"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function generateHead(_a) {
    var font = _a.font;
    return "<mj-head>\n<mj-style>\n    @media (max-width:480px) {\n      .mobileHidden {\n        display: none;\n      }\n    }\n    @media (max-width:480px) {\n      .mobileTextCenter {\n        text-align: center;\n      }\n    }\n  </mj-style>\n        <mj-title></mj-title>\n        <mj-font name=\"Montserrat\" href=\"https://fonts.googleapis.com/css?family=Montserrat:300,400,500\"></mj-font>\n        <mj-attributes>\n          <mj-all font-family=\"" + font + "\"></mj-all>\n          <mj-text font-weight=\"400\" font-size=\"16px\" color=\"{textColor}\" line-height=\"24px\"></mj-text>\n          <mj-section padding=\"0px\"></mj-section>\n        </mj-attributes>\n      </mj-head>";
}
exports.generateHead = generateHead;
exports.default = generateHead;
//# sourceMappingURL=generateHead.js.map