export interface AlternativeHtmlInfosSettings {
    key: string;
    text: boolean;
    url: boolean;
}
export interface AlternativeHtmlInfos {
    text: string;
    url: string;
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: AlternativeHtmlInfosSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
