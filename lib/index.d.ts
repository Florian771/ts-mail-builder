export { mjmlImage } from "./templates/components/image";
export declare const getSingleComponentHtml: (options: import("../../mail-builder-types").BuildOptions) => Promise<import("../../mail-builder-types").Template>;
export declare const start: (options: import("../../mail-builder-types").BuildOptions) => Promise<import("../../mail-builder-types").Result>;
