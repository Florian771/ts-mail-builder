"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
var config_1 = require("../../config");
var utils_1 = require("../../utils");
var placeholder = {
    text: "{HTML_LINK_TEXT}",
    url: "{HTML_LINK_URL}"
};
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    var translations = utils_1.getTranslations({
        placeholder: placeholder,
        settings: settings
    });
    var html = "";
    var text = "";
    text = "" + config_1.textBreak + placeholder.text + ": " + placeholder.url + config_1.textBreak;
    html = "\n  <mj-section padding=\"10px 0 20px 0\">\n  <mj-column>\n    <mj-text align=\"center\" color=\"{textColor}\" font-size=\"11px\">";
    if (settings.url) {
        html += "<a style=\"color:{textColor};text-decoration:none;\" href=\"" + placeholder.url + "\" style=\"color:{textColor};text-decoration:none;\">\n        " + placeholder.text + "\n        </a>";
    }
    else if (settings.text) {
        html += "" + placeholder.text;
    }
    html += "</mj-text>\n  </mj-column>\n</mj-section>";
    testParse_1.default({
        test: true,
        html: html,
        options: options,
        modulName: "alternativeHtmlLink"
    });
    return {
        html: html,
        text: text,
        translations: translations,
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=alternativeHtmlInfo.js.map