import { start } from "./index"

const chatifyOptions: MailBuilder.DesignSettings = {
  TemplateName: "1",
  primaryColor: "#dd2476",
  secundaryColor: "#ff512f",
  headlineColor: "#111", //NEU
  textColor: "#9B9B9B", //c
  textColorHighlight: "#000", //c
  backgroundColor: "#F2F2F2", //c
  backgroundColor2: "#fff", //c
  headlineFontName:
    '"Montserrat", "Open Sans", "Helvetica Neue", Helvetica, sans-serif',
  bodyFontName: '"Open Sans", "Helvetica Neue", Helvetica, sans-serif',
  bottomSlanted: true
}

const user: chatify.UserSettings = {
  displayName: "Thomas Muster"
}
const client: chatify.Client = {
  companyName: "Muster GmbH",
  contactPersonFirstName: "Thomas",
  contactPersonLastName: "Musterss",
  email: "test@test.at",
  street: "Neutorstr. 4",
  postalCode: "5300",
  city: "Salzburg",
  country: "Österreich"
}
const data: any = {
  user: user,
  client: client
}
const designSettings: MailBuilder.DesignSettings = chatifyOptions
const options: MailBuilder.BuildOptions = {
  name: "chatify",
  from: "noreply@chatify.info",
  to: "florian1677@gmail.com",
  data: data,
  socialMedia: [
    {
      name: "facebook",
      url: "https://wwww.facebook.com"
    },
    {
      name: "youtube",
      url: "https://wwww.youtube.com"
    },
    {
      name: "instagram",
      url: "https://wwww.instagram.com"
    },
    {
      name: "twitter",
      url: "https://wwww.twitter.com"
    },
    {
      name: "pinterest",
      url: "https://wwww.pinterest.com"
    },
    {
      name: "google",
      url: "https://wwww.google.com"
    }
  ],
  designSettings: designSettings,
  templateType: {
    name: "minimal",
    alternativeHtmlInfo: {
      text: true,
      url: true
    },
    header: {
      logo: "",
      context: true,
      menuLinks: 3
    },
    button: {
      key: "BTN"
    },
    text: {
      key: "TXT"
    },
    sectionBottom: {
      active: true,
      type: "diagonal"
    },
    footer: {
      unsubscribeText: true,
      unsubscribeUrl: true,
      name: true,
      street: true,
      postalCode: true,
      city: true,
      country: true,
      phone: true,
      fax: true,
      email: true,
      website: true,
      uid: true
    }
  },
  contentType: "userCreated",
  buildType: "file",
  testParse: true,
  translations: [
    { HTML_LINK_TEXT: "Chatify - bot up your business" },
    { HTML_LINK_URL: "" },
    { MAIL_CONTEXT: "" },
    { LOGO_URL: "https://admin.chatify.info/assets/img/logo-claim@2x.png" },
    { LOGO_TITLE: "Demo Hotel" },
    { LOGO_TARGET: "" }
  ]
}
start(options).then((res: MailBuilder.Result) => {
  return res
})
