import testParse from "../../testParse"
import { textBreak } from "../../config"
import { getTranslations } from "../../utils"

export interface ImageTextSettings {
  src: string
  key: string
  width?: number
  height?: number
}

export interface ImageText {
  alt: string
  tel: string
  email: string
}

const placeholder: ImageText = {
  alt: "{IMAGE_ALT_KEY}",
  tel: "{CONTENT_TEL}",
  email: "{CONTENT_EMAIL}"
}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: ImageTextSettings
}): MailBuilder.Component => {
  // console.log("mailBuilder - imageText - settings.src", settings.src)
  const translations: string[] = getTranslations({
    placeholder: placeholder,
    settings: settings
  })
  let html: string = ""
  let text: string = ""
  const width: number = settings.width ? settings.width : 600
  // const height: number = settings.height ? settings.height : 320
  if (settings.src) {
    text = `${textBreak}`
    html = `<mj-section padding="0px" background-color="{backgroundColor2}">
    <mj-column width="15%">
    </mj-column>
    <mj-column width="25%">
        <mj-image align="center" padding="0px" width="${width}px" src="${
      settings.src
    }" alt="${placeholder.alt.replace(
      "KEY",
      settings.key
    )}" align="center" border="none"></mj-image>
        </mj-column>
        <mj-column width="60%" padding="30px 0px 0px 0px">

        <mj-text padding="0" font-weight="300" font-size="24px" line-height="32px">
          <div class="mobileTextCenter">
            <a href="tel:${placeholder.tel.replace(
              "KEY",
              settings.key
            )}" style="color:{textColor};text-decoration:none;">${placeholder.tel.replace(
      "KEY",
      settings.key
    )}</a>
          </div>
        </mj-text>
        <mj-text padding="0">
          <div class="mobileTextCenter">
          <a href="mailto:${placeholder.email.replace(
            "KEY",
            settings.key
          )}" style="color:{textColor};text-decoration:none;">${placeholder.email.replace(
      "KEY",
      settings.key
    )}</a>
          </div>
        </mj-text>
      </mj-column>
    </mj-section>`
    // console.log("html: ", html)
    testParse({
      test: true,
      html: html,
      options: options,
      modulName: "imageText"
    })
  }
  return {
    html: html,
    text: text,
    translations: translations,
    parse: (args: any) => testParse(args)
  }
}
