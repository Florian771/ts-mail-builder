import * as minimal from "./minimal"
import * as offer from "./offer"
import * as dynamic from "./dynamic"
import * as zahlungsErinnerung from "./zahlungsErinnerung"
import * as requestImageSize from "request-image-size"

export async function getHtmlFromTemplateType({
  templateType,
  options
}: {
  templateType: MailBuilder.TemplateType
  options: MailBuilder.BuildOptions
}): Promise<MailBuilder.Component> {
  let logo = ""
  try {
    logo = options.templateType.header.logo
    console.log("logo", logo)
  } catch (err) {
    console.log("getLogo err: ", err)
  }
  if (logo) {
    let dimensions: any
    try {
      dimensions = await requestImageSize(logo)
      console.info("SIZE", dimensions)
      // The size, in octets, of the HTML source code file.
    } catch (error) {
      console.error("ERROR", error)
    }
    try {
      options.templateType.header.logoWidth = dimensions.width
      options.templateType.header.logoHeight = dimensions.height
      console.log("dimensions", dimensions)
    } catch (err) {
      console.log("templates/index logoWidth/logoHeight err:", err)
    }
  }

  // console.log("templateType.name", templateType.name)
  if (options.sections || templateType.name === "dynamic") {
    return await dynamic.getHtml({ options: options })
  } else if (templateType.name === "product") {
    return await offer.getHtml({ options: options })
  } else if (templateType.name === "zahlungsErinnerung") {
    return await zahlungsErinnerung.getHtml({ options: options })
  } else {
    return await minimal.getHtml({ options: options })
  }
}
