import testParse from "../../testParse"
import { textBreak } from "../../config"
import { getTranslations } from "../../utils"

export interface ButtonSettings {
  key: string
}

export interface Button {
  buttonText: string
  buttonUrl: string
}

const placeholder: Button = {
  buttonText: "{BUTTON_TEXT_KEY}",
  buttonUrl: "{BUTTON_URL_KEY}"
}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: ButtonSettings
}): MailBuilder.Component => {
  const translations: string[] = getTranslations({
    placeholder: placeholder,
    settings: settings
  })
  let html: string = ""
  let text: string = textBreak

  text = `${placeholder.buttonText.replace(
    "KEY",
    settings.key
  )}: ${placeholder.buttonUrl.replace("KEY", settings.key)}${textBreak}`

  html = `<mj-section background-color="{backgroundColor2}">
    <mj-column width="80%">
      <mj-button align="center" background-color="{secundaryColor}" color="{backgroundColor2}" border-radius="0px" href="${placeholder.buttonUrl.replace(
        "KEY",
        settings.key
      )}" inner-padding="15px 30px" padding-bottom="30px" padding-top="20px">
        ${placeholder.buttonText.replace("KEY", settings.key)}
      </mj-button>
    </mj-column>
  </mj-section>`

  testParse({ test: true, html: html, options: options, modulName: "button" })
  return {
    html: html,
    text: text,
    translations: translations,
    parse: (args: any) => testParse(args)
  }
}
