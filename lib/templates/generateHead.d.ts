export declare function generateHead({ font }: {
    font: string;
}): string;
export default generateHead;
