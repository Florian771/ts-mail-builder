"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
var config_1 = require("../../config");
var utils_1 = require("../../utils");
var placeholder = {
    subline: "{SUBLINE_KEY}"
};
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    var translations = utils_1.getTranslations({
        placeholder: placeholder,
        settings: settings
    });
    var html = "";
    var text = "";
    text = "" + placeholder.subline.replace("KEY", settings.key) + config_1.textBreak;
    html = "<mj-section padding=\"10px 20px 10px 20px\" background-color=\"{backgroundColor2}\">\n  <mj-column>\n    <mj-text align=\"center\" font-weight=\"300\" padding=\"15px 20px 5px 20px\" font-size=\"24px\" line-height=\"30px\" color=\"{textColor}\">\n      " + placeholder.subline.replace("KEY", settings.key) + "\n    </mj-text>\n  </mj-column>\n</mj-section>";
    testParse_1.default({ test: true, html: html, options: options, modulName: "subline" });
    return {
        html: html,
        text: text,
        translations: translations,
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=subline.js.map