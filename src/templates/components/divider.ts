import testParse from "../../testParse"
import { textBreak } from "../../config"

export interface DividerSettings {
  key?: string
}

export interface Divider {}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: DividerSettings
}): MailBuilder.Component => {
  let html: string = ""
  let text: string = ""
  text = `${textBreak}`
  html = `
  <mj-section padding="10px 20px" background-color="{backgroundColor2}">
  <mj-column>
    <mj-divider width="30px" border-width="1px" border-color="{textColor}"></mj-divider>
  </mj-column>
</mj-section>`

  testParse({ test: true, html: html, options: options, modulName: "divider" })
  return {
    html: html,
    text: text,
    translations: [],
    parse: (args: any) => testParse(args)
  }
}
