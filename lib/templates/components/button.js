"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
var config_1 = require("../../config");
var utils_1 = require("../../utils");
var placeholder = {
    buttonText: "{BUTTON_TEXT_KEY}",
    buttonUrl: "{BUTTON_URL_KEY}"
};
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    var translations = utils_1.getTranslations({
        placeholder: placeholder,
        settings: settings
    });
    var html = "";
    var text = config_1.textBreak;
    text = placeholder.buttonText.replace("KEY", settings.key) + ": " + placeholder.buttonUrl.replace("KEY", settings.key) + config_1.textBreak;
    html = "<mj-section background-color=\"{backgroundColor2}\">\n    <mj-column width=\"80%\">\n      <mj-button align=\"center\" background-color=\"{secundaryColor}\" color=\"{backgroundColor2}\" border-radius=\"0px\" href=\"" + placeholder.buttonUrl.replace("KEY", settings.key) + "\" inner-padding=\"15px 30px\" padding-bottom=\"30px\" padding-top=\"20px\">\n        " + placeholder.buttonText.replace("KEY", settings.key) + "\n      </mj-button>\n    </mj-column>\n  </mj-section>";
    testParse_1.default({ test: true, html: html, options: options, modulName: "button" });
    return {
        html: html,
        text: text,
        translations: translations,
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=button.js.map