import testParse from "../../testParse"
import { textBreak } from "../../config"
import { getTranslations } from "../../utils"

export interface TableBillSettings {
  key: string
}
export interface TableBill {
  headline: string
  header1: string
  header2: string
  header3: string
  header4: string
  header5: string
  content1: string
  content2: string
  content3: string
  content4: string
  content5: string
}

const placeholder: TableBill = {
  headline: "{TB_HEADLINE_KEY}",
  header1: "{TB_B_KEY}",
  header2: "{TB_F_KEY}",
  header3: "{TB_A_KEY}",
  header4: "{TB_P_KEY}",
  header5: "{TB_O_KEY}",
  content1: "{CONTENT1}",
  content2: "{CONTENT2}",
  content3: "{CONTENT3}",
  content4: "{CONTENT4}",
  content5: "{CONTENT5}"
}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: TableBillSettings
}): MailBuilder.Component => {
  const translations: string[] = getTranslations({
    placeholder: placeholder,
    settings: settings
  })
  let html: string = ""
  let text: string = ""
  text = `
  ${placeholder.headline.replace("KEY", settings.key)}${textBreak}
  ${placeholder.header1.replace(
    "KEY",
    settings.key
  )}: ${placeholder.content1.replace("KEY", settings.key)} ${textBreak}
  ${placeholder.header2.replace(
    "KEY",
    settings.key
  )}: ${placeholder.content2.replace("KEY", settings.key)} ${textBreak}
  ${placeholder.header3.replace(
    "KEY",
    settings.key
  )}: ${placeholder.content3.replace("KEY", settings.key)} ${textBreak}
  ${placeholder.header4.replace(
    "KEY",
    settings.key
  )}: ${placeholder.content4.replace("KEY", settings.key)} ${textBreak}
  ${placeholder.header5.replace(
    "KEY",
    settings.key
  )}: ${placeholder.content5.replace("KEY", settings.key)} ${textBreak}
  `

  html = `
  <mj-section padding-left="30px" padding-right="30px" background-color="#ffffff">
      <mj-column background-color="#eaeaea">
        <mj-text>
          <b>
            ${placeholder.headline.replace("KEY", settings.key)}
          </b>
        </mj-text>
        <mj-table>
          <tr style="border-bottom:1px solid #eaeaea;text-align:left;padding:15px 0;">
            <th style="padding: 0 15px 0 0;">${placeholder.header1.replace(
              "KEY",
              settings.key
            )}</th>
            <th style="padding: 0 15px;">${placeholder.header2.replace(
              "KEY",
              settings.key
            )}</th>
            <th style="padding: 0 0 0 15px;">${placeholder.header3.replace(
              "KEY",
              settings.key
            )}</th>
            <th style="padding: 0 0 0 15px;">${placeholder.header4.replace(
              "KEY",
              settings.key
            )}</th>
            <th style="padding: 0 0 0 15px;">${placeholder.header5.replace(
              "KEY",
              settings.key
            )}</th>
          </tr>
          <tr>
            <td style="padding: 0 15px 0 0;">${placeholder.content1.replace(
              "KEY",
              settings.key
            )}</td>
            <td style="padding: 0 15px;">${placeholder.content2.replace(
              "KEY",
              settings.key
            )}</td>
            <td style="padding: 0 0 0 15px;">${placeholder.content3.replace(
              "KEY",
              settings.key
            )}</td>
            <td style="padding: 0 0 0 15px;">${placeholder.content4.replace(
              "KEY",
              settings.key
            )}</td>
            <td style="padding: 0 0 0 15px;">${placeholder.content5.replace(
              "KEY",
              settings.key
            )}</td>
          </tr>
        </mj-table>
      </mj-column>
    </mj-section>`

  testParse({
    test: true,
    html: html,
    options: options,
    modulName: "tableBill"
  })
  return {
    html: html,
    text: text,
    translations: translations,
    parse: (args: any) => testParse(args)
  }
}
