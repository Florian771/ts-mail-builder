export default function applyDesignSettings({ html, designSettings }: {
    html: string;
    designSettings: MailBuilder.DesignSettings;
}): string;
