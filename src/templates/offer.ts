import head from "./components/head"
import hero from "./components/hero"
import accordeon from "./components/accordeon"
import image from "./components/image"
import headline from "./components/headline"
import subline from "./components/subline"
import header from "./components/header"
import text from "./components/text"
import spacer from "./components/spacer"
import divider from "./components/divider"
import carousel from "./components/carousel"
import button from "./components/button"
import sectionBottom from "./components/sectionBottom"
import footer from "./components/footer"
import end from "./components/end"
import alternativeHtmlInfo from "./components/alternativeHtmlInfo"

export async function getHtml({
  options
}: {
  options: MailBuilder.BuildOptions
}): Promise<MailBuilder.Component> {
  const mailContent: MailBuilder.MailTemplateComponent = Object.create({
    html: "",
    text: "",
    translations: [],
    add: function({ component }: { component: MailBuilder.Component }) {
      this.html += component.html
      this.text += component.text
      this.translations += component.translations
    }
  })

  /* COMPONENTS */

  mailContent.add({ component: head({ options: options, settings: {} }) })

  mailContent.add({
    component: alternativeHtmlInfo({
      options: options,
      settings: options.templateType.alternativeHtmlInfo
    })
  })

  mailContent.add({
    component: header({
      options: options,
      settings: options.templateType.header
    })
  })

  mailContent.add({
    component: hero({
      options: options,
      settings: options.templateType.hero
    })
  })

  mailContent.add({
    component: headline({
      options: options,
      settings: {
        headline: true,
        key: "HDL"
      }
    })
  })

  mailContent.add({ component: divider({ options: options, settings: {} }) })

  mailContent.add({
    component: text({ options: options, settings: options.templateType.text })
  })

  mailContent.add({
    component: carousel({
      options: options,
      settings: options.templateType.carousel
    })
  })

  mailContent.add({ component: spacer({ options: options, settings: {} }) })

  mailContent.add({
    component: subline({
      options: options,
      settings: {
        key: "SL"
      }
    })
  })

  mailContent.add({
    component: button({
      options: options,
      settings: options.templateType.button
    })
  })

  mailContent.add({ component: spacer({ options: options, settings: {} }) })

  if (
    options.templateType.image &&
    options.templateType.image.src &&
    options.templateType.image.src !== "undefined" &&
    options.templateType.image.src !== "unkown"
  ) {
    /* console.log(
      "mailBuilder - offer - image - options.templateType.image",
      options.templateType.image
    ) */
    mailContent.add({
      component: image({
        options: options,
        settings: options.templateType.image
      })
    })
  } else {
    console.log("mailBuilder - offer - no image")
  }

  mailContent.add({
    component: accordeon({
      options: options,
      settings: options.templateType.accordeon
    })
  })

  mailContent.add({
    component: sectionBottom({
      options: options,
      settings: options.templateType.sectionBottom
    })
  })

  mailContent.add({
    component: footer({
      options: options,
      settings: options.templateType.footer
    })
  })

  mailContent.add({ component: end({ options: options, settings: {} }) })

  return {
    html: mailContent.html,
    text: mailContent.text,
    translations: mailContent.translations
  }
}
