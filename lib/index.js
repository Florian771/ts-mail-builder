"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var sendEmail_1 = require("./sendEmail");
var compose_1 = require("./compose");
var createMailContents_1 = require("./createMailContents");
var config_1 = require("./config");
var utils_1 = require("./utils");
var constants_1 = require("./constants");
var dynamic_1 = require("./templates/dynamic");
var fs = require("fs-extra");
// export { mjmlHeadline } from "./templates/components/headline"
var image_1 = require("./templates/components/image");
exports.mjmlImage = image_1.mjmlImage;
/*
export { default as testParse } from "./testParse"
export { default as head } from "./templates/components/head"
export { default as hero } from "./templates/components/hero"
export { default as accordeon } from "./templates/components/accordeon"
export {
  default as headlineWithSubline
} from "./templates/components/headlineWithSubline"
export {
  default as headlineWithSublineWithLink
} from "./templates/components/headlineWithSublineWithLink"
export { default as subline } from "./templates/components/subline"
export { default as header } from "./templates/components/header"
export { default as text } from "./templates/components/text"
export { default as textWithImage } from "./templates/components/textWithImage"
export { default as spacer } from "./templates/components/spacer"
export { default as divider } from "./templates/components/divider"
export { default as carousel } from "./templates/components/carousel"
export { default as button } from "./templates/components/button"
export { default as sectionBottom } from "./templates/components/sectionBottom"
export { default as footer } from "./templates/components/footer"
export { default as end } from "./templates/components/end"
export { default as social } from "./templates/components/social"
export {
  default as alternativeHtmlInfo
} from "./templates/components/alternativeHtmlInfo"
 */
exports.getSingleComponentHtml = function (options) { return __awaiter(_this, void 0, void 0, function () {
    var component, err_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                options.getSingleComponent = true;
                return [4 /*yield*/, dynamic_1.getHtml({ options: options })
                    /* console.log(
                      "getSingleComponentHtml - getHtml - component.html:",
                      component.html
                    ) */
                    /* console.log(
                      "getSingleComponentHtml - getHtml - component.parse:",
                      typeof component.parse
                    ) */
                ];
            case 1:
                component = _a.sent();
                /* console.log(
                  "getSingleComponentHtml - getHtml - component.html:",
                  component.html
                ) */
                /* console.log(
                  "getSingleComponentHtml - getHtml - component.parse:",
                  typeof component.parse
                ) */
                console.log("getSingleComponentHtml - component type:", options.sections[0].type);
                console.log("options.sections[0].settings:", options.sections[0].settings);
                return [2 /*return*/, component.parse({
                        html: component.html,
                        options: options,
                        modulName: options.sections[0].type,
                        test: false
                    })];
            case 2:
                err_1 = _a.sent();
                console.log("index.js: error: ", err_1.message);
                return [2 /*return*/, err_1];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.start = function (options) { return __awaiter(_this, void 0, void 0, function () {
    var buildOptions, designSettings, mailContentsOutput, composedEmail, fileName, err_2, res, err_3, err_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 13, , 14]);
                console.log("mailBuilder start - options: ", options);
                buildOptions = options;
                designSettings = __assign({}, config_1.defaultOptions, options.designSettings);
                buildOptions.designSettings = designSettings;
                return [4 /*yield*/, createMailContents_1.createMailContents(options)];
            case 1:
                mailContentsOutput = _a.sent();
                console.log("mailContentsOutput", mailContentsOutput);
                return [4 /*yield*/, compose_1.composeEmail({
                        subject: mailContentsOutput.subject,
                        links: mailContentsOutput.links,
                        text: mailContentsOutput.text,
                        html: mailContentsOutput.html,
                        options: mailContentsOutput.options,
                        translations: mailContentsOutput.translations
                    })];
            case 2:
                composedEmail = _a.sent();
                fileName = utils_1.getHtmlFileName({
                    options: mailContentsOutput.options,
                    type: "final"
                });
                console.log("http://127.0.0.1:3000/" + fileName + ".html");
                if (!(mailContentsOutput.options.buildType === "html")) return [3 /*break*/, 3];
                return [2 /*return*/, {
                        mail: composedEmail.mail,
                        translations: composedEmail.translations,
                        url: "",
                        type: mailContentsOutput.options.buildType
                    }];
            case 3:
                if (!(mailContentsOutput.options.buildType === "file")) return [3 /*break*/, 8];
                _a.label = 4;
            case 4:
                _a.trys.push([4, 6, , 7]);
                return [4 /*yield*/, fs.writeFile("./public/" + fileName + ".html", composedEmail.mail.html, function (err) {
                        if (err) {
                            throw err;
                        }
                    })];
            case 5:
                _a.sent();
                console.log(fileName + ".html saved");
                return [2 /*return*/, {
                        mail: composedEmail.mail,
                        translations: composedEmail.translations,
                        url: constants_1.default.web.urlWeb + "/" + fileName + ".html",
                        type: mailContentsOutput.options.buildType
                    }];
            case 6:
                err_2 = _a.sent();
                throw err_2;
            case 7: return [3 /*break*/, 12];
            case 8:
                try {
                    sendEmail_1.initEmail();
                    console.log("sendEmail authorized");
                }
                catch (err) {
                    throw err;
                }
                _a.label = 9;
            case 9:
                _a.trys.push([9, 11, , 12]);
                if (options.subject) {
                    composedEmail.mail.subject = options.subject;
                }
                return [4 /*yield*/, sendEmail_1.sendEmail(composedEmail.mail)];
            case 10:
                res = _a.sent();
                console.log("mail " +
                    composedEmail.mail.subject +
                    " sent to " +
                    composedEmail.mail.to);
                return [2 /*return*/, {
                        mail: composedEmail.mail,
                        translations: composedEmail.translations,
                        url: constants_1.default.web.urlWeb + "/" + fileName + ".html",
                        type: mailContentsOutput.options.buildType,
                        sendResponse: {
                            statusCode: res.statusCode,
                            statusMessage: res.statusMessage,
                            body: res.body
                        }
                    }];
            case 11:
                err_3 = _a.sent();
                console.log("composedEmail subject: ", composedEmail.mail.subject);
                console.log("composedEmail from: ", composedEmail.mail.from);
                console.log("composedEmail to: ", composedEmail.mail.to);
                console.log("sendgrid err:", err_3.message);
                throw err_3;
            case 12: return [3 /*break*/, 14];
            case 13:
                err_4 = _a.sent();
                console.log("index.js: error: ", err_4.message);
                return [2 /*return*/, err_4];
            case 14: return [2 /*return*/];
        }
    });
}); };
//# sourceMappingURL=index.js.map