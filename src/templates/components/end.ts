import { textBreak } from "../../config"

export interface Settings {}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: Settings
}): MailBuilder.Component => {
  let html: string = ""
  let text: string = ""
  text = `${textBreak}`
  html = `
  </mj-body>
  </mjml>`

  return {
    html: html,
    text: text,
    translations: []
  }
}
