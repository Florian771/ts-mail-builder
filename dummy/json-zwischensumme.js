export default {
  API: {
    HotelData: {
      Ort: "Fusch",
      HotelName: "Römerhof GmbH",
      PersName: "",
      Gruss: "",
      Strasse: "Zeller Fusch 77",
      Nation: "A",
      PLZ: "5672",
      Telefon: "0043 6546 218 0",
      Fax: "0043 6546 218 16",
      UID: "AT61696025",
      KtoNr: "2011773",
      Email: "hotel@roemerhof-fusch.at",
      Bank: "Raiffeisenbank 5672 Fusch",
      BLZ: "35012",
      BIC: "RVSAAT2S012",
      IBAN: "AT093501200002011773"
    },
    ReservationData: {
      LFNUMMER: "43784",
      ZIMMER_NR: "64",
      PERS_NR: "26657",
      PERSON: "Helga Test",
      VON: "18.07.2018",
      BIS: "20.07.2018",
      ANZ_K1: "0",
      ANZ_K2: "1",
      ANZ_E: "2",
      ANZ_K3: "1",
      ANZ_K4: "0",
      ANZ_E2: "0",
      ANGEBOTSURL:
        "https://apps.weratech-online.com/wtOnlineBooking/Home/CreateOnlineReservation/MzA4Mw2?reservationid=57C056105CD05E8057C0&guestid=54605B205B2059705CD0&lang=DE"
    },
    GuestData: {
      LFNUMMER: "26657",
      ANREDE_H: "Frau",
      FAMNAME: "Helga",
      VORNAME: "Test",
      TITEL: "",
      GESCHLECHT: "",
      STRASSE: "",
      PLZ: "",
      ORT: "",
      ANREDE_FAM: "Sehr geehrte Frau Helga",
      SPRACHE: "D Deutsch",
      NATION: "A",
      ZUSNAME: "",
      P_EMAIL: "hotel@roemerhof-fusch.at; patrick_enn@yahoo.com",
      NATION_TXT: ""
    },
    RoomData: {
      ZIMMER_NR: "64",
      ZIMMER_NAME: "403",
      GEEIGNET_VON: "4",
      GEEIGNET_BIS: "4",
      ZI_KAT: "5",
      STANDARDLEISTUNG: "",
      ZIMMER_SAUBER: "J",
      GEEIGNET_STD: "4",
      TEXT1: "",
      TEXT2: "",
      ZIMMERBILD:
        "https://www.weratech-files.com/images/281623/gastrodat/Zimmer/Zimmer_Bild_64.jpg?timestamp=636157639980000000",
      ZIMMERGRUNDRISS:
        "https://www.weratech-files.com/images/281623/gastrodat/Zimmer/Grundriss/ZIMMER_GRUNDRISS_64.jpg?timestamp=635929426150000000"
    },
    ExtrasData: {
      Posio: [
        {
          FKT_NR: "89254",
          P_VON: "18.07.2018",
          P_BIS: "20.07.2018",
          P_TEXT: "Pers. DZA/HP",
          P_ANZ: "2",
          P_SUM: "276",
          P_PREIS: "69",
          P_RABATT: "0",
          P_KG_NR: "1",
          GROUPINDEX: "1",
          EXTRA_NR: "1",
          DAUER_VON: "1",
          DAUER_BIS: "999",
          BESCHREIBUNG:
            "Doppelzimmer der Kategorie A (ca. 24m²) mit Balkon und Halbpension\nZimmerausstattung mit D/WC, Haarfön, Telefon mit Weckfunktion, Flat-Screen Kabel-TV, Radio, Zimmersafe, Kühlschrank (30l), W-Lan und Balkon.\n\nDie Kurtaxe von Euro 1,10 pro Person und Nacht ab 15 Jahren ist nicht im Zimmerpreis inbegriffen und wird vor Ort separat verrechnet.",
          EXTRABILD:
            "https://www.weratech-files.com/images/281623/gastrodat/Extras/EXTRA_1.jpg?timestamp=636540496150000000",
          ZUSATZ: "10039,10041,10042,"
        },
        {
          FKT_NR: "89255",
          P_VON: "18.07.2018",
          P_BIS: "20.07.2018",
          P_TEXT: "Pers. DZA/HP KD 6-11,99 J. (-50%)",
          P_ANZ: "1",
          P_SUM: "75",
          P_PREIS: "37.5",
          P_RABATT: "0",
          P_KG_NR: "200000",
          GROUPINDEX: "1"
        },
        {
          FKT_NR: "89256",
          P_VON: "18.07.2018",
          P_BIS: "20.07.2018",
          P_TEXT: "Pers. DZA/HP KD 3-5,99 J. (-80%)",
          P_ANZ: "1",
          P_SUM: "37.2",
          P_PREIS: "18.6",
          P_RABATT: "0",
          P_KG_NR: "200000",
          GROUPINDEX: "1"
        },
        {
          FKT_NR: "89257",
          P_VON: "17.07.2018",
          P_BIS: "18.07.2018",
          P_TEXT: "UND",
          P_ANZ: "0",
          P_SUM: "0",
          P_PREIS: "0",
          P_RABATT: "0",
          P_KG_NR: "0",
          GROUPINDEX: "1",
          EXTRA_NR: "0",
          DAUER_VON: "0",
          DAUER_BIS: "0",
          BESCHREIBUNG: "",
          EXTRABILD: "/Content/images/invalidimage.png",
          ZUSATZ: ""
        },
        {
          FKT_NR: "89258",
          P_VON: "17.07.2018",
          P_BIS: "18.07.2018",
          P_TEXT: "ZSUM",
          P_ANZ: "0",
          P_SUM: "0",
          P_PREIS: "0",
          P_RABATT: "0",
          P_KG_NR: "0",
          GROUPINDEX: "1",
          EXTRA_NR: "0",
          DAUER_VON: "0",
          DAUER_BIS: "0",
          BESCHREIBUNG: "",
          EXTRABILD: "/Content/images/invalidimage.png",
          ZUSATZ: ""
        },
        {
          FKT_NR: "89259",
          P_VON: "17.07.2018",
          P_BIS: "18.07.2018",
          P_TEXT: "UND",
          P_ANZ: "0",
          P_SUM: "0",
          P_PREIS: "0",
          P_RABATT: "0",
          P_KG_NR: "0",
          GROUPINDEX: "1",
          EXTRA_NR: "0",
          DAUER_VON: "0",
          DAUER_BIS: "0",
          BESCHREIBUNG: "",
          EXTRABILD: "/Content/images/invalidimage.png",
          ZUSATZ: ""
        },
        {
          FKT_NR: "89260",
          P_VON: "18.07.2018",
          P_BIS: "20.07.2018",
          P_TEXT: "Pers. DZB/HP",
          P_ANZ: "2",
          P_SUM: "264",
          P_PREIS: "66",
          P_RABATT: "0",
          P_KG_NR: "10006",
          GROUPINDEX: "1",
          EXTRA_NR: "10006",
          DAUER_VON: "1",
          DAUER_BIS: "999",
          BESCHREIBUNG:
            "Doppelzimmer der Kategorie B (ca. 20m²) mit Balkon und Halbpension\nZimmerausstattung mit D/WC, Haarfön, Telefon mit Weckfunktion, Flat-Screen Kabel-TV, Radio, Zimmersafe, Kühlschrank (30l), W-Lan und Balkon.\n\nDie Kurtaxe von Euro 1,10 pro Person und Nacht ab 15 Jahren ist nicht im Zimmerpreis inbegriffen und wird vor Ort separat verrechnet.",
          EXTRABILD:
            "https://www.weratech-files.com/images/281623/gastrodat/Extras/EXTRA_10006.jpg?timestamp=636540402510000000",
          ZUSATZ: ""
        },
        {
          FKT_NR: "89261",
          P_VON: "18.07.2018",
          P_BIS: "20.07.2018",
          P_TEXT: "Pers. DZB/HP KD 6-11,99 J. (-50%)",
          P_ANZ: "1",
          P_SUM: "72",
          P_PREIS: "36",
          P_RABATT: "0",
          P_KG_NR: "200000",
          GROUPINDEX: "1"
        },
        {
          FKT_NR: "89262",
          P_VON: "18.07.2018",
          P_BIS: "20.07.2018",
          P_TEXT: "Pers. DZB/HP KD 3-5,99 J. (-80%)",
          P_ANZ: "1",
          P_SUM: "36",
          P_PREIS: "18",
          P_RABATT: "0",
          P_KG_NR: "200000",
          GROUPINDEX: "1"
        },
        {
          FKT_NR: "89263",
          P_VON: "17.07.2018",
          P_BIS: "18.07.2018",
          P_TEXT: "UND",
          P_ANZ: "0",
          P_SUM: "0",
          P_PREIS: "0",
          P_RABATT: "0",
          P_KG_NR: "0",
          GROUPINDEX: "1",
          EXTRA_NR: "0",
          DAUER_VON: "0",
          DAUER_BIS: "0",
          BESCHREIBUNG: "",
          EXTRABILD: "/Content/images/invalidimage.png",
          ZUSATZ: ""
        },
        {
          FKT_NR: "89264",
          P_VON: "17.07.2018",
          P_BIS: "18.07.2018",
          P_TEXT: "ZSUM",
          P_ANZ: "0",
          P_SUM: "0",
          P_PREIS: "0",
          P_RABATT: "0",
          P_KG_NR: "0",
          GROUPINDEX: "1",
          EXTRA_NR: "0",
          DAUER_VON: "0",
          DAUER_BIS: "0",
          BESCHREIBUNG: "",
          EXTRABILD: "/Content/images/invalidimage.png",
          ZUSATZ: ""
        },
        {
          FKT_NR: "89265",
          P_VON: "17.07.2018",
          P_BIS: "18.07.2018",
          P_TEXT: "ODER",
          P_ANZ: "0",
          P_SUM: "0",
          P_PREIS: "0",
          P_RABATT: "0",
          P_KG_NR: "0",
          GROUPINDEX: "-1",
          EXTRA_NR: "0",
          DAUER_VON: "0",
          DAUER_BIS: "0",
          BESCHREIBUNG: "",
          EXTRABILD: "/Content/images/invalidimage.png",
          ZUSATZ: ""
        },
        {
          FKT_NR: "89266",
          P_VON: "18.07.2018",
          P_BIS: "20.07.2018",
          P_TEXT: "Pers. DZ/HP",
          P_ANZ: "2",
          P_SUM: "276",
          P_PREIS: "69",
          P_RABATT: "0",
          P_KG_NR: "10056",
          GROUPINDEX: "2",
          EXTRA_NR: "10056",
          DAUER_VON: "1",
          DAUER_BIS: "999",
          BESCHREIBUNG:
            "Doppelzimmer mit Balkon und Halbpension\nZimmerausstattung mit D/WC, Haarfön, Telefon mit Weckfunktion, Flat-Screen Kabel-TV, Radio, Zimmersafe, Kühlschrank (30l), W-Lan und Balkon.\n\nDie Kurtaxe von Euro 1,10 pro Person und Nacht ab 15 Jahren ist nicht im Zimmerpreis inbegriffen und wird vor Ort separat verrechnet.",
          EXTRABILD:
            "https://www.weratech-files.com/images/281623/gastrodat/Extras/EXTRA_10056.jpg?timestamp=636645799990000000",
          ZUSATZ: "10039,10041,10042,"
        },
        {
          FKT_NR: "89267",
          P_VON: "18.07.2018",
          P_BIS: "20.07.2018",
          P_TEXT: "Pers. DZ/HP KD 6-11,99 J. (-50%)",
          P_ANZ: "1",
          P_SUM: "75",
          P_PREIS: "37.5",
          P_RABATT: "0",
          P_KG_NR: "200000",
          GROUPINDEX: "2"
        },
        {
          FKT_NR: "89268",
          P_VON: "18.07.2018",
          P_BIS: "20.07.2018",
          P_TEXT: "Pers. DZ/HP KD 3-5,99 J. (-80%)",
          P_ANZ: "1",
          P_SUM: "37.2",
          P_PREIS: "18.6",
          P_RABATT: "0",
          P_KG_NR: "200000",
          GROUPINDEX: "2"
        },
        {
          FKT_NR: "89269",
          P_VON: "17.07.2018",
          P_BIS: "18.07.2018",
          P_TEXT: "UND",
          P_ANZ: "0",
          P_SUM: "0",
          P_PREIS: "0",
          P_RABATT: "0",
          P_KG_NR: "0",
          GROUPINDEX: "2",
          EXTRA_NR: "0",
          DAUER_VON: "0",
          DAUER_BIS: "0",
          BESCHREIBUNG: "",
          EXTRABILD: "/Content/images/invalidimage.png",
          ZUSATZ: ""
        },
        {
          FKT_NR: "89270",
          P_VON: "17.07.2018",
          P_BIS: "18.07.2018",
          P_TEXT: "ZSUM",
          P_ANZ: "0",
          P_SUM: "0",
          P_PREIS: "0",
          P_RABATT: "0",
          P_KG_NR: "0",
          GROUPINDEX: "2",
          EXTRA_NR: "0",
          DAUER_VON: "0",
          DAUER_BIS: "0",
          BESCHREIBUNG: "",
          EXTRABILD: "/Content/images/invalidimage.png",
          ZUSATZ: ""
        }
      ]
    },
    ThemeData: {
      Themes: {
        gdatTheme: {
          theme_pics: {
            gdatPicture: [
              {
                PicId: "2656700919913",
                PicName: "Dahoam im Römerhof",
                PicPath: "$i_offer\\theme_neues Thema\\2656700919913.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/281623/gastrodat/2656700919913.jpg",
                PicHTML_Link: "",
                PicKategorie: "Hotel Römerhof",
                LastPicUpload: "2018-07-04T12:38:34.6927111+02:00"
              },
              {
                PicId: "26567c332bb73",
                PicName: "Dahoam im Römerhof",
                PicPath: "$i_offer\\theme_Sommer 2018\\26567c332bb73.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/281623/gastrodat/26567c332bb73.jpg",
                PicHTML_Link: "",
                PicKategorie: "",
                LastPicUpload: "2018-07-04T12:38:34.7237118+02:00"
              },
              {
                PicId: "26567db9f5383",
                PicName: "Dahoam im Römerhof",
                PicPath: "$i_offer\\theme_neues Thema\\26567db9f5383.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/281623/gastrodat/26567db9f5383.jpg",
                PicHTML_Link: "",
                PicKategorie: "Sommer",
                LastPicUpload: "2018-07-04T12:38:34.7487143+02:00"
              },
              {
                PicId: "26568001b1413",
                PicName: "Dahoam im Römerhof",
                PicPath: "$i_offer\\theme_Sommer 2018\\26568001b1413.jpg",
                PicUrl:
                  "https://www.weratech-files.com/images/281623/gastrodat/26568001b1413.jpg",
                PicHTML_Link: "",
                PicKategorie: "",
                LastPicUpload: "2018-07-04T12:38:34.7997162+02:00"
              }
            ]
          },
          theme_teasers: {
            gdatTeaser: [
              {
                Teaser_pic: {
                  PicId: "2656838d63dc3",
                  PicName: "im Nationalpark Hohe Tauern",
                  PicPath:
                    "$i_offer\\teaser_neuer ThemenTeaser\\2656838d63dc3.jpg",
                  PicUrl:
                    "https://www.weratech-files.com/images/281623/gastrodat/2656838d63dc3.jpg",
                  PicHTML_Link:
                    "https://www.roemerhof-fusch.at/de/sommer-in-fusch-aktivitaeten",
                  PicKategorie: "Hotel Römerhof",
                  LastPicUpload: "2018-07-04T12:38:34.8207167+02:00"
                },
                validFrom: "2018-04-14T14:40:25",
                validTo: "2018-10-14T14:40:25",
                SuchwortListe: "",
                TeaserId: "2656838d63dc3",
                TeaserName: "Traumsommer"
              },
              {
                Teaser_pic: {
                  PicId: "2656875daa9e3",
                  PicName: "Römerhof Inklusivleistungen",
                  PicPath: "$i_offer\\teaser_Ihre\\2656875daa9e3.jpg",
                  PicUrl:
                    "https://www.weratech-files.com/images/281623/gastrodat/2656875daa9e3.jpg",
                  PicHTML_Link:
                    "https://www.roemerhof-fusch.at/de/roemerhof_inklusivleistungen_sommer",
                  PicKategorie: "Hotel Römerhof",
                  LastPicUpload: "2018-07-04T12:38:34.8447191+02:00"
                },
                validFrom: "2018-04-14T14:42:08",
                validTo: "2018-10-14T14:42:08",
                SuchwortListe: "",
                TeaserId: "2656875daa9e3",
                TeaserName: "Ihre"
              },
              {
                Teaser_pic: {
                  PicId: "2c078df317f2b",
                  PicName: "Lassen Sie die Seele baumeln",
                  PicPath:
                    "$i_offer\\teaser_neuer ThemenTeaser\\2c078df317f2b.jpg",
                  PicUrl:
                    "https://www.weratech-files.com/images/281623/gastrodat/2c078df317f2b.jpg",
                  PicHTML_Link: "https://www.roemerhof-fusch.at/de/wellness",
                  PicKategorie: "",
                  LastPicUpload: "2018-07-04T12:38:34.9437258+02:00"
                },
                validFrom: "2018-04-14T11:58:52",
                validTo: "2018-10-14T11:58:52",
                SuchwortListe: "",
                TeaserId: "2c078df317f2b",
                TeaserName: "Wellness"
              },
              {
                Teaser_pic: {
                  PicId: "2c079205eaad4",
                  PicName: "Vorfreude ist die schönste Freude",
                  PicPath:
                    "$i_offer\\teaser_neuer ThemenTeaser\\2c079205eaad4.jpg",
                  PicUrl:
                    "https://www.weratech-files.com/images/281623/gastrodat/2c079205eaad4.jpg",
                  PicHTML_Link:
                    "https://www.roemerhof-fusch.at/de/gut-zu-wissen",
                  PicKategorie: "",
                  LastPicUpload: "2018-07-04T12:38:34.989728+02:00"
                },
                validFrom: "2018-04-14T12:00:42",
                validTo: "2018-10-14T12:00:42",
                SuchwortListe: { SuchWort: { Text: "FAMILIENZIMMER" } },
                TeaserId: "2c079205eaad4",
                TeaserName: "Gut zu wissen"
              }
            ]
          },
          validFrom: "2018-04-14T14:28:31",
          validTo: "2018-10-14T14:28:31",
          SuchwortListe: "",
          ThemeId: "265668ef37263",
          ThemeName: "Sommer 2018"
        }
      },
      BilderGalerie_pics: {
        gdatPicture: [
          {
            PicId: "26569e73394c3",
            PicName: "Stauseen Kaprun",
            PicPath: "$i_offer\\impressions\\26569e73394c3.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26569e73394c3.jpg",
            PicHTML_Link: "",
            PicKategorie: "Sommer",
            LastPicUpload: "2018-07-04T12:38:35.4567523+02:00"
          },
          {
            PicId: "26569fc824b03",
            PicName: "Nationalpark Hohe Tauern",
            PicPath: "$i_offer\\impressions\\26569fc824b03.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26569fc824b03.jpg",
            PicHTML_Link: "",
            PicKategorie: "Sommer",
            LastPicUpload: "2018-07-04T12:38:35.5097536+02:00"
          },
          {
            PicId: "2656a1692f623",
            PicName: "Zell am See",
            PicPath: "$i_offer\\impressions\\2656a1692f623.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2656a1692f623.jpg",
            PicHTML_Link: "",
            PicKategorie: "Sommer",
            LastPicUpload: "2018-07-04T12:38:35.5297548+02:00"
          },
          {
            PicId: "2656a2b03cf83",
            PicName: "Kohlschnait Rodelbahn",
            PicPath: "$i_offer\\impressions\\2656a2b03cf83.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2656a2b03cf83.jpg",
            PicHTML_Link: "",
            PicKategorie: "Winter",
            LastPicUpload: "2018-07-04T12:38:35.5797582+02:00"
          },
          {
            PicId: "2658432e6b69c",
            PicName: "Badeteich",
            PicPath: "$i_offer\\impressions\\2658432e6b69c.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2658432e6b69c.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.6097588+02:00"
          },
          {
            PicId: "265844ea04535",
            PicName: "Badeteich",
            PicPath: "$i_offer\\impressions\\265844ea04535.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/265844ea04535.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.6357606+02:00"
          },
          {
            PicId: "2658464af9556",
            PicName: "Speisesaal",
            PicPath: "$i_offer\\impressions\\2658464af9556.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2658464af9556.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.6797634+02:00"
          },
          {
            PicId: "265847ad1fb84",
            PicName: "Skischule Fusch",
            PicPath: "$i_offer\\impressions\\265847ad1fb84.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/265847ad1fb84.jpg",
            PicHTML_Link: "",
            PicKategorie: "Winter",
            LastPicUpload: "2018-07-04T12:38:35.7197642+02:00"
          },
          {
            PicId: "265849c782898",
            PicName: "Hallenbad",
            PicPath: "$i_offer\\impressions\\265849c782898.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/265849c782898.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.737766+02:00"
          },
          {
            PicId: "26584af5a75d9",
            PicName: "Sauna",
            PicPath: "$i_offer\\impressions\\26584af5a75d9.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26584af5a75d9.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.7897688+02:00"
          },
          {
            PicId: "26584c4a9d150",
            PicName: "Fitnessraum",
            PicPath: "$i_offer\\impressions\\26584c4a9d150.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26584c4a9d150.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.8197694+02:00"
          },
          {
            PicId: "2658503a25cc4",
            PicName: "Kitzsteinhorn",
            PicPath: "$i_offer\\impressions\\2658503a25cc4.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2658503a25cc4.jpg",
            PicHTML_Link: "",
            PicKategorie: "Winter",
            LastPicUpload: "2018-07-04T12:38:35.8487723+02:00"
          },
          {
            PicId: "26585305af8b7",
            PicName: "Großglockner Hochalpenstraße",
            PicPath: "$i_offer\\impressions\\26585305af8b7.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26585305af8b7.jpg",
            PicHTML_Link: "",
            PicKategorie: "Sommer",
            LastPicUpload: "2018-07-04T12:38:35.8997742+02:00"
          },
          {
            PicId: "26585dae15721",
            PicName: "Kinderspielzimmer",
            PicPath: "$i_offer\\impressions\\26585dae15721.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26585dae15721.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.9207747+02:00"
          },
          {
            PicId: "26586013eab7c",
            PicName: "Spielraum",
            PicPath: "$i_offer\\impressions\\26586013eab7c.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26586013eab7c.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.9497776+02:00"
          },
          {
            PicId: "2bfb62ddb979f",
            PicName: "Familie Scherer",
            PicPath: "$i_offer\\impressions\\2bfb62ddb979f.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2bfb62ddb979f.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.9997794+02:00"
          },
          {
            PicId: "2bfb643e02286",
            PicName: "Tierpark Ferleiten",
            PicPath: "$i_offer\\impressions\\2bfb643e02286.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2bfb643e02286.jpg",
            PicHTML_Link: "",
            PicKategorie: "Sommer",
            LastPicUpload: "2018-07-04T12:38:36.0207799+02:00"
          },
          {
            PicId: "2bfb67f876fee",
            PicName: "Wanderlust",
            PicPath: "$i_offer\\impressions\\2bfb67f876fee.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2bfb67f876fee.jpg",
            PicHTML_Link: "",
            PicKategorie: "Sommer",
            LastPicUpload: "2018-07-04T12:38:36.0487827+02:00"
          },
          {
            PicId: "2bfb6eef58b40",
            PicName: "Schneefreuden",
            PicPath: "$i_offer\\impressions\\2bfb6eef58b40.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2bfb6eef58b40.jpg",
            PicHTML_Link: "",
            PicKategorie: "Winter",
            LastPicUpload: "2018-07-04T12:38:36.0887843+02:00"
          },
          {
            PicId: "2bfb708dc74bc",
            PicName: "Kitzsteinhorn",
            PicPath: "$i_offer\\impressions\\2bfb708dc74bc.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2bfb708dc74bc.jpg",
            PicHTML_Link: "",
            PicKategorie: "Winter",
            LastPicUpload: "2018-07-04T12:38:36.119785+02:00"
          }
        ]
      },
      impression_pics: {
        gdatPicture: [
          {
            PicId: "2656700919913",
            PicName: "Dahoam im Römerhof",
            PicPath: "$i_offer\\theme_neues Thema\\2656700919913.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2656700919913.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:34.6927111+02:00"
          },
          {
            PicId: "26567db9f5383",
            PicName: "Dahoam im Römerhof",
            PicPath: "$i_offer\\theme_neues Thema\\26567db9f5383.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26567db9f5383.jpg",
            PicHTML_Link: "",
            PicKategorie: "Sommer",
            LastPicUpload: "2018-07-04T12:38:34.7487143+02:00"
          },
          {
            PicId: "2656838d63dc3",
            PicName: "im Nationalpark Hohe Tauern",
            PicPath: "$i_offer\\teaser_neuer ThemenTeaser\\2656838d63dc3.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2656838d63dc3.jpg",
            PicHTML_Link:
              "https://www.roemerhof-fusch.at/de/sommer-in-fusch-aktivitaeten",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:34.8207167+02:00"
          },
          {
            PicId: "2656875daa9e3",
            PicName: "Römerhof Inklusivleistungen",
            PicPath: "$i_offer\\teaser_Ihre\\2656875daa9e3.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2656875daa9e3.jpg",
            PicHTML_Link:
              "https://www.roemerhof-fusch.at/de/roemerhof_inklusivleistungen_sommer",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:34.8447191+02:00"
          },
          {
            PicId: "2bf395581b2d9",
            PicName: "Die schönsten Seiten Salzburgs entdecken",
            PicPath: "$i_offer\\teaser_neuer ThemenTeaser\\2bf395581b2d9.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2bf395581b2d9.jpg",
            PicHTML_Link:
              "https://www.roemerhof-fusch.at/de/ausflugsziele-sommer",
            PicKategorie: "Sommer",
            LastPicUpload: "2018-07-04T12:38:34.8957226+02:00"
          },
          {
            PicId: "26568cbafee23",
            PicName: "Dahoam im Römerhof",
            PicPath: "$i_offer\\theme_Winter 2017\\2018\\26568cbafee23.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26568cbafee23.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.0197286+02:00"
          },
          {
            PicId: "26568dbfc9943",
            PicName: "Dahoam im Römerhof",
            PicPath: "$i_offer\\theme_Winter 2017\\2018\\26568dbfc9943.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26568dbfc9943.jpg",
            PicHTML_Link: "",
            PicKategorie: "Winter",
            LastPicUpload: "2018-07-04T12:38:35.0407299+02:00"
          },
          {
            PicId: "26568f3bb1703",
            PicName: "Dahoam im Römerhof",
            PicPath: "$i_offer\\theme_Winter 2018\\2019\\26568f3bb1703.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26568f3bb1703.jpg",
            PicHTML_Link: "",
            PicKategorie: "Winter",
            LastPicUpload: "2018-07-04T12:38:35.0897324+02:00"
          },
          {
            PicId: "2656917474773",
            PicName: "Dahoam im Römerhof",
            PicPath: "$i_offer\\theme_Winter 2017\\2018\\2656917474773.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2656917474773.jpg",
            PicHTML_Link: "",
            PicKategorie: "Winter",
            LastPicUpload: "2018-07-04T12:38:35.119733+02:00"
          },
          {
            PicId: "265692c8d2413",
            PicName: "In der Skisportregion Zell am See-Kaprun",
            PicPath: "$i_offer\\teaser_neuer ThemenTeaser\\265692c8d2413.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/265692c8d2413.jpg",
            PicHTML_Link:
              "https://www.roemerhof-fusch.at/de/winter-in-fusch-aktivitaeten",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.1407351+02:00"
          },
          {
            PicId: "2bf3ac05e6fcf",
            PicName: "Mit dem richtigen Schwung durch den Winter",
            PicPath:
              "$i_offer\\teaser_Intersport Rent Scherer\\2bf3ac05e6fcf.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2bf3ac05e6fcf.jpg",
            PicHTML_Link: "https://www.roemerhof-fusch.at/de/skiverleih",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.2197382+02:00"
          },
          {
            PicId: "2bf3b632df392",
            PicName: "Mit Spaß zum Erfolg",
            PicPath: "$i_offer\\teaser_neuer ThemenTeaser\\2bf3b632df392.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2bf3b632df392.jpg",
            PicHTML_Link: "https://www.roemerhof-fusch.at/de/skischule",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.2407403+02:00"
          },
          {
            PicId: "2c07716c6d5fc",
            PicName: "Lass die Seele baumeln",
            PicPath: "$i_offer\\teaser_neuer ThemenTeaser\\2c07716c6d5fc.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2c07716c6d5fc.jpg",
            PicHTML_Link: "https://www.roemerhof-fusch.at/de/wellness",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.2897428+02:00"
          },
          {
            PicId: "2c07741a22992",
            PicName: "Vorfreude ist die schönste Freude",
            PicPath: "$i_offer\\teaser_neuer ThemenTeaser\\2c07741a22992.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2c07741a22992.jpg",
            PicHTML_Link: "https://www.roemerhof-fusch.at/de/gut-zu-wissen",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.3207435+02:00"
          },
          {
            PicId: "2656986fa6703",
            PicName: "Lassen Sie die Seele baumeln",
            PicPath: "$i_offer\\teaser_neuer Teaser\\2656986fa6703.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2656986fa6703.jpg",
            PicHTML_Link: "https://www.roemerhof-fusch.at/de/wellness",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.3517466+02:00"
          },
          {
            PicId: "26569b38cc5b3",
            PicName: "Vorfreude ist die schönste Freude",
            PicPath: "$i_offer\\teaser_neuer Teaser\\26569b38cc5b3.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26569b38cc5b3.jpg",
            PicHTML_Link: "https://www.roemerhof-fusch.at/de/gut-zu-wissen",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.3997482+02:00"
          },
          {
            PicId: "26569e73394c3",
            PicName: "Stauseen Kaprun",
            PicPath: "$i_offer\\impressions\\26569e73394c3.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26569e73394c3.jpg",
            PicHTML_Link: "",
            PicKategorie: "Sommer",
            LastPicUpload: "2018-07-04T12:38:35.4567523+02:00"
          },
          {
            PicId: "26569fc824b03",
            PicName: "Nationalpark Hohe Tauern",
            PicPath: "$i_offer\\impressions\\26569fc824b03.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26569fc824b03.jpg",
            PicHTML_Link: "",
            PicKategorie: "Sommer",
            LastPicUpload: "2018-07-04T12:38:35.5097536+02:00"
          },
          {
            PicId: "2656a1692f623",
            PicName: "Zell am See",
            PicPath: "$i_offer\\impressions\\2656a1692f623.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2656a1692f623.jpg",
            PicHTML_Link: "",
            PicKategorie: "Sommer",
            LastPicUpload: "2018-07-04T12:38:35.5297548+02:00"
          },
          {
            PicId: "2656a2b03cf83",
            PicName: "Kohlschnait Rodelbahn",
            PicPath: "$i_offer\\impressions\\2656a2b03cf83.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2656a2b03cf83.jpg",
            PicHTML_Link: "",
            PicKategorie: "Winter",
            LastPicUpload: "2018-07-04T12:38:35.5797582+02:00"
          },
          {
            PicId: "2658432e6b69c",
            PicName: "Badeteich",
            PicPath: "$i_offer\\impressions\\2658432e6b69c.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2658432e6b69c.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.6097588+02:00"
          },
          {
            PicId: "265844ea04535",
            PicName: "Badeteich",
            PicPath: "$i_offer\\impressions\\265844ea04535.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/265844ea04535.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.6357606+02:00"
          },
          {
            PicId: "2658464af9556",
            PicName: "Speisesaal",
            PicPath: "$i_offer\\impressions\\2658464af9556.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2658464af9556.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.6797634+02:00"
          },
          {
            PicId: "265847ad1fb84",
            PicName: "Skischule Fusch",
            PicPath: "$i_offer\\impressions\\265847ad1fb84.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/265847ad1fb84.jpg",
            PicHTML_Link: "",
            PicKategorie: "Winter",
            LastPicUpload: "2018-07-04T12:38:35.7197642+02:00"
          },
          {
            PicId: "265849c782898",
            PicName: "Hallenbad",
            PicPath: "$i_offer\\impressions\\265849c782898.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/265849c782898.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.737766+02:00"
          },
          {
            PicId: "26584af5a75d9",
            PicName: "Sauna",
            PicPath: "$i_offer\\impressions\\26584af5a75d9.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26584af5a75d9.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.7897688+02:00"
          },
          {
            PicId: "26584c4a9d150",
            PicName: "Fitnessraum",
            PicPath: "$i_offer\\impressions\\26584c4a9d150.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26584c4a9d150.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.8197694+02:00"
          },
          {
            PicId: "2658503a25cc4",
            PicName: "Kitzsteinhorn",
            PicPath: "$i_offer\\impressions\\2658503a25cc4.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2658503a25cc4.jpg",
            PicHTML_Link: "",
            PicKategorie: "Winter",
            LastPicUpload: "2018-07-04T12:38:35.8487723+02:00"
          },
          {
            PicId: "26585305af8b7",
            PicName: "Großglockner Hochalpenstraße",
            PicPath: "$i_offer\\impressions\\26585305af8b7.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26585305af8b7.jpg",
            PicHTML_Link: "",
            PicKategorie: "Sommer",
            LastPicUpload: "2018-07-04T12:38:35.8997742+02:00"
          },
          {
            PicId: "26585dae15721",
            PicName: "Kinderspielzimmer",
            PicPath: "$i_offer\\impressions\\26585dae15721.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26585dae15721.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.9207747+02:00"
          },
          {
            PicId: "26586013eab7c",
            PicName: "Spielraum",
            PicPath: "$i_offer\\impressions\\26586013eab7c.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/26586013eab7c.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.9497776+02:00"
          },
          {
            PicId: "2bfb62ddb979f",
            PicName: "Familie Scherer",
            PicPath: "$i_offer\\impressions\\2bfb62ddb979f.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2bfb62ddb979f.jpg",
            PicHTML_Link: "",
            PicKategorie: "Hotel Römerhof",
            LastPicUpload: "2018-07-04T12:38:35.9997794+02:00"
          },
          {
            PicId: "2bfb643e02286",
            PicName: "Tierpark Ferleiten",
            PicPath: "$i_offer\\impressions\\2bfb643e02286.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2bfb643e02286.jpg",
            PicHTML_Link: "",
            PicKategorie: "Sommer",
            LastPicUpload: "2018-07-04T12:38:36.0207799+02:00"
          },
          {
            PicId: "2bfb67f876fee",
            PicName: "Wanderlust",
            PicPath: "$i_offer\\impressions\\2bfb67f876fee.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2bfb67f876fee.jpg",
            PicHTML_Link: "",
            PicKategorie: "Sommer",
            LastPicUpload: "2018-07-04T12:38:36.0487827+02:00"
          },
          {
            PicId: "2bfb6eef58b40",
            PicName: "Schneefreuden",
            PicPath: "$i_offer\\impressions\\2bfb6eef58b40.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2bfb6eef58b40.jpg",
            PicHTML_Link: "",
            PicKategorie: "Winter",
            LastPicUpload: "2018-07-04T12:38:36.0887843+02:00"
          },
          {
            PicId: "2bfb708dc74bc",
            PicName: "Kitzsteinhorn",
            PicPath: "$i_offer\\impressions\\2bfb708dc74bc.jpg",
            PicUrl:
              "https://www.weratech-files.com/images/281623/gastrodat/2bfb708dc74bc.jpg",
            PicHTML_Link: "",
            PicKategorie: "Winter",
            LastPicUpload: "2018-07-04T12:38:36.119785+02:00"
          }
        ]
      },
      AngebotsName: "lalala",
      StammDaten: {
        HotelLogo_pic: {
          PicId: "1ecfce182b605",
          PicName: "HotelLogo",
          PicPath: "$i_offer\\LOGO\\StammDaten_HotelLogo_pic.jpg",
          PicUrl:
            "https://www.weratech-files.com/images/281623/gastrodat/StammDaten_HotelLogo_pic.jpg",
          PicHTML_Link: "",
          LastPicUpload: "2018-07-04T12:38:35.4227489+02:00"
        },
        Section_Liste: {
          Section: [
            {
              ModulName: "LOGO_BLOCK",
              ModulId: "",
              ModulHash: "LOGO_BLOCK",
              SortOrder: "0"
            },
            {
              ModulName: "SLIDESHOW",
              ModulId: "",
              ModulHash: "SLIDESHOW",
              SortOrder: "1"
            },
            {
              ModulName: "GREETING",
              ModulId: "",
              ModulHash: "GREETING",
              SortOrder: "2"
            },
            {
              ModulName: "WELCOME_TEXT",
              ModulId: "",
              ModulHash: "WELCOME_TEXT",
              SortOrder: "3"
            },
            {
              ModulName: "VARIANTEN_VORSCHAU",
              ModulId: "",
              ModulHash: "VARIANTEN_VORSCHAU",
              SortOrder: "4"
            },
            {
              ModulName: "ANGEBOT_DETAILS",
              ModulId: "",
              ModulHash: "ANGEBOT_DETAILS",
              SortOrder: "5"
            },
            {
              ModulName: "TEASER_BLOCK",
              ModulId: "",
              ModulHash: "TEASER_BLOCK",
              SortOrder: "6"
            },
            {
              ModulName: "ANGEBOTS_TEXT",
              ModulId: "",
              ModulHash: "ANGEBOTS_TEXT",
              SortOrder: "7"
            },
            {
              ModulName: "ANREISE_BESCHREIBUNG",
              ModulId: "",
              ModulHash: "ANREISE_BESCHREIBUNG",
              SortOrder: "8"
            },
            {
              ModulName: "NACHRICHTEN_BLOCK",
              ModulId: "",
              ModulHash: "NACHRICHTEN_BLOCK",
              SortOrder: "9"
            },
            {
              ModulName: "KACHELBILDER",
              ModulId: "",
              ModulHash: "KACHELBILDER",
              SortOrder: "10"
            },
            {
              ModulName: "SOCIAL_MEDIA_BAND",
              ModulId: "",
              ModulHash: "SOCIAL_MEDIA_BAND",
              SortOrder: "11"
            },
            {
              ModulName: "FOOTER",
              ModulId: "",
              ModulHash: "FOOTER",
              SortOrder: "12"
            },
            {
              ModulName: "IMPRESSUM",
              ModulId: "",
              ModulHash: "IMPRESSUM",
              SortOrder: "13"
            }
          ]
        },
        EinzelText_Liste: {
          EinzelText: [
            { Tag: "gdat_gesamt:", Text: "GESAMT (exkl. Kurtaxe)" },
            {
              Tag: "gdat_wir_freuen_uns_von_ihnen_zu_hoeren:",
              Text:
                "Wir würden uns sehr freuen wieder von Ihnen zu hören. Familie Scherer & das Römerhof Team"
            },
            {
              Tag: "gdat_chatten",
              Text: "haben Sie Fragen? Chatten Sie mit uns"
            },
            { Tag: "gdat_details", Text: "Details" },
            { Tag: "gdat_variante", Text: "Variante" },
            { Tag: "gdat_angebot", Text: "Ihr persönliches Angebot" },
            { Tag: "gdat_jetzt_buchen", Text: "Jetzt buchen" },
            { Tag: "gdat_personen", Text: "Personen" },
            { Tag: "gdat_datum", Text: "Datum" },
            { Tag: "gdat_naechte", Text: "Nächte" },
            { Tag: "gdat_preis", Text: "Preis" },
            { Tag: "gdat_gesamt", Text: "GESAMT" },
            { Tag: "gdat_impressionen", Text: "IMPRESSIONEN" },
            {
              Tag: "gdat_machen_sie_sich_ein_bild",
              Text: "MACHEN SIE SICH EIN BILD"
            },
            { Tag: "gdat_kontakt", Text: "KONTAKT" },
            {
              Tag: "gdat_wir_freuen_uns_von_ihnen_zu_hoeren",
              Text: "Wir freuen uns von Ihnen zu hören"
            },
            { Tag: "gdat_telefon", Text: "Telefon" },
            { Tag: "gdat_email", Text: "E-Mail" },
            { Tag: "gdat_adresse", Text: "Addresse" },
            { Tag: "gdat_karte_anzeigen", Text: "Karte anzeigen" },
            { Tag: "gdat_route_berechnen", Text: "Route berechnen" },
            { Tag: "gdat_name", Text: "Name" },
            { Tag: "gdat_nachricht", Text: "Nachricht" },
            {
              Tag: "gdat_input:name_hinweis",
              Text: "Bitte geben Sie Ihren Namen an"
            },
            {
              Tag: "gdat_input:email_hinweis",
              Text: "Bitte geben Sie Ihre E-Mail an"
            },
            {
              Tag: "gdat_textarea_nachricht_hinweis",
              Text: "Bitte geben Sie Ihren Namen an"
            },
            { Tag: "gdat_button_nachricht_senden", Text: "Nachricht senden" },
            {
              Tag: "gdat_page_loading_error",
              Text:
                "Hoppla! Die von Ihnen gesuchte Seite ist zur Zeit nicht verfügbar! Bitte laden Sie es neu, versuchen Sie es später oder melden Sie es. Wir werden es so schnell wie möglich beheben."
            },
            { Tag: "gdat_reload", Text: "Neu laden" },
            { Tag: "gdat_report", Text: "Melde dies" },
            { Tag: "gdat_newsletter", Text: "Newsletter" },
            {
              Tag: "gdat_input_newsletter_anmelden",
              Text: "Kostenlos anmelden"
            },
            { Tag: "gdat_zum_seitenanfang", Text: "Zum Seitenanfang" },
            { Tag: "gdat_please_choose", Text: "Bitte wählen" },
            { Tag: "gdat_highlights", Text: "Highlights" },
            { Tag: "gdat_angebot_ansehen", Text: "Angebot ansehen" },
            { Tag: "gat_anreise_abreise", Text: "Anreise/Abreise" },
            {
              Tag: "gdat_preis_pro_person_und_nacht",
              Text: "Preis pro Tag/Pers,"
            },
            { Tag: "gdat_jetzt_anfragen", Text: "jetzt buchen" },
            { Tag: "gdat_weitere_bilder", Text: "weitere Bilder" },
            { Tag: "gdat_fax", Text: "Fax" },
            { Tag: "gdat_uid", Text: "UID" },
            { Tag: "gdat_bank", Text: "Bank" },
            { Tag: "gdat_ktonr", Text: "KtoNr" },
            { Tag: "gdat_bic", Text: "BIC" },
            { Tag: "gdat_iban", Text: "IBAN" },
            { Tag: "gdat_webseite", Text: "Webseite" },
            { Tag: "gdat_vorname", Text: "Vorname" },
            { Tag: "gdat_nachname", Text: "Nachname" },
            { Tag: "gdat_ihr_vorname", Text: "Ihr Vorname" },
            { Tag: "gdat_ihr_nachname", Text: "Ihr Nachname" },
            { Tag: "gdat_ihre_email", Text: "Ihre E-Mail Adresse" },
            { Tag: "gdat_ihre_nachricht", Text: "Ihre Nachricht an uns..." },
            {
              Tag: "gdat_ergebnis_der_rechenaufgabe",
              Text: "Ergebnis der o.g. Rechenaufgabe"
            },
            { Tag: "gdat_anzahl", Text: "Anzahl" },
            { Tag: "gdat_impressum", Text: "Impressum" },
            { Tag: "gdat_datenschutz", Text: "Datenschutz" },
            { Tag: "gdat_teaser", Text: "Römerhof Highlights" },
            { Tag: "gdat_nr", Text: "Nr." },
            { Tag: "gdat_beschreibung", Text: "Beschreibung" },
            { Tag: "gdat_leistung", Text: "Leistung" },
            { Tag: "gdat_leistungen", Text: "Leistungen" }
          ]
        },
        MenuItem_Liste: "",
        MenuImpressumItem_Liste: "",
        HotelName: "Hotel Römerhof * * * Superior",
        WellcomeText:
          '<?xml version="1.0" ?>\r<html xmlns="https://www.w3.org/1999/xhtml">\r<head>\r<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r<title></title>\r</head>\r<body>\r<h3 style="line-height:130%;">Wir freuen uns sehr, dass Sie unser Hotel gewählt haben um Ihren Urlaub bei uns zu verbringen.<br />\rGerne können wir Ihnen untenstehend unser unverbindliches Angebot unterbreiten.</h3>\r<h3 style="line-height:130%;"> </h3>\r<h3 style="line-height:130%;">Mit feundlichen Grüßen aus Fusch,<br />\rFamilie Scherer &amp; das Römerhof Team</h3>\r<p style="line-height:130%;"> </p>\r<h5 lang="de-AT" style="line-height:130%;">Die Kurtaxe von Euro 1,10 pro Person und Nacht ab 15 Jahren ist nicht im Zimmerpreis inbegriffen und wird vor Ort separat verrechnet.   </h5>\r<h5 lang="de-AT" style="line-height:130%;">Unsere Angebote sind unverbindlich und richten sich nach dem tagesaktuellen Buchungsstand.</h5>\r</body>\r</html>',
        SubDomainUrl: "gastrodat.https://www.touristscout.com",
        AngebotsText:
          '<?xml version="1.0" ?>\r<html xmlns="https://www.w3.org/1999/xhtml">\r<head>\r<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r<title></title>\r</head>\r<body>\r<h3 style="line-height:130%;">Wir würden uns sehr freuen wieder von Ihnen zu hören!</h3>\r<h3 style="line-height:130%;">Mit freundlichen Grüßen aus Fusch,</h3>\r<h3 style="line-height:130%;"> </h3>\r<h3 style="line-height:130%;">Familie Scherer &amp; das Römerhof Team</h3>\r<p style="line-height:130%;"> </p>\r<h5 lang="de-AT" style="line-height:130%;">Hotel Römerhof * * * Superior</h5>\r<h5 lang="de-AT" style="line-height:130%;">Familie Scherer</h5>\r<h5 lang="de-AT" style="line-height:130%;">Zeller Fusch 77</h5>\r<h5 lang="en-US" style="line-height:130%;">5672 Fusch</h5>\r<h5 lang="en-US" style="line-height:130%;">Tel.: +43 (0) 6546 218 0</h5>\r<h5 lang="en-US" style="line-height:130%;">Fax: +43 (0) 6546 218 16</h5>\r<h5 lang="en-US" style="line-height:130%;"><a href="https://www.roemerhof-fusch.at/">www.roemerhof-fusch.at</a></h5>\r</body>\r</html>',
        AnreiseInfoText:
          '<?xml version="1.0" ?>\r<html xmlns="https://www.w3.org/1999/xhtml">\r<head>\r<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r<title></title>\r</head>\r<body>\r<h3 style="line-height:130%;">CHECK-IN:</h3>\r<h3 style="line-height:130%;">ab 15:00 Uhr</h3>\r</body>\r</html>',
        AbreiseInfoText:
          '<?xml version="1.0" ?>\r<html xmlns="https://www.w3.org/1999/xhtml">\r<head>\r<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r<title></title>\r</head>\r<body>\r<h3 style="line-height:130%;">CHECK-OUT:</h3>\r<h3 style="line-height:130%;">bis 10:00 Uhr</h3>\r</body>\r</html>',
        ZusatzText1:
          '<?xml version="1.0" ?>\r<html xmlns="https://www.w3.org/1999/xhtml">\r<head>\r<meta content="TX24_HTM 24.0.610.500" name="GENERATOR" />\r<title></title>\r</head>\r<body>\r<p style="line-height:130%;">fsdfsda</p>\r<p style="line-height:130%;">fsdfsd</p>\r<p style="line-height:130%;">fasdfsd</p>\r<p style="line-height:130%;">fsdf</p>\r<p style="line-height:130%;">asdfsd</p>\r<p style="line-height:130%;">fasd</p>\r</body>\r</html>',
        ZusatzText1_Header: "dfsdfsdfsdfsdfsd",
        ZusatzText1_Collapsed: "true",
        ZusatzText_Bild1: {
          PicId: "2bf395581b2d9",
          PicName: "Die schönsten Seiten Salzburgs entdecken",
          PicPath: "$i_offer\\teaser_neuer ThemenTeaser\\2bf395581b2d9.jpg",
          PicUrl:
            "https://www.weratech-files.com/images/281623/gastrodat/2bf395581b2d9.jpg",
          PicHTML_Link: "https://www.roemerhof-fusch.at/de/ausflugsziele-sommer",
          PicKategorie: "Sommer",
          LastPicUpload: "2018-07-04T12:38:34.8957226+02:00"
        },
        ZusatzText2: "",
        ZusatzText2_Header: "",
        ZusatzText2_Collapsed: "false",
        ZusatzText_Bild2: { LastPicUpload: "0001-01-01T00:00:00" },
        ZusatzText3: "",
        ZusatzText3_Header: "",
        ZusatzText3_Collapsed: "false",
        ZusatzText_Bild3: { LastPicUpload: "0001-01-01T00:00:00" },
        ZusatzText4: "",
        ZusatzText4_Header: "",
        ZusatzText4_Collapsed: "false",
        ZusatzText_Bild4: { LastPicUpload: "0001-01-01T00:00:00" },
        ZusatzText5: "",
        ZusatzText5_Header: "",
        ZusatzText5_Collapsed: "false",
        ZusatzText_Bild5: { LastPicUpload: "0001-01-01T00:00:00" },
        ZwischenBild1: { LastPicUpload: "0001-01-01T00:00:00" },
        ZwischenBild2: { LastPicUpload: "0001-01-01T00:00:00" },
        ZwischenBild3: { LastPicUpload: "0001-01-01T00:00:00" },
        ZwischenBild4: { LastPicUpload: "0001-01-01T00:00:00" },
        ZwischenBild5: { LastPicUpload: "0001-01-01T00:00:00" },
        FaceBookPageId: "GDAT_Facebook_Page_ID",
        SozMedia_FaceBook_Url: "www.facebook.com/Roemerhof.Fusch",
        SozMedia_GooglePlus_Url: "plus.google.com/+Roemerhof-fuschAt",
        SozMedia_Pinterest_Url: "",
        SozMedia_Instagram_Url: "www.instagram.com/roemerhof.fusch/",
        SozMedia_YouTube_Url: "",
        SozMedia_Twitter_Url: "",
        ImageCompressionQuality: "50"
      },
      DesignSettings: {
        TextModulStyle_Liste: {
          TextModulStyle: [
            {
              StyleName: "H1",
              InlineStyle: "false",
              Bold: "true",
              FontName: "Arial",
              FontSize: "480",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "None",
              Strikeout: "false",
              TextBackColor: "#FFFFFF",
              Underline: "false",
              UseStyle: "false"
            },
            {
              StyleName: "H2",
              InlineStyle: "false",
              Bold: "true",
              FontName: "Arial",
              FontSize: "360",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "None",
              Strikeout: "false",
              TextBackColor: "#FFFFFF",
              Underline: "false",
              UseStyle: "false"
            },
            {
              StyleName: "H3",
              InlineStyle: "false",
              Bold: "true",
              FontName: "Arial",
              FontSize: "259",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "None",
              Strikeout: "false",
              TextBackColor: "transparent",
              Underline: "false",
              UseStyle: "true"
            },
            {
              StyleName: "H4",
              InlineStyle: "false",
              Bold: "true",
              FontName: "Arial",
              FontSize: "239",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "None",
              Strikeout: "false",
              TextBackColor: "transparent",
              Underline: "false",
              UseStyle: "true"
            },
            {
              StyleName: "H5",
              InlineStyle: "false",
              Bold: "true",
              FontName: "Arial",
              FontSize: "179",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "None",
              Strikeout: "false",
              TextBackColor: "transparent",
              Underline: "false",
              UseStyle: "true"
            },
            {
              StyleName: "P",
              InlineStyle: "false",
              Bold: "false",
              FontName: "Arial",
              FontSize: "180",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "None",
              Strikeout: "false",
              TextBackColor: "#FFFFFF",
              Underline: "false",
              UseStyle: "false"
            },
            {
              StyleName: "UL",
              InlineStyle: "false",
              Bold: "false",
              FontName: "Arial",
              FontSize: "180",
              ForeColor: "#000000",
              Italic: "false",
              ListFormat: "Bulleted",
              Strikeout: "false",
              TextBackColor: "#FFFFFF",
              Underline: "false",
              UseStyle: "false"
            }
          ]
        },
        TemplateName: "",
        primaryColor: "#BD1B1C",
        secundaryColor: "#F2E6D6",
        bckg_Color3: "#F2E6D6",
        bckg_Color4: "#b0a8a3",
        bckg_Color5: "#fff",
        prim_textColor: "#fff",
        sec_textColor: "#fff",
        textColor_Color3: "#fff",
        textColor_Color4: "#fff",
        textColor_Color5: "#0080FF",
        backgroundColor: "#fff",
        headlineFontName: "'Yanone Kaffeesatz', sans-serif",
        bodyFontName: "'Open Sans', 'Helvetica Neue', Helvetica, sans-serif",
        textColor: "#000000"
      }
    }
  }
}
