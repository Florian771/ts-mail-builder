export interface HeadlineWithSublineSettings {
    key: string;
}
export interface HeadlineWithSubline {
    headline: string;
    subline: string;
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: HeadlineWithSublineSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
