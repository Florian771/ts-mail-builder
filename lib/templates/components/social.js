"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
exports.predefinedPlatforms = [
    "facebook",
    "youtube",
    "instagram",
    "twitter",
    "pinterest",
    "google"
];
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    // console.log("social.settings:", settings)
    var html = "";
    var text = "";
    text = "";
    html = "\n  <mj-section>\n  <mj-column>\n  <mj-social font-size=\"15px\" border-radius=\"15px\" icon-size=\"30px\" mode=\"horizontal\">TEST";
    if (Array.isArray(settings)) {
        if (settings && settings.length > 0) {
            settings.map(function (socialMedia) {
                html += "<mj-social-element background-color=\"{textColor}\" name=\"" + socialMedia.name + "-noshare\" href=\"" + (socialMedia.url
                    ? socialMedia.url
                    : "{" + socialMedia.name.toUpperCase() + "_URL}") + "\"></mj-social-element> ";
            });
        }
    }
    else {
        exports.predefinedPlatforms.map(function (platform) {
            if (settings && settings[platform]) {
                html += "<mj-social-element background-color=\"{textColor}\" name=\"" + platform + "-noshare\" href=\"" + (options[platform].url
                    ? options[platform].url
                    : "{" + platform.toUpperCase() + "_URL}") + "\"></mj-social-element> ";
            }
        });
    }
    html += "</mj-social>\n  </mj-column>\n</mj-section>";
    testParse_1.default({ test: true, html: html, options: options, modulName: "Social" });
    return {
        html: html,
        text: text,
        translations: [],
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=social.js.map