export interface TableBillSettings {
    key: string;
}
export interface TableBill {
    headline: string;
    header1: string;
    header2: string;
    header3: string;
    header4: string;
    header5: string;
    content1: string;
    content2: string;
    content3: string;
    content4: string;
    content5: string;
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: TableBillSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
