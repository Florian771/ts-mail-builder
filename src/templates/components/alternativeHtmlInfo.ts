import testParse from "../../testParse"
import { textBreak } from "../../config"
import { getTranslations } from "../../utils"

export interface AlternativeHtmlInfosSettings {
  key: string
  text: boolean
  url: boolean
}

export interface AlternativeHtmlInfos {
  text: string
  url: string
}

const placeholder: AlternativeHtmlInfos = {
  text: "{HTML_LINK_TEXT}",
  url: "{HTML_LINK_URL}"
}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: AlternativeHtmlInfosSettings
}): MailBuilder.Component => {
  const translations: string[] = getTranslations({
    placeholder: placeholder,
    settings: settings
  })
  let html: string = ""
  let text: string = ""
  text = `${textBreak}${placeholder.text}: ${placeholder.url}${textBreak}`
  html = `
  <mj-section padding="10px 0 20px 0">
  <mj-column>
    <mj-text align="center" color="{textColor}" font-size="11px">`

  if (settings.url) {
    html += `<a style="color:{textColor};text-decoration:none;" href="${
      placeholder.url
    }" style="color:{textColor};text-decoration:none;">
        ${placeholder.text}
        </a>`
  } else if (settings.text) {
    html += `${placeholder.text}`
  }

  html += `</mj-text>
  </mj-column>
</mj-section>`

  testParse({
    test: true,
    html: html,
    options: options,
    modulName: "alternativeHtmlLink"
  })
  return {
    html: html,
    text: text,
    translations: translations,
    parse: (args: any) => testParse(args)
  }
}
