export default `<API><HotelData><Ort>Kaprun</Ort><HotelName>AlpenParks Hotel &amp; Apartment Orgler</HotelName><PersName></PersName><Gruss></Gruss><Strasse>Schloßstr. 1</Strasse><Nation>A</Nation><PLZ>5710</PLZ><Telefon>+43 6547 70700</Telefon><Fax></Fax><UID></UID><KtoNr></KtoNr><Email>orgler@alpenparks.at</Email><Bank></Bank><BLZ></BLZ><BIC></BIC><IBAN></IBAN></HotelData><ReservationData><LFNUMMER>14778</LFNUMMER><ZIMMER_NR>75</ZIMMER_NR><PERS_NR>3801</PERS_NR><PERSON>Schiefegger Helga</PERSON><VON>14.04.2018</VON><BIS>19.04.2018</BIS><ANGEBOTSURL><![CDATA[https://apps.weratech-online.com/wtOnlineBooking/Home/CreateOnlineReservation/NzA5OA2?reservationid=52B057C05CD05CD05E80&guestid=56105E80510052B0&lang=DE]]></ANGEBOTSURL></ReservationData><GuestData><LFNUMMER>3801</LFNUMMER><ANREDE_H>Frau</ANREDE_H><FAMNAME>Schiefegger</FAMNAME><VORNAME>Helga</VORNAME><TITEL></TITEL><GESCHLECHT></GESCHLECHT><STRASSE></STRASSE><PLZ></PLZ><ORT></ORT><ANREDE_FAM>Sehr geehrte Frau Schiefegger</ANREDE_FAM><SPRACHE>D Deutsch</SPRACHE><NATION></NATION><ZUSNAME></ZUSNAME><P_EMAIL>direktion.kaprun@alpenparks.at</P_EMAIL><NATION_TXT></NATION_TXT></GuestData><RoomData><ZIMMER_NR>75</ZIMMER_NR><ZIMMER_NAME>Storno</ZIMMER_NAME><GEEIGNET_VON>0</GEEIGNET_VON><GEEIGNET_BIS>0</GEEIGNET_BIS><ZI_KAT>0</ZI_KAT><ZIMMER_SAUBER>N</ZIMMER_SAUBER><GEEIGNET_STD>0</GEEIGNET_STD><STANDARDLEISTUNG></STANDARDLEISTUNG><TEXT1>Studio Alpine, 53,99 m² Wohnraum, 4,86 m² Balkon, 1 Bad, Sauna, Blick Richtung Maiskogel + Kitzsteinhorn, am Wasser, standardbelegung 2 Personen</TEXT1><TEXT2>Standardbelegung 2 Personen, 1 Schlafzimmer (Doppelbett, Kleiderschrank), Vorraum mit Garderobe, 1 Bad, offener Wohn-Küche-Essbereich, hochwertige grosszügige Einbauküche (im Wohnraum integriert), LCD</TEXT2><ZIMMERBILD>/Content/images/invalidimage.png</ZIMMERBILD><ZIMMERGRUNDRISS>/Content/images/invalidimage.png</ZIMMERGRUNDRISS></RoomData><ExtrasData><Posio><FKT_NR>55964</FKT_NR><P_VON>14.04.2018</P_VON><P_BIS>16.04.2018</P_BIS><P_TEXT>Logis Alpine Classic</P_TEXT><P_ANZ>1</P_ANZ><P_SUM>360</P_SUM><P_PREIS>180</P_PREIS><P_RABATT>0</P_RABATT><P_KG_NR>10088</P_KG_NR><GROUPINDEX>0</GROUPINDEX><DAUER_VON>1</DAUER_VON><DAUER_BIS>999</DAUER_BIS><BESCHREIBUNG>Appartement Alpine Classic 
für 4 bis 6 Personen
Ausstattung (70m²) 

* Wohlfühlen &amp; Wohnen auf **** Niveau 
* Erdgeschoss oder 1. Stock
* Standardbelegung 4 Personen
   + 2 Zusatzbetten möglich (komfortable 
   ausziehbare Schlafcouch im Wohnbereich)
* 2 Schlafzimmer (Doppelbett, Kleiderschrank)
* Vorraum mit Garderobe, 2 Bäder, offener 
   Wohn- Küche- Essbereich, hochwertige großzügige Einbauküche 
* LCD Fernseher, Musikanlage
* Kostenloses W-LAN, Balkon oder Terrasse 
* Bettwäsche und Handtücher inklusive</BESCHREIBUNG><EXTRABILD>https://www.weratech-files.com/images/281941/gastrodat/Extras/EXTRA_10088.jpg?timestamp=636512699510000000</EXTRABILD></Posio><Posio><FKT_NR>55966</FKT_NR><P_VON>14.04.2018</P_VON><P_BIS>15.04.2018</P_BIS><P_TEXT>Endreinigung Alpine Classic</P_TEXT><P_ANZ>1</P_ANZ><P_SUM>100</P_SUM><P_PREIS>100</P_PREIS><P_RABATT>0</P_RABATT><P_KG_NR>10089</P_KG_NR><GROUPINDEX>0</GROUPINDEX><DAUER_VON>1</DAUER_VON><DAUER_BIS>1</DAUER_BIS><BESCHREIBUNG></BESCHREIBUNG><EXTRABILD>/Content/images/invalidimage.png</EXTRABILD></Posio><Posio><FKT_NR>55967</FKT_NR><P_VON>14.04.2018</P_VON><P_BIS>16.04.2018</P_BIS><P_TEXT>Ortstaxe Orgler Kaprun</P_TEXT><P_ANZ>4</P_ANZ><P_SUM>9.2</P_SUM><P_PREIS>1.15</P_PREIS><P_RABATT>0</P_RABATT><P_KG_NR>10086</P_KG_NR><GROUPINDEX>0</GROUPINDEX><DAUER_VON>1</DAUER_VON><DAUER_BIS>999</DAUER_BIS><BESCHREIBUNG></BESCHREIBUNG><EXTRABILD>/Content/images/invalidimage.png</EXTRABILD></Posio><Posio><FKT_NR>55969</FKT_NR><P_VON>14.04.2018</P_VON><P_BIS>16.04.2018</P_BIS><P_TEXT>Vermietung Parkplatz</P_TEXT><P_ANZ>1</P_ANZ><P_SUM>16</P_SUM><P_PREIS>8</P_PREIS><P_RABATT>0</P_RABATT><P_KG_NR>10047</P_KG_NR><GROUPINDEX>0</GROUPINDEX><DAUER_VON>1</DAUER_VON><DAUER_BIS>999</DAUER_BIS><BESCHREIBUNG></BESCHREIBUNG><EXTRABILD>/Content/images/invalidimage.png</EXTRABILD></Posio><Posio><FKT_NR>55970</FKT_NR><P_VON>08.03.2018</P_VON><P_BIS>09.03.2018</P_BIS><P_TEXT>ZSUM</P_TEXT><P_ANZ>0</P_ANZ><P_SUM>0</P_SUM><P_PREIS>0</P_PREIS><P_RABATT>0</P_RABATT><P_KG_NR>0</P_KG_NR><GROUPINDEX>0</GROUPINDEX><DAUER_VON></DAUER_VON><DAUER_BIS></DAUER_BIS><BESCHREIBUNG></BESCHREIBUNG><EXTRABILD>/Content/images/invalidimage.png</EXTRABILD></Posio></ExtrasData><ThemeData><Themes><gdatTheme><theme_pics><gdatPicture><PicId>236196d3826d6</PicId><PicName /><PicPath>$i_offer____theme_Winter____236196d3826d6.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/281941/gastrodat/236196d3826d6.jpg</PicUrl><PicHTML_Link /><PicKategorie>Winter</PicKategorie></gdatPicture><gdatPicture><PicId>2361a0fb1b94e</PicId><PicName /><PicPath>$i_offer____theme_Winter____2361a0fb1b94e.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/281941/gastrodat/2361a0fb1b94e.jpg</PicUrl><PicHTML_Link /><PicKategorie>Wellness</PicKategorie></gdatPicture><gdatPicture><PicId>2363eb7f7fb39</PicId><PicName /><PicPath>$i_offer____theme_Winter____2363eb7f7fb39.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/281941/gastrodat/2363eb7f7fb39.jpg</PicUrl><PicHTML_Link /><PicKategorie /></gdatPicture><gdatPicture><PicId>2363eed2cd206</PicId><PicName /><PicPath>$i_offer____theme_Winter____2363eed2cd206.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/281941/gastrodat/2363eed2cd206.jpg</PicUrl><PicHTML_Link /><PicKategorie /></gdatPicture></theme_pics><theme_teasers><gdatTeaser><Teaser_pic><PicId>2361a4d508696</PicId><PicName>Perfekte Pisten und 100%-ige Schneegarantie erwarten Sie am Kitzsteinhorn!</PicName><PicPath>$i_offer____teaser_Urlaubsregion Kaprun____2361a4d508696.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/281941/gastrodat/2361a4d508696.jpg</PicUrl><PicHTML_Link>https://hotel-apartment-orgler.alpenparks.at/de/winterurlaub/</PicHTML_Link><PicKategorie /></Teaser_pic><validFrom>2017-10-26T09:54:14</validFrom><validTo>2018-04-30T09:54:14</validTo><SuchwortListe /><TeaserId>2361a4d508696</TeaserId><TeaserName>Urlaubsregion Kaprun</TeaserName></gdatTeaser><gdatTeaser><Teaser_pic><PicId>2361c0c8b216d</PicId><PicName>Gemütlichkeit und moderne alpine Küche erwarten Sie im Orgler's Restaurant...</PicName><PicPath>$i_offer____teaser_Genusswelt____2361c0c8b216d.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/281941/gastrodat/2361c0c8b216d.jpg</PicUrl><PicHTML_Link /><PicKategorie /></Teaser_pic><validFrom>2017-10-26T10:06:45</validFrom><validTo>2023-04-01T10:06:45</validTo><SuchwortListe /><TeaserId>2361c0c8b216d</TeaserId><TeaserName>AlpenParks Genusswelten</TeaserName></gdatTeaser><gdatTeaser><Teaser_pic><PicId>23633928bc14a</PicId><PicName>Hier finden Sie wissenswerte Informationen, damit Sie gut vorbereitet in Ihren Urlaub starten...</PicName><PicPath>$i_offer____teaser_neuer Teaser____23633928bc14a.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/281941/gastrodat/23633928bc14a.jpg</PicUrl><PicHTML_Link>https://hotel-apartment-orgler.alpenparks.at/de/hotel-apartment/wissenswertes/</PicHTML_Link><PicKategorie /></Teaser_pic><validFrom>2017-12-21T12:55:08.3285834+01:00</validFrom><validTo>2018-04-30T12:55:08</validTo><SuchwortListe /><TeaserId>23633928bc14a</TeaserId><TeaserName>Gut zu Wissen</TeaserName></gdatTeaser></theme_teasers><validFrom>2017-12-21T09:47:19.4764741+01:00</validFrom><validTo>2018-04-30T09:47:19</validTo><SuchwortListe /><ThemeId>2361955c961c5</ThemeId><ThemeName>Winter</ThemeName></gdatTheme></Themes><BilderGalerie_pics><gdatPicture><PicId>24509a514f33f</PicId><PicName /><PicPath>$i_offer____impressions____24509a514f33f.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/281941/gastrodat/24509a514f33f.jpg</PicUrl><PicHTML_Link /><PicKategorie>Team</PicKategorie></gdatPicture><gdatPicture><PicId>24509fb2ebcf8</PicId><PicName>Penthouse Alpine Luxury</PicName><PicPath>$i_offer____impressions____24509fb2ebcf8.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/281941/gastrodat/24509fb2ebcf8.jpg</PicUrl><PicHTML_Link /><PicKategorie>Zimmer</PicKategorie></gdatPicture><gdatPicture><PicId>271f5dc56a465</PicId><PicName>Doppelzimmer</PicName><PicPath>$i_offer____impressions____271f5dc56a465.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/281941/gastrodat/271f5dc56a465.jpg</PicUrl><PicHTML_Link /><PicKategorie>Zimmer</PicKategorie></gdatPicture><gdatPicture><PicId>271f5f7965fc9</PicId><PicName>Frühstücksbuffet</PicName><PicPath>$i_offer____impressions____271f5f7965fc9.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/281941/gastrodat/271f5f7965fc9.jpg</PicUrl><PicHTML_Link /><PicKategorie>Genuss</PicKategorie></gdatPicture></BilderGalerie_pics><impression_pics><gdatPicture><PicId>236196d3826d6</PicId><PicName /><PicPath>$i_offer____theme_Winter____236196d3826d6.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/281941/gastrodat/236196d3826d6.jpg</PicUrl><PicHTML_Link /><PicKategorie>Winter</PicKategorie></gdatPicture><gdatPicture><PicId>2361a0fb1b94e</PicId><PicName /><PicPath>$i_offer____theme_Winter____2361a0fb1b94e.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/281941/gastrodat/2361a0fb1b94e.jpg</PicUrl><PicHTML_Link /><PicKategorie>Wellness</PicKategorie></gdatPicture><gdatPicture><PicId>24509a514f33f</PicId><PicName /><PicPath>$i_offer____impressions____24509a514f33f.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/281941/gastrodat/24509a514f33f.jpg</PicUrl><PicHTML_Link /><PicKategorie>Team</PicKategorie></gdatPicture><gdatPicture><PicId>24509fb2ebcf8</PicId><PicName>Penthouse Alpine Luxury</PicName><PicPath>$i_offer____impressions____24509fb2ebcf8.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/281941/gastrodat/24509fb2ebcf8.jpg</PicUrl><PicHTML_Link /><PicKategorie>Zimmer</PicKategorie></gdatPicture><gdatPicture><PicId>271f5dc56a465</PicId><PicName>Doppelzimmer</PicName><PicPath>$i_offer____impressions____271f5dc56a465.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/281941/gastrodat/271f5dc56a465.jpg</PicUrl><PicHTML_Link /><PicKategorie>Zimmer</PicKategorie></gdatPicture><gdatPicture><PicId>271f5f7965fc9</PicId><PicName>Frühstücksbuffet</PicName><PicPath>$i_offer____impressions____271f5f7965fc9.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/281941/gastrodat/271f5f7965fc9.jpg</PicUrl><PicHTML_Link /><PicKategorie>Genuss</PicKategorie></gdatPicture></impression_pics><AngebotsName>lalala</AngebotsName><StammDaten><HotelLogo_pic><PicId>1ecfce182b605</PicId><PicName>HotelLogo</PicName><PicPath>$i_offer____LOGO____StammDaten_HotelLogo_pic.jpg</PicPath><PicUrl>https://www.weratech-files.com/images/281941/gastrodat/StammDaten_HotelLogo_pic.jpg</PicUrl><PicHTML_Link /></HotelLogo_pic><EinzelText_Liste><EinzelText><Tag>gdat_chatten</Tag><Text>Haben Sie Fragen? Wir sind gerne für Sie da +43 6547 70700</Text></EinzelText></EinzelText_Liste><HotelName>Alpen Parks Hotel &amp; Apartment Orgler</HotelName><WellcomeText>&lt;?xml version="1.0" ?&gt;
&lt;html xmlns="https://www.w3.org/1999/xhtml"&gt;
&lt;head&gt;
&lt;meta content="TX24_HTM 24.0.610.500" name="GENERATOR" /&gt;
&lt;title&gt;&lt;/title&gt;
&lt;style type="text/css"&gt;
/* &lt;![CDATA[ */
BODY {
	widows: 2;
	orphans: 2;
	font-family: 'Calibri';
	font-size: 12pt;
	font-weight: normal;
	font-style: normal;
}
H2 {
	widows: 2;
	orphans: 2;
	margin: 0pt 0pt 0pt 0pt;
	text-indent: 0pt;
	line-height: 150%;
	font-family: 'Calibri';
	font-size: 11pt;
	font-weight: normal;
	font-style: normal;
}
H1 {
	widows: 2;
	orphans: 2;
	margin: 8pt 0pt 16pt 0pt;
	text-indent: 0pt;
	font-family: 'Calibri';
	font-size: 24pt;
	font-weight: bold;
	font-style: normal;
}
H3 {
	widows: 2;
	orphans: 2;
	margin: 4.65pt 0pt 9.3pt 0pt;
	text-indent: 0pt;
	font-family: 'Calibri';
	font-size: 14pt;
	font-weight: bold;
	font-style: normal;
}
H4 {
	widows: 2;
	orphans: 2;
	margin: 3.65pt 0pt 7.3pt 0pt;
	text-indent: 0pt;
	font-family: 'Calibri';
	font-size: 11pt;
	font-weight: bold;
	font-style: normal;
}
H5 {
	widows: 2;
	orphans: 2;
	margin: 3pt 0pt 6pt 0pt;
	text-indent: 0pt;
	font-family: 'Calibri';
	font-size: 9pt;
	font-weight: bold;
	font-style: normal;
}
H6 {
	widows: 2;
	orphans: 2;
	margin: 2.3pt 0pt 4.65pt 0pt;
	text-indent: 0pt;
	font-family: 'Calibri';
	font-size: 7pt;
	font-weight: bold;
	font-style: normal;
}
BLOCKQUOTE {
	widows: 2;
	orphans: 2;
	margin: 6pt 30pt 6pt 30pt;
	text-indent: 0pt;
	font-family: 'Calibri';
	font-size: 12pt;
	font-weight: normal;
	font-style: normal;
}
p {
	widows: 2;
	orphans: 2;
	margin: 0pt 0pt 0pt 0pt;
	text-indent: 0pt;
	font-family: 'Calibri';
	font-size: 12pt;
	font-weight: normal;
	font-style: normal;
}
.body-text {
	widows: 2;
	orphans: 2;
	margin: 0pt 0pt 0pt 0pt;
	text-indent: 0pt;
	font-family: 'Calibri';
	font-size: 12pt;
	font-weight: normal;
	font-style: normal;
}
.Standard {
	widows: 2;
	orphans: 2;
	margin: 0pt 0pt 0pt 0pt;
	text-indent: 0pt;
	font-family: 'Verdana';
	font-size: 11pt;
	font-weight: normal;
	font-style: normal;
}
A {
}
PRE {
	widows: 2;
	orphans: 2;
	margin: 0pt 0pt 0pt 0pt;
	text-indent: 0pt;
	font-family: 'Courier New';
	font-size: 12pt;
	font-weight: normal;
	font-style: normal;
}
B {
	font-family: 'Arial';
	font-weight: bold;
}
CAPTION {
	widows: 2;
	orphans: 2;
	margin: 0pt 0pt 0pt 0pt;
	text-indent: 0pt;
	font-family: 'Arial';
	font-size: 12pt;
	font-weight: normal;
	font-style: normal;
}
CODE {
	font-family: 'Courier New';
}
DEL {
}
EM {
	font-family: 'Arial';
	font-style: italic;
}
I {
	font-family: 'Arial';
	font-style: italic;
}
INS {
}
KBD {
	font-family: 'Courier New';
}
MARK {
	widows: 2;
	orphans: 2;
	margin: 0pt 0pt 0pt 0pt;
	text-indent: 0pt;
	font-family: 'Arial';
	font-size: 12pt;
	font-weight: normal;
	font-style: normal;
	background-color: #FFFF00;
}
S {
}
SAMP {
	font-family: 'Courier New';
}
STRIKE {
}
STRONG {
	font-family: 'Arial';
	font-weight: bold;
}
TH {
	widows: 2;
	orphans: 2;
	margin: 0pt 0pt 0pt 0pt;
	text-indent: 0pt;
	font-family: 'Arial';
	font-size: 12pt;
	font-weight: bold;
	font-style: normal;
}
TT {
	font-family: 'Courier New';
}
U {
}
/* ]]&gt; */
&lt;/style&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;h2 style="text-align:center;"&gt;&lt;br /&gt;
&lt;br /&gt;
&lt;br /&gt;
herzlichen Dank für Ihre Urlaubsanfrage !&lt;/h2&gt;
&lt;h2 style="text-align:center;"&gt;Wir freuen uns, dass Sie Ihre Ferien im neuen AlpenParks Hotel &amp;amp; Apartment Orgler Kaprun verbringen möchten.&lt;/h2&gt;
&lt;/body&gt;
&lt;/html&gt;</WellcomeText><SubDomainUrl>.https://www.touristscout.com</SubDomainUrl><AngebotsText>&lt;?xml version="1.0" ?&gt;
&lt;html xmlns="https://www.w3.org/1999/xhtml"&gt;
&lt;head&gt;
&lt;meta content="TX24_HTM 24.0.610.500" name="GENERATOR" /&gt;
&lt;title&gt;&lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;h2 style="text-align:center;text-indent:0pt;margin-left:0pt;margin-right:0pt;line-height:100%;"&gt;&lt;span style="font-size:11pt;font-weight:normal;"&gt;Unser bestes Urlaubsangebot für Sie:&lt;/span&gt;&lt;/h2&gt;
&lt;h2 style="text-align:center;text-indent:0pt;margin-left:0pt;margin-right:0pt;line-height:100%;"&gt;&lt;span style="font-size:11pt;font-weight:normal;"&gt; &lt;/span&gt;&lt;/h2&gt;
&lt;h2 style="text-align:center;text-indent:0pt;margin-left:0pt;margin-right:0pt;line-height:100%;"&gt;&lt;span style="font-size:11pt;font-weight:normal;"&gt;Bezahlung: Nach Erhalt Ihrer Buchungsbestätigung ist eine Anzahlung in Höhe&lt;/span&gt;&lt;/h2&gt;
&lt;h2 style="text-align:center;text-indent:0pt;margin-left:0pt;margin-right:0pt;line-height:100%;"&gt;&lt;span style="font-size:11pt;font-weight:normal;"&gt;von 30% des Reisepreises innerhalb der angeführten Frist fällig. Zahlbar&lt;/span&gt;&lt;/h2&gt;
&lt;h2 style="text-align:center;text-indent:0pt;margin-left:0pt;margin-right:0pt;line-height:100%;"&gt;&lt;span style="font-size:11pt;font-weight:normal;"&gt;per Überweisung oder Kreditkarte. Die Restzahlung ist bei Anreise fällig.&lt;/span&gt;&lt;/h2&gt;
&lt;h2 style="text-align:center;text-indent:0pt;margin-left:0pt;margin-right:0pt;line-height:100%;"&gt;&lt;span style="font-size:11pt;font-weight:normal;"&gt; &lt;/span&gt;&lt;/h2&gt;
&lt;/body&gt;
&lt;/html&gt;</AngebotsText><AnreiseInfoText>&lt;?xml version="1.0" ?&gt;
&lt;html xmlns="https://www.w3.org/1999/xhtml"&gt;
&lt;head&gt;
&lt;meta content="TX24_HTM 24.0.610.500" name="GENERATOR" /&gt;
&lt;title&gt;&lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;Check-in&lt;br /&gt;
15:00 Uhr - 20:00 Uhr&lt;/p&gt;
&lt;/body&gt;
&lt;/html&gt;</AnreiseInfoText><AbreiseInfoText>&lt;?xml version="1.0" ?&gt;
&lt;html xmlns="https://www.w3.org/1999/xhtml"&gt;
&lt;head&gt;
&lt;meta content="TX24_HTM 24.0.610.500" name="GENERATOR" /&gt;
&lt;title&gt;&lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;p class="body-text" style="text-indent:0pt;margin-left:0pt;margin-top:0pt;margin-bottom:0pt;margin-right:0pt;line-height:100%;"&gt;Check-out&lt;br /&gt;
08:00 Uhr - 10:00 Uhr&lt;/p&gt;
&lt;/body&gt;
&lt;/html&gt;</AbreiseInfoText><FaceBookPageId>GDAT_Facebook_Page_ID</FaceBookPageId><SozMedia_FaceBook_Url>www.facebook.com/alpenparksorglerkaprun/</SozMedia_FaceBook_Url><SozMedia_GooglePlus_Url>www.googleplus.....</SozMedia_GooglePlus_Url><SozMedia_Pinterest_Url>www.pinterest.....</SozMedia_Pinterest_Url><SozMedia_Instagram_Url>www.instagramm.....</SozMedia_Instagram_Url><SozMedia_YouTube_Url>www.youtube......</SozMedia_YouTube_Url><SozMedia_Twitter_Url>www.twitter....</SozMedia_Twitter_Url><ImageCompressionQuality>50</ImageCompressionQuality></StammDaten><DesignSettings><TemplateName /><primaryColor>#FF80C0</primaryColor><secundaryColor>#0080FF</secundaryColor><textColor>#00FFFF</textColor><backgroundColor>#F7FEC5</backgroundColor><headlineFontName>Verdana</headlineFontName><bodyFontName>Verdana</bodyFontName></DesignSettings></ThemeData></API>`
