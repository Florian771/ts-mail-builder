export interface HeadlineSettings {
    headline: boolean;
    key: string;
}
export interface Headline {
    headline: string;
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: HeadlineSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
