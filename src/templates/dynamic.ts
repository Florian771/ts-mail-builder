import head from "./components/head"
import header from "./components/header"
import headerDesigner from "./components/headerDesigner"
import headOnly from "./components/headOnly"
import hero from "./components/hero"
import divider from "./components/divider"
import accordeon from "./components/accordeon"
import image from "./components/image"
import imageText from "./components/imageText"
import headline from "./components/headline"
import headlineWithSubline from "./components/headlineWithSubline"
import headlineWithSublineWithLink from "./components/headlineWithSublineWithLink"
import logo from "./components/logoSection"
import subline from "./components/subline"
import text from "./components/text"
import textWithImage from "./components/textWithImage"
import spacer from "./components/spacer"
import carousel from "./components/carousel"
import carouselDesigner from "./components/carouselDesigner"
import button from "./components/button"
import sectionBottom from "./components/sectionBottom"
import footer from "./components/footer"
import end from "./components/end"
import social from "./components/social"
import alternativeHtmlInfo from "./components/alternativeHtmlInfo"
import tableBill from "./components/tableBill"

export async function getHtml({
  options
}: {
  options: MailBuilder.BuildOptions
}): Promise<MailBuilder.Component> {
  try {
    const mailContent: MailBuilder.MailTemplateComponent = Object.create({
      html: "",
      text: "",
      translations: [],
      add: function({ component }: { component: MailBuilder.Component }) {
        this.html += component.html
        this.text += component.text
        ;(this.translations += component.translations),
          (this.parse = component.parse)
      }
    })

    /* COMPONENTS */

    let currentOptions = options

    if (!options.getSingleComponent) {
      mailContent.add({
        component: head({ options: currentOptions, settings: {} })
      })

      /* mailContent.add({
    component: alternativeHtmlInfo({
      options: currentOptions,
      settings: sectionSettings ? sectionSettings : currentOptions.templateType.alternativeHtmlInfo
    })
  }) */

      /* mailContent.add({
    component: header({
      options: currentOptions,
      settings: sectionSettings ? sectionSettings : currentOptions.templateType.header
    })
  }) */
    }

    let keyId: number = 0

    currentOptions.sections.map(
      (section: MailBuilder.Section, index: number) => {
        keyId++
        const sectionType: string = section.type.toLocaleLowerCase()
        let sectionSettings: any = false
        if (section && options.getSingleComponent && section.settings) {
          sectionSettings = section.settings
        }

        currentOptions = options /* else {
      currentOptions.designSettings.backgroundColor2 =
        options.designSettings.backgroundColor2
      currentOptions.designSettings.textColor = options.designSettings.textColor
    } */
        /* if (String(section.inverseColors) === "true") {
      currentOptions.designSettings.backgroundColor2 =
        options.designSettings.secundaryColor
      currentOptions.designSettings.textColor =
        options.designSettings.sec_textColor
    } */

        if (sectionType === "htmlWrapper") {
          mailContent.add({
            component: head({
              options: currentOptions,
              settings: sectionSettings
            })
          })
          mailContent.add({
            component: end({
              options: currentOptions,
              settings: sectionSettings
            })
          })
        } else if (sectionType === "head") {
          mailContent.add({
            component: head({
              options: currentOptions,
              settings: sectionSettings
            })
          })
        } else if (sectionType === "headonly") {
          mailContent.add({
            component: headOnly({
              options: currentOptions,
              settings: sectionSettings
            })
          })
        } else if (sectionType === "end") {
          mailContent.add({
            component: end({
              options: currentOptions,
              settings: sectionSettings
            })
          })
        } else if (sectionType === "alternativehtmlinfo") {
          mailContent.add({
            component: alternativeHtmlInfo({
              options: currentOptions,
              settings: sectionSettings
                ? sectionSettings
                : currentOptions.templateType.alternativeHtmlInfo
            })
          })
        } else if (sectionType === "header") {
          mailContent.add({
            component: header({
              options: currentOptions,
              settings: sectionSettings
                ? sectionSettings
                : currentOptions.templateType.header
            })
          })
        } else if (sectionType.toLowerCase() === "headerdesigner") {
          mailContent.add({
            component: headerDesigner({
              options: currentOptions,
              settings: sectionSettings
                ? sectionSettings
                : currentOptions.templateType.header
            })
          })
        } else if (sectionType === "hero") {
          mailContent.add({
            component: hero({
              options: currentOptions,
              // settings: currentOptions.templateType.hero
              settings: sectionSettings
                ? sectionSettings
                : currentOptions.templateType.hero
            })
          })
        } else if (sectionType === "headline") {
          mailContent.add({
            component: headline({
              options: currentOptions,
              settings: {
                headline: true,
                key: "HDL"
              }
            })
          })
        } else if (sectionType === "headlinewithsubline") {
          mailContent.add({
            component: headlineWithSubline({
              options: currentOptions,
              settings: {
                key: "HDL_WSBL" + keyId
              }
            })
          })
        } else if (sectionType === "headlinewithsublinewithlink") {
          mailContent.add({
            component: headlineWithSublineWithLink({
              options: currentOptions,
              settings: {
                key: "HDL_WSBL_WL" + keyId
                /* section.inverseColors +
              currentOptions.designSettings.backgroundColor2 */
              }
            })
          })
        } else if (sectionType.includes("zusatz_text")) {
          let i: number = Number(sectionType.replace("zusatz_text", ""))
          let settings: any = {
            key: "ZUSATZ_" + i
          }

          if (
            currentOptions.zusatzTextBilder &&
            currentOptions.zusatzTextBilder.length > 0 &&
            currentOptions.zusatzTextBilder[i - 1]
          ) {
            settings.image = currentOptions.zusatzTextBilder[i - 1]
          } else {
            settings.image = {}
          }

          mailContent.add({
            component: headline({
              options: currentOptions,
              settings: {
                headline: true,
                key: "ZUSATZ_TEXT_" + i
              }
            })
          })

          mailContent.add({
            component: divider({
              options: currentOptions,
              settings: sectionSettings
            })
          })

          mailContent.add({
            component: textWithImage({
              options: currentOptions,
              settings: settings
            })
          })
        } else if (sectionType === "textwithimage") {
          mailContent.add({
            component: textWithImage({
              options: currentOptions,
              settings: sectionSettings
                ? sectionSettings
                : currentOptions.templateType.textWithImage
            })
          })
        } else if (sectionType === "text") {
          mailContent.add({
            component: text({
              options: currentOptions,
              settings: sectionSettings
                ? sectionSettings
                : currentOptions.templateType.text
            })
          })
        } else if (sectionType === "spacer") {
          mailContent.add({
            component: spacer({
              options: currentOptions,
              settings: sectionSettings
                ? sectionSettings
                : currentOptions.templateType.spacer
            })
          })
        } else if (sectionType === "textwithdivider") {
          mailContent.add({
            component: divider({
              options: currentOptions,
              settings: sectionSettings
            })
          })

          mailContent.add({
            component: text({
              options: currentOptions,
              settings: sectionSettings
                ? sectionSettings
                : currentOptions.templateType.text
            })
          })
        } else if (sectionType === "carousel") {
          mailContent.add({
            component: carousel({
              options: currentOptions,
              settings: sectionSettings
                ? sectionSettings
                : currentOptions.templateType.carousel
            })
          })
        } else if (sectionType.toLowerCase() === "carouseldesigner") {
          mailContent.add({
            component: carouselDesigner({
              options: currentOptions,
              settings: sectionSettings
                ? sectionSettings
                : currentOptions.templateType.carousel
            })
          })
        } else if (sectionType === "subline") {
          // mailContent.add({ component: spacer({ options: currentOptions, settings: sectionSettings }) })

          mailContent.add({
            component: subline({
              options: currentOptions,
              settings: {
                key: "SL"
              }
            })
          })
        } else if (sectionType === "button") {
          mailContent.add({
            component: button({
              options: currentOptions,
              settings: sectionSettings
                ? sectionSettings
                : currentOptions.templateType.button
            })
          })
        } else if (sectionType === "logo") {
          mailContent.add({
            component: logo({
              options: currentOptions,
              settings: sectionSettings
                ? sectionSettings
                : currentOptions.templateType.logo
            })
          })
        } else if (sectionType === "divider") {
          mailContent.add({
            component: divider({
              options: currentOptions,
              settings: sectionSettings
            })
          })
        } else if (sectionType === "image") {
          let settings: any = {
            key: "IMG_" + keyId,
            src: "",
            width: 600
          }

          if (
            currentOptions.templateType &&
            currentOptions.templateType.image
          ) {
            settings = {
              ...settings,
              ...currentOptions.templateType.image
            }
          }
          // console.log("img settings", settings)

          mailContent.add({
            component: image({
              options: currentOptions,
              settings: sectionSettings ? sectionSettings : settings
            })
          })
        } else if (sectionType === "imagetext") {
          let settings: any = {
            key: "IMG_" + keyId,
            src: "",
            width: 600
          }

          if (
            currentOptions.templateType &&
            currentOptions.templateType.imageText
          ) {
            settings = {
              ...settings,
              ...currentOptions.templateType.imageText
            }
          }
          console.log("imageText settings", settings)
          mailContent.add({
            component: imageText({
              options: currentOptions,
              settings: settings
            })
          })
        } else if (sectionType === "accordeon") {
          mailContent.add({
            component: accordeon({
              options: currentOptions,
              settings: sectionSettings
                ? sectionSettings
                : currentOptions.templateType.accordeon
            })
          })
        } else if (sectionType === "footer") {
          mailContent.add({
            component: footer({
              options: currentOptions,
              settings: sectionSettings
                ? sectionSettings
                : currentOptions.templateType.footer
            })
          })
        } else if (sectionType === "tablebill") {
          mailContent.add({
            component: tableBill({
              options: currentOptions,
              settings: sectionSettings
                ? sectionSettings
                : currentOptions.templateType.tableBill
            })
          })
        } else if (sectionType === "social") {
          mailContent.add({
            component: social({
              options: currentOptions,
              settings: sectionSettings
                ? sectionSettings
                : currentOptions.socialMedia
            })
          })
        }
      }
    )

    if (!options.getSingleComponent) {
      mailContent.add({
        component: sectionBottom({
          options: currentOptions,
          settings: currentOptions.templateType.sectionBottom
        })
      })

      // console.log(currentOptions.socialMedia)
      // if (currentOptions.socialMedia && currentOptions.socialMedia.length > 0) {
      mailContent.add({
        component: social({
          options: currentOptions,
          settings: currentOptions.socialMedia
        })
      })
      // }

      mailContent.add({
        component: footer({
          options: currentOptions,
          settings: currentOptions.templateType.footer
        })
      })

      mailContent.add({
        component: end({
          options: currentOptions,
          settings: {}
        })
      })
    }
    return {
      html: mailContent.html,
      text: mailContent.text,
      translations: mailContent.translations,
      parse: mailContent.parse
    }
  } catch (err) {
    console.log("dynamic template err:", err)
    return err
  }
}
