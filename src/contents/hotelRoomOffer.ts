import { createLink } from "../compose"

export default async function(
  options: MailBuilder.BuildOptions
): Promise<MailBuilder.MailContentsOutput> {
  const subject: string = "Liebe Viktoria Hofmann"

  let words: string = ""
  let text: string =
    "Das **** Superior Baby & Kinderhotel Laurentius bietet die allerbesten Voraussetzungen für einen traumhaften Winter- und Sommerurlaub, mit einem riesigen Angebot an Wellness, Genuss, Spaß und Sport für die ganze Familie – und das - direkt an den Pisten von Tirols Skidimension Serfaus-Fiss-Ladis."
  let html: string =
    "Das **** Superior Baby & Kinderhotel Laurentius bietet die allerbesten Voraussetzungen für einen traumhaften Winter- und Sommerurlaub, mit einem riesigen Angebot an Wellness, Genuss, Spaß und Sport für die ganze Familie – und das - direkt an den Pisten von Tirols Skidimension Serfaus-Fiss-Ladis."
  let translations: string[] = []

  /* if (options.data.user.displayName) {
    words = "Berechtigungen angefordert für Account: "
    text += textBreak + words + options.data.user.displayName
    html += "<br>" + words + options.data.user.displayName
  } */

  const links: MailBuilder.MailLink = createLink({
    type: "button",
    text: "Angebot öffnen",
    url: "https://touristscout.com/?template=1b"
  })

  return {
    subject: subject,
    links: [links],
    text: text,
    translations: translations,
    html: html,
    options: options
  }
}
