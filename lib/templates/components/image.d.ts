export interface ImageSettings {
    src: string;
    key: string;
    width?: number;
}
export interface Image {
    alt: string;
}
export declare const mjmlImage: string;
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: ImageSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
