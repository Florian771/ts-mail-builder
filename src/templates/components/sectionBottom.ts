import testParse from "../../testParse"

export interface SectionBottomSettings {
  active: boolean
  type?: "diagonal"
}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: SectionBottomSettings
}): MailBuilder.Component => {
  let html: string = ""
  const text: string = ""
  if (settings.active) {
    if (settings.type === "diagonal") {
      html = `<mj-section vertical-align="middle" background-size="cover" background-repeat="no-repeat">
  <mj-column width="100%">
    <mj-image src="https://admin.chatify.info/assets/mail/shadow.png" alt="" align="center" border="none" padding="0px"></mj-image>
  </mj-column>
</mj-section>`
    }
  }

  testParse({
    test: true,
    html: html,
    options: options,
    modulName: "sectionBottom"
  })
  return {
    html: html,
    text: text,
    translations: [],
    parse: (args: any) => testParse(args)
  }
}
