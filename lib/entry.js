"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = require("./index");
var chatifyOptions = {
    TemplateName: "1",
    primaryColor: "#dd2476",
    secundaryColor: "#ff512f",
    headlineColor: "#111",
    textColor: "#9B9B9B",
    textColorHighlight: "#000",
    backgroundColor: "#F2F2F2",
    backgroundColor2: "#fff",
    headlineFontName: '"Montserrat", "Open Sans", "Helvetica Neue", Helvetica, sans-serif',
    bodyFontName: '"Open Sans", "Helvetica Neue", Helvetica, sans-serif',
    bottomSlanted: true
};
var user = {
    displayName: "Thomas Muster"
};
var client = {
    companyName: "Muster GmbH",
    contactPersonFirstName: "Thomas",
    contactPersonLastName: "Musterss",
    email: "test@test.at",
    street: "Neutorstr. 4",
    postalCode: "5300",
    city: "Salzburg",
    country: "Österreich"
};
var data = {
    user: user,
    client: client
};
var designSettings = chatifyOptions;
var options = {
    name: "chatify",
    from: "noreply@chatify.info",
    to: "florian1677@gmail.com",
    data: data,
    socialMedia: [
        {
            name: "facebook",
            url: "https://wwww.facebook.com"
        },
        {
            name: "youtube",
            url: "https://wwww.youtube.com"
        },
        {
            name: "instagram",
            url: "https://wwww.instagram.com"
        },
        {
            name: "twitter",
            url: "https://wwww.twitter.com"
        },
        {
            name: "pinterest",
            url: "https://wwww.pinterest.com"
        },
        {
            name: "google",
            url: "https://wwww.google.com"
        }
    ],
    designSettings: designSettings,
    templateType: {
        name: "minimal",
        alternativeHtmlInfo: {
            text: true,
            url: true
        },
        header: {
            logo: "",
            context: true,
            menuLinks: 3
        },
        button: {
            key: "BTN"
        },
        text: {
            key: "TXT"
        },
        sectionBottom: {
            active: true,
            type: "diagonal"
        },
        footer: {
            unsubscribeText: true,
            unsubscribeUrl: true,
            name: true,
            street: true,
            postalCode: true,
            city: true,
            country: true,
            phone: true,
            fax: true,
            email: true,
            website: true,
            uid: true
        }
    },
    contentType: "userCreated",
    buildType: "file",
    testParse: true,
    translations: [
        { HTML_LINK_TEXT: "Chatify - bot up your business" },
        { HTML_LINK_URL: "" },
        { MAIL_CONTEXT: "" },
        { LOGO_URL: "https://admin.chatify.info/assets/img/logo-claim@2x.png" },
        { LOGO_TITLE: "Demo Hotel" },
        { LOGO_TARGET: "" }
    ]
};
index_1.start(options).then(function (res) {
    return res;
});
//# sourceMappingURL=entry.js.map