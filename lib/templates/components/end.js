"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = require("../../config");
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    var html = "";
    var text = "";
    text = "" + config_1.textBreak;
    html = "\n  </mj-body>\n  </mjml>";
    return {
        html: html,
        text: text,
        translations: []
    };
});
//# sourceMappingURL=end.js.map