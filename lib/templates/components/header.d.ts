export interface HeaderSettings {
    length?: number;
    logo: string;
    logoWidth?: number;
    logoHeight?: number;
    context: boolean;
    menuLinks: number;
}
export interface Header {
    logo?: MailBuilder.ImageLink;
    context?: string;
    menuLink: MailBuilder.TextLink;
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: HeaderSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
