export interface ImageTextSettings {
    src: string;
    key: string;
    width?: number;
    height?: number;
}
export interface ImageText {
    alt: string;
    tel: string;
    email: string;
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: ImageTextSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
