"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
var config_1 = require("../../config");
var utils_1 = require("../../utils");
var placeholder = {
    title: "{COMPANY_NAME}",
    src: "{LOGO_SRC}",
    url: "{WEBSITE_URL}"
};
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    var translations = utils_1.getTranslations({
        placeholder: placeholder,
        settings: settings
    });
    var html = "";
    var text = "";
    var textLogo = "";
    var htmlLogo = "";
    // const padding = "&nbsp;&nbsp;&nbsp;"
    var width = "";
    var height = "";
    if (settings.width) {
        width = "width=\"" + settings.width + "px\"";
    }
    if (settings.height) {
        height = "height=\"" + settings.height + "px\"";
    }
    textLogo = placeholder.title;
    if (settings.src) {
        htmlLogo = "<mj-image padding=\"100px 40px 100px 40px\" " + width + " " + height + " src=\"" + settings.src + "\" alt=\"" + placeholder.title.replace("KEY", settings.key) + "\" align=\"center\" border=\"none\"></mj-image>";
    }
    else if (settings.logo) {
        htmlLogo = "<mj-image padding=\"100px 40px 100px 40px\" width=\"176px\" height=\"48px\" src=\"{LOGO_SRC}\" alt=\"" + placeholder.title + "\" align=\"center\" border=\"none\"></mj-image>";
    }
    text = textLogo + config_1.textBreak;
    html = "<mj-section padding=\"20px 20px 20px 20px\" background-color=\"{primaryColor}\">\n  <mj-column>\n  " + htmlLogo + "\n  </mj-column>\n</mj-section>";
    testParse_1.default({
        test: true,
        html: html,
        options: options,
        modulName: "logoSection"
    });
    return {
        html: html,
        text: text,
        translations: translations,
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=logoSection.js.map