import testParse from "../../testParse"
import { textBreak } from "../../config"
import { getTranslations } from "../../utils"
import templateReplacer from "../templateReplacer"

export interface ImageSettings {
  src: string
  key: string
  width?: number
}

export interface Image {
  alt: string
}

const placeholder: Image = {
  alt: "{IMAGE_ALT_KEY}"
}

export const mjmlImage: string = `<mj-section padding="0px" background-color="{primaryColor}">
<mj-column>
  <mj-image padding="0px" width="{width}px" rel="{settings.key}" src="{settings.src}" alt="{placeholder.alt}" align="center" border="none"></mj-image>
  </mj-column>
</mj-section>`

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: ImageSettings
}): MailBuilder.Component => {
  // console.log("mailBuilder - image - settings.src", settings.src)
  const translations: string[] = getTranslations({
    placeholder: placeholder,
    settings: settings
  })
  let html: string = ""
  let text: string = ""
  const width: number = settings.width ? settings.width : 600
  // const height: number = settings.height ? settings.height : 320
  // if (settings.src) {
  text = `${textBreak}`
  html = templateReplacer({ html: mjmlImage, settings, placeholder, width })

  // console.log("html: ", html)
  testParse({ test: true, html: html, options: options, modulName: "image" })
  // }
  return {
    html: html,
    text: text,
    translations: translations,
    parse: (args: any) => testParse(args)
  }
}
