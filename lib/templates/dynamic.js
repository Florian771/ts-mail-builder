"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var head_1 = require("./components/head");
var header_1 = require("./components/header");
var headerDesigner_1 = require("./components/headerDesigner");
var headOnly_1 = require("./components/headOnly");
var hero_1 = require("./components/hero");
var divider_1 = require("./components/divider");
var accordeon_1 = require("./components/accordeon");
var image_1 = require("./components/image");
var imageText_1 = require("./components/imageText");
var headline_1 = require("./components/headline");
var headlineWithSubline_1 = require("./components/headlineWithSubline");
var headlineWithSublineWithLink_1 = require("./components/headlineWithSublineWithLink");
var logoSection_1 = require("./components/logoSection");
var subline_1 = require("./components/subline");
var text_1 = require("./components/text");
var textWithImage_1 = require("./components/textWithImage");
var spacer_1 = require("./components/spacer");
var carousel_1 = require("./components/carousel");
var carouselDesigner_1 = require("./components/carouselDesigner");
var button_1 = require("./components/button");
var sectionBottom_1 = require("./components/sectionBottom");
var footer_1 = require("./components/footer");
var end_1 = require("./components/end");
var social_1 = require("./components/social");
var alternativeHtmlInfo_1 = require("./components/alternativeHtmlInfo");
var tableBill_1 = require("./components/tableBill");
function getHtml(_a) {
    var options = _a.options;
    return __awaiter(this, void 0, void 0, function () {
        var mailContent_1, currentOptions_1, keyId_1;
        return __generator(this, function (_b) {
            try {
                mailContent_1 = Object.create({
                    html: "",
                    text: "",
                    translations: [],
                    add: function (_a) {
                        var component = _a.component;
                        this.html += component.html;
                        this.text += component.text;
                        (this.translations += component.translations),
                            (this.parse = component.parse);
                    }
                });
                currentOptions_1 = options;
                if (!options.getSingleComponent) {
                    mailContent_1.add({
                        component: head_1.default({ options: currentOptions_1, settings: {} })
                    });
                    /* mailContent.add({
                  component: alternativeHtmlInfo({
                    options: currentOptions,
                    settings: sectionSettings ? sectionSettings : currentOptions.templateType.alternativeHtmlInfo
                  })
                }) */
                    /* mailContent.add({
                  component: header({
                    options: currentOptions,
                    settings: sectionSettings ? sectionSettings : currentOptions.templateType.header
                  })
                }) */
                }
                keyId_1 = 0;
                currentOptions_1.sections.map(function (section, index) {
                    keyId_1++;
                    var sectionType = section.type.toLocaleLowerCase();
                    var sectionSettings = false;
                    if (section && options.getSingleComponent && section.settings) {
                        sectionSettings = section.settings;
                    }
                    currentOptions_1 = options; /* else {
                  currentOptions.designSettings.backgroundColor2 =
                    options.designSettings.backgroundColor2
                  currentOptions.designSettings.textColor = options.designSettings.textColor
                } */
                    /* if (String(section.inverseColors) === "true") {
                  currentOptions.designSettings.backgroundColor2 =
                    options.designSettings.secundaryColor
                  currentOptions.designSettings.textColor =
                    options.designSettings.sec_textColor
                } */
                    if (sectionType === "htmlWrapper") {
                        mailContent_1.add({
                            component: head_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                            })
                        });
                        mailContent_1.add({
                            component: end_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                            })
                        });
                    }
                    else if (sectionType === "head") {
                        mailContent_1.add({
                            component: head_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                            })
                        });
                    }
                    else if (sectionType === "headonly") {
                        mailContent_1.add({
                            component: headOnly_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                            })
                        });
                    }
                    else if (sectionType === "end") {
                        mailContent_1.add({
                            component: end_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                            })
                        });
                    }
                    else if (sectionType === "alternativehtmlinfo") {
                        mailContent_1.add({
                            component: alternativeHtmlInfo_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                                    ? sectionSettings
                                    : currentOptions_1.templateType.alternativeHtmlInfo
                            })
                        });
                    }
                    else if (sectionType === "header") {
                        mailContent_1.add({
                            component: header_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                                    ? sectionSettings
                                    : currentOptions_1.templateType.header
                            })
                        });
                    }
                    else if (sectionType.toLowerCase() === "headerdesigner") {
                        mailContent_1.add({
                            component: headerDesigner_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                                    ? sectionSettings
                                    : currentOptions_1.templateType.header
                            })
                        });
                    }
                    else if (sectionType === "hero") {
                        mailContent_1.add({
                            component: hero_1.default({
                                options: currentOptions_1,
                                // settings: currentOptions.templateType.hero
                                settings: sectionSettings
                                    ? sectionSettings
                                    : currentOptions_1.templateType.hero
                            })
                        });
                    }
                    else if (sectionType === "headline") {
                        mailContent_1.add({
                            component: headline_1.default({
                                options: currentOptions_1,
                                settings: {
                                    headline: true,
                                    key: "HDL"
                                }
                            })
                        });
                    }
                    else if (sectionType === "headlinewithsubline") {
                        mailContent_1.add({
                            component: headlineWithSubline_1.default({
                                options: currentOptions_1,
                                settings: {
                                    key: "HDL_WSBL" + keyId_1
                                }
                            })
                        });
                    }
                    else if (sectionType === "headlinewithsublinewithlink") {
                        mailContent_1.add({
                            component: headlineWithSublineWithLink_1.default({
                                options: currentOptions_1,
                                settings: {
                                    key: "HDL_WSBL_WL" + keyId_1
                                    /* section.inverseColors +
                                  currentOptions.designSettings.backgroundColor2 */
                                }
                            })
                        });
                    }
                    else if (sectionType.includes("zusatz_text")) {
                        var i = Number(sectionType.replace("zusatz_text", ""));
                        var settings = {
                            key: "ZUSATZ_" + i
                        };
                        if (currentOptions_1.zusatzTextBilder &&
                            currentOptions_1.zusatzTextBilder.length > 0 &&
                            currentOptions_1.zusatzTextBilder[i - 1]) {
                            settings.image = currentOptions_1.zusatzTextBilder[i - 1];
                        }
                        else {
                            settings.image = {};
                        }
                        mailContent_1.add({
                            component: headline_1.default({
                                options: currentOptions_1,
                                settings: {
                                    headline: true,
                                    key: "ZUSATZ_TEXT_" + i
                                }
                            })
                        });
                        mailContent_1.add({
                            component: divider_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                            })
                        });
                        mailContent_1.add({
                            component: textWithImage_1.default({
                                options: currentOptions_1,
                                settings: settings
                            })
                        });
                    }
                    else if (sectionType === "textwithimage") {
                        mailContent_1.add({
                            component: textWithImage_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                                    ? sectionSettings
                                    : currentOptions_1.templateType.textWithImage
                            })
                        });
                    }
                    else if (sectionType === "text") {
                        mailContent_1.add({
                            component: text_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                                    ? sectionSettings
                                    : currentOptions_1.templateType.text
                            })
                        });
                    }
                    else if (sectionType === "spacer") {
                        mailContent_1.add({
                            component: spacer_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                                    ? sectionSettings
                                    : currentOptions_1.templateType.spacer
                            })
                        });
                    }
                    else if (sectionType === "textwithdivider") {
                        mailContent_1.add({
                            component: divider_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                            })
                        });
                        mailContent_1.add({
                            component: text_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                                    ? sectionSettings
                                    : currentOptions_1.templateType.text
                            })
                        });
                    }
                    else if (sectionType === "carousel") {
                        mailContent_1.add({
                            component: carousel_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                                    ? sectionSettings
                                    : currentOptions_1.templateType.carousel
                            })
                        });
                    }
                    else if (sectionType.toLowerCase() === "carouseldesigner") {
                        mailContent_1.add({
                            component: carouselDesigner_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                                    ? sectionSettings
                                    : currentOptions_1.templateType.carousel
                            })
                        });
                    }
                    else if (sectionType === "subline") {
                        // mailContent.add({ component: spacer({ options: currentOptions, settings: sectionSettings }) })
                        mailContent_1.add({
                            component: subline_1.default({
                                options: currentOptions_1,
                                settings: {
                                    key: "SL"
                                }
                            })
                        });
                    }
                    else if (sectionType === "button") {
                        mailContent_1.add({
                            component: button_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                                    ? sectionSettings
                                    : currentOptions_1.templateType.button
                            })
                        });
                    }
                    else if (sectionType === "logo") {
                        mailContent_1.add({
                            component: logoSection_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                                    ? sectionSettings
                                    : currentOptions_1.templateType.logo
                            })
                        });
                    }
                    else if (sectionType === "divider") {
                        mailContent_1.add({
                            component: divider_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                            })
                        });
                    }
                    else if (sectionType === "image") {
                        var settings = {
                            key: "IMG_" + keyId_1,
                            src: "",
                            width: 600
                        };
                        if (currentOptions_1.templateType &&
                            currentOptions_1.templateType.image) {
                            settings = __assign({}, settings, currentOptions_1.templateType.image);
                        }
                        // console.log("img settings", settings)
                        mailContent_1.add({
                            component: image_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings ? sectionSettings : settings
                            })
                        });
                    }
                    else if (sectionType === "imagetext") {
                        var settings = {
                            key: "IMG_" + keyId_1,
                            src: "",
                            width: 600
                        };
                        if (currentOptions_1.templateType &&
                            currentOptions_1.templateType.imageText) {
                            settings = __assign({}, settings, currentOptions_1.templateType.imageText);
                        }
                        console.log("imageText settings", settings);
                        mailContent_1.add({
                            component: imageText_1.default({
                                options: currentOptions_1,
                                settings: settings
                            })
                        });
                    }
                    else if (sectionType === "accordeon") {
                        mailContent_1.add({
                            component: accordeon_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                                    ? sectionSettings
                                    : currentOptions_1.templateType.accordeon
                            })
                        });
                    }
                    else if (sectionType === "footer") {
                        mailContent_1.add({
                            component: footer_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                                    ? sectionSettings
                                    : currentOptions_1.templateType.footer
                            })
                        });
                    }
                    else if (sectionType === "tablebill") {
                        mailContent_1.add({
                            component: tableBill_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                                    ? sectionSettings
                                    : currentOptions_1.templateType.tableBill
                            })
                        });
                    }
                    else if (sectionType === "social") {
                        mailContent_1.add({
                            component: social_1.default({
                                options: currentOptions_1,
                                settings: sectionSettings
                                    ? sectionSettings
                                    : currentOptions_1.socialMedia
                            })
                        });
                    }
                });
                if (!options.getSingleComponent) {
                    mailContent_1.add({
                        component: sectionBottom_1.default({
                            options: currentOptions_1,
                            settings: currentOptions_1.templateType.sectionBottom
                        })
                    });
                    // console.log(currentOptions.socialMedia)
                    // if (currentOptions.socialMedia && currentOptions.socialMedia.length > 0) {
                    mailContent_1.add({
                        component: social_1.default({
                            options: currentOptions_1,
                            settings: currentOptions_1.socialMedia
                        })
                    });
                    // }
                    mailContent_1.add({
                        component: footer_1.default({
                            options: currentOptions_1,
                            settings: currentOptions_1.templateType.footer
                        })
                    });
                    mailContent_1.add({
                        component: end_1.default({
                            options: currentOptions_1,
                            settings: {}
                        })
                    });
                }
                return [2 /*return*/, {
                        html: mailContent_1.html,
                        text: mailContent_1.text,
                        translations: mailContent_1.translations,
                        parse: mailContent_1.parse
                    }];
            }
            catch (err) {
                console.log("dynamic template err:", err);
                return [2 /*return*/, err];
            }
            return [2 /*return*/];
        });
    });
}
exports.getHtml = getHtml;
//# sourceMappingURL=dynamic.js.map