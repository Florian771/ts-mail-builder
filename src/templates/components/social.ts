import testParse from "../../testParse"

export interface SocialMediaSettings {
  key?: string
  facebook: boolean
  youtube: boolean
  instagram: boolean
  twitter: boolean
  pinterest: boolean
  google: boolean
}
export interface SocialMedia {
  key?: string
  name: string
  url: string
}

export const predefinedPlatforms: string[] = [
  "facebook",
  "youtube",
  "instagram",
  "twitter",
  "pinterest",
  "google"
]
export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: SocialMedia[] | SocialMediaSettings
}): MailBuilder.Component => {
  // console.log("social.settings:", settings)
  let html: string = ""
  let text: string = ""
  text = ``
  html = `
  <mj-section>
  <mj-column>
  <mj-social font-size="15px" border-radius="15px" icon-size="30px" mode="horizontal">TEST`
  if (Array.isArray(settings)) {
    if (settings && settings.length > 0) {
      settings.map((socialMedia: SocialMedia) => {
        html += `<mj-social-element background-color="{textColor}" name="${
          socialMedia.name
        }-noshare" href="${
          socialMedia.url
            ? socialMedia.url
            : "{" + socialMedia.name.toUpperCase() + "_URL}"
        }"></mj-social-element> `
      })
    }
  } else {
    predefinedPlatforms.map((platform: string) => {
      if (settings && settings[platform]) {
        html += `<mj-social-element background-color="{textColor}" name="${platform}-noshare" href="${
          options[platform].url
            ? options[platform].url
            : "{" + platform.toUpperCase() + "_URL}"
        }"></mj-social-element> `
      }
    })
  }
  html += `</mj-social>
  </mj-column>
</mj-section>`

  testParse({ test: true, html: html, options: options, modulName: "Social" })
  return {
    html: html,
    text: text,
    translations: [],
    parse: (args: any) => testParse(args)
  }
}
