"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mjml2html = require("mjml");
// const mjml2html = require("mjml").default
var replacerDesign_1 = require("./replacerDesign");
var replacerContent_1 = require("./replacerContent");
var imageText_1 = require("./templates/components/imageText");
var constants_1 = require("./constants");
var accordeon_1 = require("./templates/components/accordeon");
var headOnly_1 = require("./templates/components/headOnly");
var startMarkerHeader = "{HEADER_START}";
var endMarkerHeader = "{HEADER_END}";
var startMarker = "{CONTENT_START}";
var endMarker = "{CONTENT_END}";
function wrapHtmlToTestParse(html, options, modulName, test) {
    var content_start = "";
    var content_end = "";
    console.log("wrapHtmlToTestParse - modulName:", modulName);
    console.log("wrapHtmlToTestParse - options.getSingleComponent", options.getSingleComponent);
    if (options.getSingleComponent) {
        console.log("testParse - getSingleComponent - add mj-raw");
        content_start = "<mj-raw>" + startMarker + "</mj-raw>";
        content_end = "<mj-raw>" + endMarker + "</mj-raw>";
    }
    if (modulName === "headOnly") {
        // PARSE HEAD ONLY WITH ELEMENTS, WHICH NEED ADDITIONAL HEAD STYLES
        console.log("testParse - headOnly");
        return "<mjml>\n    " + html + "\n    <mj-body>\n    " + accordeon_1.default({
            options: options,
            settings: {
                key: "ACD",
                items: 1
            }
        }).html + "\n    " + imageText_1.default({
            options: options,
            settings: {
                key: "TEST_IMAGE_TEXT",
                src: "https://via.placeholder.com/600x250/000000/FFFFFF?text={124x119}",
                width: 124,
                height: 119
            }
        }).html + "\n    </mj-body>\n    </mjml>";
        // } else if (modulName.toLowerCase() === "carouseldesigner" && !test) {
    }
    else if (!test && options.getSingleComponent) {
        // PARSE HEAD ONLY WITH ELEMENTS, WHICH NEED ADDITIONAL HEAD STYLES
        console.log("parse with head");
        return "<mjml>\n    " + headOnly_1.default({
            options: options,
            settings: options.sections[0].settings
        }).html + "\n    <mj-body>\n    " + content_start + "\n    " + html + "\n    " + content_end + "\n    </mj-body>\n    </mjml>";
    }
    else {
        console.log("testParse - normal");
        return "<mjml><mj-head>\n  <mj-title></mj-title></mj-head><mj-body>\n  " + content_start + "\n  " + html + "\n  " + content_end + "\n  </mj-body>\n  </mjml>";
    }
}
function default_1(_a) {
    var html = _a.html, options = _a.options, modulName = _a.modulName, test = _a.test;
    var res;
    // console.log("testParser - modulName: ", modulName)
    try {
        if (options.testParse) {
            /* if (modulName === "headline") {
              console.log("2.testParser - html:", html)
            } */
            var updatedHtml = wrapHtmlToTestParse(html, options, modulName, test);
            updatedHtml = replacerDesign_1.default({
                html: updatedHtml,
                designSettings: options.designSettings
            });
            updatedHtml = replacerContent_1.replaceTextAndHtml({
                mail: { subject: "", from: "", to: "", html: updatedHtml, text: "" },
                from: "{urlWeb}",
                to: constants_1.default.web.urlWeb
            }).html;
            if (modulName.toLowerCase() === "hero") {
                // if (modulName.toLowerCase() === "carouseldesigner") {
                // if (options.getSingleComponent) {
                console.log("testParser - updatedHtml:", updatedHtml);
            }
            var mjml2htmlOptions = {};
            res = mjml2html(updatedHtml, mjml2htmlOptions);
            if (modulName === "headOnly" ||
                // modulName.toLowerCase() === "carouseldesigner"
                (!test && options.getSingleComponent)) {
                res.html = res.html.replace("<head>", "<head>" + startMarkerHeader);
                res.html = res.html.replace("</head>", endMarkerHeader + "</head>");
            }
            if (res.errors && res.errors.length > 0) {
                throw {
                    code: 0,
                    message: res.errors
                };
            }
        }
        return res;
    }
    catch (err) {
        console.log("testParser - err", err.message);
        throw err;
        return err;
    }
}
exports.default = default_1;
//# sourceMappingURL=testParse.js.map