import userCreated from "./contents/userCreated"
import accountActivation from "./contents/accountActivation"
import hotelRoomOffer from "./contents/hotelRoomOffer"
import notDefined from "./contents/notDefined"

export async function createMailContents(
  options: MailBuilder.BuildOptions
): Promise<MailBuilder.MailContentsOutput> {
  let mailContentsOutput: MailBuilder.MailContentsOutput
  if (options.contentType === "userCreated") {
    mailContentsOutput = await userCreated(options)
  } else if (options.contentType === "accountActivation") {
    mailContentsOutput = await accountActivation(options)
  } else if (options.contentType === "hotelRoomOffer") {
    mailContentsOutput = await hotelRoomOffer(options)
  } else {
    mailContentsOutput = await notDefined(options)
  }

  return mailContentsOutput
}
