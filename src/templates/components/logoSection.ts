import testParse from "../../testParse"
import { textBreak } from "../../config"
import { getTranslations } from "../../utils"

export interface LogoSettings {
  key?: string
  logo?: boolean
  src?: string
  width?: number
  height?: number
}
export interface Logo {
  title: string
}

const placeholder: MailBuilder.ImageLink = {
  title: "{COMPANY_NAME}",
  src: "{LOGO_SRC}",
  url: "{WEBSITE_URL}"
}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: LogoSettings
}): MailBuilder.Component => {
  const translations: string[] = getTranslations({
    placeholder: placeholder,
    settings: settings
  })
  let html: string = ""
  let text: string = ""
  let textLogo: string = ""
  let htmlLogo: string = ""
  // const padding = "&nbsp;&nbsp;&nbsp;"
  let width: string = ""
  let height: string = ""

  if (settings.width) {
    width = `width="${settings.width}px"`
  }
  if (settings.height) {
    height = `height="${settings.height}px"`
  }

  textLogo = placeholder.title
  if (settings.src) {
    htmlLogo = `<mj-image padding="100px 40px 100px 40px" ${width} ${height} src="${
      settings.src
    }" alt="${placeholder.title.replace(
      "KEY",
      settings.key
    )}" align="center" border="none"></mj-image>`
  } else if (settings.logo) {
    htmlLogo = `<mj-image padding="100px 40px 100px 40px" width="176px" height="48px" src="{LOGO_SRC}" alt="${
      placeholder.title
    }" align="center" border="none"></mj-image>`
  }

  text = textLogo + textBreak

  html = `<mj-section padding="20px 20px 20px 20px" background-color="{primaryColor}">
  <mj-column>
  ${htmlLogo}
  </mj-column>
</mj-section>`

  testParse({
    test: true,
    html: html,
    options: options,
    modulName: "logoSection"
  })
  return {
    html: html,
    text: text,
    translations: translations,
    parse: (args: any) => testParse(args)
  }
}
