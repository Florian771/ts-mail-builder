export declare const log: (message?: any, ...optionalParams: any[]) => void;
export declare function timestamp(): number;
export declare function removeUndefinedFromObject(dataObject: any): any;
