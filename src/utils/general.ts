import * as moment from "moment"

export const log = console.log

export function timestamp() {
  return parseInt(moment().format("x"))
}

export function removeUndefinedFromObject(dataObject: any): any {
  for (const key in dataObject) {
    if (dataObject[key] === undefined) {
      dataObject[key] = ""
    }
  }
  console.log("after removeUndefinedFromObject", dataObject)
  return dataObject
}
