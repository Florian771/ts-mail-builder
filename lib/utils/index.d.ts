export declare function getHtmlFileName({ options, type }: {
    options: MailBuilder.BuildOptions;
    type: "pebuild" | "final";
}): string;
export declare function getTranslations({ placeholder, settings }: {
    placeholder: any;
    settings: any;
}): string[];
