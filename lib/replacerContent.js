"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("./constants");
function replaceTextAndHtml(_a) {
    /* if (from === "{urlWeb}") {
      console.log("from:", from)
      console.log("to:", to)
      if (mail.html.includes(from)) {
        console.log("html before replace includes from")
      }
      if (mail.text.includes(from)) {
        console.log("text before replace includes from")
      }
    } */
    var mail = _a.mail, from = _a.from, to = _a.to;
    var updatedText = mail.text.replace(new RegExp(from, "g"), to);
    var updatedHtml = mail.html.replace(new RegExp(from, "g"), to);
    new RegExp("hello");
    var updatedMail = __assign({}, mail, {
        text: updatedText,
        html: updatedHtml
    });
    /* if (from === "{urlWeb}") {
      if (updatedHtml.includes(from)) {
        console.log("html after replace includes from")
      }
      if (updatedText.includes(from)) {
        console.log("text after replace includes from")
      }
    } */
    return updatedMail;
}
exports.replaceTextAndHtml = replaceTextAndHtml;
function replacer(_a) {
    var mail = _a.mail, mailContentsOutput = _a.mailContentsOutput;
    try {
        if (!mail.text) {
            throw { code: 0, message: "replacer: no text" };
        }
        else if (!mail.html) {
            throw { code: 0, message: "replacer: no html" };
        }
        else {
            var updatedMail_1 = mail;
            if (mailContentsOutput.options.translations &&
                mailContentsOutput.options.translations.length > 0) {
                mailContentsOutput.options.translations.map(function (translation) {
                    Object.keys(translation).forEach(function (key) {
                        updatedMail_1 = replaceTextAndHtml({
                            mail: updatedMail_1,
                            from: "{" + key + "}",
                            to: translation[key]
                        });
                    });
                });
            }
            /*
            updatedMail = replaceTextAndHtml({
              mail: updatedMail,
              from: "{LOGO_URL}",
              to: mailContentsOutput.options.general.logo
            })
      
            updatedMail = replaceTextAndHtml({
              mail: updatedMail,
              from: "{HEADLINE}",
              to: mail.subject
            })
      
            updatedMail = replaceTextAndHtml({
              mail: updatedMail,
              from: "{CONTENT}",
              to: mailContentsOutput.html
            })
      
            if (mailContentsOutput.links && mailContentsOutput.links.length > 0) {
              mail.html = mail.html.replace(
                "{BUTTON_URL}",
                mailContentsOutput.links[0].url
              )
              mail.html = mail.html.replace(
                "{BUTTON_TEXT}",
                mailContentsOutput.links[0].text
              )
            }
      
            updatedMail = replaceTextAndHtml({
              mail: updatedMail,
              from: "{HTML_LINK_TEXT}",
              to: mailContentsOutput.options.general.alternativeHtmlText
            })
      
            updatedMail = replaceTextAndHtml({
              mail: updatedMail,
              from: "{HTML_LINK_URL}",
              to: mailContentsOutput.options.general.alternativeHtmlUrl
            })
            */
            // console.log("CONSTANTS.web.urlWeb:", CONSTANTS.web.urlWeb)
            updatedMail_1 = replaceTextAndHtml({
                mail: updatedMail_1,
                from: "{urlWeb}",
                to: constants_1.default.web.urlWeb
            });
            return updatedMail_1;
        }
    }
    catch (err) {
        console.log("compose - replacerContent - error: ", err.message);
        throw err;
    }
}
exports.default = replacer;
//# sourceMappingURL=replacerContent.js.map