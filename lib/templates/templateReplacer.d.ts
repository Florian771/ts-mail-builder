export declare function templateReplacer({ html, settings, placeholder, width, height, font }: {
    html: string;
    settings: any;
    placeholder?: any;
    width?: number;
    height?: number;
    font?: string;
}): string;
export default templateReplacer;
