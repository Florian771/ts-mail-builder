export interface CarouselDesignerSettings {
    key: string;
    items: number;
}
export interface CarouselDesigner {
    src: string;
}
declare const _default: ({ options, settings }: {
    options: import("../../../../mail-builder-types").BuildOptions;
    settings: CarouselDesignerSettings;
}) => import("../../../../mail-builder-types/components").Component;
export default _default;
