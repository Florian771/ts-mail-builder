"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function templateReplacer(_a) {
    var html = _a.html, settings = _a.settings, placeholder = _a.placeholder, width = _a.width, height = _a.height, font = _a.font;
    html = html.replace(new RegExp("{settings.key}", "g"), settings.key);
    html = html.replace(new RegExp("KEY", "g"), settings.key);
    html = html.replace(new RegExp("{width}", "g"), String(width));
    html = html.replace(new RegExp("{placeholder.alt}", "g"), String(placeholder.alt));
    html = html.replace(new RegExp("{settings.src}", "g"), settings.src
        ? settings.src
        : "https://via.placeholder.com/" +
            width +
            "x250/000000/FFFFFF?text={" +
            settings.key +
            "}");
    html = html.replace(new RegExp("{FONT}", "g"), font);
    return html;
}
exports.templateReplacer = templateReplacer;
exports.default = templateReplacer;
//# sourceMappingURL=templateReplacer.js.map