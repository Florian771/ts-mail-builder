import CONSTANTS from "../constants"
import { createLink } from "../compose"
import { textBreak } from "../config"

export default async function(
  options: MailBuilder.BuildOptions
): Promise<MailBuilder.MailContentsOutput> {
  const subject: string = "Account Activation: " + options.data.user.displayName

  let words: string = ""
  let text: string = ""
  let html: string = ""
  let translations: string[] = []

  if (options.data.user.displayName) {
    words = "Berechtigungen angefordert für Account: "
    text += textBreak + words + options.data.user.displayName
    html += "<br>" + words + options.data.user.displayName
  }

  if (options.data.user.displayName) {
    words = "Benutzer: "
    text += textBreak + words + options.data.user.displayName
    html += "<br" + words + options.data.user.displayName
  }

  if (options.data.user.email) {
    words = "E-Mail: "
    text += textBreak + words + options.data.user.email
    html +=
      "<br>" +
      words +
      ' <a style="color:#000000;text-decoration:none;" href="mailto:' +
      options.data.user.email +
      '">' +
      options.data.user.email +
      "</a>"
  }

  const links: MailBuilder.MailLink = createLink({
    type: "button",
    text:
      CONSTANTS.web.urlAdmin +
      "/superAdministration/activation" +
      options.data.user.uid,
    url:
      CONSTANTS.web.urlAdmin +
      "/superAdministration/activation" +
      options.data.user.uid
  })

  return {
    subject: subject,
    links: [links],
    text: text,
    html: html,
    translations: translations,
    options: options
  }
}
