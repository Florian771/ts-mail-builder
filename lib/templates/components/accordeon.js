"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
var config_1 = require("../../config");
var utils_1 = require("../../utils");
var placeholder = {
    title: "{ACCORDEON_TITLE_KEY_XXX}",
    content: "{ACCORDEON_CONTENT_KEY_XXX}"
};
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    var translations = utils_1.getTranslations({
        placeholder: placeholder,
        settings: settings
    });
    var html = "";
    var text = "";
    var accordeonHtml = "";
    // let accordeonText: string = textBreak
    if (settings["length"]) {
        settings.items = Number(settings["length"]);
    }
    else {
        settings.items = Number(settings.items);
    }
    if (settings.items && settings.items > 0) {
        for (var index = 0; index < settings.items; index++) {
            var color = "backgroundColor";
            if (index % 2 > 0) {
                color = "backgroundColor2";
            }
            accordeonHtml += "<mj-accordion-element background-color=\"{" + color + "}\">\n      <mj-accordion-title font-size=\"15px\" color=\"{textColor}\">" + placeholder.title
                .replace("KEY", settings.key)
                .replace("XXX", String(index)) + "</mj-accordion-title>\n        <mj-accordion-text>\n          <span style=\"line-height:20px\">\n          " + placeholder.content
                .replace("KEY", settings.key)
                .replace("XXX", String(index)) + "\n          </span>\n        </mj-accordion-text>\n      </mj-accordion-element>\n      ";
            /* accordeonText +=
              placeholder.title
                .replace("KEY", settings.key)
                .replace("XXX", String(index)) + textBreak
            accordeonText +=
              placeholder.content
                .replace("KEY", settings.key)
                .replace("XXX", String(index)) + textBreak */
        }
    }
    text = "" + config_1.textBreak;
    html = "\n  <mj-section padding=\"0px 0px 0px 0px\" background-color=\"{backgroundColor2}\">\n    <mj-column width=\"600px\" padding=\"0px 0px 0px 0px\">\n      <mj-accordion padding=\"0px 0px 0px 0px\" border=\"0px\" container-background-color=\"{primaryColor}\"  icon-wrapped-url=\"{urlWeb}/arrow-bottom.png\" icon-unwrapped-url=\"{urlWeb}/arrow-top.png\">\n        " + accordeonHtml + "\n      </mj-accordion>\n    </mj-column>\n  </mj-section>";
    testParse_1.default({
        test: true,
        html: html,
        options: options,
        modulName: "accordeon"
    });
    return {
        html: html,
        text: text,
        translations: translations,
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=accordeon.js.map