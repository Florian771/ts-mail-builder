import testParse from "../../testParse"
import { textBreak } from "../../config"
import { getTranslations } from "../../utils"

export interface ZusatzTextBilder {
  name?: string
  url: string
}

export interface TextWithImageSettings {
  key: string
  image?: ZusatzTextBilder
}

export interface TextWithImage {
  text: string
  imageName: string
}

const placeholder: TextWithImage = {
  text: "{CONTENT_TEXT_WITH_IMAGE_KEY}",
  imageName: "{CONTENT_TEXT_WITH_IMAGE_IMAGE_NAME_KEY}"
}

export default ({
  options,
  settings
}: {
  options: MailBuilder.BuildOptions
  settings: TextWithImageSettings
}): MailBuilder.Component => {
  const translations: string[] = getTranslations({
    placeholder: placeholder,
    settings: settings
  })
  let html: string = ""
  let text: string = ""
  text = `${placeholder.text.replace("KEY", settings.key)}${textBreak}`

  html = `
  <mj-section padding="0 20px 20px 20px" background-color="{backgroundColor2}">`
  if (settings.image && settings.image.url) {
    html += `
    <mj-column width="360px">
      <mj-text align="left" padding="15px 40px 15px 40px" font-weight="300">
        ${placeholder.text.replace("KEY", settings.key)}
      </mj-text>
    </mj-column>
    <mj-column width="200px">
        <mj-image padding="15px 0px" width="160px" src="${
          settings.image.url
        }" alt="${placeholder.imageName.replace(
      "KEY",
      "ZUSATZBILD_NAME_" + settings.key
    )}" align="center" border="none">
        </mj-image>
    </mj-column>`
  } else {
    html += `
    <mj-column width="100%">
      <mj-text align="left" padding="15px 40px 15px 40px" font-weight="300">
        ${placeholder.text.replace("KEY", settings.key)}
      </mj-text>
    </mj-column>`
  }
  html += `</mj-section>`

  testParse({
    test: true,
    html: html,
    options: options,
    modulName: "textWithImage"
  })
  return {
    html: html,
    text: text,
    translations: translations,
    parse: (args: any) => testParse(args)
  }
}
