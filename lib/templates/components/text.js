"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testParse_1 = require("../../testParse");
var config_1 = require("../../config");
var utils_1 = require("../../utils");
var placeholder = {
    text: "{CONTENT_TEXT_KEY}"
};
exports.default = (function (_a) {
    var options = _a.options, settings = _a.settings;
    var translations = utils_1.getTranslations({
        placeholder: placeholder,
        settings: settings
    });
    var html = "";
    var text = "";
    text = "" + placeholder.text.replace("KEY", settings.key) + config_1.textBreak;
    html = "\n  <mj-section padding=\"0 20px 20px 20px\" background-color=\"{backgroundColor2}\">\n  <mj-column width=\"100%\">\n    <mj-text align=\"center\" padding=\"15px 40px 15px 40px\" font-weight=\"300\">\n    " + placeholder.text.replace("KEY", settings.key) + "\n    </mj-text>\n  </mj-column>\n</mj-section>";
    testParse_1.default({ test: true, html: html, options: options, modulName: "text" });
    return {
        html: html,
        text: text,
        translations: translations,
        parse: function (args) { return testParse_1.default(args); }
    };
});
//# sourceMappingURL=text.js.map